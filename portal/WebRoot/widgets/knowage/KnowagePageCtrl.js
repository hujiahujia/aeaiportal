angular.module('${menuCode}')
.filter('to_trusted', ['$sce', function ($sce) {
	return function (text) {
	    return $sce.trustAsHtml(text);
	};
}])
.controller("${widgetCode}Ctrl",function($scope,AppKit,$state){
	AppKit.addSwipeSupport($scope);
	
	$scope.doSearch = function(){
		$state.go("tab.km-main-search");
	}	
	
	$scope.openKmModal = function(id,text){
		$scope.modalTitle = '知讯详情';
		$scope.currentInfoId = id;
		$scope.addKmClickNumber(id);
		AppKit.createModal("km-info-modal",$scope);	
	}
	
	$scope.loadListData = function(){
		$scope.onInitState = true;
		var url = '/portal/index?MobileInformationsDataProvider&actionType=findKnowledgeInformations';
		var promise = AppKit.getJsonApi(url);
		promise.success(function(rspJson){
			$scope.corpNews = rspJson.corpNews;
			$scope.industryNews = rspJson.industryNews;
			$scope.techTrends = rspJson.techTrends;
			$scope.traincenter = rspJson.traincenter;
			$scope.tabCount['CorpInfoList'] = rspJson.corpNewsCount;
			$scope.tabCount['IndustryInfoList'] = rspJson.industryNewsCount;
			$scope.tabCount['TechInfoList'] = rspJson.techTrendsCount;
			$scope.tabCount['TrainingInfoList'] = rspJson.traincenterCount;

			$scope.onInitState = false;
			$scope.tabMores[$scope.activeTab] = true;
		});
	}
	$scope.loadListData();
	
	$scope.loadKnowData = function(){
		var url = '/portal/index?MobileInformationsDataProvider&actionType=getKnowledgeInformation&ID='+$scope.currentInfoId;
		var promise = AppKit.getJsonApi(url);
		promise.success(function(rspJson){
			$scope.id = rspJson.id;
			$scope.title = rspJson.title;
			$scope.createTime = rspJson.createTime;
			$scope.content = rspJson.content;
			$scope.isPraised = rspJson.isPraised;
			$scope.isFavorited = rspJson.isFavorited;
			$scope.praiseCount = rspJson.praiseCount;
			$scope.infoFavCount = rspJson.infoFavCount;
			$scope.infoReviewCount = rspJson.infoReviewCount;
			$scope.readCount = rspJson.readCount;
			if(rspJson.reviewList.length == 0){
				$scope.reviewNoRecord = true;
			}else{
				$scope.reviewNoRecord = false;
			}
			$scope.reviewList = rspJson.reviewList;
		});
	}
	
	$scope.doPraise = function(){
		var url = "/portal/index?ContentProvider&actionType=mobileChangePraiseCount&infoId="+$scope.currentInfoId;
		AppKit.postJsonApi(url,$scope.praiseInfo).then(function(response){
			if ("success" == response.data){
				$scope.loadKnowData();
				if($scope.isPraised == 'N'){
					AppKit.successPopup({"title":"点赞成功"});	
				}else{
					AppKit.successPopup({"title":"取消成功"});
				}
			}else{
				AppKit.errorPopup();
			}
		});
	}
	
	$scope.favoriteInfo = {"title":"","favType":"OTHERS"};
	$scope.doFavorite = function(){
		var url = "/portal/index?InfomationFavEdit&actionType=mobileAddToFavorite&infoId="+$scope.currentInfoId;
		AppKit.postJsonApi(url,$scope.favoriteInfo).then(function(response){
			if ("success" == response.data){
				$scope.loadKnowData();
				if($scope.isFavorited == 'N'){
					AppKit.successPopup({"title":"收藏成功"});	
				}else{
					AppKit.successPopup({"title":"取消成功"});	
				}			
			}else{
				AppKit.errorPopup();
			}
		});
	}
	
	$scope.doRefresh = function(){
		var url = '/portal/index?MobileInformationsDataProvider&actionType=findKnowledgeInformations&queryPolicy=refresh&tabCode='+$scope.activeTab+'&tabPage='+$scope.tabPages[$scope.activeTab];
		AppKit.getJsonApi(url).success(function(rspJson){
			var tabCode = $scope.activeTab;
			if(tabCode == "CorpInfoList"){
				$scope.corpNews = rspJson.corpNews;
			}else if(tabCode == "IndustryInfoList"){
				$scope.industryNews = rspJson.industryNews;
			}else if(tabCode == "TechInfoList"){
				$scope.techTrends = rspJson.techTrends;
			}else if(tabCode == "TrainingInfoList"){
				$scope.traincenter = rspJson.traincenter;
			}
			if (rspJson.tabCount != $scope.tabCount[$scope.activeTab]){
				$scope.tabMores[$scope.activeTab] = true;				
			}
		}).finally(function(){
			AppKit.refreshOver($scope);
		});
	}
	
	$scope.doReload = function(){
		var url = '/portal/index?MobileInformationsDataProvider&actionType=findKnowledgeInformations&queryPolicy=reload&tabPage='+$scope.tabPages[$scope.activeTab]+'&tabCode='+$scope.activeTab;
		AppKit.getJsonApi(url).success(function(rspJson){
			$scope.tabPages[$scope.activeTab] = $scope.tabPages[$scope.activeTab]+1;
			var tabCode = $scope.activeTab;
			if(tabCode == "CorpInfoList"){
				if (rspJson.corpNews.length > 0){
					if($scope.tabPages[$scope.activeTab] > 1){
						for (var i=0;i < rspJson.corpNews.length;i++){
							var obj = rspJson.corpNews[i];
							$scope.corpNews.push(obj);				
						}	
					}
					$scope.tabMores[$scope.activeTab] = true;
				}else{
					$scope.tabMores[$scope.activeTab] = false;
				}
			}else if(tabCode == "IndustryInfoList"){
				if (rspJson.industryNews.length > 0){
					if($scope.tabPages[$scope.activeTab] > 1){
						for (var i=0;i < rspJson.industryNews.length;i++){
							var obj = rspJson.industryNews[i];
							$scope.industryNews.push(obj);				
						}
					}
					$scope.tabMores[$scope.activeTab] = true;
				}else{
					$scope.tabMores[$scope.activeTab] = false;
				}
			}else if(tabCode == "TechInfoList"){
				if (rspJson.techTrends.length > 0){
					if($scope.tabPages[$scope.activeTab] > 1){
						for (var i=0;i < rspJson.techTrends.length;i++){
							var obj = rspJson.techTrends[i];
							$scope.techTrends.push(obj);				
						}
					}	
					$scope.tabMores[$scope.activeTab] = true;
				}else{
					$scope.tabMores[$scope.activeTab] = false;
				}
			}else if(tabCode == "TrainingInfoList"){
				if (rspJson.traincenter.length > 0){
					if($scope.tabPages[$scope.activeTab] > 1){
						for (var i=0;i < rspJson.traincenter.length;i++){
							var obj = rspJson.traincenter[i];
							$scope.traincenter.push(obj);				
						}	
					}
					$scope.tabMores[$scope.activeTab] = true;
				}else{
					$scope.tabMores[$scope.activeTab] = false;
				}
			}
		}).finally(function(){
			AppKit.reloadOver($scope);
		});
	}
	
	$scope.hasMore = function(){
		return $scope.tabMores[$scope.activeTab];
	}
	
	$scope.openCommentModal = function(){
		AppKit.createModal("km-comment-modal",$scope);
	}
	
	$scope.commentInfo = {"rewContent":""};
	$scope.resetCommentInfo = function(){
		$scope.commentInfo = {"rewContent":""};
	}
	$scope.doComment = function(){
		var url = "/portal/index?ContentProvider&actionType=mobileAddNewReview&ID="+$scope.currentInfoId;
		AppKit.postJsonApi(url,$scope.commentInfo).then(function(response){
			if ("success" == response.data){
				$scope.loadKnowData();
				AppKit.successPopup({"title":"评论成功"});
				$scope.closeModal();
			}else{
				AppKit.errorPopup({"title":"评论失败"});
			}
		});
	}
	
	$scope.isValidCommentInfo = function(){
		var commentInfo = $scope.commentInfo;
		if (commentInfo.rewContent && commentInfo.rewContent != ''){
			return true;
		}
		else{
			return false;
		}
	}
	
	$scope.openBbsModal = function(id,text){
//		var newScope = $scope.$new(false);
		$scope.modalTitle = '问答详情';
		$scope.currentInfoId = id;
		$scope.addBbsClickNumber(id)
		AppKit.createModal("bbs-content-modal",$scope);
	}
	
	$scope.openCategoryModal = function(category) {
		$scope.currentCategory = category;
		$scope.modalTitle = category + '类型帖子';
		if($scope.modal != undefined){
			$scope.closeModal();
		}
		AppKit.createModal("bbs-category-modal",$scope);
	};
	
	$scope.openReplyModal = function(floor){
		if('楼主' == floor && floor){
			$scope.postReplyInfo.replyFloor = '楼主';
			$scope.displayReplyFloor = floor;
		}else{
			$scope.postReplyInfo.replyFloor = floor;
			$scope.displayReplyFloor = floor+"楼";
		}
		AppKit.createModal("bbs-reply-modal",$scope);
	}
	
	$scope.postReplyInfo = {"replyFloor":"","friContent":""};
	$scope.doPostReply = function(){
		var url = "/portal/index?ForumPost&actionType=mobileCreateRepleceForumInfo&fpmId="+$scope.currentInfoId;
		AppKit.postJsonApi(url,$scope.postReplyInfo).then(function(response){
			if ("success" == response.data){
				$scope.loadBbsData();
				$scope.postReplyInfo.friContent = "";
				AppKit.successPopup();
			}else{
				AppKit.errorPopup();
			}
		});
	}
	
	$scope.doFollow = function(){
		var url = "/portal/resource?ForumPost&actionType=mobileCreateForumFavorite&fpmId="+$scope.currentInfoId;
		AppKit.postJsonApi(url,$scope.followInfo).then(function(response){
			if ("success" == response.data){
				$scope.loadBbsData();
				if($scope.isAttention == 'N'){
					AppKit.successPopup({"title":"关注成功"});
				}else{
					AppKit.successPopup({"title":"取消成功"});
				}				
			}else{
				AppKit.errorPopup();
			}
		});
	}
	
	$scope.loadBbsData = function(){
		var url = '/portal/index?MobileForumDataProvider&actionType=getForumInformation&fpmId='+$scope.currentInfoId;
		var promise = AppKit.getJsonApi(url);
		promise.success(function(rspJson){
			$scope.id = rspJson.id;
			$scope.title = rspJson.title;
			$scope.name = rspJson.name;
			$scope.createTime = rspJson.createTime;
			$scope.content = rspJson.content;
			$scope.type = rspJson.type;
			$scope.category = rspJson.category;
			$scope.isAttention = rspJson.isAttention;
			$scope.count = rspJson.count;
			$scope.clickNum = rspJson.clickNum;
			$scope.attentionCount = rspJson.attentionCount;
			if(rspJson.comments.length == 0){
				$scope.commentsNoRecord = true;
			}else{
				$scope.commentsNoRecord = false;
			}
			$scope.comments = rspJson.comments;
		});
	}
	
	$scope.isValidPostReplyInfo = function(){
		var postReplyInfo = $scope.postReplyInfo;
		if (postReplyInfo.friContent && postReplyInfo.friContent != ''){
			return true;
		}
		else{
			return false;
		}
	}
	
	$scope.addBbsClickNumber = function(id){
		$scope.info = {"fpmId":id};
		var url = "/portal/resource?ForumPost&actionType=mobileUpdatePostClickNumber"
		AppKit.postJsonApi(url,$scope.info).then(function(response){
		});
	}
	
	$scope.addKmClickNumber = function(id){
		$scope.info = {"id":id};
		var url = "/portal/index?ContentProvider&actionType=mobileIncreaseInfoReadCounts"
		AppKit.postJsonApi(url,$scope.info,{noMask:true}).then(function(response){
		});
	}
});