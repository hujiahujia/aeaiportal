angular.module('${menuCode}')
.filter('to_trusted', ['$sce', function ($sce) {
	return function (text) {
	    return $sce.trustAsHtml(text);
	};
}])
.controller("${widgetCode}Ctrl",function($scope,AppKit){
	$scope.openBbsModal = function(id,text){
//		var newScope = $scope.$new(false);
		$scope.modalTitle = '问答详情';
		$scope.currentInfoId = id;
		$scope.addBbsClickNumber(id)
		AppKit.createModal("bbs-content-modal",$scope);
	}
	
	$scope.openCategoryModal = function(category) {
		$scope.currentCategory = category;
		$scope.modalTitle = category + '类型帖子';
		if($scope.modal != undefined){
			$scope.closeModal();
		}
		AppKit.createModal("bbs-category-modal",$scope);
	};
	
	$scope.openReplyModal = function(floor){
		if('楼主' == floor && floor){
			$scope.postReplyInfo.replyFloor = '楼主';
			$scope.displayReplyFloor = floor;
		}else{
			$scope.postReplyInfo.replyFloor = floor;
			$scope.displayReplyFloor = floor+"楼";
		}
		AppKit.createModal("bbs-reply-modal",$scope);
	}
	
	$scope.postReplyInfo = {"replyFloor":"","friContent":""};
	$scope.doPostReply = function(){
		var url = "/portal/index?ForumPost&actionType=mobileCreateRepleceForumInfo&fpmId="+$scope.currentInfoId;
		AppKit.postJsonApi(url,$scope.postReplyInfo).then(function(response){
			if ("success" == response.data){
				$scope.loadBbsData();
				$scope.postReplyInfo.friContent = "";
				AppKit.successPopup();
			}else{
				AppKit.errorPopup();
			}
		});
	}
	
	$scope.doFollow = function(){
		var url = "/portal/resource?ForumPost&actionType=mobileCreateForumFavorite&fpmId="+$scope.currentInfoId;
		AppKit.postJsonApi(url,$scope.followInfo).then(function(response){
			if ("success" == response.data){
				$scope.loadBbsData();
				if($scope.isAttention == 'N'){
					AppKit.successPopup({"title":"关注成功"});
				}else{
					AppKit.successPopup({"title":"取消成功"});
				}				
			}else{
				AppKit.errorPopup();
			}
		});
	}
	
	$scope.loadConcernDatas = function (){
		var url = '/portal/index?MobileForumDataProvider&actionType=findUserAttentionForumPostInfos';
		var promise = AppKit.getJsonApi(url);
		promise.success(function(rspJson){
			$scope.datas = rspJson.datas;
		});
	}
	$scope.loadConcernDatas();
	
	$scope.doRefresh = function(){
		var url = '/portal/index?MobileForumDataProvider&actionType=findUserAttentionForumPostInfos';
		AppKit.getJsonApi(url).success(function(rspJson){
			$scope.datas = rspJson.datas;
		}).finally(function(){
			AppKit.refreshOver($scope);
		});
	}
	
	$scope.loadBbsData = function(){
		var url = '/portal/index?MobileForumDataProvider&actionType=getForumInformation&fpmId='+$scope.currentInfoId;
		var promise = AppKit.getJsonApi(url);
		promise.success(function(rspJson){
			$scope.id = rspJson.id;
			$scope.title = rspJson.title;
			$scope.name = rspJson.name;
			$scope.createTime = rspJson.createTime;
			$scope.content = rspJson.content;
			$scope.type = rspJson.type;
			$scope.category = rspJson.category;
			$scope.isAttention = rspJson.isAttention;
			$scope.count = rspJson.count;
			$scope.clickNum = rspJson.clickNum;
			$scope.attentionCount = rspJson.attentionCount;
			if(rspJson.comments.length == 0){
				$scope.commentsNoRecord = true;
			}else{
				$scope.commentsNoRecord = false;
			}
			$scope.comments = rspJson.comments;
		});
	}
	
	$scope.isValidPostReplyInfo = function(){
		var postReplyInfo = $scope.postReplyInfo;
		if (postReplyInfo.friContent && postReplyInfo.friContent != ''){
			return true;
		}
		else{
			return false;
		}
	}
	
	$scope.addBbsClickNumber = function(id){
		$scope.info = {"fpmId":id};
		var url = "/portal/resource?ForumPost&actionType=mobileUpdatePostClickNumber"
		AppKit.postJsonApi(url,$scope.info).then(function(response){
		});
	}
});