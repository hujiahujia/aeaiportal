angular.module('${menuCode}')
.controller("${widgetCode}Ctrl",function($scope,AppKit){
	$scope.setupProfile = function(){
		$scope.modalTitle = "我的个人设置";
		AppKit.createModal("mine-setting-modal",$scope);			
	}
	
	$scope.isBoundStateInfo = function(){
		if($scope.wxOpenId && $scope.wxOpenId != "-1"){
			return true;
		}else{
			return false;
		}
	}
	
	$scope.openBindModal = function(){
		$scope.modalTitle = "微信绑定";
		AppKit.createModal("wx-bind-modal",$scope);
	}
	
	$scope.doUnbund = function(){
		AppKit.confirm({content:'确认解除绑定吗？',action:function(){
			var url = "/portal/index?WxConfig&actionType=wxUnbundling";
			AppKit.postJsonApi(url,$scope.user).then(function(response){
				if ("success" == response.data){
					AppKit.successPopup({"title":"解除成功",callback:function(){
						$scope.logout();						
					}});
				}else{
					AppKit.errorPopup({"title":"解除失败"});
				}
			});
		}});
	}
});


