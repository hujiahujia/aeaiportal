angular.module('${menuCode}')
.controller("${widgetCode}Ctrl",function($scope,AppKit){
	var tmpList = [];
	$scope.loadModuleData=function(){
		var url = '/portal/index?MobileDataProvider&actionType=findAppModuleList';
		AppKit.getJsonApi(url).success(function(jsonData){
			tmpList = jsonData.moduleList;
			$scope.list = tmpList;
		});
	}
	$scope.loadModuleData();
	  
	/*
	$scope.sortableOptions = {
	    stop: function(e, ui) {
	    	 $scope.sortingLog = [];
	      var logEntry = tmpList.map(function(i){
	        return i.id;
	      });
	      $scope.sortingLog = logEntry;
	      var url = "/portal/index?MobileDataProvider&actionType=mobileSaveAppModule&mark=sort";
	  		AppKit.postJsonApi(url,$scope.sortingLog).then(function(response){
	  		});
	    }
	};	 
	*/
	
	$scope.addAppModal = function(){
		$scope.modalTitle = "待选应用模块";
		AppKit.createModal("tochoose-module-modal",$scope);			
	}
	
	$scope.toChooseModuleData=function(){
		var url = '/portal/index?MobileDataProvider&actionType=findToChooseAppModuleList';
		var promise = AppKit.getJsonApi(url);
		promise.success(function(rspJson){
			$scope.toChooseModuleList = rspJson.toChooseModuleList;
			if(rspJson.toChooseModuleList.length == 0){
				$scope.noneModules = true;
			}else{
				$scope.noneModules = false;
			}
		});
	}
	
	$scope.deleteAppModal = function(id){
		AppKit.confirm({operaType:'delete',action:function(){
			var url = "/portal/index?MobileDataProvider&actionType=mobileSaveAppModule&mark=delete&id="+id;
	  		AppKit.postJsonApi(url).then(function(response){
	  			if ("success" == response.data.text){
					$scope.loadModuleData();
					AppKit.hideMask();
				}else{
					AppKit.errorPopup();
				}
	  		});			
		}});
	}
	
	$scope.upMoveTopAppModal = function(id){
		var url = "/portal/index?MobileDataProvider&actionType=mobileSaveAppModule&mark=upMoveTop&id="+id;
  		AppKit.postJsonApi(url).then(function(response){
  			if ("success" == response.data.text){
				$scope.loadModuleData();
				if("fail" == response.data.msg){
					AppKit.errorPopup({"title":"已经是首位模块！"});
				}
				AppKit.hideMask();
			}else{
				AppKit.errorPopup();
			}
  		});			
	}
	
	$scope.upMoveAppModal = function(id){
		var url = "/portal/index?MobileDataProvider&actionType=mobileSaveAppModule&mark=upMove&id="+id;
		AppKit.postJsonApi(url).then(function(response){
  			if ("success" == response.data.text){
				if("fail" == response.data.msg){
					AppKit.errorPopup({"title":"已经是首位模块！"});
				}else{
					AppKit.hideMask();
					$scope.loadModuleData();
				}
			}else{
				AppKit.errorPopup();
			}
  		});			
	}
	
	$scope.downMoveAppModal = function(id){
		var url = "/portal/index?MobileDataProvider&actionType=mobileSaveAppModule&mark=downMove&id="+id;
  		AppKit.postJsonApi(url).then(function(response){
  			if ("success" == response.data.text){
				if("fail" == response.data.msg){
					AppKit.errorPopup({"title":"已经末位模块！"});
				}else{
					AppKit.hideMask();
					$scope.loadModuleData();					
				}
			}else{
				AppKit.errorPopup();
			}
  		});			
	}
	
	$scope.downMoveBottomAppModal = function(id){
		var url = "/portal/index?MobileDataProvider&actionType=mobileSaveAppModule&mark=downMoveBottom&id="+id;
  		AppKit.postJsonApi(url).then(function(response){
  			if ("success" == response.data.text){
				$scope.loadModuleData();
				if("fail" == response.data.msg){
					AppKit.errorPopup({"title":"已经末位模块！"});
				}
				AppKit.hideMask();
			}else{
				AppKit.errorPopup();
			}
  		});			
	}
	
	$scope.doSelectModule = function(){
		$scope.receiveList = [];
		for(var i=0;i < $scope.toChooseModuleList.length;i++){
			var item =  $scope.toChooseModuleList[i];
			if(item.checked){
				$scope.receiveList.push(item);
			}
		}
		if($scope.receiveList.length > 0){
			var url = "/portal/index?MobileDataProvider&actionType=mobileSaveAppModule&mark=selected";
			AppKit.postJsonApi(url,$scope.receiveList).then(function(response){
				if ("success" == response.data.text){
					$scope.loadModuleData();
					$scope.closeModal();
					AppKit.successPopup({"title":"添加模块成功！"});
				}else{
					AppKit.errorPopup();
				}
			});
		}else{
			AppKit.errorPopup({"title":"选择有效模块！"});
		}
	}
});