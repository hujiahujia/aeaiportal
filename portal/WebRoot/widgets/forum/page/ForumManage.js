angular.module('${menuCode}')
.filter('to_trusted', ['$sce', function ($sce) {
	return function (text) {
	    return $sce.trustAsHtml(text);
	};
}])
.controller("${widgetCode}Ctrl",function($scope,AppKit){
	
	$scope.fullHeight = {"min-height":(AppKit.getConfig("ScreenHeight")-90)+"px","background-color":"white"};
	
	$scope.setActiveTab = function (activeTab) {     
		　　$scope.activeTab = activeTab; 
	};
	
	$scope.initTabs = function(tabs){
		$scope.tabs = tabs;	
		$scope.activeTab = tabs[0];
	}

	$scope.findForumListInfos();
	
	$scope.onSwipeLeft = function(){
		for(var i=0;i< $scope.tabs.length;i++){
			var tempTab = $scope.tabs[i];
			if (tempTab == $scope.activeTab){
				if (i == ($scope.tabs.length-1))continue;
				
				$scope.activeTab = $scope.tabs[i+1];
				break;
			}
		}
	};

	$scope.onSwipeRight = function(){
		for(var i=0;i< $scope.tabs.length;i++){
			var tempTab = $scope.tabs[i];
			if (tempTab == $scope.activeTab){
				if (i == 0)continue;
				
				$scope.activeTab = $scope.tabs[i-1];
				break;
			}
		}
	};	
});