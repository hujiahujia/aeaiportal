angular.module('${menuCode}')
.filter('to_trusted', ['$sce', function ($sce) {
	return function (text) {
	    return $sce.trustAsHtml(text);
	};
}])
.controller("${widgetCode}Ctrl",function($scope,AppKit,$state,$stateParams){
	$scope.contentId = null;
	
	AppKit.addSwipeSupport($scope);
	
	$scope.loadListData = function(){
		$scope.onInitState = true;
		
		var url = '/portal/index?MobileForumDataProvider&actionType=findForumInformations';
		var promise = AppKit.getJsonApi(url);
		promise.success(function(rspJson){
			$scope.applicationPlatform = rspJson.applicationPlatform;
			$scope.bracePlatform = rspJson.bracePlatform;		
			$scope.hotnewPosts = rspJson.hotnewPosts;
			$scope.integratePlatform = rspJson.integratePlatform;
			$scope.tabCount['HotNewPosts'] = rspJson.hotnewPostsCount;
			$scope.tabCount['IntegratePlatform'] = rspJson.integratePlatformCount;
			$scope.tabCount['BracePlatform'] = rspJson.bracePlatformCount;
			$scope.tabCount['ApplicationPlatform'] = rspJson.applicationPlatformCount;
			
			$scope.onInitState = false;
			$scope.tabMores[$scope.activeTab] = true;
		});
	}
	$scope.loadListData();
	
	$scope.doSearch = function(){
		$state.go("tab.bbs-main-search");
	}

	$scope.doPosting = function(){
		AppKit.createModal("bbs-post-modal",$scope);
	}
	
	$scope.info = {"fciCode":"selected","title":"","fpmExpression":"","points":"","fpmExpression":"selected","fpmContent":""}
	$scope.doPost = function(){
		var url = "/portal/index?ForumPost&actionType=mobileCreatePostForumInfo"
		AppKit.postJsonApi(url,$scope.info).then(function(response){
			if ("success" == response.data){
				AppKit.successPopup();				
			}else{
				AppKit.errorPopup();
			}
		});
	}
	
	var url = '/portal/index?MobileForumDataProvider&actionType=mobileIsGuest';
	var promise = AppKit.getJsonApi(url);
	promise.success(function(rspJson){
		$scope.isGuest = rspJson.isGuest;
	});
	
	$scope.openCategoryModal = function(category) {
		$scope.currentCategory = category;
		$scope.modalTitle = category + '类型帖子';
		if($scope.modal != undefined){
			$scope.closeModal();
		}
		AppKit.createModal("bbs-category-modal",$scope);
	};
	
	$scope.openBbsModal = function(id,text){
//		var newScope = $scope.$new(false);
		$scope.modalTitle = '问答详情';
		$scope.currentInfoId = id;
		$scope.addBbsClickNumber(id)
		AppKit.createModal("bbs-content-modal",$scope);
	}
	
	$scope.currentInfoId = $stateParams.infoId;
	$scope.doFollow = function(){
		var url = "/portal/resource?ForumPost&actionType=mobileCreateForumFavorite&fpmId="+$scope.currentInfoId;
		AppKit.postJsonApi(url,$scope.followInfo).then(function(response){
			if ("success" == response.data){
				$scope.loadBbsData();
				if($scope.isAttention == 'N'){
					AppKit.successPopup({"title":"关注成功"});
					$scope.loadListData();
				}else{
					AppKit.successPopup({"title":"取消成功"});
					$scope.loadListData();
				}				
			}else{
				AppKit.errorPopup();
			}
		});
	}
	
	$scope.loadBbsData = function(){
		var url = '/portal/index?MobileForumDataProvider&actionType=getForumInformation&fpmId='+$scope.currentInfoId;
		var promise = AppKit.getJsonApi(url);
		promise.success(function(rspJson){
			$scope.id = rspJson.id;
			$scope.title = rspJson.title;
			$scope.name = rspJson.name;
			$scope.createTime = rspJson.createTime;
			$scope.content = rspJson.content;
			$scope.type = rspJson.type;
			$scope.category = rspJson.category;
			$scope.isAttention = rspJson.isAttention;
			$scope.count = rspJson.count;
			$scope.clickNum = rspJson.clickNum;
			$scope.attentionCount = rspJson.attentionCount;
			if(rspJson.comments.length == 0){
				$scope.commentsNoRecord = true;
			}else{
				$scope.commentsNoRecord = false;
			}
			$scope.comments = rspJson.comments;
		});
	}
	
	$scope.doRefresh = function(){
		var url = '/portal/index?MobileForumDataProvider&actionType=findForumInformations&queryPolicy=refresh&tabCode='+$scope.activeTab+'&tabPage='+$scope.tabPages[$scope.activeTab];
		AppKit.getJsonApi(url).success(function(rspJson){
			var tabCode = $scope.activeTab;
			if(tabCode == "HotNewPosts"){
				$scope.hotnewPosts = rspJson.hotnewPosts;
			}else if(tabCode == "IntegratePlatform"){
				$scope.integratePlatform = rspJson.integratePlatform;
			}else if(tabCode == "BracePlatform"){
				$scope.bracePlatform = rspJson.bracePlatform;
			}else if(tabCode == "ApplicationPlatform"){
				$scope.applicationPlatform = rspJson.applicationPlatform;
			}
			if (rspJson.tabCount != $scope.tabCount[$scope.activeTab]){
				$scope.tabMores[$scope.activeTab] = true;				
			}
		}).finally(function(){
			AppKit.refreshOver($scope);
		});
	}
	
	$scope.doReload = function(){
		var url = '/portal/index?MobileForumDataProvider&actionType=findForumInformations&queryPolicy=reload&tabPage='+$scope.tabPages[$scope.activeTab]+'&tabCode='+$scope.activeTab;
		AppKit.getJsonApi(url).success(function(rspJson){
			$scope.tabPages[$scope.activeTab] = $scope.tabPages[$scope.activeTab]+1;
			var tabCode = $scope.activeTab;
			if(tabCode == "HotNewPosts"){
				if (rspJson.hotnewPosts.length > 0){
					if($scope.tabPages[$scope.activeTab] > 1){
						for (var i=0;i < rspJson.hotnewPosts.length;i++){
							var obj = rspJson.hotnewPosts[i];
							$scope.hotnewPosts.push(obj);				
						}
					}
				}else{
					$scope.tabMores[$scope.activeTab] = false;
				}
			}else if(tabCode == "IntegratePlatform"){
				if (rspJson.integratePlatform.length > 0){
					if($scope.tabPages[$scope.activeTab] > 1){
						for (var i=0;i < rspJson.integratePlatform.length;i++){
							var obj = rspJson.integratePlatform[i];
							$scope.integratePlatform.push(obj);				
						}
					}
				}else{
					$scope.tabMores[$scope.activeTab] = false;
				}
			}else if(tabCode == "BracePlatform"){
				if (rspJson.bracePlatform.length > 0){
					if($scope.tabPages[$scope.activeTab] > 1){
						for (var i=0;i < rspJson.bracePlatform.length;i++){
							var obj = rspJson.bracePlatform[i];
							$scope.bracePlatform.push(obj);				
						}
					}
				}else{
					$scope.tabMores[$scope.activeTab] = false;
				}
			}else if(tabCode == "ApplicationPlatform"){
				if (rspJson.applicationPlatform.length > 0){
					if($scope.tabPages[$scope.activeTab] > 1){
						for (var i=0;i < rspJson.applicationPlatform.length;i++){
							var obj = rspJson.applicationPlatform[i];
							$scope.applicationPlatform.push(obj);				
						}
					}
				}else{
					$scope.tabMores[$scope.activeTab] = false;
				}
			}else{
				$scope.tabMores[$scope.activeTab] = false;
			}
		}).finally(function(){
			AppKit.reloadOver($scope);
		});
	}
	
	$scope.hasMore = function(){
		return $scope.tabMores[$scope.activeTab];
	}
	
	$scope.openReplyModal = function(floor){
		if('楼主' == floor && floor){
			$scope.postReplyInfo.replyFloor = '楼主';
			$scope.displayReplyFloor = floor;
		}else{
			$scope.postReplyInfo.replyFloor = floor;
			$scope.displayReplyFloor = floor+"楼";
		}
		AppKit.createModal("bbs-reply-modal",$scope);
	}
	
	$scope.postReplyInfo = {"replyFloor":"","friContent":""};
	$scope.resetPostReplyInfo = function(){
		$scope.postReplyInfo = {"replyFloor":"","friContent":""};
	}
	$scope.doPostReply = function(scope){
		var url = "/portal/index?ForumPost&actionType=mobileCreateRepleceForumInfo&fpmId="+$scope.currentInfoId;
		AppKit.postJsonApi(url,$scope.postReplyInfo).then(function(response){
			if ("success" == response.data){
				$scope.postReplyInfo.friContent = "";
				AppKit.successPopup({"title":"回复成功"});
				$scope.closeModal();
				$scope.loadBbsData();
			}else{
				AppKit.errorPopup({"title":"回复失败"});
			}
		});
	}
	
	$scope.isValidPostReplyInfo = function(){
		var postReplyInfo = $scope.postReplyInfo;
		if (postReplyInfo.friContent && postReplyInfo.friContent != ''){
			return true;
		}
		else{
			return false;
		}
	}
	
	$scope.openKmModal = function(id,text){
		$scope.modalTitle = '知讯详情';
		$scope.currentInfoId = id;
		$scope.addKmClickNumber(id);
		AppKit.createModal("km-info-modal",$scope);	
	}
	
	$scope.loadKnowData = function(){
		var url = '/portal/index?MobileInformationsDataProvider&actionType=getKnowledgeInformation&ID='+$scope.currentInfoId;
		var promise = AppKit.getJsonApi(url);
		promise.success(function(rspJson){
			$scope.id = rspJson.id;
			$scope.title = rspJson.title;
			$scope.createTime = rspJson.createTime;
			$scope.content = rspJson.content;
			$scope.isPraised = rspJson.isPraised;
			$scope.isFavorited = rspJson.isFavorited;
			$scope.praiseCount = rspJson.praiseCount;
			$scope.infoFavCount = rspJson.infoFavCount;
			$scope.infoReviewCount = rspJson.infoReviewCount;
			$scope.readCount = rspJson.readCount;
			if(rspJson.reviewList.length == 0){
				$scope.reviewNoRecord = true;
			}else{
				$scope.reviewNoRecord = false;
			}
			$scope.reviewList = rspJson.reviewList;
		});
	}
	
	$scope.doPraise = function(){
		var url = "/portal/index?ContentProvider&actionType=mobileChangePraiseCount&infoId="+$scope.currentInfoId;
		AppKit.postJsonApi(url,$scope.praiseInfo).then(function(response){
			if ("success" == response.data){
				$scope.loadKnowData();
				if($scope.isPraised == 'N'){
					AppKit.successPopup({"title":"点赞成功"});	
				}else{
					AppKit.successPopup({"title":"取消成功"});
				}
			}else{
				AppKit.errorPopup();
			}
		});
	}
	
	$scope.favoriteInfo = {"title":"","favType":"OTHERS"};
	$scope.doFavorite = function(){
		var url = "/portal/index?InfomationFavEdit&actionType=mobileAddToFavorite&infoId="+$scope.currentInfoId;
		AppKit.postJsonApi(url,$scope.favoriteInfo).then(function(response){
			if ("success" == response.data){
				$scope.loadKnowData();
				if($scope.isFavorited == 'N'){
					AppKit.successPopup({"title":"收藏成功"});	
				}else{
					AppKit.successPopup({"title":"取消成功"});	
				}			
			}else{
				AppKit.errorPopup();
			}
		});
	}
	
	$scope.openCommentModal = function(){
		AppKit.createModal("km-comment-modal",$scope);
	}
	
	$scope.commentInfo = {"rewContent":""};
	$scope.doComment = function(){
		var url = "/portal/resource?ContentProvider&actionType=mobileAddNewReview&ID="+$scope.currentInfoId;
		AppKit.postJsonApi(url,$scope.commentInfo).then(function(response){
			if ("success" == response.data){
				$scope.loadKnowData();
				AppKit.successPopup({"title":"评论成功"});
			}else{
				AppKit.errorPopup({"title":"评论失败"});
			}
		});
	}
	
	$scope.isValidCommentInfo = function(){
		var commentInfo = $scope.commentInfo;
		if (commentInfo.rewContent && commentInfo.rewContent != ''){
			return true;
		}
		else{
			return false;
		}
	}
	
	$scope.addBbsClickNumber = function(id,num){
		$scope.info = {"fpmId":id};
		var url = "/portal/resource?ForumPost&actionType=mobileUpdatePostClickNumber"
		AppKit.postJsonApi(url,$scope.info).then(function(response){
		});
	}
	
	$scope.addKmClickNumber = function(id){
		$scope.info = {"id":id};
		var url = "/portal/index?ContentProvider&actionType=mobileIncreaseInfoReadCounts"
		AppKit.postJsonApi(url,$scope.info,{noMask:true}).then(function(response){
		});
	}
	
	
	$scope.findForumListInfos = function() {
		var url = '/portal/index?ForumPost&actionType=mobilefindForumManageListInfos';
		var promise = AppKit.getJsonApi(url);
		promise.success(function(rspJson){
			$scope.report = rspJson.report;
			$scope.reportTitle = $scope.report[0].reportTitle;
			$scope.garbage = rspJson.garbage;
			$scope.garbageTitle = $scope.garbage[0].garbageTitle;
			$scope.reportCount = rspJson.reportCount;
			$scope.garbageCount = rspJson.garbageCount;
		});
	}
	
	$scope.openReportPopup = function(id){
		$scope.reportId = id;
		AppKit.createPopup("forummanage-popup",$scope);
	}
	
	$scope.reportPost = function(id){
		$scope.info = {"fpmId":id};
		var url = "/portal/index?ForumPost&actionType=mobileReportPost";
		AppKit.postJsonApi(url,$scope.info).then(function(response){
			if ("success" == response.data){
				AppKit.closePopup($scope);
				$scope.findForumListInfos();
				AppKit.successPopup({"title":"确认举报成功！"});	
			}else{
				AppKit.closePopup($scope);
				$scope.findForumListInfos();
				AppKit.errorPopup();
			}
		});
	}
	
	$scope.cancelReportPost = function(id){
		$scope.info = {"fpmId":id};
		var url = "/portal/index?ForumPost&actionType=mobileCancelReportPost";
		AppKit.postJsonApi(url,$scope.info).then(function(response){
			if ("success" == response.data){
				AppKit.closePopup($scope);
				$scope.findForumListInfos();
				AppKit.successPopup({"title":"取消举报成功！"});	
			}else{
				AppKit.closePopup($scope);
				$scope.findForumListInfos();
				AppKit.errorPopup();
			}
		});
	}
	
});