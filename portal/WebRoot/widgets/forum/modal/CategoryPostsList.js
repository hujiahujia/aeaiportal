angular.module('${menuCode}')
.controller("${widgetCode}Ctrl",function($scope,AppKit){
	var url = '/portal/resource?MobileForumDataProvider&actionType=findForumInfosByCode&sectionCode=' + $scope.currentCategory;
	var promise = AppKit.getJsonApi(url);
	promise.success(function(rspJson){
		$scope.forumInfos = rspJson.forumInfos;
	});
	
	
	$scope.openModal = function(id,text){
		$scope.modalTitle = '问答详情';
		$scope.currentInfoId = id;
		AppKit.createModal("bbs-content-modal",$scope);	
	}
});