angular.module('${menuCode}')
.controller("${widgetCode}Ctrl",function($scope,AppKit){
	$scope.loadBbsData();
	
	var url = '/portal/index?MobileForumDataProvider&actionType=mobileIsGuest';
	var promise = AppKit.getJsonApi(url);
	promise.success(function(rspJson){
		$scope.isGuest = rspJson.isGuest;
		AppKit.hideMask();
	});
});