angular.module('${menuCode}')
.controller("${widgetCode}Ctrl",function($scope,AppKit){
	$scope.modalTitle = "任务正文";
	var url = '/portal/resource?MobileTaskListDataProvider&actionType=findTaskLists&userCode=admin';
	var promise = AppKit.getJsonApi(url);
	promise.success(function(rspJson){
		$scope.todoTasks = rspJson.todoTasks;
		$scope.todoTaskCount = rspJson.todoTaskCount;
		$scope.ondoTasks = rspJson.ondoTasks;
		$scope.ondoTaskCount = rspJson.ondoTaskCount;
		$scope.didTasks = rspJson.didTasks;
		$scope.didTaskCount = rspJson.didTaskCount;
	});
	
	$scope.fullHeight = {"min-height":(AppKit.getConfig("ScreenHeight")-90)+"px","background-color":"white"};
	
	$scope.openModal = function(id,text) {
		$scope.currentId = id;
		$scope.currentText = text;
		AppKit.createModal("task-modal",$scope);	
	}
});