angular.module('${menuCode}')
.controller("${widgetCode}Ctrl",function($scope,AppKit){
	   var date = new Date();
	    var d = date.getDate();
	    var m = date.getMonth();
	    var y = date.getFullYear();
	    
	    $scope.events = [
	      {title: '有事',start: new Date(y, m, d),url: '#/tab/schedule-list'}
	    ];

	    $scope.alertOnEventClick = function(calEvent, jsEvent, view){
//	        alert('Event: ' + calEvent.title);
//	        alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
//	        alert('View: ' + view.name);
	    };
	    
	    $scope.uiConfig = {
	      calendar:{
	        height: 450,
	        editable: true,
	        header:{
	          left: 'title',
	          center: '',
	          right: 'today prev,next'
	        },
	        eventClick: $scope.alertOnEventClick
	      }
	    };

	    $scope.eventSources = [$scope.events];
});