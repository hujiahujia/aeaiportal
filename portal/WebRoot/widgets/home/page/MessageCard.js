angular.module('${menuCode}')
.controller("${widgetCode}Ctrl",function($scope,AppKit){
	$scope.loadMessageData('card');
	
	$scope.setActiveTab = function (activeTab) {     
		　　$scope.activeTab = activeTab; 
	};
	
	$scope.openModal = function(id,text,category) {
		$scope.currentId = id;
		$scope.modalTitle = '信息正文';
		$scope.title = text;
		$scope.category = category;
		AppKit.createModal("msg-modal",$scope);
	}
	
	$scope.openSendModal = function() {
		AppKit.createModal("send-msg-modal",$scope);	
	}
	
	$scope.initTabs = function(tabs){
		$scope.tabs = tabs;
		$scope.activeTab = tabs[0];
	};	
	
	$scope.onSwipeLeft = function(){
		for(var i=0;i< $scope.tabs.length;i++){
			var tempTab = $scope.tabs[i];
			if (tempTab == $scope.activeTab){
				if (i == ($scope.tabs.length-1))continue;
				
				$scope.activeTab = $scope.tabs[i+1];
				break;
			}
		}
	};

	$scope.onSwipeRight = function(){
		for(var i=0;i< $scope.tabs.length;i++){
			var tempTab = $scope.tabs[i];
			if (tempTab == $scope.activeTab){
				if (i == 0)continue;
				
				$scope.activeTab = $scope.tabs[i-1];
				break;
			}
		}
	};		
});