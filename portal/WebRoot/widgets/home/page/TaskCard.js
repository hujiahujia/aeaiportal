angular.module('${menuCode}')
.controller("${widgetCode}Ctrl",function($scope,AppKit,$state){
	var url = '/aeaiem/index?MobileEvection&actionType=findTaskList&requestFrom=home';
	var promise = AppKit.getJsonApi(url);
	promise.success(function(rspJson){
		$scope.allTaskList = rspJson.allTaskList;
		if($scope.allTaskList.length == 0){
			$scope.allTaskListNoRecords = true;
		}else{
			$scope.allTaskListNoRecords = false;
		}
	});
	$scope.openModal = function(id,text) {
		$scope.currentId = id;
		$scope.modalTitle = text;
		AppKit.createModal("task-modal",$scope);
	}
});