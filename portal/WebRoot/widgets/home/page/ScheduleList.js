angular.module('${menuCode}')
.controller("${widgetCode}Ctrl",function($scope,AppKit){
    $scope.scheduleLists = function(){
    	var date = new Date();
    	var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();
        var scheduleList = [];
        for(var i = 1;i<=8;i++){
        	var object = {"title":"今天要做的第"+i+"件事","time":new Date(y, m, d)};
        	scheduleList.push(object);
        }
        $scope.scheduleList = scheduleList;
    }
	$scope.scheduleLists();
});