angular.module('${menuCode}')
.filter('to_trusted', ['$sce', function ($sce) {
	return function (text) {
	    return $sce.trustAsHtml(text);
	};
}])
.controller("${widgetCode}Ctrl",function($scope,$state,AppKit,$ionicPopup,$timeout){
	$scope.isFirstCard = false;
	$scope.isLastCard = false;
	$scope.cardInfos = {};
	$scope.currrentWidgetCode;
	$scope.selectedWidgetId = -1;
	
	$scope.onInitState = true;
	
	var menuBarId = AppKit.getConfig('MenuBarId');
	var url = "/portal/index?PersonalSetting&actionType=getCardInfos&menuBarId="+menuBarId+"&menuCode=${menuCode}";
	$scope.initData = function(){
		AppKit.getJsonApi(url).then(function(rspJson){
			$scope.cardInfos = rspJson.data.cardInfos;
			$scope.cardSize = rspJson.data.cardSize;
			
			$scope.loadAppModuleInfo();
			
			$scope.onInitState = false;
		});		
	}
	$scope.initData();
	
	$scope.showPopoverSetting = function($event,widgetCode){
		var cardIndex = parseInt($scope.cardInfos[widgetCode]);
		if (cardIndex == 0){
			$scope.isFirstCard = true;
		}else{
			$scope.isFirstCard = false;
		}
		
		if (cardIndex == ($scope.cardSize-1)){
			$scope.isLastCard = true;
		}else{
			$scope.isLastCard = false;
		}
		
		$scope.currrentWidgetCode = widgetCode;
		AppKit.createPopover("card-setting-popover",$scope,$event);
	};
	
	$scope.moveTopCard = function(){
		var widgetCode = $scope.currrentWidgetCode;
		var url = "/portal/index?PersonalSetting&actionType=moveTopCard&menuBarId="+menuBarId+"&menuCode=${menuCode}&widgetCode="+widgetCode;
		AppKit.getJsonApi(url).then(function(rspJson){
			if (rspJson.data == 'success'){
				window.location.reload();	
			}else{
				console.log("moveTopCard failure !");
			}
		});
	};	
	
	$scope.moveUpCard = function(){
		var widgetCode = $scope.currrentWidgetCode;
		var url = "/portal/index?PersonalSetting&actionType=moveUpCard&menuBarId="+menuBarId+"&menuCode=${menuCode}&widgetCode="+widgetCode;
		AppKit.getJsonApi(url).then(function(rspJson){
			if (rspJson.data == 'success'){
				window.location.reload();	
			}else{
				console.log("moveUpCard failure !");
			}
		});
	};
	
	$scope.moveDownCard = function(){
		var widgetCode = $scope.currrentWidgetCode;
		var url = "/portal/index?PersonalSetting&actionType=moveDownCard&menuBarId="+menuBarId+"&menuCode=${menuCode}&widgetCode="+widgetCode;
		AppKit.getJsonApi(url).then(function(rspJson){
			if (rspJson.data == 'success'){
				window.location.reload();	
			}else{
				console.log("moveDownCard failure !");
			}
		});
	};	
	
	$scope.removeCard = function(){
		var widgetCode = $scope.currrentWidgetCode;
		var url = "/portal/index?PersonalSetting&actionType=removeCard&menuBarId="+menuBarId+"&menuCode=${menuCode}&widgetCode="+widgetCode;
		AppKit.getJsonApi(url).then(function(rspJson){
			if (rspJson.data == 'success'){
				window.location.reload();	
			}else{
				console.log("moveTopCard failure !");
			}
		});
	};
	
	$scope.addCard = function(){
		var widgetId = $scope.selectedWidgetId;
		var url = "/portal/index?PersonalSetting&actionType=addCard&menuBarId="+menuBarId+"&menuCode=${menuCode}&widgetId="+widgetId;
		AppKit.getJsonApi(url).then(function(rspJson){
			if (rspJson.data == 'success'){
				window.location.reload();	
			}else{
				console.log("moveTopCard failure !");
			}
		});
	};	
	
	$scope.showRestCards = function(){
		var menuBarId = AppKit.getConfig("MenuBarId");
		var url = "/portal/index?PersonalSetting&actionType=getSelectCardList&menuBarId="+menuBarId+"&menuCode=${menuCode}";
		AppKit.getJsonApi(url).then(function(response){
			$scope.selectedWidgetId = -1;
			$scope.restWidgets = response.data.datas;
		});		
	}
	
	$scope.selectWidget = function(widgetId){
		$scope.selectedWidgetId = widgetId;
	}	

	$scope.openAddWidgetModal = function(){
		AppKit.createModal("add-widget",$scope);
	}
	
	
	$scope.doSearch = function(){
		$state.go("tab.home-main-search");
	}
	
	$scope.loadMessageData = function(){
		var url = '/portal/resource?MobileLatestMessagesDataProvider&actionType=findLatestMessages&type=card';
		AppKit.getJsonApi(url).success(function(rspJson){
			if(rspJson.sendMessages.length == 0){
				$scope.sendNoRecord = true;
			}else{
				$scope.sendNoRecord = false;
			}
			$scope.sendMessages = rspJson.sendMessages;
			if(rspJson.receiveMessages.length == 0){
				$scope.receiveNoRecord = true;
			}else{
				$scope.receiveNoRecord = false;
			}
			$scope.receiveMessages = rspJson.receiveMessages;
		});	
	}
	
	$scope.msg = {"title":"","receiveCode":"","content":"","userName":""};
	
	$scope.resetMsg = function(){
		$scope.msg = {"title":"","receiveCode":"","content":"","userName":""};
	}
	
	$scope.isValidmsgInfo = function(){
		var msg = $scope.msg;
		if (msg.title && msg.title != '' && msg.receiveCode && msg.receiveCode != '' 
				&& msg.content && msg.content != '' ){
			return true;
		}
		else{
			return false;
		}
	}
	
	$scope.sendMessage = function(){
		var url = "/portal_portlets/index?NoticeReminderList&actionType=mobileSendMessage"
		AppKit.postJsonApi(url,$scope.msg).then(function(response){
			if ("success" == response.data){
				AppKit.successPopup();
				$scope.loadMessageData();
			}else{
				AppKit.errorPopup();
			}
		});
	}
    
    $scope.loadAppModuleInfo = function(){
		var url = '/portal/index?MobileDataProvider&actionType=findAppModuleList';
		AppKit.getJsonApi(url).success(function(jsonData){
			$scope.appModuleList = jsonData.moduleList;
			$scope.extraList = jsonData.extraList;
			AppKit.hideMask();
		});
	}
	
	$scope.addAppModal = function(){
		$scope.modalTitle = "待选应用模块";
		AppKit.createModal("tochoose-module-modal",$scope);			
	}
	
	$scope.toChooseModuleData=function(){
		var url = '/portal/index?MobileDataProvider&actionType=findToChooseAppModuleList';
		var promise = AppKit.getJsonApi(url);
		promise.success(function(rspJson){
			$scope.toChooseModuleList = rspJson.toChooseModuleList;
			if(rspJson.toChooseModuleList.length == 0){
				$scope.noneModules = true;
			}else{
				$scope.noneModules = false;
			}
		});
	}
	
	$scope.doSelectModule = function(){
		$scope.receiveList = [];
		for(var i=0;i < $scope.toChooseModuleList.length;i++){
			var item =  $scope.toChooseModuleList[i];
			if(item.checked){
				$scope.receiveList.push(item);
			}
		}
		if($scope.receiveList.length > 0){
			var url = "/portal/index?MobileDataProvider&actionType=mobileSaveAppModule&mark=selected";
			AppKit.postJsonApi(url,$scope.receiveList).then(function(response){
				if ("success" == response.data.text){
					$scope.loadAppModuleInfo();
					AppKit.successPopup({"title":"添加模块成功！",callback:function(){
						$scope.closeModal();
					}});
				}else{
					AppKit.errorPopup();
				}
			});
		}else{
			AppKit.errorPopup({"title":"选择有效模块！"});
		}
	}
	
	$scope.setModuleActiveTab = function (activeTab) {     
		　　$scope.activeTab = activeTab; 
	};
	
	$scope.initModuleTabs = function(tabs){
		$scope.tabs = tabs;	
		$scope.activeTab = tabs[0];
	};
	
	$scope.onModuleSwipeLeft = function(){
		for(var i=0;i< $scope.tabs.length;i++){
			var tempTab = $scope.tabs[i];
			if (tempTab == $scope.activeTab){
				if (i == ($scope.tabs.length-1))continue;
				
				$scope.activeTab = $scope.tabs[i+1];
				break;
			}
		}
	};

	$scope.onModuleSwipeRight = function(){
		for(var i=0;i< $scope.tabs.length;i++){
			var tempTab = $scope.tabs[i];
			if (tempTab == $scope.activeTab){
				if (i == 0)continue;
				
				$scope.activeTab = $scope.tabs[i-1];
				break;
			}
		}
	};
	
    $scope.signOut = function(){
		var url = '/aeaihr/index?MobileAttendance&actionType=getSignOutInfo';
		var promise = AppKit.getJsonApi(url);
		promise.success(function(rspJson){
			if(rspJson.isSignInOpera == 'N'){
				AppKit.errorPopup({"title":"没有签到,不能签退"});
			}else{
				$state.go("tab.signout-info");
			}
		});
    	
    }
	
	$scope.openDayExamPopup = function() {
		AppKit.createPopup("dayexam-popup",$scope);
	}
	$scope.openWeekExamPopup = function() {
		AppKit.createPopup("weekexam-popup",$scope);
	}
	
	$scope.openKmModal = function(id,text){
		$scope.modalTitle = '知讯详情';
		$scope.currentInfoId = id;
		$scope.addKmClickNumber(id);
		AppKit.createModal("km-info-modal",$scope);	
	}
	
	$scope.loadKnowData = function(){
		var url = '/portal/index?MobileInformationsDataProvider&actionType=getKnowledgeInformation&ID='+$scope.currentInfoId;
		var promise = AppKit.getJsonApi(url);
		promise.success(function(rspJson){
			$scope.id = rspJson.id;
			$scope.title = rspJson.title;
			$scope.createTime = rspJson.createTime;
			$scope.content = rspJson.content;
			$scope.isPraised = rspJson.isPraised;
			$scope.isFavorited = rspJson.isFavorited;
			$scope.praiseCount = rspJson.praiseCount;
			$scope.infoFavCount = rspJson.infoFavCount;
			$scope.infoReviewCount = rspJson.infoReviewCount;
			$scope.readCount = rspJson.readCount;
			if(rspJson.reviewList.length == 0){
				$scope.reviewNoRecord = true;
			}else{
				$scope.reviewNoRecord = false;
			}
			$scope.reviewList = rspJson.reviewList;
		});
	}
	
	$scope.doPraise = function(){
		var url = "/portal/index?ContentProvider&actionType=mobileChangePraiseCount&infoId="+$scope.currentInfoId;
		AppKit.postJsonApi(url,$scope.praiseInfo).then(function(response){
			if ("success" == response.data){
				$scope.loadKnowData();
				if($scope.isPraised == 'N'){
					AppKit.successPopup({"title":"点赞成功"});	
				}else{
					AppKit.successPopup({"title":"取消成功"});
				}
			}else{
				AppKit.errorPopup();
			}
		});
	}
	
	$scope.favoriteInfo = {"title":"","favType":"OTHERS"};
	$scope.doFavorite = function(){
		var url = "/portal/index?InfomationFavEdit&actionType=mobileAddToFavorite&infoId="+$scope.currentInfoId;
		AppKit.postJsonApi(url,$scope.favoriteInfo).then(function(response){
			if ("success" == response.data){
				$scope.loadKnowData();
				if($scope.isFavorited == 'N'){
					AppKit.successPopup({"title":"收藏成功"});	
				}else{
					AppKit.successPopup({"title":"取消成功"});	
				}			
			}else{
				AppKit.errorPopup();
			}
		});
	}
	
	$scope.openCommentModal = function(){
		AppKit.createModal("km-comment-modal",$scope);
	}
	
	$scope.commentInfo = {"rewContent":""};
	$scope.doComment = function(){
		var url = "/portal/resource?ContentProvider&actionType=mobileAddNewReview&ID="+$scope.currentInfoId;
		AppKit.postJsonApi(url,$scope.commentInfo).then(function(response){
			if ("success" == response.data){
				$scope.loadKnowData();
				AppKit.successPopup({"title":"评论成功"});
			}else{
				AppKit.errorPopup({"title":"评论失败"});
			}
		});
	}
	
	$scope.isValidCommentInfo = function(){
		var commentInfo = $scope.commentInfo;
		if (commentInfo.rewContent && commentInfo.rewContent != ''){
			return true;
		}
		else{
			return false;
		}
	}
	
	$scope.openBbsModal = function(id,text,num){
		var newScope = $scope.$new(false);
		newScope.modalTitle = '问答详情';
		newScope.currentInfoId = id;
		$scope.addBbsClickNumber(id)
		AppKit.createModal("bbs-content-modal",newScope);
	}
	
	$scope.openCategoryModal = function(category) {
		$scope.currentCategory = category;
		$scope.modalTitle = category + '类型帖子';
		if($scope.modal != undefined){
			$scope.closeModal();
		}
		AppKit.createModal("bbs-category-modal",$scope);
	};
	
	$scope.openReplyModal = function(floor){
		if('楼主' == floor && floor){
			$scope.postReplyInfo.replyFloor = '楼主';
			$scope.displayReplyFloor = floor;
		}else{
			$scope.postReplyInfo.replyFloor = floor;
			$scope.displayReplyFloor = floor+"楼";
		}
		AppKit.createModal("bbs-reply-modal",$scope);
	}
	
	$scope.postReplyInfo = {"replyFloor":"","friContent":""};
	$scope.doPostReply = function(){
		var url = "/portal/index?ForumPost&actionType=mobileCreateRepleceForumInfo&fpmId="+$scope.currentInfoId;
		AppKit.postJsonApi(url,$scope.postReplyInfo).then(function(response){
			if ("success" == response.data){
				$scope.loadBbsData();
				$scope.postReplyInfo.friContent = "";
				AppKit.successPopup();
			}else{
				AppKit.errorPopup();
			}
		});
	}
	
	$scope.doFollow = function(){
		var url = "/portal/resource?ForumPost&actionType=mobileCreateForumFavorite&fpmId="+$scope.currentInfoId;
		AppKit.postJsonApi(url,$scope.followInfo).then(function(response){
			if ("success" == response.data){
				$scope.loadBbsData();
				if($scope.isAttention == 'N'){
					AppKit.successPopup({"title":"关注成功"});
				}else{
					AppKit.successPopup({"title":"取消成功"});
				}				
			}else{
				AppKit.errorPopup();
			}
		});
	}
	
	$scope.loadBbsData = function(){
		var url = '/portal/index?MobileForumDataProvider&actionType=getForumInformation&fpmId='+$scope.currentInfoId;
		var promise = AppKit.getJsonApi(url);
		promise.success(function(rspJson){
			$scope.id = rspJson.id;
			$scope.title = rspJson.title;
			$scope.name = rspJson.name;
			$scope.createTime = rspJson.createTime;
			$scope.content = rspJson.content;
			$scope.type = rspJson.type;
			$scope.category = rspJson.category;
			$scope.isAttention = rspJson.isAttention;
			$scope.count = rspJson.count;
			$scope.clickNum = rspJson.clickNum;
			$scope.attentionCount = rspJson.attentionCount;
			if(rspJson.comments.length == 0){
				$scope.commentsNoRecord = true;
			}else{
				$scope.commentsNoRecord = false;
			}
			$scope.comments = rspJson.comments;
		});
	}
	
	$scope.isValidPostReplyInfo = function(){
		var postReplyInfo = $scope.postReplyInfo;
		if (postReplyInfo.friContent && postReplyInfo.friContent != ''){
			return true;
		}
		else{
			return false;
		}
	}
	
	$scope.openViewDetailPopup = function() {
		AppKit.createPopup("view-detail-popup",$scope);
	}
	
	$scope.addBbsClickNumber = function(id){
		$scope.info = {"fpmId":id};
		var url = "/portal/resource?ForumPost&actionType=mobileUpdatePostClickNumber"
		AppKit.postJsonApi(url,$scope.info).then(function(response){
		});
	}
	
	$scope.addKmClickNumber = function(id){
		$scope.info = {"id":id};
		var url = "/portal/index?ContentProvider&actionType=mobileIncreaseInfoReadCounts"
		AppKit.postJsonApi(url,$scope.info,{noMask:true}).then(function(response){
		});
	}
	
	$scope.openModuleOperatePopup = function(id){
		$scope.moduleId = id;
		var url = "/portal/index?MobileDataProvider&actionType=isFirstOrLastModule&moduleId="+id;
		AppKit.getJsonApi(url).then(function(rspJson){
			$scope.showMoveTopBtn = true;
			$scope.showMoveUpBtn = true;
			$scope.showMoveDownBtn = true;
			$scope.showMoveBottomBtn = true;
			if (rspJson.data == 'top'){
				$scope.showMoveTopBtn = false;
				$scope.showMoveUpBtn = false;
			}else if(rspJson.data == 'bottom'){
				$scope.showMoveDownBtn = false;
				$scope.showMoveBottomBtn = false;
			}
		});
		AppKit.createPopup("module-operate-popup",$scope);
	}
	
	$scope.upMoveTopAppModal = function(id){
		var url = "/portal/index?MobileDataProvider&actionType=mobileSaveAppModule&mark=upMoveTop&id="+id;
  		AppKit.postJsonApi(url).then(function(response){
  			if ("success" == response.data.text){
  				AppKit.closePopup($scope);
				$scope.loadAppModuleInfo();
				if("fail" == response.data.msg){
					AppKit.errorPopup({"title":"已经是首位模块！"});
				}
				AppKit.hideMask();
			}else{
				AppKit.errorPopup();
			}
  		});			
	}
	
	$scope.upMoveAppModal = function(id){
		var url = "/portal/index?MobileDataProvider&actionType=mobileSaveAppModule&mark=upMove&id="+id;
		AppKit.postJsonApi(url).then(function(response){
  			if ("success" == response.data.text){
				if("fail" == response.data.msg){
					AppKit.errorPopup({"title":"已经是首位模块！"});
				}else{
					AppKit.hideMask();
					$scope.loadAppModuleInfo();
					AppKit.closePopup($scope);
				}
			}else{
				AppKit.errorPopup();
			}
  		});			
	}
	
	$scope.downMoveAppModal = function(id){
		var url = "/portal/index?MobileDataProvider&actionType=mobileSaveAppModule&mark=downMove&id="+id;
  		AppKit.postJsonApi(url).then(function(response){
  			if ("success" == response.data.text){
				if("fail" == response.data.msg){
					AppKit.errorPopup({"title":"已经末位模块！"});
				}else{
					AppKit.hideMask();
					$scope.loadAppModuleInfo();		
					AppKit.closePopup($scope);
				}
			}else{
				AppKit.errorPopup();
			}
  		});			
	}
	
	$scope.downMoveBottomAppModal = function(id){
		var url = "/portal/index?MobileDataProvider&actionType=mobileSaveAppModule&mark=downMoveBottom&id="+id;
  		AppKit.postJsonApi(url).then(function(response){
  			if ("success" == response.data.text){
				$scope.loadAppModuleInfo();
				AppKit.closePopup($scope);
				if("fail" == response.data.msg){
					AppKit.errorPopup({"title":"已经末位模块！"});
				}
				AppKit.hideMask();
			}else{
				AppKit.errorPopup();
			}
  		});			
	}
	
	$scope.deleteAppModal = function(id){
		AppKit.confirm({operaType:'delete',action:function(){
			var url = "/portal/index?MobileDataProvider&actionType=mobileSaveAppModule&mark=delete&id="+id;
	  		AppKit.postJsonApi(url).then(function(response){
	  			if ("success" == response.data.text){
					$scope.loadAppModuleInfo();
					AppKit.hideMask();
					AppKit.closePopup($scope);
				}else{
					AppKit.errorPopup();
				}
	  		});			
		}});
	}
	
});