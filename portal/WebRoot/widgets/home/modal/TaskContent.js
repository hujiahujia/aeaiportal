angular.module('${menuCode}')
.controller("${widgetCode}Ctrl",function($scope,AppKit){
	var url = '/portal/resource?MobileTaskListDataProvider&actionType=getTaskRecord&ID='+$scope.currentId;
	var promise = AppKit.getJsonApi(url);
	promise.success(function(rspJson){
		$scope.datas = rspJson;
	});
	
	$scope.opinionContent = "";
	$scope.doApprove = function(result){
		$scope.info = {"id":$scope.currentId,"opinion":result,"opinionContent":$scope.opinionContent}
		var url = "/portal/index?MobileTaskListEdit&actionType=mobileSubmitTask"
		AppKit.postJsonApi(url,$scope.info).then(function(response){
			if ("success" == response.data){
				AppKit.successPopup();				
			}else{
				AppKit.errorPopup();
			}
		});
	}
});