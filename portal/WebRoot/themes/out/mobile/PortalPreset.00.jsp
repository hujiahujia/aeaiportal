<!DOCTYPE html>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://portal.agileai.com" prefix="pt" %>
<%@ page import="com.agileai.portal.driver.model.MenuBar"%>
<%@ page import="com.agileai.portal.driver.model.MenuItem"%>
<%
MenuBar menuBar = (MenuBar)request.getAttribute("_currentMenuBar_");
MenuItem menuItem = (MenuItem)request.getAttribute("_currentMenuItem_");
String navCode = menuBar.getId();

String windowMode =(String)request.getAttribute("WINDOW_MODE_KEY");
String modeText = "view".equals(windowMode)?"编辑组件":"展示信息";
String hostIp = request.getRemoteAddr();
if ("0:0:0:0:0:0:0:1".equals(hostIp)){
	hostIp = "localhost";
}
%>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=320,minimum-scale=0.5, maximum-scale=5, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>手机数通畅联</title>
<link href="<c:out value="${pageContext.request.contextPath}"/>/themes/out/mobile/res/jquery.mobile.structure-1.4.5.min.css" rel="stylesheet" type="text/css" />
<link href="<c:out value="${pageContext.request.contextPath}"/>/themes/out/mobile/res/jquery.mobile.inline-png-1.4.5.min.css" rel="stylesheet" type="text/css" />
<link href="<c:out value="${pageContext.request.contextPath}"/>/themes/out/mobile/res/jquery.mobile.theme-1.4.5.min.css" rel="stylesheet" type="text/css" />
<link href="<c:out value="${pageContext.request.contextPath}"/>/themes/out/mobile/res/jquery.mobile-1.4.5.min.css" rel="stylesheet" type="text/css" />
<link href="<c:out value="${pageContext.request.contextPath}"/>/themes/out/mobile/css/iconfonts/iconfont.css" rel="stylesheet" type="text/css" />
<link href="<c:out value="${pageContext.request.contextPath}"/>/themes/out/mobile/css/index.css" rel="stylesheet" type="text/css" />
<%if ("edit".equals(windowMode)){%>	
<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/style.css" type="text/css" />
<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/decorator.css" type="text/css" />
<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/jquery.qtip.css" type="text/css" />
<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/contextmenu.css" type="text/css" />
<link rel="stylesheet" href="<c:out value="${pageContext.request.contextPath}"/>/css/jquery.excoloSlider.css" type="text/css" />
<style type="text/css">
body {
	font-size:1em;
}
div,td,span,th {
	font-size:1em;
}
</style>
<%}%>
<%if(menuItem.getCustomCssURL() != null && !"".equals(menuItem.getCustomCssURL())){%>
	<link rel="stylesheet" href="<%=menuItem.getCustomCssURL()%>" type="text/css" />
<%}%>	
<script src="<c:out value="${pageContext.request.contextPath}"/>/themes/out/mobile/res/jquery.min.js" type="text/javascript"></script>
<script src="<c:out value="${pageContext.request.contextPath}"/>/themes/out/mobile/res/jquery.mobile-1.4.5.min.js" type="text/javascript"></script>
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.excoloSlider.min.js" language="javascript"></script>
<%if ("edit".equals(windowMode)){%>	
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/Map.js" language="javascript"></script>
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/util.js" language="javascript"></script>
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/portletaction.js" language="javascript"></script>
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/ContextMenu.js" language="javascript"></script>
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/PortletContextMenu.js" language="javascript"></script>
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/PopupBox.js" language="javascript"></script>
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/jquery.qtip.js" language="javascript"></script>
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/imagesloaded.pkgd.js" language="javascript"></script>
<%}%>
<%if(menuItem.getCustomJsURL() != null && !"".equals(menuItem.getCustomJsURL())){%>
<script type="text/javascript" src="<%=menuItem.getCustomJsURL()%>"></script>
<%}%>	
<script type="text/javascript">
<%if ("edit".equals(windowMode)){%>	
$.ajaxSetup({cache:false});
var __renderPortlets = new Map();	
var _directConfigPortletBox;
function directConfigDecoratorRequest(portletId){
	if (!_directConfigPortletBox){
		_directConfigPortletBox = new PopupBox('_directConfigPortletBox','配置窗口外观',{size:'normal',height:'380px'});
	}
	var url = '/portal/index?PagePortletConfig&portletId='+portletId;
	_directConfigPortletBox.sendRequest(url);	
}
$(function(){
	<%if ("edit".equals(windowMode)){%>
	$("#content_container > h3").hover(
	  function () {
	    $(this).find("span[id=stateAnchor]").fadeIn('fast');
	  }, 
	  function () {
	    $(this).find("span[id=stateAnchor]").fadeOut('fast');
	  }
	);
	<%}%>
	$("#content_container > h3").find("span[id=stateAnchor]").hide();
	PopupBox.contextPath="<c:out value="${pageContext.request.contextPath}"/>/";	
});
<%}%>
$(document).ready(function() { 
    jQuery.mobile.ajaxEnabled = false; 
}); 
function viewDetail(infomationId){
	window.location.href="/portal/website/<%=navCode%>/content.ptml/<%=menuItem.getCode()%>/"+infomationId
}
function goTop(){
	 $("body,html").animate({scrollTop:0},300);
}
</script>
<style type="text/css">
.wrap {margin:0px;font:0.80em/1.6 sans-serif;font-weight:normal;font-family: serif;}

#footer{background-color:#000;text-shadow:none;color:#eee;font-size:13px;position: fixed;bottom: 0; width: 320px;height: 45px;}
#footer span{color:#eee;height:20px;line-height:20px;}
#footer span a{text-decoration:none;color:#eee;font-weight:normal;}
#footer span a:visited,#footer span a:hover{text-decoration:none;color:#eee;font-weight:normal;}
#footer span img{vertical-align: middle;}

</style>
</head>
<body style="zoom: 1;margin:0 auto; background:#FFF;">
<div style="width:320px;min-height:548px;margin: 0 auto;background-image:none;overflow-x: hidden;position: relative;">
  <div data-role="header" style="background-color:#FFF" data-position="fixed" data-tap-toggle="false">   
  <h3 style="font-size:1em;">
  </h3>
  	<a href="/portal/website/<%=navCode%>/index.ptml" data-role="none"><img src="<c:out value="${pageContext.request.contextPath}"/>/themes/out/mobile/images/logo.jpg" width="133" height="37"></a>
	<a href="/portal/casindex?Redirect" data-role="none"><i style="display:inline-block;font-size:23px;margin-right: 5px;" class="icon iconfont"></i></a>  
  </div>
<div data-role="none" class="wrap" style="width:320px;height:548px;max-height:548px;min-height:548px;margin: 0 auto;position: relative;">
	<pt:mlayout/>   
	<div id="footer">
	<div class="foot-fluid">
		<span class="foot-span4"><a href="index.ptml"><img src="/HotServer/reponsitory/images/mobile-icons/home.png">主页</a></span>
		<span class="foot-span4"><a href="tel:024-22962011"><img src="/HotServer/reponsitory/images/mobile-icons/tel.png">电话</a></span>
		<span class="foot-span4"><a href="http://shang.qq.com/wpa/qunwpa?idkey=366d7f20977a19335ecbf578959f3fbd607436af9b18fea7bd3e4f9f0710d040"><img src="/HotServer/reponsitory/images/mobile-icons/qq.png">QQ</a></span>
		<span class="foot-span4"><a href="http://mp.weixin.qq.com/s?__biz=MzA5NzU3NTg3NA==&mid=502008401&idx=1&sn=a438661a0f69386085bd0ca828490524&scene=0&previewkey=C54lfUpwfLCUgabsRCXbtcNS9bJajjJKzz%2F0By7ITJA%3D#wechat_redirect"><img src="/HotServer/reponsitory/images/mobile-icons/wx.png">微信</a></span>
	</div>
</div>
</div>
</body>
</html>