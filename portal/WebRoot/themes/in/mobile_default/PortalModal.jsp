<!DOCTYPE html>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://portal.agileai.com" prefix="pt" %>
<%@ page import="java.util.List"%>
<%@ page import="com.agileai.util.StringUtil"%>
<%@ page import="com.agileai.portal.driver.model.MenuBar"%>
<%@ page import="com.agileai.portal.driver.model.MenuItem"%>
<%@ page import="com.agileai.portal.driver.model.Page"%>
<%@ page import="com.agileai.portal.extend.mobile.MobileHelper"%>
<%@ page import="com.agileai.portal.driver.mobile.AngularWidget"%>
<%
MenuBar menuBar = (MenuBar)request.getAttribute("_currentMenuBar_");
MenuItem menuItem = (MenuItem)request.getAttribute("_currentMenuItem_");
MobileHelper mobileHelper = new MobileHelper((HttpServletRequest)request);
Page pageModel = (Page)request.getAttribute("_currentPage_");
boolean customHeader = mobileHelper.isCustomHeader(menuItem);
boolean hasFooter = mobileHelper.hasFooter(menuItem);
List<AngularWidget> angularWidgetList = mobileHelper.getAngularWidgetList(pageModel);
%>
<ion-modal-view>

<%
if (customHeader){
	%>
<ion-header-bar class="bar bar-header bar-<%=mobileHelper.getHeaderColor()%>">
<%
if (angularWidgetList.size() > 0){
	AngularWidget angularWidget = (AngularWidget)angularWidgetList.get(0);
%>
  <%=mobileHelper.getWidgetTemplateContent(menuItem, angularWidget)%>
<%
	}
%>  
</ion-header-bar>
  <ion-content<%if (hasFooter){%> class="has-footer"<%}%>>
<%
	for (int i=0;i < angularWidgetList.size();i++){
	if (i == 0){
		continue;
	}
	if (hasFooter && i == angularWidgetList.size()-1){
		continue;
	}
	AngularWidget angularWidget = (AngularWidget)angularWidgetList.get(i);
%>
	<div ng-controller="<%=angularWidget.getCode()%>Ctrl">
	<%=mobileHelper.getWidgetTemplateContent(menuItem, angularWidget)%>
	</div>
<%
	}
%> 
  </ion-content>
  <%if (hasFooter){
	  AngularWidget angularWidget = (AngularWidget)angularWidgetList.get(angularWidgetList.size()-1);
  %>
	<%=mobileHelper.getWidgetTemplateContent(menuItem, angularWidget)%>
  <%}%>

  
<%
	}else{
%>
  <ion-header-bar class="bar bar-header bar-<%=mobileHelper.getHeaderColor()%>">
	<button ng-click="closeModal()" class="button back-button buttons button-clear header-item">
		<i class="icon ion-android-arrow-back"></i> 
    </button>
    <h1 class="title">{{modalTitle}}</h1>
  </ion-header-bar>
  <ion-content <%if (hasFooter){%> class="has-footer"<%}%>>
<%
for (int i=0;i < angularWidgetList.size();i++){
	AngularWidget angularWidget = (AngularWidget)angularWidgetList.get(i);
	if (hasFooter && i == angularWidgetList.size()-1){
		continue;
	}
%>
	<div ng-controller="<%=angularWidget.getCode()%>Ctrl">
	<%=mobileHelper.getWidgetTemplateContent(menuItem, angularWidget)%>
	</div>
<%}%> 
  </ion-content>
    
  <%if (hasFooter){
	  AngularWidget angularWidget = (AngularWidget)angularWidgetList.get(angularWidgetList.size()-1);
  %>
	<%=mobileHelper.getWidgetTemplateContent(menuItem, angularWidget)%>
  <%}%>  
  
<%}%>
</ion-modal-view>