<!DOCTYPE html>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://portal.agileai.com" prefix="pt" %>
<%@ page import="java.util.List"%>
<%@ page import="com.agileai.portal.driver.model.MenuBar"%>
<%@ page import="com.agileai.portal.driver.model.MenuItem"%>
<%@ page import="com.agileai.portal.driver.model.Page"%>
<%@ page import="com.agileai.portal.extend.mobile.MobileHelper"%>
<%@ page import="com.agileai.portal.driver.mobile.AngularWidget"%>
<%
MenuBar menuBar = (MenuBar)request.getAttribute("_currentMenuBar_");
MobileHelper mobileHelper = new MobileHelper((HttpServletRequest)request);
MenuItem menuItem = (MenuItem)request.getAttribute("_currentMenuItem_");
Page pageModel = (Page)request.getAttribute("_currentPage_");

String title = menuItem.getName();
if (menuItem.getProperties() != null && menuItem.getProperties().has("title")){
	  title = menuItem.getProperties().getString("title");
}	
%>
<ion-view view-title="<%=title%>">
  <%=mobileHelper.getPageTemplateContent(menuItem)%>  
  <ion-content class="overall-background-color">
<%
List<AngularWidget> angularWidgetList = mobileHelper.getAngularWidgetList(pageModel);
for (int i=0;i < angularWidgetList.size();i++){
	AngularWidget angularWidget = (AngularWidget)angularWidgetList.get(i);
%>
	<div ui-view="<%=angularWidget.getCode()%>"></div>
<%}%> 
  <div class="padding">
	<button class="button button-block button-balanced" ng-click="openAddWidgetModal()">添加卡片</button>
  </div>
  </ion-content>
</ion-view>