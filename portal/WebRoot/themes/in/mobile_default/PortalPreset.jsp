<!DOCTYPE html>
<%@page import="org.codehaus.jettison.json.JSONObject"%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://portal.agileai.com" prefix="pt" %>
<%@ page import="java.util.*"%>
<%@ page import="com.agileai.portal.driver.model.MenuBar"%>
<%@ page import="com.agileai.portal.driver.model.MenuItem"%>
<%@ page import="com.agileai.portal.driver.mobile.AngularWidget"%>
<%@ page import="com.agileai.portal.extend.mobile.MobileHelper"%>
<%
MenuBar menuBar = (MenuBar)request.getAttribute("_currentMenuBar_");
MenuItem menuItem = (MenuItem)request.getAttribute("_currentMenuItem_");
MobileHelper mobileHelper = new MobileHelper((HttpServletRequest)request);
String viewTemplatePrefix = mobileHelper.getViewTemplatePrefix();
MenuItem indexMenuItem = mobileHelper.getIndexMenuItem();
MenuItem firstMenuItem = mobileHelper.getFirstValidMenuItem();

String indexMenuCode = "";
String firstMenuCode = "";
if (indexMenuItem != null){
	indexMenuCode = indexMenuItem.getCode();
}

if (firstMenuItem != null){
	String wxFirstMenuCode = mobileHelper.getWxFirstMenuCode(); 
	if (wxFirstMenuCode != null){
		firstMenuCode = wxFirstMenuCode;
	} else {
		firstMenuCode = firstMenuItem.getCode();		
	}
}
%>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">
<title></title>
<link href="${pageContext.request.contextPath}/themes/in/mobile_default/lib/ionic/css/ionic.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/themes/in/mobile_default/lib/chart/css/angular-chart.min.css" type="text/css" />
<link href="${pageContext.request.contextPath}/themes/in/mobile_default/lib/jquery/css/jquery-ui-1.10.0.custom.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/themes/in/mobile_default/lib/calendar/css/fullcalendar.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/themes/in/mobile_default/lib/mui/css/mui.ext.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/themes/in/mobile_default/css/style.css" rel="stylesheet" type="text/css" />
</head>
<body ng-app="<%=menuBar.getId()%>">
  <ion-nav-bar class="bar-<%=mobileHelper.getHeaderColor()%>">
    <ion-nav-back-button>
    </ion-nav-back-button>
  </ion-nav-bar>

  <ion-nav-view></ion-nav-view>
  <script id="<%=viewTemplatePrefix%>/<%=indexMenuCode%>.html" type="text/ng-template">
  <ion-tabs class="tabs-icon-top tabs-color-active-<%=mobileHelper.getFooterColor()%>" ng-class="{'tabs-item-hide': $root.hideTabs}">
  <%=mobileHelper.getTabConfigs()%>
  </ion-tabs>
  </script>
</body>
</html>

<script src="${pageContext.request.contextPath}/themes/in/mobile_default/lib/jquery/jquery-2.0.3.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/themes/in/mobile_default/lib/jquery/js/jquery-ui.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/themes/in/mobile_default/lib/ionic/js/ionic.bundle.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/themes/in/mobile_default/lib/ionic/js/angular/angular-locale_zh-cn.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/themes/in/mobile_default/plugins/ionic-close-popup.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/themes/in/mobile_default/lib/chart/js/Chart.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/themes/in/mobile_default/lib/chart/js/angular-chart.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/themes/in/mobile_default/lib/calendar/js/moment.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/themes/in/mobile_default/lib/calendar/js/fullcalendar.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/themes/in/mobile_default/lib/calendar/js/calendar.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/themes/in/mobile_default/plugins/ocLazyLoad.min.js" type="text/javascript"></script>
<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=j6x1iGtGaxQcxaEDgbHRKDUp"></script>

<script src="/portal/index?MobileDataProvider&actionType=retrieveAppJs&menuBarCode=<%=menuBar.getId()%>" type="text/javascript"></script>
<script type="text/javascript">
<%=mobileHelper.getModulesDefine()%>
angular.module('<%=menuBar.getId()%>').config(function($stateProvider, $urlRouterProvider,$ionicConfigProvider) {
    $ionicConfigProvider.tabs.style("standard");
    $ionicConfigProvider.tabs.position("bottom");
    var prefix = "<%=viewTemplatePrefix%>";  
  	
    $stateProvider.state('tab', {
     url: '/tab',
     abstract: true,
     templateUrl: prefix+'/<%=indexMenuCode%>.html'
    });
    
    <%=mobileHelper.getStateConfigs()%>
    
    $urlRouterProvider.otherwise('/tab/<%=firstMenuCode%>');
});
</script>