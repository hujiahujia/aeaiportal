<!DOCTYPE html>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://portal.agileai.com" prefix="pt" %>
<%@ page import="java.util.List"%>
<%@ page import="com.agileai.util.StringUtil"%>
<%@ page import="com.agileai.portal.driver.model.MenuBar"%>
<%@ page import="com.agileai.portal.driver.model.MenuItem"%>
<%@ page import="com.agileai.portal.driver.model.Page"%>
<%@ page import="com.agileai.portal.extend.mobile.MobileHelper"%>
<%@ page import="com.agileai.portal.driver.mobile.AngularWidget"%>
<%
MenuBar menuBar = (MenuBar)request.getAttribute("_currentMenuBar_");
MenuItem menuItem = (MenuItem)request.getAttribute("_currentMenuItem_");
MobileHelper mobileHelper = new MobileHelper((HttpServletRequest)request);
Page pageModel = (Page)request.getAttribute("_currentPage_");
List<AngularWidget> angularWidgetList = mobileHelper.getAngularWidgetList(pageModel);
for (int i=0;i < angularWidgetList.size();i++){
	AngularWidget angularWidget = (AngularWidget)angularWidgetList.get(i);
%>
	<div ng-controller="<%=angularWidget.getCode()%>Ctrl">
	<%=mobileHelper.getWidgetTemplateContent(menuItem, angularWidget)%>
	</div>
<%}%>
</ion-popover-view>