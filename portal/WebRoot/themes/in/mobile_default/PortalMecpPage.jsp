<!DOCTYPE html>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://portal.agileai.com" prefix="pt" %>
<%@ page import="java.util.List"%>
<%@ page import="com.agileai.portal.driver.model.MenuBar"%>
<%@ page import="com.agileai.portal.driver.model.MenuItem"%>
<%@ page import="com.agileai.portal.driver.model.Page"%>
<%@ page import="com.agileai.portal.extend.mobile.MobileHelper"%>
<%@ page import="com.agileai.portal.driver.mobile.AngularWidget"%>
<%@ page import="com.agileai.portal.controller.mobile.AppModule"%>
<%
MenuBar menuBar = (MenuBar)request.getAttribute("_currentMenuBar_");
MobileHelper mobileHelper = new MobileHelper((HttpServletRequest)request);
MenuItem menuItem = (MenuItem)request.getAttribute("_currentMenuItem_");
Page pageModel = (Page)request.getAttribute("_currentPage_");

String title = menuItem.getName();
if (menuItem.getProperties() != null && menuItem.getProperties().has("title")){
	  title = menuItem.getProperties().getString("title");
}	
%>
<ion-view view-title="<%=title%>">
  <%=mobileHelper.getPageTemplateContent(menuItem)%>
  <ion-content class="overall-background-color">
	<div class="card top-module">
		<div class="<%=mobileHelper.getHeaderColor()%>-bg item tabs tabs-secondary tabs-icon-top">
			<a href="#/tab/signin-info" class="tab-item active" >
			<i class="icon ion-android-arrow-dropdown-circle"></i><b class="home-font">签&nbsp;到</b></a>
			<a href="#" class="tab-item active" ng-click="signOut()">
			<i class="icon ion-android-arrow-dropup-circle"></i><b class="home-font">签&nbsp;退</b></a>
			<a href="#/tab/location-info" class="tab-item active" >
			<i class="icon ion-android-pin" ></i><b class="home-font">定&nbsp;位</b></a>
			<a ng-click="openViewDetailPopup()" class="tab-item active" >
			<i class="icon ion-android-cloud-circle" ></i><b class="home-font">查&nbsp;看</b></a>
		</div>
	</div>
	<ion-spinner ng-if="onInitState" icon="android" class="aeai-spinner"></ion-spinner>
	<div class="item mui-content module-content">
	    <ul class="mui-table-view mui-grid-view mui-grid-9 home-module-ul">
	    	<li class="mui-table-view-cell mui-media mui-col-xs-3 mui-col-sm-3 home-module-li" ng-repeat="x in appModuleList">
	         <a ng-if="x.methodType=='href'" href="{{x.methodText}}"  on-hold="openModuleOperatePopup('{{x.id}}');">
	          	<i class="icon icon-aeai-module {{x.css}}"></i>
	          	<div class="home-font" style="padding-top:5px;"><b>{{x.text}}</b></div>
	         </a>
	         <a ng-if="x.methodType=='ng-click'" ng-click="{{x.methodText}}"  on-hold="openModuleOperatePopup('{{x.id}}');">
	          	<i class="icon icon-aeai-module {{x.css}}"></i>
	          	<div class="home-font" style="padding-top:5px;"><b>{{x.text}}</b></div>
	         </a>         
	        </li>
	    	<li class="mui-table-view-cell mui-media mui-col-xs-3 mui-col-sm-3 home-module-li-plus">
	         	<i class="icon ion-plus-round" style="font-size:32px;height:34px;display:block;" ng-click="addAppModal()"></i>
	        </li>
	        <li class="mui-table-view-cell mui-media mui-col-xs-3 mui-col-sm-3 home-module-li-plus" ng-repeat="x in extraList">
	        	<i class="icon" style="height:34px;display:block;color:white;"></i>
	        </li>
	    </ul>
	</div>
<%
List<AngularWidget> angularWidgetList = mobileHelper.getAngularWidgetList(pageModel);
for (int i=0;i < angularWidgetList.size();i++){
	AngularWidget angularWidget = (AngularWidget)angularWidgetList.get(i);
%>
	<div ui-view="<%=angularWidget.getCode()%>"></div>
<%}%> 
  <div class="padding">
	<button class="button button-block button-balanced" ng-click="openAddWidgetModal()">添加卡片</button>
  </div>
</ion-content>
</ion-view>