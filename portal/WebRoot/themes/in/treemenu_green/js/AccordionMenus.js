﻿var ContentHeight = 200;
var TimeToSlide = 250.0;

var openAccordion = '';

var perHeaderHeight = 30;
var selectedAccordionIndex;

function runAccordion(index)
{
	  var targetContentId = "Accordion" + index + "Content";
	  if (ele(targetContentId)){
		  if ($("#"+targetContentId).css('display') == 'none'){
			  $(".AccordionContent").css('display','none');
			  $("#"+targetContentId).css('display','block');
			  var containerHeight = $("#AccordionContainer").height()-10;
			  var containerHeaderNumber = $(".headerContainer").size();
			  var leftHeight = containerHeight - (containerHeaderNumber * perHeaderHeight);
			  var displayHeigh = leftHeight > ContentHeight?ContentHeight:leftHeight;
			  if (displayHeigh < 0){
				  displayHeigh = ContentHeight;
			  }			  
			  $("#"+targetContentId).css('height',displayHeigh);	 				  
		  }else{
			  $("#"+targetContentId).css('display','none');
		  }
	  }
}

function resetAccordionHeight()
{
	  if (selectedAccordionIndex){
		  var targetContentId = "Accordion" + selectedAccordionIndex + "Content";
		  if (ele(targetContentId)){
			  if ($("#"+targetContentId).css('display') == 'block'){
				  var containerHeight = $("#AccordionContainer").height()-10;
				  var containerHeaderNumber = $(".headerContainer").size();
				  var leftHeight = containerHeight - (containerHeaderNumber * perHeaderHeight);
				  var displayHeigh = leftHeight > ContentHeight?ContentHeight:leftHeight;
				  if (displayHeigh < 0){
					  displayHeigh = ContentHeight;
				  }			  
				  $("#"+targetContentId).css('height',displayHeigh);
			  }
		  }
	  }
}

/*
function runAccordion(index)
{
  var nID = "Accordion" + index + "Content";
  if(openAccordion == nID)
    nID = '';
  
  console.log('come in')
  setTimeout("animate(" + new Date().getTime() + "," + TimeToSlide + ",'" + openAccordion + "','" + nID + "')", 33);
  
  openAccordion = nID;
}

function animate(lastTick, timeLeft, closingId, openingId)
{  
  var curTick = new Date().getTime();
  var elapsedTicks = curTick - lastTick;
  
  var opening = (openingId == '') ? null : document.getElementById(openingId);
  var closing = (closingId == '') ? null : document.getElementById(closingId);
 
  if(timeLeft <= elapsedTicks)
  {
    if(opening != null)
      opening.style.height = ContentHeight + 'px';
    
    if(closing != null)
    {
      closing.style.display = 'none';
      closing.style.height = '0px';
    }
    return;
  }
 
  timeLeft -= elapsedTicks;
  var newClosedHeight = Math.round((timeLeft/TimeToSlide) * ContentHeight);

  if(opening != null)
  {
    if(opening.style.display != 'block')
      opening.style.display = 'block';
    opening.style.height = (ContentHeight - newClosedHeight) + 'px';
  }
  
  if(closing != null)
    closing.style.height = newClosedHeight + 'px';
  console.log('come in 1')
  setTimeout("animate(" + curTick + "," + timeLeft +",'" + closingId + "','" + openingId + "')", 33);
}*/