<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>安全提示</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function checkIsTop(){
	if (top.location.href != self.location.href) {
		top.location.href=self.location.href;
	}
}
</script>
</head>
<body onLoad="checkIsTop()" style="background-color:#F7F7F7">
<div style="background-image: url('<%=request.getContextPath()%>/images/error-bg.jpg');background-repeat:no-repeat;background-position:50% 0% ;height:500px;">
<div style="padding-top:250px;padding-left: 350px;">
<div id="LoginResult" style="margin:auto;width:400px;font-size:16px;color:red;">访问权限异常,请联系管理员！请点击<a href="<%=request.getContextPath()%>/index?Login&actionType=logout">这里</a>，重新登录。</div>
</div>
</div>
</body>
</html>
