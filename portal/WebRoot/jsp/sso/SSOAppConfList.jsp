<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>单点应用配置</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
var addAppBox;
function addSSORequest(){
	if (!addAppBox){
		addAppBox = new PopupBox('addAppBox','待选应用列表',{size:'normal',height:'390px',top:'10px'});
	}
	var url = '<%=request.getContextPath()%>/index?SSOAppPickSelectList&userId=<%=pageBean.inputValue("userId")%>';
	addAppBox.sendRequest(url);
}

var editAppBox;
function editSSORquest(ticketId){
	if (!editAppBox){
		editAppBox = new PopupBox('editAppBox','单点应用配置',{size:'normal',height:'250px',top:'10px'});
	}
	var url = '<%=request.getContextPath()%>/index?SSOAppConfig&ticketId='+ticketId;
	editAppBox.sendRequest(url);	
}
function deleteSSO(ticketId){
	if (confirm('确认要删除么？')){
		$("#ticketId").val(ticketId);
		doSubmit({actionType:'deleteSSO'});		
	}
}
function refreshSSOList(){
	doSubmit({actionType:''});
}
function moveUp(ticketId){
	$("#ticketId").val(ticketId);
	doSubmit({actionType:'moveUp'});
}
function moveDown(ticketId){
	$("#ticketId").val(ticketId);
	doSubmit({actionType:'moveDown'});
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table class="dataTable" cellspacing="0" cellpadding="0" style="margin:2px">
<thead>
  <tr>
    <th width="70%" align="center" nowrap="nowrap">名称</th>
    <th width="80" align="center" nowrap="nowrap" style="text-align: center;">显示</th>
    <th width="80" align="center" nowrap ><input type="button" class="formbutton" name="button5" id="button5" value="添 加" onclick="addSSORequest()" /></th>
  </tr>
</thead>
<%
int listCount = pageBean.listSize();
for (int i=0;i < listCount;i++){
%>
  <tr>
    <td height="24">&nbsp;<%=pageBean.labelValue(i,"APP_NAME")%></td>
	<td height="24">&nbsp;<%=pageBean.labelValue(i,"IS_VISIABLE")%></td>    
    <td height="24" align="center" nowrap style="text-align:center">
    <input style="margin:1px;" type="button" class="formbutton" name="button5" id="button5" value="编辑" onclick="editSSORquest('<%=pageBean.labelValue(i,"TICKET_ID")%>')" />
    <input style="margin:1px;" type="button" class="formbutton" name="button5" id="button5" value="删除" onclick="deleteSSO('<%=pageBean.labelValue(i,"TICKET_ID")%>')" />
    <input style="margin:1px;" type="button" class="formbutton" name="button5" id="button5" value="上移" onclick="moveUp('<%=pageBean.labelValue(i,"TICKET_ID")%>')" <%=pageBean.disabled(i == 0) %> />
    <input style="margin:1px;" type="button" class="formbutton" name="button5" id="button5" value="下移" onclick="moveDown('<%=pageBean.labelValue(i,"TICKET_ID")%>')" <%=pageBean.disabled(listCount-1 == i) %> /></td>
  </tr>
<%}%>  
</table>
<input type="hidden" name="ticketId" id="ticketId" value="" />
<input type="hidden" name="actionType" id="actionType" value=""/>
</form>
</body>
</html>
<script language="JavaScript">
</script>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
