<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@include file="/jsp/inc/resource.inc.jsp"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/aeditors/kdeditor/themes/default/default.css" type="text/css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/aeditors/kdeditor/extends.css" type="text/css" />
<script type="text/javascript" src="<%=request.getContextPath()%>/aeditors/kdeditor/kindeditor-min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/aeditors/kdeditor/extends.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/aeditors/kdeditor/lang/zh_CN.js"></script>
<style>
.postrequired{
	border: solid 0px #CCC;
	background-color: #FFFFFF;
	margin: 0px;
	width:890px;
	float: left;
	height:200px;
}
.add{ 
	color:red;
}
</style>
<script type="text/javascript">
function updateReplyPostInfo(){
	editor.sync();
	postRequest('replyForm1',{actionType:'updateReplyPostInfo',onComplete:function(responseText){
		if (responseText == 'fail'){
			alert("修改失败请检查操作!!");
		}else{
			parent.refreshCurrentPage();
		}
	}});
}

function updateMobileReplyPostInfo(){
	editor.sync();
	postRequest('replyForm1',{actionType:'updateMobileReplyPostInfo',onComplete:function(responseText){
		if (responseText == 'fail'){
			alert("修改失败请检查操作!!");
		}else{
			parent.refreshCurrentPage();
		}
	}});
}

function closePopup(){
	javascript:parent.PopupBox.closeCurrent();
	parent.refreshCurrentPage();
}

function refreshCurrentPage(){
	document.location.reload(); 
}


var addforumPostImgBox;
function openAddResourceRequestBox(){
	var title = "上传图片";
	var top = $('#FRI_CONTENT').position().top;
	if (!addforumPostImgBox){
		addforumPostImgBox = new PopupBox('addforumPostImgBox',title,{size:'big',width:'260px',height:'260px',top:top});
	}
	var url = '/portal/resource?ForumReplyImgResouceUploader&BIZ_ID='+$('#FPM_ID').val();
	addforumPostImgBox.sendRequest(url);	
}

$(document).ready(function() {
    $("#li_pc").click(function(){
		$(this).addClass("add").siblings().removeClass("add");
		$("#tr_mobile").hide();
		$("#tr_pc").show();
		$("#btn_mobile").hide();
		$("#btn_pc").show();
	});
    $("#li_mobile").click(function(){
		$(this).addClass("add").siblings().removeClass("add");
		$("#tr_pc").hide();
		$("#tr_mobile").show();
		$("#btn_pc").hide();
		$("#btn_mobile").show();
	});
});
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="replyForm1" id="replyForm1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<ul style="height:5px;margin-top:0px;width:200px;display:inline-block;list-style: none;">
<li id="li_pc" style="float: left;width: 25%;cursor: pointer;text-align:center;" class="add" onclick="changeEditer('editorPC')"><span>电脑版</span></li>
<li id="li_mobile" style="float: left;width: 25%;cursor: pointer;text-align:center;" class="" onclick="changeEditer('editorMobile')"><span>移动版</span></li>
</ul>
<table class="detailTable" cellspacing="0" cellpadding="0">
  <tr id="tr_pc">
	<td colspan="2">
  	  <textarea name="FRI_CONTENT" id="FRI_CONTENT" class="postrequired" rows="10" cols="70"><%=pageBean.inputValue("friContent")%></textarea>
	</td>
  </tr>
  <tr id="btn_pc">
    <td colspan="4" align="center">
    <input class="formbutton" type="button" name="Button23" value="修改" onclick="updateReplyPostInfo()"/>&nbsp; &nbsp;
    <input class="formbutton" type="button" name="Button22" value="关闭" onclick="closePopup()"/></td>
  </tr>
  
  <tr id="tr_mobile" style="display:none;">
	<td colspan="2">
  	  <textarea name="FPM_CONTENT_MOBILE" id="FPM_CONTENT_MOBILE" class="postrequired" rows="10" cols="70"><%=pageBean.inputValue("mobileReplayContent")%></textarea>
	</td>
  </tr>
  <tr id="btn_mobile" style="display:none;">
    <td colspan="4" align="center">
    <input class="formbutton" type="button" name="Button23" value="修改" onclick="updateMobileReplyPostInfo()"/>&nbsp; &nbsp;
    <input class="formbutton" type="button" name="Button22" value="关闭" onclick="closePopup()"/></td>
  </tr>
</table>
<input type="hidden" name="actionType" id="actionType" />
<input type="hidden" name="friId" id="friId" value="<%=pageBean.inputValue("friId")%>" />
</form>
<script language="javascript">
var editor;
var editorPC;
var editorMobile;

function changeEditer(editerMark){
	if(editerMark == 'editorPC'){
		editor =  editorPC;
	}else{
		editor = editorMobile;
	}
}

KindEditor.ready(function(K) {
	editorPC = K.create('textarea[name="FRI_CONTENT"]', {
		resizeType : 1,
		height:"470px",
		width:"890px",
		allowPreviewEmoticons : false,
		allowImageUpload : false,
		items : [
				'source','fontsize', '|', 'bold','removeformat','copy', 'paste', 'plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 'insertorderedlist',
				'insertunorderedlist', '|', 'emoticons', 'forumimage', 'link']
			});
	editor =  editorPC;
});

KindEditor.ready(function(K) {
	editorMobile = K.create('textarea[name="FPM_CONTENT_MOBILE"]', {
		resizeType : 1,
		height:"470px",
		width:"890px",
		allowPreviewEmoticons : false,
		allowImageUpload : false,
		items : [
				'source','fontsize', '|', 'bold','removeformat','copy', 'paste', 'plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 'insertorderedlist',
				'insertunorderedlist', '|', 'emoticons', 'forumimage', 'link']
			});
});


function afterSelectImage(uploadeImgObj){
	var url = uploadeImgObj.url;
    var width = uploadeImgObj.width;
    var height = uploadeImgObj.height;
    var title = ""; 
    var border = "0";
    var align = null;
    editor.exec('insertimage', url, title, width, height, border, align);
}
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
