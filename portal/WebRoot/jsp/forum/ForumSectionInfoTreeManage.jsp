<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>模块管理</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function doRefresh(nodeId){
	$('#FCI_ID').val(nodeId);
	doSubmit({actionType:'refresh'});
}
var targetTreeBox;
function showParentSelectBox(){
	if (!targetTreeBox){
		targetTreeBox = new PopupBox('targetTreeBox','请选择目标目录',{size:'normal',width:'350px',top:'3px'});
	}
	var handlerId = "ForumSectionInfoParentSelect";
	var url = 'index?'+handlerId+'&FCI_ID='+$("#FCI_ID").val();
	targetTreeBox.sendRequest(url);
}
function doChangeParent(){
	postRequest('form1',{actionType:'changeParent',onComplete:function(responseText){
		if (responseText == 'success'){
			doRefresh($('#FCI_ID').val());			
		}else {
			alert('迁移父节点出错啦！');
		}
	}});
}
function doSave(){
	if (checkSave()){
		$("#operaType").val('update');
		doSubmit({actionType:'save'});
	}
}
function checkSave(){
	var result = true;
if (validation.checkNull($('#FCI_CODE').val())){
	writeErrorMsg($("#FCI_CODE").attr("label")+"不能为空!");
	selectOrFocus('FCI_CODE');
	return false;
}
if (validation.checkNull($('#FCI_NAME').val())){
	writeErrorMsg($("#FCI_NAME").attr("label")+"不能为空!");
	selectOrFocus('FCI_NAME');
	return false;
}
	return result;
}
function doMoveUp(){
	doSubmit({actionType:'moveUp'});
}
function doMoveDown(){
	doSubmit({actionType:'moveDown'});
}
function doCopyCurrent(){
	doSubmit({actionType:'copyCurrent'});
}
function doDelete(){
	if (confirm('确定要进行节点删除操做吗？')){
		doSubmit({actionType:'delete'});
	}
}
function doInsertChild(){
	if (checkInsertChild()){
		$("#operaType").val('insert');
		doSubmit({actionType:'insertChild'});
	}
}
function checkInsertChild(){
	var result = true;
if (validation.checkNull($('#CHILD_FCI_CODE').val())){
	writeErrorMsg($("#CHILD_FCI_CODE").attr("label")+"不能为空!");
	selectOrFocus('CHILD_FCI_CODE');
	return false;
}
if (validation.checkNull($('#CHILD_FCI_NAME').val())){
	writeErrorMsg($("#CHILD_FCI_NAME").attr("label")+"不能为空!");
	selectOrFocus('CHILD_FCI_NAME');
	return false;
}
	return result;
}
function doCancel(){
	doRefresh($('#FCI_ID').val());
}

var fuIdBox;
function openFuIDListSelectBox(){
	var handlerId ="ManagerListSelectToChangeIt"; 
	if (!fuIdBox){
		fuIdBox = new PopupBox('fuIdBox','请选择版主信息 ',{size:'normal',width:'400',top:'2px'});
	}
	var url = 'index?'+handlerId+'&targetId=FU_ID&targetName=FU_NAME&fciId='+$("#FCI_ID").val();
	fuIdBox.sendRequest(url);
} 

function setupRefColumns(fuIds){
	var reqURL = "<%=pageBean.getHandlerURL()%>&actionType=setupRefColumns&FCI_ID=<%=pageBean.inputValue("FCI_ID")%>&newRelfuIds="+fuIds;
	sendRequest(reqURL,{onComplete:function(responseText){
		eval(responseText);
		PopupBox.closeCurrent();
	}});	
}

function delUserSecRel(){
	if (!isValid($("#fuIds").val())){
		writeErrorMsg('请先选中一条关联栏目!');
		return;
	}
	if (confirm("确定要删除该版主么？")){
		postRequest('form1',{actionType:'delUserSecRel',onComplete:function(responseText){
			eval(responseText);
		}});		
	}
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post" >
<%@include file="/jsp/inc/message.inc.jsp"%>
<table width="100%" style="margin:0px;">
<tr>
	<td valign="top">
	<div id="leftTree" class="sharp color2" style="margin-top:0px;">
	<b class="b1"></b><b class="b2"></b><b class="b3"></b><b class="b4"></b>
    <div class="content">
    <h3 class="portletTitle">&nbsp;&nbsp;分组列表</h3>        
	<div id="treeArea" style="overflow:auto; height:450px;width:230px;background-color:#F9F9F9;padding-top:5px;padding-left:5px;">
	<%=pageBean.getStringValue("menuTreeSyntax")%></div>
    <b class="b9"></b>
    </div>
	</td>
	<td width="85%" valign="top">

	<fieldset id ="currentArea" style="padding:3px;margin-top:0px;">
	  <legend style="font-weight: bolder;">编辑当前节点</legend>
	    <div id="__ToolBar__" style="margin-top: 2px">
   		<table border="0" cellpadding="0" cellspacing="1">
	    <tr>
		<td <%if(!pageBean.getBoolValue("isRootNode")){%> onmouseover="onMover(this);" onmouseout="onMout(this);" onclick="doSave()"<%}%> class="bartdx" hotKey="E" align="center"><input id="saveImgBtn" value="&nbsp;" title="保存" type="button" class="saveImgBtn" style="margin-right:0px;" />保存</td>
	    <td <%if(!pageBean.getBoolValue("isRootNode")){%> onmouseover="onMover(this);" onmouseout="onMout(this);" onclick="doDelete()"<%}%> class="bartdx" hotKey="D" align="center"><input id="delImgBtn" value="&nbsp;" title="删除" type="button" class="delImgBtn" style="margin-right:0px;" />删除</td>
		<td <%if(!pageBean.getBoolValue("isRootNode")){%> onmouseover="onMover(this);" onmouseout="onMout(this);" onclick="doCopyCurrent()"<%}%> class="bartdx" align="center"><input id="copyImgBtn" value="&nbsp;" title="复制" type="button" class="copyImgBtn" style="margin-right:0px;" />复制</td>    
	    <td <%if(!pageBean.getBoolValue("isRootNode")){%> onmouseover="onMover(this);" onmouseout="onMout(this);" onclick="showParentSelectBox()"<%}%> class="bartdx" align="center"><input id="moveImgBtn" value="&nbsp;" title="迁移" type="button" class="moveImgBtn" style="margin-right:0px;" />迁移</td>
	    <td <%if(!pageBean.getBoolValue("isRootNode")){%> onmouseover="onMover(this);" onmouseout="onMout(this);" onclick="doMoveUp()"<%}%> class="bartdx" align="center"><input id="upImgBtn" value="&nbsp;" title="上移" type="button" class="upImgBtn" style="margin-right:0px;" />上移</td>
	    <td <%if(!pageBean.getBoolValue("isRootNode")){%> onmouseover="onMover(this);" onmouseout="onMout(this);" onclick="doMoveDown()"<%}%> class="bartdx" align="center"><input id="downImgBtn" value="&nbsp;" title="下移" type="button" class="downImgBtn" style="margin-right:0px;" />下移</td>
	    </tr></table></div>    
	    <table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>版块编码</th>
	<td><input id="FCI_CODE" label="版块编码" name="FCI_CODE" type="text" value="<%=pageBean.inputValue("FCI_CODE")%>" size="24" class="text" />
</td>
	<th width="100" nowrap>版块名称</th>
	<td><input id="FCI_NAME" label="版块名称" name="FCI_NAME" type="text" value="<%=pageBean.inputValue("FCI_NAME")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>状态</th>
	<td colspan="3"><select id="FCI_STATE" label="状态" name="FCI_STATE" class="select"><%=pageBean.selectValue("FCI_STATE")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>版主</th>
	<td>
		<span style="float:left"><select name=fuIds size="8" id="fuIds" style="width:270px;" class="xselect"><%=pageBean.selectValue("fuIds")%>
		</select></span>
		<input type="hidden" id="FU_ID" label="版主" name="FU_ID" type="text" value="<%=pageBean.inputValue("FU_ID")%>" size="24" class="text" />
		<span><input name="addFuIdRef" type="button" value="添加" style="margin:2px;" onclick="openFuIDListSelectBox()" />
      	<br />
     	<input name="delFuIdRef" type="button" value="删除" style="margin:2px;" onclick="delUserSecRel()" /></span>
</td>
	<th width="100" nowrap>版块描述</th>
	<td><textarea id="FCI_MOTTO" label="版块描述" name="FCI_MOTTO" cols="40" rows="9" class="textarea"><%=pageBean.inputValue("FCI_MOTTO")%></textarea>
</td>
</tr>
	    </table>
	</fieldset>	
    
	<fieldset id="childArea" style="margin-top: 5px;padding: 5px;height: 120.5px;">
	  <legend style="font-weight: bolder;">添加子节点</legend>
	    <div id="__ToolBar__">
		<table border="0" cellpadding="0" cellspacing="1">
	    <tr>
		<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doInsertChild()"><input value="&nbsp;" title="新增" type="button" class="createImgBtn" style="margin-right:0px;" />新增</td>
		<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doCancel()"><input value="&nbsp;" title="取消" type="button" class="cancelImgBtn" style="margin-right:0px;" />取消</td>    
	    </tr></table>
	    </div>     
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>版块编码</th>
	<td><input id="CHILD_FCI_CODE" label="版块编码" name="CHILD_FCI_CODE" type="text" value="<%=pageBean.inputValue("CHILD_FCI_CODE")%>" size="24" class="text" />
</td>
	<th width="100" nowrap>版块名称</th>
	<td><input id="CHILD_FCI_NAME" label="版块名称" name="CHILD_FCI_NAME" type="text" value="<%=pageBean.inputValue("CHILD_FCI_NAME")%>" size="24" class="text" />
</td>
</tr>
<tr>
<tr>
	<th width="100" nowrap>状态</th>
	<td><select id="CHILD_FCI_STATE" label="状态" name="CHILD_FCI_STATE" class="select"><%=pageBean.selectValue("CHILD_FCI_STATE")%></select>
</td>
	<th width="100" nowrap>版块描述</th>
	<td><textarea id="CHILD_FCI_MOTTO" label="版块描述" name="CHILD_FCI_MOTTO" cols="27" rows="1" class="textarea"><%=pageBean.inputValue("CHILD_FCI_MOTTO")%></textarea>
</td>
</tr>	
</tr>
</table>
	</fieldset> 
    </td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value=""/>
<input type="hidden" id="FCI_ID" name="FCI_ID" value="<%=pageBean.inputValue("FCI_ID")%>" />
<input type="hidden" id="FCI_PID" name="FCI_PID" value="<%=pageBean.inputValue("FCI_PID")%>" />
<input type="hidden" id="FCI_SORT" name="FCI_SORT" value="<%=pageBean.inputValue("FCI_SORT")%>" />
<input type="hidden" id="fuIds" name="fuIds" value="" />
</form>
</body>
</html>
<script language="javascript">
$(function(){
	postRequest('form1',{actionType:'retrieveRefColumns',onComplete:function(responseText){
		eval(responseText);
	}});	
});
<%if(pageBean.getBoolValue("isRootNode")){%>
setImgDisabled('saveImgBtn',true);
setImgDisabled('delImgBtn',true);
setImgDisabled('copyImgBtn',true);
setImgDisabled('moveImgBtn',true);
setImgDisabled('upImgBtn',true);
setImgDisabled('downImgBtn',true);
<%}%>

$(window).load(function() {
	setTimeout(function(){
		resetTreeHeight(70);
		var areaHeight = $("#leftTree").height()/2;
		$("#currentArea").height(areaHeight+80);
		$("#childArea").height(areaHeight-78);			
	},1);	
});

</script>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
<script language="javascript">
</script>
