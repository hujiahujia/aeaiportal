<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>主题管理</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script type="text/javascript">
var _directConfigPortletBox;
function directConfigDecoratorRequest(themeId){
	if (!_directConfigPortletBox){
		_directConfigPortletBox = new PopupBox('_directConfigPortletBox','配置窗口外观',{size:'normal',height:'420px'});
	}
	var url = '/portal/index?UnifiedPagePortletConfig&themeId='+themeId;
	_directConfigPortletBox.sendRequest(url);	
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="directConfigDecoratorRequest('<%=pageBean.inputValue("THEME_ID")%>')" ><input value="&nbsp;" type="button" class="modelImgBtn" id="modelImgBtn" title="装饰设置" />装饰设置</td>   
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSubmit({actionType:'save'})"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBack();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>名称</th>
	<td><input id="THEME_NAME" label="名称" name="THEME_NAME" type="text" value="<%=pageBean.inputValue("THEME_NAME")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>类型</th>
	<td><%if ("insert".equals(pageBean.getOperaType())){%><select id="THEME_TYPE" label="类型" name="THEME_TYPE" class="select"><%=pageBean.selectValue("THEME_TYPE")%></select><%}else{%><%=pageBean.selectedText("THEME_TYPE")%><input type="hidden" id="THEME_TYPE" name="THEME_TYPE" value="<%=pageBean.selectedValue("THEME_TYPE")%>" /><%}%>
</td>
</tr>
<tr>
	<th width="100" nowrap>个性化</th>
	<td><select id="PERSONALABLE" label="个性化" name="PERSONALABLE" class="select"><%=pageBean.selectValue("PERSONALABLE")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>预设高度</th>
	<td><input id="BODY_HEIGHT" label="排序" name="BODY_HEIGHT" type="text" value="<%=pageBean.inputValue("BODY_HEIGHT")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>路径</th>
	<td><input id="THEME_PATH" label="路径" name="THEME_PATH" type="text" value="<%=pageBean.inputValue("THEME_PATH")%>" size="46" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>宽度类型</th>
	<td><select id="THEME_WIDTH_TYPE" label="宽度类型" name="THEME_WIDTH_TYPE" class="select"><%=pageBean.selectValue("THEME_WIDTH_TYPE")%></select></td>
</tr>
<tr>
	<th width="100" nowrap>主题宽度</th>
	<td><input id="THEME_WIDTH" label="主题宽度" name="THEME_WIDTH" type="text" value="<%=pageBean.inputValue("THEME_WIDTH")%>" size="46" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>留白宽度</th>
	<td><input id="THEME_MARGIN" label="留白宽度" name="THEME_MARGIN" type="text" value="<%=pageBean.inputValue("THEME_MARGIN")%>" size="46" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>其他宽度</th>
	<td><input id="THEME_VNAV_WIDTH" label="其他宽度" name="THEME_VNAV_WIDTH" type="text" value="<%=pageBean.inputValue("THEME_VNAV_WIDTH")%>" size="46" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>外围高度</th>
	<td><input id="THEME_OUTER_HEIGHT" label="外围高度" name="THEME_OUTER_HEIGHT" type="text" value="<%=pageBean.inputValue("THEME_OUTER_HEIGHT")%>" size="46" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>排序</th>
	<td><input id="THEME_SORT" label="排序" name="THEME_SORT" type="text" value="<%=pageBean.inputValue("THEME_SORT")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>描述</th>
	<td><textarea name="THEME_DESC" cols="46" rows="3"  class="textarea" id="THEME_DESC" label="描述" style="width:400px;"><%=pageBean.inputValue("THEME_DESC")%></textarea>
</td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="THEME_ID" label="THEME_ID" name="THEME_ID" value="<%=pageBean.inputValue("THEME_ID")%>" />
</form>
<script language="javascript">
requiredValidator.add("THEME_NAME");
lengthValidators[0].set(32).add("THEME_NAME");
requiredValidator.add("THEME_TYPE");
lengthValidators[1].set(32).add("THEME_TYPE");
requiredValidator.add("THEME_SORT");
intValidator.add("THEME_SORT");
requiredValidator.add("THEME_PATH");
lengthValidators[2].set(128).add("THEME_PATH");
lengthValidators[3].set(128).add("THEME_DESC");
initDetailOpertionImage();
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
