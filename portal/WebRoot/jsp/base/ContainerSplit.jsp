<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>子容器拆分</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function splitContainer(){
	var splitContainerNumber = $('#splitContainerNumber').val();
	if (validation.checkNull(splitContainerNumber)){
		alert('拆分数量不能为空！');
		return;
	}
	if (!validation.isDigit(splitContainerNumber)){
		alert('拆分数量必须为数字！');
		return;
	}	
	parent.splitContainer(splitContainerNumber);
	parent.PopupBox.closeCurrent();
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="splitContainer()"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="javascript:parent.PopupBox.closeCurrent();"><input value="&nbsp;" type="button" class="closeImgBtn" title="关闭" />关闭</td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>容器类型</th>
	<td width="80%"><%=pageBean.getStringValue("curContainerTypeName") %><input type="hidden" name="curContainerType" id="curContainerType" value="<%=pageBean.getStringValue("curContainerType")%>"/></td>
</tr>
<tr>
	<th width="100" nowrap>拆分数量</th>
	<td width="80%"><input id="splitContainerNumber" name="splitContainerNumber" type="text" value="" size="10" class="text" /></td>
</tr>
</table>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
