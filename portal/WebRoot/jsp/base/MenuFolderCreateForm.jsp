<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>菜单目录</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function saveRecord(){
	if (validate()){
		var reg = /^[0-9A-Za-z-]+$/;
		if (!reg.test($("#CHILD_MENU_CODE").val())) {
			writeErrorMsg('节点编码只能由字符、数字或者短横线组成！');
			return;
		}
		postRequest("form1",{actionType:'save',onComplete:function(responseText){
			if ('duplicate' == responseText){
				showMessage('编码重复，请检查！');
			}else{
				var newMenuId = responseText;
				parent.doRefresh(newMenuId);
			}
		}})
	}
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="saveRecord()"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
	<td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="javascript:parent.PopupBox.closeCurrent();"><input value="&nbsp;" type="button" class="closeImgBtn" title="关闭" />关闭</td>        
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0" style="margin-bottom:2px">
  <tr>
    <th width="100" nowrap="nowrap">编码</th>
    <td width="80%"><input id="CHILD_MENU_CODE" label="编码" name="CHILD_MENU_CODE" type="text" value="" size="24" class="text" /></td>
  </tr>
  <tr>
    <th width="100" nowrap="nowrap">名称</th>
    <td width="80%"><input id="CHILD_MENU_NAME" label="名称" name="CHILD_MENU_NAME" type="text" value="" size="24" class="text" /></td>
  </tr>
    <tr>
        <th width="100" nowrap>主题</th>
        <td width="80%"><select id="CHILD_MENU_THEME" label="主题" name="CHILD_MENU_THEME" class="select"><%=pageBean.selectValue("MENU_THEME")%></select></td>
    </tr>  
<tr>
    <th width="100" nowrap="nowrap">是否可见</th>
    <td width="80%"><%=pageBean.selectRadio("MENU_VISIABLE").addAlias("CHILD_MENU_VISIABLE")%></td>
  </tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" id="NAV_ID" name="NAV_ID" value="<%=pageBean.inputValue("NAV_ID")%>" />
<input type="hidden" id="MENU_ID" name="MENU_ID" value="<%=pageBean.inputValue("MENU_ID")%>" />
<input type="hidden" id="CHILD_MENU_TYPE" name="CHILD_MENU_TYPE" value="F" />
</form>
<script language="javascript">
requiredValidator.add("CHILD_MENU_NAME");
requiredValidator.add("CHILD_MENU_CODE");
requiredValidator.add("CHILD_MENU_THEME");
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
