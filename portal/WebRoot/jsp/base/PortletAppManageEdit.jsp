<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Portlet管理</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function refreshContext(){
	postRequest('form1',{actionType:'refreshContext',onComplete:function(responseText){
		eval(responseText);
	}});
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onClick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onClick="doSubmit({actionType:'save'})"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onClick="goToBack();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>标题</th>
	<td><input id="PA_TITLE" style="width:240px" label="标题" name="PA_TITLE" type="text" value="<%=pageBean.inputValue("PA_TITLE")%>" size="24" class="text" /></td>
</tr>
<tr id="contextTr">
	<th width="100" nowrap>所属应用</th>
	<td><select style="width:240px" id="PA_CONTEXT" label="所属应用" name="PA_CONTEXT" class="select" onchange="refreshContext()"><%=pageBean.selectValue("PA_CONTEXT")%></select></td>
</tr>
<tr id="nameTr">
	<th width="100" nowrap>Portlet名称</th>
	<td><select style="width:240px" id="PA_NAME" label="名称" name="PA_NAME" class="select"><%=pageBean.selectValue("PA_NAME")%></select></td>
</tr>
<tr>
	<th width="100" nowrap>分组</th>
	<td><select style="width:240px" id="PA_GROUP" label="分组" name="PA_GROUP" class="select"><%=pageBean.selectValue("PA_GROUP")%></select></td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="PA_ID" name="PA_ID" value="<%=pageBean.inputValue("PA_ID")%>" />
<br />
<br />
</form>
<script language="javascript">
requiredValidator.add("PA_GROUP");
requiredValidator.add("PA_TITLE");
requiredValidator.add("PA_TYPE");
initDetailOpertionImage();
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>