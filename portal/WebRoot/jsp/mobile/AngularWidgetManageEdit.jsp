<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>手机门户管理</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script type="text/javascript" src="js/jquery.zclip.js"></script>
<script language="javascript">
function reload(){
	postRequest('form1',{actionType:'reload',onComplete:function(responseText){
		if (responseText == 'success'){
			alert("重新加载成功！");
		}else{
			alert('加载缓存失败！！')
		}
	}});
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSubmit({actionType:'save',checkUnique:'yes'})"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="reload()"><input value="&nbsp;" type="button" class="refreshImgBtn" id="refreshImgBtn" title="重新加载" />重新加载</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="goToBack();"><input value="&nbsp;" type="button" class="backImgBtn" title="返回" />返回</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" ><a id="copyInfoId" style="display:inline-block;width:74px;height:14px;">复制信息ID</a></td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>编码</th>
	<td><input id="AW_CODE" label="编码" name="AW_CODE" type="text" value="<%=pageBean.inputValue("AW_CODE")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>名称</th>
	<td><input id="AW_NAME" label="名称" name="AW_NAME" type="text" value="<%=pageBean.inputValue("AW_NAME")%>" size="24" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>分组</th>
	<td><select style="width:213px" id="AW_GROUP" label="分组" name="AW_GROUP" class="select"><%=pageBean.selectValue("AW_GROUP")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>模板</th>
	<td><input id="AW_TEMPLATEFILE" label="模板" name="AW_TEMPLATEFILE" type="text" value="<%=pageBean.inputValue("AW_TEMPLATEFILE")%>" size="65" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>控制器</th>
	<td><input id="AW_CONTROLERFILE" label="控制器" name="AW_CONTROLERFILE" type="text" value="<%=pageBean.inputValue("AW_CONTROLERFILE")%>" size="65" class="text" />
</td>
</tr>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="AW_ID" name="AW_ID" value="<%=pageBean.inputValue4DetailOrUpdate("AW_ID","")%>" />
</form>
<script language="javascript">
initDetailOpertionImage();
requiredValidator.add("AW_CODE");
requiredValidator.add("AW_NAME");
requiredValidator.add("AW_GROUP");
$(function(){
	$("#copyInfoId").zclip({
		path:"<%=request.getContextPath()%>/js/ZeroClipboard.swf",
		copy:$("#AW_ID").val()
	});
})
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
