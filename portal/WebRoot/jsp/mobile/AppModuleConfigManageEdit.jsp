<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>手机门户管理</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script type="text/javascript" src="js/jquery.zclip.js"></script>
<script type="text/javascript">
function focusTab(tabId){
	var reqUrl = "<%=pageBean.getHandlerURL()%>&actionType=focusTab&currentTabId="+tabId;
	sendRequest(reqUrl,{onComplete:function(responseText){
		
	}});
}
function showSecurityConfig(){
	//var reqUrl = "<%=pageBean.getHandlerURL()%>&actionType=focusTab&currentTabId=security";
	//sendRequest(reqUrl,{onComplete:function(responseText){
		var url = "index?SecurityAuthorizationConfig&resourceType=AppModuleConfig&resourceId=<%=pageBean.inputValue("AC_ID")%>";
		$('#SecurityFrame').attr('src',url);
		//tab.focus(2);
	//}});
}
function doSave(){
	postRequest('form1',{'actionType':"save",onComplete:function(responseText){
		if ('success' == responseText){
			parent.window.location.reload();
		}
	}});
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div class="photobg1" id="tabHeader" style="margin-top:5px;">
<div class="newarticle1"  onclick="focusTab('base')">基本信息</div>
<%if (!pageBean.getBoolValue("isInsertOpera")){%>
<div class="newarticle1" onclick="showSecurityConfig();focusTab('security')">安全管理</div>
<%}%>
</div>
<div class="photobox newarticlebox" id="Layer0" style="height:427px;">
<div style="margin:2px">
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="enableSave()" ><input value="&nbsp;" type="button" class="editImgBtn" id="modifyImgBtn" title="编辑" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="doSave()"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="parent.PopupBox.closeCurrent();"><input value="&nbsp;" type="button" class="closeImgBtn" title="关闭" />关闭</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" ><a id="copyInfoId" style="display:inline-block;width:74px;height:14px;">复制信息ID</a></td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>名称</th>
	<td><input id="AC_NAME" label="名称" name="AC_NAME" type="text" value="<%=pageBean.inputValue("AC_NAME")%>" style="width:211px" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>分组</th>
	<td><select style="width:213px" id="AC_GROUP" label="分组" name="AC_GROUP" class="select"><%=pageBean.selectValue("AC_GROUP")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>有效性</th>
	<td><select style="width:213px" id="AC_VAILD" label="有效性" name="AC_VAILD" class="select"><%=pageBean.selectValue("AC_VAILD")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>排序</th>
	<td><input id="AC_SORT" label="排序" name="AC_SORT" type="text" value="<%=pageBean.inputValue("AC_SORT")%>" style="width:211px" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>CSS</th>
	<td><input id="AC_CSS" label="CSS" name="AC_CSS" type="text" value="<%=pageBean.inputValue("AC_CSS")%>" size="81" class="text" />
</td>
</tr>
<tr>
	<th width="100" nowrap>方法</th>
	<td>
	<textarea id="AC_METHOD" label="方法" name="AC_METHOD" cols="80" rows="3" class="text"><%=pageBean.inputValue("AC_METHOD")%></textarea>
</td>
</tr>
</table>
</div>    
</div>
<div class="photobox newarticlebox" id="Layer1" style="height:427px;display:none;overflow:hidden;">
<iframe id="SecurityFrame" src="" width="100%" height="430" frameborder="0" scrolling="no"></iframe>
</div>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<input type="hidden" id="AC_ID" name="AC_ID" value="<%=pageBean.inputValue4DetailOrUpdate("AC_ID","")%>" />
</form>
<script language="javascript">
var tab = new Tab('tab','tabHeader','Layer',0);
$(function(){
	if ("security" == "<%=pageBean.getStringValue("currentTabId")%>"){
		showSecurityConfig();
		tab.focus(1);
	}
});
initDetailOpertionImage();
requiredValidator.add("AC_NAME");
requiredValidator.add("AC_GROUP");
requiredValidator.add("AC_SORT");
numValidator.add("AC_SORT");
$(function(){
	$("#copyInfoId").zclip({
		path:"<%=request.getContextPath()%>/js/ZeroClipboard.swf",
		copy:$("#AC_ID").val()
	});
})
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
