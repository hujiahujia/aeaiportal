<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>手机门户管理</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function reloadAll(){
	postRequest('form1',{actionType:'reloadAll',onComplete:function(responseText){
		if (responseText == 'success'){
			alert("重新加载成功！");
		}else{
			alert('加载缓存失败！！')
		}
	}});
}
var appModuleConfigBox;
function appModuleConfigRequest(operaType){
	var acId = $("#AC_ID").val();
 	if ("insert" != operaType && acId == ""){
		writeErrorMsg('请先选中一条记录!');
		return;
	}	
	
	if (!appModuleConfigBox){
		appModuleConfigBox = new PopupBox('appModuleConfigBox','应用模块信息',{size:'normal',width:'900px',height:'490px',top:'2px'});
	}
	var url = 'index?AppModuleConfigManageEdit&operaType='+operaType+'&AC_ID='+acId;
	appModuleConfigBox.sendRequest(url);	
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ToolBar__">
<table class="toolTable" border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="A" align="center" onclick="appModuleConfigRequest('insert')"><input value="&nbsp;" title="新增" type="button" class="createImgBtn" />新增</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="E" align="center" onclick="appModuleConfigRequest('update')"><input value="&nbsp;" title="编辑" type="button" class="editImgBtn" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="C" align="center" onclick="appModuleConfigRequest('copy')"><input value="&nbsp;" title="复制" type="button" class="copyImgBtn" />复制</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="V" align="center" onclick="appModuleConfigRequest('detail')"><input value="&nbsp;" title="查看" type="button" class="detailImgBtn" />查看</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="reloadAll()"><input value="&nbsp;" type="button" class="refreshImgBtn" id="refreshImgBtn" title="重新加载" />重新加载</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="D" align="center" onclick="doDelete($('#'+rsIdTagId).val());"><input value="&nbsp;" title="删除" type="button" class="delImgBtn" />删除</td>
</tr>
</table>
</div>
<div id="__ParamBar__">
<table class="queryTable"><tr><td>
&nbsp;标识&nbsp;<input id="acId" label="标识" name="acId" type="text" value="<%=pageBean.inputValue("acId")%>" size="30" class="text" />
&nbsp;名称&nbsp;<input id="acName" label="名称" name="acName" type="text" value="<%=pageBean.inputValue("acName")%>" size="20" class="text" />
&nbsp;分组&nbsp;<select id="acGroup" label="分组" name="acGroup" class="select" onchange="doQuery()"><%=pageBean.selectValue("acGroup")%></select>
&nbsp;<input type="button" name="button" id="button" value="查询" class="formbutton" onclick="doQuery()" />
</td></tr></table>
</div>
<ec:table 
form="form1"
var="row"
items="pageBean.rsList" csvFileName="手机门户管理.csv"
retrieveRowsCallback="process" xlsFileName="手机门户管理.xls"
useAjax="true" sortable="true"
doPreload="false" toolbarContent="navigation|pagejump |pagesize |export|extend|status"
width="100%" rowsDisplayed="15"
listWidth="100%" 
height="390px"
>
<ec:row styleClass="odd" ondblclick="clearSelection();appModuleConfigRequest('detail')" oncontextmenu="selectRow(this,{AC_ID:'${row.AC_ID}'});refreshConextmenu()" onclick="selectRow(this,{AC_ID:'${row.AC_ID}'})">
	<ec:column width="50" style="text-align:center" property="_0" title="序号" value="${GLOBALROWCOUNT}" />
	<ec:column width="100" property="AC_NAME" title="名称"   />
	<ec:column width="70" property="AC_GROUP" title="分组" mappingItem="AC_GROUP"  />
	<ec:column width="50" property="AC_VAILD" title="有效" mappingItem="AC_VAILD"  />
	<ec:column width="145" property="AC_CSS" title="CSS"   />
	<ec:column width="145" property="AC_METHOD" title="方法"   />
	<ec:column width="50" property="AC_SORT" title="排序"   />
</ec:row>
</ec:table>
<input type="hidden" name="AC_ID" id="AC_ID" value="" />
<input type="hidden" name="actionType" id="actionType" />
<script language="JavaScript">
setRsIdTag('AC_ID');
var ectableMenu = new EctableMenu('contextMenu','ec_table');
</script>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
