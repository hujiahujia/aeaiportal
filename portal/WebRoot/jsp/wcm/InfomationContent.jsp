<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://ckeditor.com" prefix="ck" %>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<% 
String path = request.getContextPath();
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>信息编辑</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<link rel="stylesheet" href="<%=path%>/aeditors/kdeditor/themes/default/default.css" type="text/css" />
<link rel="stylesheet" href="<%=path%>/aeditors/kdeditor/extends.css" type="text/css" />
<script type="text/javascript" src="js/jquery.zclip.js"></script>
<script type="text/javascript" src="<%=path%>/aeditors/kdeditor/kindeditor.js"></script>
<script type="text/javascript" src="<%=path%>/aeditors/kdeditor/extends.js"></script>
<script type="text/javascript" src="<%=path%>/aeditors/kdeditor/lang/zh_CN.js"></script>
</head>
<body style="margin:0;padding:0;">
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<style type="text/css">
.add{ 
color:red;

}
</style>
<script type="text/javascript">
$(document).ready(function() {
    $("#li_pc").click(function(){
		$(this).addClass("add").siblings().removeClass("add");
		$("#div_mobile").hide();
		$("#div_pc").show();
	});
    $("#li_mobile").click(function(){
		$(this).addClass("add").siblings().removeClass("add");
		$("#div_pc").hide();
		$("#div_mobile").show();
	});
});
</script>
<div ondblclick="javascript:parent.qBox.Close();">
<ul style="height:5px;margin-top:0px;width:200px;display:inline-block;list-style: none;">
<li id="li_pc" style="float: left;width: 25%;cursor: pointer;text-align:center;" class="add" onclick="changeEditer('editorPC')"><span>电脑版</span></li>
<li id="li_mobile" style="float: left;width: 25%;cursor: pointer;text-align:center;" class="" onclick="changeEditer('editorMobile')"><span>移动版</span></li>
</ul>
<a id="copyInfoId" style="display:block;width:70px;height:17px;float:right;">复制信息ID</a>
</div>
<div id="div_pc">
<textarea name="content" id="content" cols="120" rows="5" style="height:561px;">
<%=pageBean.inputValue("content")%>
</textarea>
</div>
<div id="div_mobile" style="display:none;">
<textarea name="mobileContent" id="mobileContent" cols="120" rows="5" style="height:561px;">
<%=pageBean.inputValue("mobileContent")%>
</textarea>
</div>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="editMode" id="editMode" value="<%=pageBean.inputValue("editMode")%>" />
<input type="hidden" name="userCode" id="userCode" value="<%=pageBean.inputValue("userCode")%>" />
<input type="hidden" name="columnId" id="columnId" value="<%=pageBean.inputValue("columnId")%>"/>
<input type="hidden" name="infomationId" id="infomationId" value="<%=pageBean.inputValue("infomationId")%>"/>
</form>
</body>
</html>
<script language="javascript">
var editor;
var editorPC;
var editorMobile;

function changeEditer(editerMark){
	if(editerMark == 'editorPC'){
		editor =  editorPC;
	}else{
		editor = editorMobile;
	}
}

KindEditor.ready(function(K) {
	editorPC = K.create('textarea[name="content"]', {
		allowFileManager : true,
		filterMode: false,
		width : '998px',
		items : [
				<%if("draft".equals(pageBean.getAttribute("state"))){%>
				'cmsave',<%}%>'source', 'undo', 'redo', '|', 'preview','template', 'code', 'cut', 'copy', 'paste',
		         'plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 'justifyright',
		         'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript',
		         'superscript', 'clearhtml', 'quickformat', 'selectall', '|', 'fullscreen', '/',
		         'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold',
		         'italic', 'underline', 'strikethrough', 'lineheight', 'removeformat', '|', 'cmuploadimage','cmviewimage',
		          'table', 'hr', 'emoticons',
		         'anchor', 'link', 'unlink', '|', 'cmprofile'
		 		]
	});
	$("#copyInfoId").zclip({
		path:"<%=request.getContextPath()%>/js/ZeroClipboard.swf",
		copy:$("#infomationId").val()
	});
	editor =  editorPC;
});

KindEditor.ready(function(K) {
	editorMobile = K.create('textarea[name="mobileContent"]', {
		allowFileManager : true,
		filterMode: false,
		width : '998px',
		items : [
				<%if("draft".equals(pageBean.getAttribute("state"))){%>
				'cmsave',<%}%>'source', 'undo', 'redo', '|', 'preview','template', 'code', 'cut', 'copy', 'paste',
		         'plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 'justifyright',
		         'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript',
		         'superscript', 'clearhtml', 'quickformat', 'selectall', '|', 'fullscreen', '/',
		         'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold',
		         'italic', 'underline', 'strikethrough', 'lineheight', 'removeformat', '|', 'cmuploadimage','cmviewimage',
		          'table', 'hr', 'emoticons',
		         'anchor', 'link', 'unlink', '|', 'cmprofile'
		 		]
	});
	$("#copyInfoId").zclip({
		path:"<%=request.getContextPath()%>/js/ZeroClipboard.swf",
		copy:$("#infomationId").val()
	});
});

function afterSelectImage(uploadeImgObj){
	var url = uploadeImgObj.url;
    var width = uploadeImgObj.width;
    var height = uploadeImgObj.height;
    var title = ""; 
    var border = "0";
    var align = null;
    editor.exec('insertimage', url, title, width, height, border, align);
}

var uploadImageRequestBox;
function addUploadImage(){
	var title = "上载图片";
	if (!uploadImageRequestBox){
		uploadImageRequestBox = new PopupBox('uploadImageRequestBox',title,{size:'big',width:'260px',height:'400px',top:'3px'});
	}
	var columnIdValue = $("#columnId").val();
    var url = "/portal/index?ImageColumnSelect8Upload"; 
    uploadImageRequestBox.sendRequest(url);	
}

var browseImageRequestBox;
function addBrowseImage(){
	var title = "浏览图片";
	if (!browseImageRequestBox){
		browseImageRequestBox = new PopupBox('browseImageRequestBox',title,{size:'big',width:'990px',height:'525px',top:'3px'});
	}
	var columnIdValue = $("#columnId").val();
    var url = "/portal/index?PictureBrowser"; 
    browseImageRequestBox.sendRequest(url);	
}

</script>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
