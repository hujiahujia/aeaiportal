<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title></title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function closeImageViewer(){
	parent.qBox.Close();
}
</script>
</head>
<body>
<div>
<div style="text-align: center;height: 20px;">链接地址：<a href="<%=pageBean.getAttribute("imgSrc")%>" target="_blank"><%=pageBean.getAttribute("imgSrc")%></a></div>
<div title="双击图片关闭窗口" style="overflow: auto;height:580px;line-height:580px;width:980px;text-align: center;">
<img src="<%=pageBean.getAttribute("imgSrc")%>" ondblclick="closeImageViewer()" />
</div>
</div>
</body>
</html>
