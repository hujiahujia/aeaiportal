<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<portlet:actionURL name="saveConfig" var="saveConfigURL"></portlet:actionURL>
<portlet:renderURL portletMode="edit" var="editURL"></portlet:renderURL>
<%
String isContent = (String)request.getAttribute("isContent");
String isCache = (String)request.getAttribute("isCache");
%>
<form id="<portlet:namespace/>Form">
  <table width="90%" border="1">
<tr>
      <td width="120">是否内容正文</td>
      <td>
        <input data-role="none" type="radio" name="isContent" id="isContentY" value="Y" onclick="setCacheSetting()" />是&nbsp;&nbsp;
        <input data-role="none" type="radio" name="isContent" id="isContentN" value="N" />否</td>
    </tr>      
    <tr>
      <td width="120">内容正文ID</td>
      <td>
        <input data-role="none" name="contentId" type="text" id="contentId" value="${contentId}" size="60" /></td>
    </tr>           
    <tr>
      <td width="120">数据URL</td>
      <td>
        <input data-role="none" name="dataURL" type="text" id="dataURL" value="${dataURL}" size="60" /></td>
    </tr>
     <tr>
      <td width="120">默认值</td>
      <td>
        <input data-role="none"name="defaultVariableValues" type="text" id="defaultVariableValues" value="${defaultVariableValues}" size="60" />      </td>
    </tr>
    <tr>
      <td width="120">是否缓存</td>
      <td>
        <input data-role="none" type="radio" name="isCache" id="isCacheY" value="Y" />
        是
      &nbsp;&nbsp;
        <input data-role="none" type="radio" name="isCache" id="isCacheN" value="N" />
      否</td>
    </tr> 
    <tr>
      <td width="120">缓存时间</td>
      <td>
        <input data-role="none" type="text" name="cacheMinutes" id="cacheMinutes" value="${cacheMinutes}" />
        分钟</td>
    </tr>     
	<tr>
      <td colspan="2" align="center">
        <input type="button" data-role="none" name="button" id="button" value="保存" onclick="submitAction('${saveConfigURL}',{formId:'<portlet:namespace/>Form'})" /> &nbsp;
        <input type="button" data-role="none" name="button2" id="button2" value="取消" onclick="fireAction('${editURL}')"/></td>
    </tr>
  </table>
</form>
<script language="javascript">
<%if ("Y".equals(isContent)){%>
	$('#<portlet:namespace/>Form #isContentY').attr("checked","checked");
<%}else{%>
	$('#<portlet:namespace/>Form #isContentN').attr("checked","checked");
<%}%>

<%if ("Y".equals(isCache)){%>
	$('#<portlet:namespace/>Form #isCacheY').attr("checked","checked");
<%}else{%>
	$('#<portlet:namespace/>Form #isCacheN').attr("checked","checked");
<%}%>
function setCacheSetting(){
	if ($("input:radio[name='isContent']:checked").val() == 'Y'){
		$("input:radio[name='isCache'][value='Y']").attr("checked",'checked');
		$("#cacheMinutes").val("0");
	}
}
$(document).ready(function(){
	var idParam = $('#<portlet:namespace/>Form #contentId').attr('value');
	var url = '/portal_portlets/resource?TitleObtain&actionType=getTitle&idParam=' + idParam + '&type=content';
	sendRequest(url,{dataType:'text',onComplete:function(responseText){
		$('#<portlet:namespace/>Form #contentId').qtip( 
		{ 
			content: {
				text: responseText
			},
			style: {
				classes:'qtip-youtube'
			}
		});
	}});
});
</script>