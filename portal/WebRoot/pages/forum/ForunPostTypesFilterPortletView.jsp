<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.List"%>
<portlet:defineObjects/>
<portlet:resourceURL id="getAjaxData" var="getAjaxDataURL">
</portlet:resourceURL>
<%
String portletId = (String)request.getAttribute("portletId");
List<HashMap> records1 = (List<HashMap>)request.getAttribute("reocrds");
%>
<style type="text/css">
.main_div{
	height:30px;
	width:100%;
	line-height:30px;
	margin-left:-10px;
}
.btn_li{
	display:inline;
	margin-left: 20px;
}
</style>
<script type="text/javascript">
$(function(){    
	 var href = parsePageId();
	 var spilt = href.indexOf("?");
	 var startIndex = href.indexOf("code=");
	 var endIndex = href.indexOf("&typeId");
	 var code = '';
	 if(spilt>-1){
    	 href = href.substring(startIndex,endIndex);
    	 code = href.substring(5);
    	 $("#"+code).attr("style","color:white;background-color:#F60;border: solid 1px #F60;"); 
    }else{
    	$("#ALL").attr("style","color:white;background-color:#F60;border: solid 1px #F60;"); 
    }
});
function doPostFilter(code,typeId){
    var href = parsePageId();
    var spilt = href.indexOf("?");
    if(spilt>-1){
    	 href = href.substring(3,spilt);
    }else{
    	href = href.substring(3);
    }
    window.location.href=href+"?code="+code+"&typeId="+typeId;
}
</script>
<div class="main_div">
		<%
		for (int i=0;i < records1.size(); i++){ 
			HashMap dataRow =  (HashMap)records1.get(i);
		%>
		<li class="btn_li">
			<a id="<%=dataRow.get("code")%>" href="javascript:void(0)" class="button-link skyblue btnwidth" onclick="doPostFilter('<%=dataRow.get("code")%>','<%=dataRow.get("typeId")%>')" ><%=dataRow.get("name") %></a>
		</li>
        <%}%>
 	<input type="hidden" id="code" name="code" value="" />
</div>