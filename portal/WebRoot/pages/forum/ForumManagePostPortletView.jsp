<%@page import="com.agileai.hotweb.domain.PageBean"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<portlet:resourceURL id="load" var="loadURL"></portlet:resourceURL>
<%
String showManageBtn = (String)request.getAttribute("showManageBtn");
String showDelBtn = (String)request.getAttribute("showDelBtn");
String isGuestId = (String)request.getAttribute("isGuestId");
String hasEndBtn = (String)request.getAttribute("hasEndBtn");
String hasReport = (String)request.getAttribute("hasReport");
String portletId = (String)request.getAttribute("portletId");
%>

<style>
.commentTitle{
	margin: 2px;
    padding-left: 10px;
    background-color: #F5F5F5;
    height: 30px;
    font-size: 16px;
    font-weight: bold;
    height: 30px;
    line-height: 30px;
}
.title{
    width: 300px;
    display: inline-block;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
}
.commentlist{
    margin:1px;
    padding: 2px;
}
.timeTd{
    text-align:center;
    border-right-style: groove;
    border-right-width:1px;    
}
.floorTd{
    text-align:right;
    margin-left:10px;
}
.userTd{
    border-right-style: groove;
    border-right-width:1px;
    text-align: center;
    vertical-align: initial;
}
#separate{
    border-bottom-style: groove;
    border-bottom-width:1px;
}
.poststyle_ORDINARY_POSTS {
	vertical-align: super;
    display: inline-block;
    height: 16px;    
    background-image: url("/portal/themes/in/forum_default/images/ordinary.png");
   	background-size: 32px 16px;
    background-repeat: no-repeat;
    padding-left: 40px;
    background-position-y: -2px;
}
.poststyle_KEY_POSTS{
	vertical-align: super;
    display: inline-block;
    height: 16px;
    background-image: url("/portal/themes/in/forum_default/images/key.png");
    background-size: 32px 16px;
    background-repeat: no-repeat;
    padding-left: 40px;
    background-position-y: -2px;
}
.poststyle_ESSENCE_POSTS{
	vertical-align: super;
    display: inline-block;
    height: 16px;
    background-image: url("/portal/themes/in/forum_default/images/essence.png");
    background-size: 32px 16px;
    background-repeat: no-repeat;
    padding-left: 40px;
    background-position-y: -2px;
}
.poststyle_GARBAGE_POSTS{
	vertical-align: super;
    display: inline-block;
    height: 16px;
    background-image: url("/portal/themes/in/forum_default/images/garbage.png");
    background-size: 32px 16px;
    background-repeat: no-repeat;
    padding-left: 40px;
    background-position-y: -2px;
}
.spanbgcolor{
	height: 30px;
	line-height:30px;
}
</style>
<script type="text/javascript">
function scrollWindow(replySource) {
	$ ("#replySource").val (replySource);
	$ ("#replyFloorShow").html ("楼主");
	window.scrollTo(0,document.body.scrollHeight)
}

function load(){
	$ ("#replyFloorShow").html ("楼主");
	postAction('${loadURL}',{formId:'<portlet:namespace/>Form',onComplete:function(responeText){
		if (responeText == 'success'){
			var fpmId = $ ("#FPM_ID").val ();
			window.location.href = "modile-page.ptml?contentId="+fpmId;
		}
	}});
}

function saveCollection(){
	var fpmId = $ ("#FPM_ID").val ();
	var url = '/portal/resource?ForumProvider&actionType=saveCollection&fpmId='+fpmId;
	sendRequest(url,{actionType:'saveCollection',onComplete:function(responeText){
		if (responeText == 'success'){
			alert("关注成功！！");
		}else{
			alert("帖子已经被关注!!");
		}
	}});
}

function deleForumPostMessage(){
	var confirmMsg="您确认要是删除数据吗？";
	if(confirm(confirmMsg)) {
		var fpmId = $ ("#FPM_ID").val ();
		var url = '/portal/resource?ForumProvider&actionType=deleForumPostMessage&fpmId='+fpmId;
		sendRequest(url,{actionType:'deleForumPostMessage',onComplete:function(responeText){
			if (responeText != 'fail'){
				alert("成功刪除帖子！！");
				window.location.href = responeText+".ptml";
			}else{
				alert("帖子已经包含回复信息,不能刪除!!");
			}
		}});
	}
}

function deleForumPostRepliesInfos(){
	var fpmId = $ ("#FPM_ID").val ();
	var confirmMsg="您确认要是删除数据吗？";
	if(confirm(confirmMsg)) {
		var url = '/portal/resource?ForumProvider&actionType=deleForumPostRepliesInfos&fpmId='+fpmId;
		sendRequest(url,{actionType:'deleForumPostRepliesInfos',onComplete:function(responeText){
			if (responeText != 'fail'){
				alert("成功刪除帖子以及回复信息！！");
				window.location.href = responeText+".ptml";
			}else{
				alert("删除失败请检查!!");
			}
		}});
	}
}


function changeEndPost(){
	var fpmId = $ ("#FPM_ID").val ();
	var url = '/portal/resource?ForumProvider&actionType=changeEndPost&fpmId='+fpmId;
	sendRequest(url,{actionType:'changeEndPost',onComplete:function(responeText){
		if (responeText == 'success'){
			alert("帖子已完结！！");
			location.replace(location) 
		}else{
			alert("悬赏分数未散尽，不能结帖!!");
		}
	}});
}

function reportPost(){
	var fpmId = $ ("#FPM_ID").val ();
	var url = '/portal/resource?ForumProvider&actionType=reportPost&fpmId='+fpmId;
	sendRequest(url,{actionType:'reportPost',onComplete:function(responeText){
		if (responeText == 'success'){
			alert("确认举报成功,请在垃圾帖中查看！！");
			location.replace(location) 
		}else{
			alert("确认举报未成功，请检查操作!!");
		}
	}});
}

function cancelReportPost(){
	var fpmId = $ ("#FPM_ID").val ();
	var url = '/portal/resource?ForumProvider&actionType=cancelReportPost&fpmId='+fpmId;
	sendRequest(url,{actionType:'cancelReportPost',onComplete:function(responeText){
		if (responeText == 'success'){
			alert("取消举报成功！！");
			location.replace(location) 
		}else{
			alert("取消举报未成功，请检查操作!!");
		}
	}});
}

var reportPostForumBox;
function openReportPostForumBox(){
	var fpmTitle = $('#postfpmTitle').val();
	var forumCurrentPostId = $('#FPM_ID').val();
	var title = "举报信息";
	var top = $('#reportPost').position().top;
	if (!reportPostForumBox){
		reportPostForumBox = new PopupBox('reportPostForumBox',title,{size:'big',width:'460px',height:'260px',top:top});
	}
	var url = '/portal/resource?ForumReportPostInfo&FPFR_ID='+$('#FPM_ID').val()+'&frrResType=POSTMESSAGE'+'&fpmTitle='+fpmTitle+'&forumCurrentPostId='+forumCurrentPostId;
	reportPostForumBox.sendRequest(url);	
}


var y;
function positionBody(event){
	var scrollTop = document.body.scrollTop;
	y =  scrollTop + 10;
	openEditPostForumBox(y);
}

var editPostForumBox;
function openEditPostForumBox(y){
	var title = "编辑信息";
	var top = y;
	$('#editPostForumBoxDiv').css("top",top);
	var fpmContent = $('#fpmContent').val();
	if (!editPostForumBox){
		editPostForumBox = new PopupBox('editPostForumBox',title,{size:'big',width:'900px',height:'550px',top:top});
	}
	var url = '/portal/resource?ForumEditPostInfo&FPM_ID='+$('#FPM_ID').val()+'&fpmContent='+fpmContent;
	editPostForumBox.sendRequest(url);	
}

var disposalPostForumBox;
function openDisposalPostForumBox(){
	var title = "处置信息";
	var top = $('#disposalPost').position().top;
	var fpmTitle = $('#postfpmTitle').val();
	var fpmContent = $('#fpmContent').val();
	if (!disposalPostForumBox){
		disposalPostForumBox = new PopupBox('disposalPostForumBox',title,{size:'big',width:'460px',height:'260px',top:top});
	}
	var url = '/portal/resource?ForumDisposalPostInfo&FPM_ID='+$('#FPM_ID').val()+'&fpmTitle='+fpmTitle+'&url='+window.location.href;
	disposalPostForumBox.sendRequest(url);	
}

function refreshCurrentPage(){
	document.location.reload(); 
}
</script>
<body onload="load()">
<form id="<portlet:namespace/>Form">
 <div class='commentlist'>
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
	        <td width="19%" class="timeTd"><span style="float: left;font-size: 15px;">查看:${fpmClickNumber}</span>
	        <span style="float: right;font-size: 15px;">回复:${count}&nbsp;&nbsp;</span></td>
	        <td width="81%" class="commentTitle"><span class="poststyle_${fpmTypeCode}"></span><span class="title" id="fpmTitle" title="${fpmTitle}" onmouseover="this.style.cursor='pointer'">${fpmTitle}</span>
	        <span style="float: right;">
	        <% if("hasReport".equals(hasReport)){%>&nbsp;待确认举报&nbsp;<%} %>
	        <% if("hasConfirmReport".equals(hasReport)){%>&nbsp;已举报&nbsp;<%} %>
	       	 	 分类:&nbsp;${fpmTypeName}&nbsp;&nbsp;类型:&nbsp;${fpmExpression}&nbsp;&nbsp;悬赏积分:&nbsp;${fpmRewardPoints}</span>
	        </td>
        </tr>
        <tr>
        	<td width="19%" class="timeTd">&nbsp;&nbsp;</td>
        	<td width="81%"><span style="float: left;font-size: 15px;padding-left: 10px;">发布于: &nbsp;&nbsp;${fpmCreateTime}</span></td>
        </tr>
        <tr>
	        <td class='userTd' style="font-size: 15px;">
	        <img src='${imgUrl}' height="64" width="64"><br/>昵称:&nbsp;${fuName}<br/> 等级:&nbsp;${fuLevel }
	        </td>
	        <td style="padding-left: 10px;font-size: 15px;">${fpmContent}
	        </td>
         </tr>
         <% if(!"disHasReport".equals(hasReport)&&"showManageBtn".equals(showManageBtn)){%>
        <tr>
       		 <td width="19%" class="timeTd">&nbsp;&nbsp;</td>
	         <td style="padding-left: 10px;font-size: 15px;">举报信息：<br/>${forumReplrtRecords}
	        </td>
        </tr>
        <%} %>
        <tr>
        	<td class="timeTd">&nbsp;&nbsp;</td>
        	<td style="float: right;font-size: 15px;">
        <% if(!"isGuestId".equals(isGuestId)){%>
			<% if("disHasReport".equals(hasReport)){%> 
        	<span id="reportPost" class ="spanbgcolor" onmouseover="this.style.cursor='pointer'">&nbsp;<a style="font-size:15px;" onclick="openReportPostForumBox()">举报</a></span>
        	<%} %>
        	<% if("disHasEndBtn".equals(hasEndBtn)){ %>
        		<span class ="spanbgcolor" onmouseover="this.style.cursor='pointer'">&nbsp;<a style="font-size:15px;" onclick="scrollWindow('POST_MESSAGE')">回复</a></span>
        	<%} %>
        	<% if("disHasEndBtn".equals(hasEndBtn) && "disHasReport".equals(hasReport)){%>
        		<% if("showDelBtn".equals(showDelBtn)){%>
		        <span class ="spanbgcolor" onmouseover="this.style.cursor='pointer'">&nbsp;<a style="font-size:15px;" onclick="changeEndPost()">结帖</a></span>
	        	<span class ="spanbgcolor" onmouseover="this.style.cursor='pointer'">&nbsp;<a style="font-size:15px;" onclick="deleForumPostMessage()">刪除</a></span>
	        	<%} %>
	        <%} %>
	        <% if("hasEndBtn".equals(hasEndBtn)){%>
				<span style="height: 30px;line-height:30px;">&nbsp;<a style="font-size:15px;" onclick=""></a>已结帖</span>
			<%} %>
			<span class ="spanbgcolor" onmouseover="this.style.cursor='pointer'">&nbsp;<a style="font-size:15px;" onclick="saveCollection()">关注</a></span>
			<% if("showManageBtn".equals(showManageBtn)){%>
				<span class ="spanbgcolor" onmouseover="this.style.cursor='pointer'">&nbsp;<a style="font-size:15px;" onclick="positionBody(event)">编辑</a></span>
				<% if("hasReport".equals(hasReport)){%>
				<span class ="spanbgcolor" onmouseover="this.style.cursor='pointer'">&nbsp;<a style="font-size:15px;" onclick="reportPost()">确认举报</a></span>
				<span class ="spanbgcolor" onmouseover="this.style.cursor='pointer'">&nbsp;<a style="font-size:15px;" onclick="cancelReportPost()">取消举报</a></span>
				<%} %>
				<span id="disposalPost" class ="spanbgcolor" onmouseover="this.style.cursor='pointer'">&nbsp;<a style="font-size:15px;" onclick="openDisposalPostForumBox()">处置</a></span>
				<span class ="spanbgcolor" onmouseover="this.style.cursor='pointer'">&nbsp;<a style="font-size:15px;" onclick="deleForumPostRepliesInfos()">强制刪除</a></span>
			<%} %>
		<%} %>
        </td>
        </tr>
    </table>
  </div>
<input type="hidden" name="USER_CODE" id="USER_CODE" value="${currentUserId }"/>
<input type="hidden" name="FPM_ID" id="FPM_ID" value="${fpmId }"/>
<input type="hidden" name="replySource" id="replySource" value=""/>
<input type="hidden" name="postfpmTitle" id="postfpmTitle" value="${fpmTitle }"/>
</form>
</body>
<script type="text/javascript">
$(document).ready(function(){ 
    document.title=$('#postfpmTitle').val();
});
</script>
