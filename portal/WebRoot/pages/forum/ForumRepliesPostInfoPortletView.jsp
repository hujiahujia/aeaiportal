<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/aeditors/kdeditor/themes/default/default.css" type="text/css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/aeditors/kdeditor/extends.css" type="text/css" />
<script type="text/javascript" src="<%=request.getContextPath()%>/aeditors/kdeditor/kindeditor-min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/aeditors/kdeditor/extends.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/aeditors/kdeditor/lang/zh_CN.js"></script>
<portlet:defineObjects/>
<portlet:resourceURL id="saveReview" var="saveReviewURL"></portlet:resourceURL>
<%
String hasEndBtn = (String)request.getAttribute("hasEndBtn");
String isGuestId = (String)request.getAttribute("isGuestId");
String portletId = (String)request.getAttribute("portletId");
%>
<style>
.postrequired{
	border: solid 0px #CCC;
	background-color: #FFFFFF;
	margin: 0px;
	width:auto;
	float: left;
	width:99%;
	height:200px;
}
#button_publish{
	height: 30px;
	width: 80px;
	font-size: 15px;
	font-weight: bolder;
}
#rewtable{
	padding:0px;
	margin: 0px;
}
</style>
<script type="text/javascript">

var addforumPostImgBox;
function openAddResourceRequestBox(){
	var title = "上传图片";
	var top = $('#replyFpmTitle').position().top;
	if (!addforumPostImgBox){
		addforumPostImgBox = new PopupBox('addforumPostImgBox',title,{size:'big',width:'260px',height:'260px',top:top});
	}
	var url = '/portal/resource?ForumReplyImgResouceUploader&BIZ_ID='+$('#FPM_ID').val();
	addforumPostImgBox.sendRequest(url);	
}
function refreshPage(){
	addforumPostImgBox.closeBox();
	document.location.reload(); 
}

function checkReplyInfo(){
	//手动更新editor字段
	editor.sync();
	var friContent = editor.text();
	if(friContent != ''){
		saveReplyInfo();
	}else{
		alert("回复内容不能为空，请检查!!");
	}
	
}
function saveReplyInfo(){
	var replySource = $ ("#replySource").val ();
	var fpmId = $ ("#FPM_ID").val ();
	var floor = $ ("#floor").val ();
	var replyFloor = $ ("#replyFloor").val (floor);
	var replySourceInfo = $ ("#replySourceInfo").val (replySource);
	if("" == replySource){
		$ ("#replySourceInfo").val ("POST_MESSAGE");
	}
	$("#RepliesUrl").val(window.location.href);
	 //手动更新editor字段
	editor.sync();
	postRequest('<portlet:namespace/>Form',{actionType:'saveReplyInfo',onComplete:function(responseText){
		if (responseText == 'success'){
			$ ("textarea").val ('');//清空textarea
			recordtotal = recordtotal+1;
			doAjaxRefreshPortalPage('recordtotal:'+recordtotal); 
			window.location.href = "forum-info-page.ptml?contentId="+fpmId;
		}else{
			alert("回复失败请检查操作!!");
		}
	}});	
}
</script>
<form action="/portal/resource?ForumProvider" id="<portlet:namespace/>Form" name="<portlet:namespace/>Form" method="post">
<div>
<table width="100%" id="rewtable">
<tr>
 	<td colspan="2" height="40px">
 	 	<span id="replyFpmTitle"><a style="font-size:15px;">&nbsp;&nbsp;标题:&nbsp;&nbsp;${fpmTitle}</a></span>
 	 	<span><a style="font-size:15px;">&nbsp;&nbsp;回复:&nbsp;&nbsp;</a><label style="font-size:15px;" id="replyFloorShow"/></span>
 	</td>
 </tr>
  <tr>
	<td height="100px" align="center">
  	  <textarea class="postrequired" name="FRI_CONTENT" id="FRI_CONTENT" rows="10" cols="70"></textarea>
	</td>
  </tr>
  <tr>
	<td align="right">
  	<span><input type="button" id="button_publish" value="回   复 "
  	<% if("isGuestId".equals(isGuestId) || "hasEndBtn".equals(hasEndBtn)){%>	  
  	  disabled="disabled"
  	<%} %>
  	   onclick="checkReplyInfo()"/></span>
	</td>
  </tr>
</table>
</div>
<input type="hidden" name="replySourceInfo" id="replySourceInfo" value=""/>
<input type="hidden" name="replyFloor" id="replyFloor" value=""/>
<input type="hidden" name="replyPostTitle" id="replyPostTitle" value="${fpmTitle}"/>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="FU_CODE" id="FU_CODE" value="${currentUserId }"/>
<input type="hidden" name="FPM_ID" id="FPM_ID" value="${fpmId }"/>
<input type="hidden" name="RepliesUrl" id="RepliesUrl"/>
</form>

<script type="text/javascript">
var editor;
KindEditor.ready(function(K) {
	editor = K.create('textarea[name="FRI_CONTENT"]', {
		resizeType : 1,
		height:"300px",
		allowPreviewEmoticons : false,
		allowImageUpload : false,
		items : [
			'fontsize', '|', 'bold','removeformat', '|', 'justifyleft', 'justifycenter', 'insertorderedlist',
			'insertunorderedlist', '|', 'emoticons', 'forumimage', 'link']
	});
});

function afterSelectImage(uploadeImgObj){
	var url = uploadeImgObj.url;
    var width = uploadeImgObj.width;
    var height = uploadeImgObj.height;
    var title = ""; 
    var border = "0";
    var align = null;
    editor.exec('insertimage', url, title, width, height, border, align);
}

</script>
