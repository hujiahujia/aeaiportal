<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<portlet:renderURL portletMode="edit" var="editURL"></portlet:renderURL>
<form id="<portlet:namespace/>Form">
</form>
<script language="javascript">
</script>