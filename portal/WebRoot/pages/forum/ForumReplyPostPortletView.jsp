<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<portlet:resourceURL id="getAjaxData" var="getAjaxDataURL"></portlet:resourceURL>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/pagination.css" type="text/css">
<script src="<%=request.getContextPath()%>/js/jquery.pagination.js" type="text/javascript" language="javascript"></script>
<%
	String showManageBtn = (String)request.getAttribute("showManageBtn");
	String portletId = (String)request.getAttribute("portletId");
	String replyIds = (String)request.getAttribute("replyIds");
%>
<script type="text/javascript">
function sharePoints(fpmId,sharePointsValue,friId,friTime){
	var sharePointsValueNum =  $("#"+sharePointsValue).val();
	var re = /^[0-9]+.?[0-9]*$/; 
	if (!re.test(sharePointsValueNum)) {
		alert("散分必须为正整数!!");
		return false;
	}
    var url = '/portal/resource?ForumProvider&actionType=sharePoints&fpmId='+fpmId+"&sharePointsValue="+sharePointsValueNum+'&friId='+friId+'&friTime='+friTime;
	sendRequest(url,{actionType:'sharePoints',onComplete:function(responeText){
		if (responeText == 'success'){
			alert("散分成功！！");
			document.getElementById("share_"+friId).style.display="none";
		}else{
			alert("散分未成功,请检查分数是否超出悬赏积分！！");
		}
	}});
}

function isSharePoint(fpmId,friId,friTime){
	document.getElementById("isShare_"+friId).style.display="none";
	document.getElementById("share_"+friId).style.display="";
}

function reportReplyPost(friId,friUserId){
	var url = '/portal/resource?ForumProvider&actionType=reportReplyPost&friId='+friId+'&friUserId='+friUserId;
	sendRequest(url,{actionType:'reportReplyPost',onComplete:function(responeText){
		if (responeText == 'success'){
			alert("举报成功！！");
			location.replace(location) 
		}else{
			alert("举报未成功，请检查操作!!");
		}
	}});
}
var reportReplyForumBox;
function openReporReplyForumBox(friId){
	var title = "举报信息";
	var top = $('#friId').position().top;
	var forumCurrentPostId = $('#FPM_ID').val();
	$('#reportReplyForumBoxDiv').css("top",top);
	var fpmTitle = $('#postfpmTitle').val();
	if (!reportReplyForumBox){
		reportReplyForumBox = new PopupBox('reportReplyForumBox',title,{size:'big',width:'460px',height:'260px',top:top});
	}
	var url = '/portal/resource?ForumReportPostInfo&FPFR_ID='+friId+'&frrResType=REPLAYMESSAGE&fpmTitle='+fpmTitle+'&forumCurrentPostId='+forumCurrentPostId;
	reportReplyForumBox.sendRequest(url);	
}

function replyFloorPost(friId,floor) {
	$ ("#replySource").val ("REPLIES_INFO");
	$ ("#friId").val (friId);
	$ ("#floor").val (floor);
	$ ("#replyFloorShow").html(floor+'楼');
	window.scrollTo(0,document.body.scrollHeight)
}

function mousePosition(friId,evt){
    evt = event || window.event;
    absoluteY = event.clientY + scrollY;
    relativeY = event.clientY;
    var yPos = absoluteY - relativeY + 10;
    openEditReplyPostForumBox(friId,yPos);
}

var editReplyPostForumBox;
function openEditReplyPostForumBox(friId,yPos){
	var title = "编辑信息";
	var top = yPos;
	$('#editReplyPostForumBoxDiv').css("top",top);
	if (!editReplyPostForumBox){
		editReplyPostForumBox = new PopupBox('editReplyPostForumBox',title,{size:'big',width:'900px',height:'550px',top:top});
	}
	var url = '/portal/resource?ForumEditReplyPostInfo&FRI_ID='+friId;
	
	editReplyPostForumBox.sendRequest(url);	
}

function cancelReportReply(friId,friUserId){
	var url = '/portal/resource?ForumProvider&actionType=cancelReportReply&friId='+friId+'&friUserId='+friUserId;
	sendRequest(url,{actionType:'cancelReportPost',onComplete:function(responeText){
		if (responeText == 'success'){
			alert("取消举报成功！！");
			location.replace(location) 
		}else{
			alert("取消举报未成功，请检查操作!!");
		}
	}});
}
</script>
<div id="Searchresult"></div>
<div class="pagelist" style="height:25px">
<div id="Pagination" style="float:right;margin-top:5px" class="pagination"></div>
</div>
<form id="<portlet:namespace/>Form" >
<input type="hidden" name="friId" id="friId" value=""/>
<input type="hidden" name="floor" id="floor" value=""/>
</form>
<script>
function replyLoad(){
	var replyIds = "<%=replyIds%>";
	var strs= new Array(); //定义一数组 
	strs=replyIds.split(","); //字符分割 
	for (i=0;i < strs.length ;i++ ) 
	{ 
		var friId = "share_"+strs[i];
		if(friId !== "share_"){
			$("#"+friId).css({"display":"none"});
		}
	} 
}
//首先利用这个js需要进行两次后台的访问。
//第一次是查找到分页的个数，也就是说分几页。
//第二次是查找当前页的数据

var items_per_page=10; 
var pageIndex=0;     
var recordtotal=${total};

$(function(){
	var __renderPortlet<portlet:namespace/> = function(){
        $("#Pagination").pagination(recordtotal,{
	        callback:PageCallback,  
	        items_per_page:items_per_page,  //每页显示的条目数
	        num_display_entries:10,  //默认值10可以不修改
	        num_edge_entries:1,  //两侧显示的首尾分页的条目数
	        prev_text:"上一页",
	        next_text:"下一页", 
	        current_page:pageIndex //当前页索引
        });
	};
	__renderPortlets.put("<%=portletId%>",__renderPortlet<portlet:namespace/>);
	__renderPortlet<portlet:namespace/>();
});                  

//翻页调用的函数
function PageCallback(index, jq) {
	InitTable(index);
    return false;         
} 

//请求分页数据 
function InitTable(pageIndex) {
    sendAction('${getAjaxDataURL}',{data:'pageNum='+pageIndex+'&pageSize='+items_per_page+'&contentId=<%=request.getAttribute("contentId")%>',dataType:'text',onComplete:function(responseText){
		if (responseText){
			$('#Searchresult').html("");
			$('#Searchresult').append(responseText);
			replyLoad();
		}
	}});
}
</script>