<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String isSetting = (String)request.getAttribute("isSetting");
if ("Y".equals(isSetting)){
String portletId = (String)request.getAttribute("portletId");
String recordsCount = (String)request.getAttribute("recordsCount");
String recCountPerPage = (String)request.getAttribute("recCountPerPage");
String mainNumCount = (String)request.getAttribute("mainNumCount");
String sideNumCount = (String)request.getAttribute("sideNumCount");
String themeType = (String)request.getAttribute("themeType");
Boolean isGuest = (Boolean)request.getAttribute("isGuest");
%>
<portlet:defineObjects/>
<portlet:resourceURL id="getAjaxData" var="getAjaxDataURL">
	<portlet:param name="themeType" value="<%=themeType%>" />
</portlet:resourceURL>
<portlet:resourceURL id="getRecordCount" var="getRecordCountURL">
</portlet:resourceURL>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/pagination.css" />
<style>
.listAreaMain{
    min-height:${minHeight}px; 
    height:auto !important; 
    height:${minHeight}px; 
    overflow:visible;
}
</style>
<script type="text/javascript" language="javascript" src="<%=request.getContextPath()%>/js/jquery.pagination.js"></script>
<div id="SearchResult" class="listAreaMain">
</div>
<div id="Pagination" class="pagination">
</div>
<script type="text/javascript">
function delPostMessage(fpmId){
	var confirmMsg="您确认要是删除数据吗？";
	if(confirm(confirmMsg)) {
		var url = '/portal/resource?ForumProvider&actionType=deleForumPostMessage&fpmId='+fpmId;
		sendAction(url,{dataType:'text',onComplete:function(responseText){
			if("fail"==responseText){
				alert("该帖子存在回帖，无法删除！");
			}else{
				alert("帖子已删除！");
				parent.location.reload(); 
			}
		}});
	}
}
function topPost(fpmId){
	var url = '/portal/resource?ForumProvider&actionType=topForumPostMessage&fpmId='+fpmId;
	sendAction(url,{dataType:'text',onComplete:function(responseText){
		if("fail"==responseText){
			alert("置顶失败，请联系管理员！");
		}else{
			alert("置顶成功！");
			parent.location.reload(); 
		}
	}});
}
function canclePost(fpmId){
	var url = '/portal/resource?ForumProvider&actionType=cancleTopForumPostMessage&fpmId='+fpmId;
	sendAction(url,{dataType:'text',onComplete:function(responseText){
		if("fail"==responseText){
			alert("取消置顶出问题了，请联系管理员！");
		}else{
			alert("该帖子已取消置顶！");
			parent.location.reload(); 
		}
	}});
}
function topSectionPost(fpmId){
	var url = '/portal/resource?ForumProvider&actionType=topSectionPostMessage&fpmId='+fpmId;
	sendAction(url,{dataType:'text',onComplete:function(responseText){
		if("fail"==responseText){
			alert("置顶失败，请联系管理员！");
		}else{
			alert("置顶成功！");
			parent.location.reload(); 
		}
	}});
}
function cancleSectionPost(fpmId){
	var url = '/portal/resource?ForumProvider&actionType=cancleTopSectionPostMessage&fpmId='+fpmId;
	sendAction(url,{dataType:'text',onComplete:function(responseText){
		if("fail"==responseText){
			alert("取消置顶出问题了，请联系管理员！");
		}else{
			alert("该帖子已取消置顶！");
			parent.location.reload(); 
		}
	}});
}
var targetTreeBox;
function openTargetTreeBox(fpmId){
	var columnIdValue = $("#columnId").val();
	if(!targetTreeBox){
		targetTreeBox = new PopupBox('targetTreeBox','请选择目标分组',{size:'normal',width:'300px',top:'3px'});
	}
	var handlerId = "ForumSectionPick";
	var url = '/portal/index?'+handlerId+'&FPM_ID='+fpmId;
	targetTreeBox.sendRequest(url);
}
function reload(){
	parent.location.reload(); 
}
function getQueryString(name) {  
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");  
    var r = window.location.search.substr(1).match(reg);  
    if (r != null) return unescape(r[2]); return null;  
}  
var recordsCount;
function pageselectCallback(page_index, jq){
    var items_per_page = <%=recCountPerPage%>;
    var startIndex = page_index*items_per_page;
    var endIndex = Math.min((page_index+1) * items_per_page,recordsCount);
    var code = getQueryString("code");
    var typeId = getQueryString("typeId");
    var datas = "startIndex="+startIndex+"&endIndex="+endIndex+"&code="+code+"&typeId="+typeId;
	sendAction('${getAjaxDataURL}',{dataType:'html',data:datas,onComplete:function(responseText){
		if (responseText){
		    var newcontent = responseText;
		    $('#SearchResult').html(newcontent);
		}else{
			$('#SearchResult').html("");
		}
	}});    
    return false;
}
var __renderPortlet<portlet:namespace/> = function(){
	var code = getQueryString("code");
    var typeId = getQueryString("typeId");
	var datas = "&code="+code+"&typeId="+typeId;
	sendAction('${getRecordCountURL}',{dataType:'text',data:datas,onComplete:function(responseText){
		recordsCount = parseInt(responseText);
		var optInit = {callback: pageselectCallback,items_per_page:<%=recCountPerPage%>,num_display_entries:<%=mainNumCount%>,num_edge_entries:<%=sideNumCount%>,prev_text:'向前',next_text:'向后'}
		$("#Pagination").pagination(recordsCount,optInit);		
	}});
};
__renderPortlets.put("<%=portletId%>",__renderPortlet<portlet:namespace/>);
__renderPortlet<portlet:namespace/>();
</script>
<%
}else{
	out.println("请正确设置相关属性！");
}
%>