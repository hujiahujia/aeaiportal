<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<style type="text/css">
.span_nav{
	background:url(/portal/themes/in/standard_green/images/ico_home_16.png) left center no-repeat;
	padding-left:18px;
	display: inline-block;
	width: 560px;
	text-overflow: ellipsis; 
	white-space: nowrap; 
	overflow: hidden;
}
</style>
<%
String content = (String)request.getAttribute("content");
%>
<div class="FlatMenuList">
<%=content%>
</div>
