<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.agileai.portal.driver.model.MenuItem"%>
<portlet:defineObjects/>
<portlet:resourceURL id="changeFormSelect" var="changeFormSelectURL"></portlet:resourceURL>
<%
	String isSetting = (String)request.getAttribute("isSetting");
if ("Y".equals(isSetting)){
	String portletId = (String)request.getAttribute("portletId");
	String isGuestId = (String)request.getAttribute("isGuestId");
	MenuItem menuItem = (MenuItem)request.getAttribute("_currentMenuItem_");
	String editorHeight = "post-message".equals(menuItem.getCode()) ? "400px":"230px";
%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/aeditors/kdeditor/themes/default/default.css" type="text/css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/aeditors/kdeditor/extends.css" type="text/css" />
<script type="text/javascript" src="<%=request.getContextPath()%>/aeditors/kdeditor/kindeditor-min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/aeditors/kdeditor/extends.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/aeditors/kdeditor/lang/zh_CN.js"></script>
<style>
.postrequired{
	border: solid 0px #CCC;
	background-color: #FFFFFF;
	margin: 0px;
	width:99%;
	float: left;
	height:200px;
}
#button_publish{
	height: 30px;
	width: 80px;
	font-size: 15px;
	font-weight: bolder;
}
#rewtable{
	padding:0px;
	margin: 0px;
}
</style>

<script type="text/javascript">
function checkChanged(fpm_expression) {
	if("DIFFICULT_TO_HELP" != fpm_expression){
		document.getElementById("fpmRewardPoints").style.display="none";
	}else{
		document.getElementById("fpmRewardPoints").style.display="";
	}
}
function checkPostInfo(){
	editor.sync();
	var fciName =  $('#fciName').val();
	var fpmTitle = $("#FPM_TITLE").val();
	var fpmContent = editor.text();
	if(fciName == ''){
		alert("请选择所属模块!!");
		return false;
	}else if(fpmTitle == ""){
		alert("帖子标题不能为空，请检查!!");
		return false;
	}else if(fpmContent == ""){
		alert("帖子内容不能为空，请检查!!");
		return false;
	}
	var fpmRewardPoints = $('#FPM_REWARD_POINTS').val();
	var obj = document.getElementsByName("fpm_expression");
	for(var i=0; i<obj.length; i ++){
       if(obj[i].checked){
       		if(obj[i].value == 'DIFFICULT_TO_HELP'){
       			var re = /^[0-9]+.?[0-9]*$/; 
       			if (!re.test(fpmRewardPoints)) {
       				alert("请填写悬赏分数，悬赏分数必须为正整数！！！！");
       				return false;
       			}else{
       				postRequest('<portlet:namespace/>Form',{actionType:'checkPostInfo',onComplete:function(responseText){
       					if(responseText == 'shieldWords'){
       						alert("帖子信息包含敏感词语，不能发帖!!");
       						return false;
       					}else if(responseText == 'isContinuiPostInfo'){
       						alert("发帖间隔较短,不能连续发帖!!");
       						return false;
       					}else if(responseText == 'overIntegral'){
       						alert("悬赏分数超过现有积分不能发帖!!");
       						return false;
       					}else if(responseText == 'success'){
       						createPostInfo();
       					}
       				}});
       				return true;
       			}
       		}
      	}
	}
	postRequest('<portlet:namespace/>Form',{actionType:'checkPostInfo',onComplete:function(responseText){
		if(responseText == 'shieldWords'){
			alert("帖子信息包含敏感词语，不能发帖!!");
			return false;
		}else if(responseText == 'isContinuiPostInfo'){
			alert("发帖间隔较短,不能连续发帖!!");
			return false;
		}else if(responseText == 'overIntegral'){
			alert("悬赏分数超过现有积分不能发帖!!");
			return false;
		}else if(responseText == 'success'){
			createPostInfo();
		}
	}});
}

function createPostInfo(){
	editor.sync();
	var fpmTitle = $("#FPM_TITLE").val();
	postRequest('<portlet:namespace/>Form',{actionType:'createPostInfo',onComplete:function(responseText){
		if (responseText == 'fail'){
			alert("发帖失败请检查操作!!");
		}else {
			window.location.href = ""+responseText+".ptml";
		}
	}});
}

function changeFormSelect(){
	var fciId = $('#moduleFciName').val();
	var fpmId = $('#FPM_ID').val();
	var url = '${changeFormSelectURL}&fciId='+fciId+'&fpmId='+fpmId
	sendAction(url,{onComplete:function(responseText){
		$('#fciName').html("");
		$('#fciName').html(responseText);
	}});
}

var addforumPostImgBox;
function openAddResourceRequestBox(){
	var title = "上传图片";
	var top = $('#FPM_TITLE').position().top;
	if (!addforumPostImgBox){
		addforumPostImgBox = new PopupBox('addforumPostImgBox',title,{size:'big',width:'260px',height:'260px',top:top});
	}
	var url = '/portal/resource?ForumPostImgResouceUploader&BIZ_ID='+$('#FPM_ID').val();
	addforumPostImgBox.sendRequest(url);	
}
function refreshPage(){
	addforumPostImgBox.closeBox();
	document.location.reload(); 
}
</script>
<body onload="changeFormSelect()">
<form action="/portal/resource?ForumProvider" id="<portlet:namespace/>Form" name="<portlet:namespace/>Form" method="post">
<div>
<table width="100%" id="rewtable">
 <tr>
 	<td colspan="2" height="40px">
 	 	<span><a style="font-size:15px;">&nbsp;&nbsp;标题:&nbsp;&nbsp;</a><input name="FPM_TITLE" type="text" id="FPM_TITLE" size="121" maxlength="60"/></span>
 	</td>
 </tr>
 <tr>
	<td width="600px" height="40px">
  	    <span><input type="radio" id="rew_anonymity" name="fpm_expression" value="DIFFICULT_TO_HELP" onclick="checkChanged('DIFFICULT_TO_HELP')" checked="checked" /><a style="font-size:15px;">疑难求助</a></span>
  	   	<span id="fpmRewardPoints"><a style="font-size:15px;">&nbsp;&nbsp;悬赏积分:&nbsp;&nbsp;</a><input name="FPM_REWARD_POINTS" type="text" id="FPM_REWARD_POINTS" size="10" value="0"/></span>
  	    <span><input type="radio" id="rew_anonymity" name="fpm_expression" value="DEFECT_FEEDBACK" onclick="checkChanged('DEFECT_FEEDBACK')"/><a style="font-size:15px;">缺陷反馈</a></span>
  	    <span><input type="radio" id="rew_anonymity" name="fpm_expression" value="DEVE_SUGGESTIONS" onclick="checkChanged('DEVE_SUGGESTIONS')" /><a style="font-size:15px;">发展建议</a></span>
	</td>
	<td>
		<a style="font-size:15px;">所属版块:</a><span><select id="moduleFciName" label="模块" name="moduleFciName" class="select" onchange="changeFormSelect()">${moduleFciName}
		</select>&nbsp;&nbsp;<select id="fciName" label="二级模块" name="fciName" class="select"></select></span>
	</td>
  </tr>
  <tr>
	<td colspan="2">
  	  <textarea name="FPM_CONTENT" id="FPM_CONTENT" type="text" class="postrequired" maxlength="2000" rows="10" cols="70"></textarea> 
	</td>
  </tr>
  <tr>
	<td align="left">
  	  <span><input type="button" id="button_publish" value="发  帖"
  	  <% if("isGuestId".equals(isGuestId)){%>	  
  	  disabled="disabled"
  	  <%} %>
  	   onclick="checkPostInfo()"/></span>
	</td>
  </tr>
</table>

</div>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="fciId" id="fciId" value=""/>
<input type="hidden" name="FU_CODE" id="FU_CODE" value="${currentUserId }"/>
<input type="hidden" name="FPM_ID" id="FPM_ID" value="${fpmId }"/>
</form>
</body>
<script language="javascript">
var editor;
KindEditor.ready(function(K) {
	editor = K.create('textarea[name="FPM_CONTENT"]', {
		resizeType : 1,
		height:"<%=editorHeight%>",
		allowPreviewEmoticons : false,
		allowImageUpload : false,
		items : [
				'fontsize', '|', 'bold','removeformat', '|', 'justifyleft', 'justifycenter', 'insertorderedlist',
				'insertunorderedlist', '|', 'emoticons', 'forumimage', 'link']
	});
});

function afterSelectImage(uploadeImgObj){
	var url = uploadeImgObj.url;
    var width = uploadeImgObj.width;
    var height = uploadeImgObj.height;
    var title = ""; 
    var border = "0";
    var align = "null";
    editor.exec('insertimage', url, title, width, height, border, align);
}
</script>
<%
}else{
	out.println("请正确设置相关属性！");
}
%>