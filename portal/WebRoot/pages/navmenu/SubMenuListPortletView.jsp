<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<%
String isSetting = (String)request.getAttribute("isSetting");
if ("Y".equals(isSetting)){
String content = (String)request.getAttribute("content");
String isFromMenuList = (String)request.getAttribute("isFromMenuList");
%>
<div class="FlatMenuList">
<%=content%>
</div>
<%
if ("N".equals(isFromMenuList)){
%>
<script>
$(document).ready(function () {
	$('.FlatMenuList li').click(function(){
		$('.FlatMenuList li.selected').removeClass('selected');
		$(this).addClass('selected');
	});
});
</script>
<%}%>
<%
}else{
	out.println("请正确设置相关属性！");
}
%>