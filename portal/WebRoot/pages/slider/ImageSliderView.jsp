<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<portlet:resourceURL id="getAjaxData" var="getAjaxDataURL">
</portlet:resourceURL>
<%
String isSetting = (String)request.getAttribute("isSetting");
if ("Y".equals(isSetting)){
String isLazyLoad = (String)request.getAttribute("isLazyLoad");
String isContent = (String)request.getAttribute("isContent");
String sliderHtml = (String)request.getAttribute("sliderHtml");
String sliderWidth = (String)request.getAttribute("sliderWidth");
String sliderHeight = (String)request.getAttribute("sliderHeight");
int preNextSpanTop = (Integer.parseInt(sliderHeight)-100)/2;
%>
<style>
	.slidecontainer {
	  margin: 0;
	  width: ${sliderWidth}px
	}
	.slidecontainer:before,
	.slidecontainer:after {
	  display: table;
	  content: "";
	  line-height: 0;
	}
	.slidecontainer:after {
	  clear: both;
	}
	.slidecontainer-fluid {
	  padding-right: 20px;
	  padding-left: 20px;
	  *zoom: 1;
	}
	.slidecontainer-fluid:before,
	.slidecontainer-fluid:after {
	  display: table;
	  content: "";
	  line-height: 0;
	}
	.slidecontainer-fluid:after {
	  clear: both;
	}  
	.slidecontainer,
	.navbar-static-top .slidecontainer,
	.navbar-fixed-top .slidecontainer,
	.navbar-fixed-bottom .slidecontainer {
	  width: ${sliderWidth}px;
   }
   #slides {
     display: none
   }
   #slides .slidesjs-navigation {
     margin-top:3px;
   }
   a.slidesjs-next,
   a.slidesjs-previous,
   a.slidesjs-play,
   a.slidesjs-stop {
     background-image: url(<%=request.getContextPath()%>/pages/slider/img/btns-next-prev.png);
     background-repeat: no-repeat;
     display:block;
     width:12px;
     height:14px;
     overflow: hidden;
     text-indent: -9999px;
     float: left;
     margin-right:5px;
   }

   a.slidesjs-next {
     margin-right:10px;
     background-position: -12px 0;
   }

   a:hover.slidesjs-next {
     background-position: -12px -15px;
   }

   a.slidesjs-previous {
     background-position: 0 0;
   }

   a:hover.slidesjs-previous {
     background-position: 0 -15px;
   }

   a.slidesjs-play {
     width:15px;
     background-position: -25px 0;
   }

   a:hover.slidesjs-play {
     background-position: -25px -15px;
   }

   a.slidesjs-stop {
     width:15px;
     background-position: -41px 0;
   }

   a:hover.slidesjs-stop {
     background-position: -41px -15px;
   }
   .slidesjs-pagination {
     margin: 7px 0 0;
     float: right;
     list-style: none;
   }
   .slidesjs-pagination li {
     float: left;
     margin: 0 1px;
   }
   .slidesjs-pagination li a {
     display: block;
     width: 13px;
     height: 0;
     padding-top: 13px;
     background-image: url(<%=request.getContextPath()%>/pages/slider/img/pagination.png);
     background-position: 0 0;
     float: left;
     overflow: hidden;
   }
   .slidesjs-pagination li a.active,
   .slidesjs-pagination li a:hover.active {
     background-position: 0 -13px
   }

   .slidesjs-pagination li a:hover {
     background-position: 0 -26px
   }

   #slides a:link,
   #slides a:visited {
     color: #333
   }

   #slides a:hover,
   #slides a:active {
     color: #9e2020
   }

   .navbar {
     overflow: hidden
   }
</style>
<script src="<%=request.getContextPath()%>/pages/slider/js/jquery.slides.min.js"></script>
<div class="slidecontainer">
<%if ("N".equals(isLazyLoad)){%>
<%=sliderHtml%>
<%}%>
</div>
<script type="text/javascript">
function sliderRedirect(url){
	window.location.href=url;
}
function renderImageSlider(){
    $('#slides').slidesjs({
        width: ${sliderWidth},
        height: ${sliderHeight},
        play: {
          active: true,
          auto: true,
          interval: 4000,
          swap: true
        }
     });
}

$(function() {
<%if ("N".equals(isLazyLoad)){%>
	renderImageSlider();
<%}else{%>
	var __renderPortlet<portlet:namespace/> = function(){
		sendAction('${getAjaxDataURL}',{dataType:'html',onComplete:function(responseText){
			if (responseText){
				$(".slidecontainer").html(responseText);
				renderImageSlider();
			}
		}});
	};
	__renderPortlet<portlet:namespace/>();		
<%}%>
});
</script>
<%
}else{
	out.println("请正确设置相关属性！");
}
%>