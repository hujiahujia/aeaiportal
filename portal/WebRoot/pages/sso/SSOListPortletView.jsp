<%@page import="com.agileai.util.StringUtil"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.portal.driver.PortletBean"/>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:resourceURL id="processSSO" var="processSSOURL" />
<%
String isSetting = (String)request.getAttribute("isSetting");
if ("Y".equals(isSetting)){
%>	
<script language="javascript">
function <portlet:namespace/>openSSOApplication(redirectURL){
	sendAction('${processSSOURL}'+'t='+Math.random(),{onComplete:function(responseText){
		if (responseText=='success'){
			window.open(redirectURL);
		} else {
			alert('something wrong in process sso action !');
		}
	}});
}
var <portlet:namespace />configAppBox;
function <portlet:namespace />configSSORquest(){
	if (!<portlet:namespace />configAppBox){
		<portlet:namespace />configAppBox = new PopupBox('<portlet:namespace />configAppBox','单点应用配置',{size:'big',top:'10px'});
	}
	var url = '<%=request.getContextPath()%>/index?SSOAppConfList';
	<portlet:namespace />configAppBox.sendRequest(url);	
}
</script>
<div class="${divClass}">
   <div>
	<ul>
	<%
	int count = pageBean.getRsList().size();
	for (int i=0;i < count;i++){
	String appName = (String)pageBean.getAttribute(i,"APP_NAME");
	String ticketId = (String)pageBean.getAttribute(i,"TICKET_ID");
	String cssClass = (String)pageBean.getAttribute(i,"APP_CSS_CLASS");
	if (StringUtil.isNullOrEmpty(cssClass)){
		cssClass="";
	}
	%>
	  <li class="<%=cssClass%>"><a  hidefocus="true" href="javascript:<portlet:namespace/>openSSOApplication('<%=request.getContextPath()%>/index?SSORedirect&ticketId=<%=ticketId%>')"><%=appName%></a></li>
	<%}%>  
	</ul>
  <span style="float:right; font-size: 14px; margin-top: 5px; margin-right:5px; cursor:pointer;" onclick="<portlet:namespace />configSSORquest()">应用配置</span>
  </div>  
</div>
<%
}else{
	out.println("请正确设置相关属性！");
}
%>