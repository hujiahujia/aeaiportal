<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ page language="java" import="com.agileai.portal.driver.model.PortletWindowConfig" %>
<%@ page language="java" import="com.agileai.util.StringUtil" %>
<%@ page language="java" import="com.agileai.portal.driver.AttributeKeys" %>
<%@ page language="java" import="com.agileai.portal.driver.model.Theme" %>
<%@ taglib uri="http://portal.agileai.com" prefix="pt" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%
String windowMode = (String)request.getAttribute("WINDOW_MODE_KEY");
String porletId = (String)request.getAttribute("portlet");
Theme theme = (Theme)request.getAttribute(AttributeKeys.THEME_KEY);
PortletWindowConfig windowConfig =(PortletWindowConfig)request.getAttribute(porletId);
String portletMode = (String)request.getAttribute(porletId+"PortletMode");
String height = windowConfig.getHeight();
if (!"auto".equals(height)){
	height = height+"px";
}
if ("edit".equals(portletMode)){
	height = "auto";
}
String bodyStyle = "style=\"";
String defStyle = windowConfig.getBodyStyle();
bodyStyle = bodyStyle + defStyle;
bodyStyle = bodyStyle+"\"";
boolean isCircular = windowConfig.getPropertyBoolValue(pageContext.getServletContext(),theme,"circular");
String color = windowConfig.getPropertyValue(pageContext.getServletContext(),theme,"colour");
String iconPath = windowConfig.getPropertyValue(pageContext.getServletContext(),theme,"iconPath");
boolean hasIcon = !StringUtil.isNullOrEmpty(iconPath);
String fullIconPath = null;
if (hasIcon){
	fullIconPath = iconPath;
}
String moreURL = windowConfig.getPropertyValue(pageContext.getServletContext(),theme,"moreURL");
boolean hasMoreURL = !StringUtil.isNullOrEmpty(moreURL);
String fullMoreURL = null;
if (hasMoreURL){
	if (moreURL.toLowerCase().startsWith("http://") || moreURL.toLowerCase().startsWith("https://")){
		fullMoreURL = moreURL;
	}else{
		fullMoreURL = request.getContextPath() + "/" + moreURL;
	}
}
String stateAnchorMarginTop = isCircular?"3":"5";
String modeDropDownMarginTo = isCircular?"0":"1";
%>
<pt:portlet portletId="${portlet}">
<div class="sharp <%=color%>">
	<%if(isCircular){%><b class="b1"></b><b class="b2"></b><b class="b3"></b><b class="b4"></b><%}else{%><b class="b9"></b><%}%>
    <div class="content" portletId="${portlet}" style="height:<%=height%>">
        <h3 class="portletTitle" <%if(!isCircular){%> style="line-height:27px;height:27px;"<%}%>><%if(hasIcon){%><span><img style="float:left;margin-left:5px;<%if(isCircular){%>margin-top:2px;<%}else{%>margin-top:4px;<%}%>" src="<%=fullIconPath%>" /></span><%}%><%if("edit".equals(windowMode)){%><span id="modeDropDown" style="float:right;margin-right:5px;margin-top:<%=modeDropDownMarginTo%>px;"><pt:modeDropDown /></span><%}%><span id="stateAnchor" style="display:block;float:right;margin-right:5px;margin-top:<%=stateAnchorMarginTop%>px;"><pt:windowStateAnchor /></span><span style="font-size:13px;float:left;margin-top:<%=stateAnchorMarginTop%>px;<%if(hasIcon){%>text-indent:5px;<%}else{%>text-indent:10px;<%}%>"><pt:title/></span><%if(hasMoreURL){%><span style="float:right;margin-right:5px;margin-top:<%=stateAnchorMarginTop%>px;"><a hideFocus="true" href="<%=fullMoreURL%>" target="_blank">更多</a></span><%}%></h3>
		<%
		Boolean minimized = (Boolean)pageContext.getAttribute("minimized");
		if (!minimized){
		%>        
        <div <%=bodyStyle%> class="portletBox"><pt:render/></div><%}%>
     </div>
     <%if(isCircular){%><b class="b5"></b><b class="b6"></b><b class="b7"></b><b class="b8"></b><%}else{%><b class="b9"></b><%}%>
  </div>
</pt:portlet>