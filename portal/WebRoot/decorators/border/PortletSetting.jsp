<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ page language="java" import="com.agileai.portal.driver.model.PortletWindowConfig" %>
<%@ page language="java" import="com.agileai.util.StringUtil" %>
<%@ page language="java" import="com.agileai.portal.driver.AttributeKeys" %>
<%@ page language="java" import="com.agileai.portal.driver.model.Theme" %>
<%@ taglib uri="http://portal.agileai.com" prefix="pt" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%
String windowMode = (String)request.getAttribute("WINDOW_MODE_KEY");
String portletId = (String)request.getAttribute("portlet");
Theme theme = (Theme)request.getAttribute(AttributeKeys.THEME_KEY);
String columnIndex = (String)request.getAttribute("columnIndex");
String menuItemId = (String)request.getAttribute("menuItemId");
PortletWindowConfig windowConfig = (PortletWindowConfig)request.getAttribute(portletId);
String height = windowConfig.getHeight();
if (!"auto".equals(height)){
	height = height+"px";
}
String bodyStyle = "style=\"";
String defStyle = windowConfig.getBodyStyle();
bodyStyle = bodyStyle + defStyle;
bodyStyle = bodyStyle+"\"";
boolean isCircular = windowConfig.getPropertyBoolValue(pageContext.getServletContext(),theme,"circular");
String color = windowConfig.getPropertyValue(pageContext.getServletContext(),theme,"colour");

String stateAnchorMarginTop = isCircular?"3":"5";
String modeDropDownMarginTo = isCircular?"0":"1";
%>
<pt:portlet portletId="${portlet}">
<div class="sharp <%=color%>">
	<%if(isCircular){%><b class="b1"></b><b class="bb2"></b><b class="bb3"></b><b class="bb4"></b><%}else{%><b class="b9"></b><%}%>
    <div class="content droppable" style="height:<%=height%>" portletId="<%=portletId%>" columnIndex="<%=columnIndex%>">
		<%
		Boolean minimized = (Boolean)pageContext.getAttribute("minimized");
		if (!minimized){
		%>        
        <div <%=bodyStyle%> class="portletBox"><pt:render/></div><%}%>
     </div>
     <%if(isCircular){%><b class="b5"></b><b class="b6"></b><b class="b7"></b><b class="b8"></b><%}else{%><b class="b9"></b><%}%>
</div>
</pt:portlet>