package com.agileai.portal.portlets.content;

import java.util.ArrayList;
import java.util.List;

import com.agileai.domain.KeyNamePair;

public class InfomationModel {
	private String id = null;
	private String readCount = null;
	private String title = null;
	private String content = null;
	private String author = null;
	private String publishTime = null;
	private int praise = 0;
	public int getPraise() {
		return praise;
	}
	public void setPraise(int praise) {
		this.praise = praise;
	}
	private int reviewCount = 0;
	
	private List<KeyNamePair> keywords = new ArrayList<KeyNamePair>();
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getReadCount() {
		return readCount;
	}
	public void setReadCount(String readCount) {
		this.readCount = readCount;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getPublishTime() {
		return publishTime;
	}
	public void setPublishTime(String publishTime) {
		this.publishTime = publishTime;
	}
	public List<KeyNamePair> getKeywords() {
		return keywords;
	}
	public void setKeywords(List<KeyNamePair> keywords) {
		this.keywords = keywords;
	}
	public int getReviewCount() {
		return reviewCount;
	}
	public void setReviewCount(int reviewCount) {
		this.reviewCount = reviewCount;
	}
}
