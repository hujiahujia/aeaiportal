package com.agileai.portal.portlets.mobile;

import java.io.IOException;

import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.RenderMode;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import com.agileai.portal.bizmoduler.mobile.AngularWidgetManage;
import com.agileai.portal.driver.GenericPotboyPortlet;
import com.agileai.portal.driver.common.PreferencesHelper;
import com.agileai.portal.driver.mobile.AngularWidget;

public class AngularWidgetPortlet extends GenericPotboyPortlet{
	public static final String PortletAppId = "ANGULARW-IDGE-T111-1111-111111111111";
	
	@RenderMode(name = "view")
	public void view(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		PortletPreferences preferences =  PreferencesHelper.getPublicPreference(request);
		try {
			String widgetId = preferences.getValue("widgetId", "");
			AngularWidgetManage angularWidgetManage = this.lookupService(AngularWidgetManage.class);
			AngularWidget angularWidget = angularWidgetManage.retrieveAngularWidget(widgetId);
			request.setAttribute("angularWidget", angularWidget);
			super.doView(request, response);	
		} catch (Exception e) {
			logger.error(e.getLocalizedMessage(),e);
		}
	}
}
