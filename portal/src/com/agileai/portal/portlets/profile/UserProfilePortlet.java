package com.agileai.portal.portlets.profile;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletMode;
import javax.portlet.PortletModeException;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletSession;
import javax.portlet.ProcessAction;
import javax.portlet.ReadOnlyException;
import javax.portlet.RenderMode;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ValidatorException;

import com.agileai.hotweb.domain.core.Profile;
import com.agileai.hotweb.domain.core.User;
import com.agileai.portal.driver.GenericPotboyPortlet;
import com.agileai.portal.driver.common.PreferenceException;
import com.agileai.portal.driver.common.PreferencesHelper;
import com.agileai.portal.driver.common.PreferencesWrapper;
import com.agileai.util.StringUtil;

public class UserProfilePortlet extends GenericPotboyPortlet {
	@RenderMode(name = "view")
	public void view(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		PortletPreferences preferences =  PreferencesHelper.getPublicPreference(request);
		String isShowRoleList = preferences.getValue("isShowRoleList", null);
		String minHeight = preferences.getValue("minHeight", null);

		if (!StringUtil.isNullOrEmpty(isShowRoleList) && !StringUtil.isNullOrEmpty(minHeight) 
				){
			request.setAttribute("isSetting", "Y");
			PortletSession session = request.getPortletSession(false);
			if (session != null){
				Profile profile = (Profile)session.getAttribute(Profile.PROFILE_KEY,PortletSession.APPLICATION_SCOPE);
				User user = (User)profile.getUser();
				request.setAttribute("user",user);
			}
			request.setAttribute("isShowRoleList", isShowRoleList);
			request.setAttribute("minHeight", minHeight);
		}else{
			request.setAttribute("isSetting", "N");
		}
		super.doView(request, response);
	}

	@RenderMode(name = "edit")
	public void edit(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		PortletPreferences preferences = PreferencesHelper.getPublicPreference(request);
		String isShowRoleList = preferences.getValue("isShowRoleList", "Y");
		String minHeight = preferences.getValue("minHeight", "200");
		
		request.setAttribute("isShowRoleList", isShowRoleList);
		request.setAttribute("minHeight", minHeight);		
		super.doEdit(request, response);
	}

	@ProcessAction(name = "saveConfig")
	public void saveConfig(ActionRequest request, ActionResponse response)
			throws ReadOnlyException, PortletModeException, ValidatorException,
			IOException, PreferenceException {
		String isShowRoleList = request.getParameter("isShowRoleList");
		String minHeight = request.getParameter("minHeight");

		PreferencesWrapper preferWapper = new PreferencesWrapper();		
		preferWapper.setValue("isShowRoleList", isShowRoleList);
		preferWapper.setValue("minHeight", minHeight);
		
		PreferencesHelper.savePublicPreferences(request, preferWapper.getPreferences());	
		response.setPortletMode(PortletMode.VIEW);
	}
}
