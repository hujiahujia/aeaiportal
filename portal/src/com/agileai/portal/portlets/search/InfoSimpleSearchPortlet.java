package com.agileai.portal.portlets.search;

import java.io.IOException;
import java.io.PrintWriter;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletMode;
import javax.portlet.PortletModeException;
import javax.portlet.PortletPreferences;
import javax.portlet.ProcessAction;
import javax.portlet.ReadOnlyException;
import javax.portlet.RenderMode;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.portlet.ValidatorException;

import com.agileai.portal.driver.GenericPotboyPortlet;
import com.agileai.portal.driver.Resource;
import com.agileai.portal.driver.common.PreferenceException;
import com.agileai.portal.driver.common.PreferencesHelper;
import com.agileai.portal.driver.common.PreferencesWrapper;
import com.agileai.util.StringUtil;

public class InfoSimpleSearchPortlet extends GenericPotboyPortlet {
	@RenderMode(name = "view")
	public void view(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		PortletPreferences preferences =  PreferencesHelper.getPublicPreference(request);
		String supportSplitSearchWords = preferences.getValue("supportSplitSearchWords", null);
		String targetPageURL = preferences.getValue("targetPageURL", null);

		if (!StringUtil.isNullOrEmpty(supportSplitSearchWords) && !StringUtil.isNullOrEmpty(targetPageURL) 
				){
			request.setAttribute("isSetting", "Y");
			request.setAttribute("supportSplitSearchWords", supportSplitSearchWords);
			targetPageURL = "/portal/request/"+targetPageURL;
			request.setAttribute("targetPageURL", targetPageURL);
		}else{
			request.setAttribute("isSetting", "N");
		}
		super.doView(request, response);
	}

	@RenderMode(name = "edit")
	public void edit(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		PortletPreferences preferences = PreferencesHelper.getPublicPreference(request);
		String supportSplitSearchWords = preferences.getValue("supportSplitSearchWords", "N");
		String targetPageURL = preferences.getValue("targetPageURL", null);
		
		request.setAttribute("supportSplitSearchWords", supportSplitSearchWords);
		request.setAttribute("targetPageURL", targetPageURL);
		super.doEdit(request, response);
	}

	@ProcessAction(name = "saveConfig")
	public void saveConfig(ActionRequest request, ActionResponse response)
			throws ReadOnlyException, PortletModeException, ValidatorException,
			IOException, PreferenceException {
		String supportSplitSearchWords = request.getParameter("supportSplitSearchWords");
		String targetPageURL = request.getParameter("targetPageURL");

		PreferencesWrapper preferWapper = new PreferencesWrapper();		
		preferWapper.setValue("supportSplitSearchWords", supportSplitSearchWords);
		preferWapper.setValue("targetPageURL", targetPageURL);
		
		PreferencesHelper.savePublicPreferences(request, preferWapper.getPreferences());	
		response.setPortletMode(PortletMode.VIEW);
	}
	@Resource(id="convertWords")
	public void convertWords(ResourceRequest request, ResourceResponse response)
			throws PortletException, IOException {
		String result = "";
		try {
			String keyword = request.getParameter("keyword");
			if (!StringUtil.isNullOrEmpty(keyword)){
				result = StringUtil.string2Number(keyword);				
			}
		} catch (Exception e) {
			this.logger.error("获取取数据失败getAjaxData", e);
		}
		PrintWriter writer = response.getWriter();
		writer.print(result);
		writer.close();
	}
}