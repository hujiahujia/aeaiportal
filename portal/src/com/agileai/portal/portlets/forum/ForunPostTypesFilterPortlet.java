package com.agileai.portal.portlets.forum;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.RenderMode;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.codehaus.jettison.json.JSONObject;

import com.agileai.domain.DataRow;
import com.agileai.hotweb.domain.core.Profile;
import com.agileai.hotweb.domain.core.User;
import com.agileai.portal.bizmoduler.forum.PostManage;
import com.agileai.portal.driver.GenericPotboyPortlet;
import com.agileai.portal.driver.model.MenuItem;
import com.agileai.portal.extend.forum.ForumPrivilegeHelper;

public class ForunPostTypesFilterPortlet extends GenericPotboyPortlet
{
  @SuppressWarnings({ "rawtypes", "unchecked" })
@RenderMode(name="view")
  public void view(RenderRequest request, RenderResponse response) throws PortletException, IOException{
	  try {
	    request.setAttribute("portletId", request.getWindowID());
	    MenuItem currentItem = this.getCurrentMenuItem(request);
		String description = currentItem.getRemark();
		JSONObject jsonObject = new JSONObject(description); 
		String moduleId = jsonObject.getString("sectionId");
	    Profile profile = (Profile) request.getUserPrincipal();
	    User user = (User) profile.getUser();
	    ForumPrivilegeHelper forumPrivilegeHelper = new ForumPrivilegeHelper(user);
		String isNotAdmin = "isNotAdmin";
		String isNotAdmin8Moderator = "isNotAdmin8Moderator";
		if(forumPrivilegeHelper.isForumAdmin()){
			isNotAdmin = null;
		}
		if(forumPrivilegeHelper.isModerator(moduleId)){
			isNotAdmin8Moderator = null;
		}
		PostManage postManage = this.lookupService(PostManage.class);
		List<DataRow> postTypes = postManage.findPostTypes(isNotAdmin,isNotAdmin8Moderator);
		List records = new ArrayList();
	    for (int i = 0; i < postTypes.size(); i++) {
	    	DataRow row = postTypes.get(i); 
	    	HashMap row1 = new HashMap();
	    	row1.put("code", row.get("CODE_ID"));
	    	row1.put("name", row.get("CODE_NAME"));
	    	row1.put("typeId", row.get("TYPE_ID"));
	        records.add(row1);
	    }
	    request.setAttribute("reocrds", records);
	   } catch (Exception e) {
	      this.logger.error(e.getMessage());
	   }
	   super.doView(request, response);
  }
}