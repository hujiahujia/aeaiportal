package com.agileai.portal.portlets.forum;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.RenderMode;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.codehaus.jettison.json.JSONObject;

import com.agileai.domain.DataRow;
import com.agileai.portal.bizmoduler.forum.ForumSectionInfoTreeManage;
import com.agileai.portal.driver.GenericPotboyPortlet;
import com.agileai.portal.driver.model.MenuItem;

public class SectionInfoShowPortlet extends GenericPotboyPortlet {
	@SuppressWarnings({ "rawtypes", "unchecked" })
 	@RenderMode(name="view")
   public void view(RenderRequest request, RenderResponse response) throws PortletException, IOException{
	  try {
		  request.setAttribute("portletId", request.getWindowID());
		  MenuItem currentItem = this.getCurrentMenuItem(request);
		  String description = currentItem.getRemark();
		  JSONObject jsonObject = new JSONObject(description); 
		  String sectionId = jsonObject.getString("sectionId");
		  ForumSectionInfoTreeManage forumSectionInfoTreeManage = this.lookupService(ForumSectionInfoTreeManage.class);
		  DataRow dataRow = forumSectionInfoTreeManage.getSectionInfo(sectionId,"All","All");
		  List<DataRow> moderatorRecords = forumSectionInfoTreeManage.getModeratorInfoRecords(sectionId);
		  Integer replyPostCount = forumSectionInfoTreeManage.getReplyPostCount(sectionId);
		  String newCreateTime = forumSectionInfoTreeManage.getNewCreateTime(sectionId);
		  String moderators = "";
		  for(int i = 0; i<moderatorRecords.size();i++){
			  DataRow moderatorRecord = moderatorRecords.get(i);
			  String moderator = moderatorRecord.getString("FU_NAME");
			  moderators = moderator+" "+moderators;
		  }
		  HashMap row = new HashMap();
		  String fciMotto = dataRow.stringValue("FCI_MOTTO");
		  String menuCode = "";
		  String menuCodes = "";
		  JSONObject jsObject = new JSONObject(fciMotto);
		  menuCode = (String) jsObject.get("menuCode");
		  menuCodes = "/portal/request/05/"+menuCode+".ptml";
		  row.put("fciName", dataRow.getString("FCI_NAME"));
		  row.put("fciCode", dataRow.getString("FCI_CODE"));
		  row.put("menuCodes", menuCodes);
		  row.put("menuCode", menuCode);
		  row.put("fpm_counts", String.valueOf(dataRow.getInt("FPM_COUNTS")));
		  row.put("replyPostCount", String.valueOf(replyPostCount));
		  row.put("moderators",moderators);
		  row.put("newCreateTime", newCreateTime);
		  request.setAttribute("reocrds", row);
	   }catch (Exception e) {
		this.logger.error(e.getMessage());
	   }
	   super.doView(request, response);
   }
}
