package com.agileai.portal.portlets.forum;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletMode;
import javax.portlet.PortletModeException;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequest;
import javax.portlet.ProcessAction;
import javax.portlet.ReadOnlyException;
import javax.portlet.RenderMode;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ValidatorException;
import javax.servlet.http.HttpServletRequest;

import com.agileai.domain.DataRow;
import com.agileai.hotweb.common.StringTemplateLoader;
import com.agileai.portal.bizmoduler.forum.PostManage;
import com.agileai.portal.controller.forum.ForumProviderHandler;
import com.agileai.portal.driver.GenericPotboyPortlet;
import com.agileai.portal.driver.common.PortletRequestHelper;
import com.agileai.portal.driver.common.PreferenceException;
import com.agileai.portal.driver.common.PreferencesHelper;
import com.agileai.portal.driver.common.PreferencesWrapper;
import com.agileai.portal.portlets.PortletCacheManager;
import com.agileai.util.DateUtil;
import com.agileai.util.StringUtil;

import freemarker.template.Configuration;
import freemarker.template.ObjectWrapper;
import freemarker.template.Template;

public class RelativePostGridPortlet extends GenericPotboyPortlet {
	
	public static final class QueryParamKeys{
		public static String searchWord = "searchWord";
		public static String keyWordsId = "keyWordsId";
	}
	
	@RenderMode(name = "view")
	public void view(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		PortletPreferences preferences =  PreferencesHelper.getPublicPreference(request);
		String templateId = preferences.getValue("templateId", null);
		String minHeight = preferences.getValue("minHeight", null);
		String queryParamKey = preferences.getValue("queryParamKey", null);
		String infoLinkURL = preferences.getValue("infoLinkURL", null);
		
		String recCountPerPage = preferences.getValue("recCountPerPage",null);
		String mainNumCount = preferences.getValue("mainNumCount", null);
		String sideNumCount = preferences.getValue("sideNumCount", null);
		String maxCharNumber = preferences.getValue("maxCharNumber", "30");
		
		if (!StringUtil.isNullOrEmpty(templateId)
				&& !StringUtil.isNullOrEmpty(minHeight)
				&& !StringUtil.isNullOrEmpty(infoLinkURL)
				&& !StringUtil.isNullOrEmpty(recCountPerPage) 
				&& !StringUtil.isNullOrEmpty(mainNumCount) 
				&& !StringUtil.isNullOrEmpty(sideNumCount)
				&& !StringUtil.isNullOrEmpty(queryParamKey)
				){
			request.setAttribute("isSetting", "Y");
		}else{
			request.setAttribute("isSetting", "N");
		}
		
		String isSetting = (String)request.getAttribute("isSetting");
		if ("Y".equals(isSetting)){
			HttpServletRequest httpServletRequest = PortletRequestHelper.getRequestContext(request).getServletRequest();
			String searchWord = httpServletRequest.getParameter(queryParamKey);
			if (!StringUtil.isNullOrEmpty(searchWord)){
				PostManage postManage = this.lookupService(PostManage.class);
				ForumProviderHandler forumProviderHandler = new ForumProviderHandler();
				List<DataRow> records = postManage.findPostRecordsBysearchWord(searchWord);
				String urlPrefix = PortletCacheManager.buildDataURLPefix(request);
				ForumModulesModel forumModulesModel = new ForumModulesModel();
				try {
					forumModulesModel.setInfoLinkURL(getURLPrefix(request)+infoLinkURL);
					forumModulesModel.setSearchParam(searchWord);
					int recordsCount = 0;
					String content = "";
					if (records != null && records.size() > 0){
						StringWriter writer = new StringWriter();
						recordsCount = records.size();
						for (int i=0;i < recordsCount;i++){
							DataRow temp = records.get(i);
							DataRow row =  new DataRow();
							forumModulesModel.getRecordList().add(row);
							row.put("id",temp.stringValue("FPM_ID"));
							String title = temp.stringValue("FPM_TITLE");
							row.put("title",this.substring(title, maxCharNumber));
							String repliesCount = forumProviderHandler.getRepliesCount(temp.stringValue("FPM_ID"));
							row.put("repliesCount",repliesCount);
							row.put("readCount",temp.stringValue("FPM_CLICK_NUMBER"));
							row.put("postCreator",temp.stringValue("FPM_USERNAME"));
							Date date = (Date)temp.get("FPM_CREATE_TIME");
							row.put("createTime",DateUtil.format(DateUtil.YYMMDDHHMI_HORIZONTAL, date));
						}
						String tempateURL = urlPrefix+"/resource?PortletTemptProvider&actionType=retrieveContent&contentId="+templateId;
						String template = this.retrieveData(preferences, tempateURL);
						generate(forumModulesModel, template, writer);
						content = writer.toString();
					}
					request.setAttribute("recordsCount",String.valueOf(recordsCount));
					request.setAttribute("content",content);
				} catch (Exception e) {
					logger.error(e.getLocalizedMessage(), e);
				}
			}
			request.setAttribute("recCountPerPage", recCountPerPage);
			request.setAttribute("minHeight", minHeight);
			request.setAttribute("mainNumCount", mainNumCount);
			request.setAttribute("sideNumCount", sideNumCount);
		}
		super.doView(request, response);
	}
	
	private String getURLPrefix(PortletRequest request){
		return request.getContextPath()+"/"+ "request/";
	}
	
	private String retrieveData(PortletPreferences preferences,String dataURL) throws Exception{
		String ajaxData = "";
		String cacheMinutes = (String)preferences.getValue("cacheMinutes",defaultCacheMinutes);
		String isCache = preferences.getValue("isCache", defaultIsCache);
		ajaxData = PortletCacheManager.getOnly().getCachedData(isCache, dataURL, cacheMinutes);
		return ajaxData;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void generate(ForumModulesModel forumModulesModel,String template,StringWriter writer) {
		String encoding = "utf-8";
		try {
        	Configuration cfg = new Configuration();
        	cfg.setTemplateLoader(new StringTemplateLoader(template));  
        	cfg.setEncoding(Locale.getDefault(), encoding);
            cfg.setObjectWrapper(ObjectWrapper.BEANS_WRAPPER);
        	Template temp = cfg.getTemplate("");
        	temp.setEncoding(encoding);
            Map root = new HashMap();
            root.put("model",forumModulesModel);
            temp.process(root, writer);
            writer.flush();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	@RenderMode(name = "edit")
	public void edit(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		PortletPreferences preferences = PreferencesHelper.getPublicPreference(request);
		String templateId = preferences.getValue("templateId", null);
		String minHeight = preferences.getValue("minHeight", "400");
		String queryParamKey = preferences.getValue("queryParamKey", "keyWordsId");
		String queryParamValue = preferences.getValue("queryParamValue", null);
		String infoLinkURL = preferences.getValue("infoLinkURL", null);
		String recCountPerPage = preferences.getValue("recCountPerPage", "20");
		String mainNumCount = preferences.getValue("mainNumCount", "10");
		String sideNumCount = preferences.getValue("sideNumCount", "2");
		String maxCharNumber = preferences.getValue("maxCharNumber", "30");
		String isCache = preferences.getValue("isCache", defaultIsCache);
		String cacheMinutes = preferences.getValue("cacheMinutes", defaultCacheMinutes);
		
		request.setAttribute("templateId", templateId);
		request.setAttribute("minHeight", minHeight);
		request.setAttribute("queryParamKey", queryParamKey);
		request.setAttribute("queryParamValue", queryParamValue);
		request.setAttribute("infoLinkURL", infoLinkURL);
		
		request.setAttribute("recCountPerPage", recCountPerPage);
		request.setAttribute("mainNumCount", mainNumCount);
		request.setAttribute("sideNumCount", sideNumCount);
		request.setAttribute("maxCharNumber", maxCharNumber);
		
		request.setAttribute("isCache", isCache);
		request.setAttribute("cacheMinutes", cacheMinutes);
		
		super.doEdit(request, response);
	}

	@ProcessAction(name = "saveConfig")
	public void saveConfig(ActionRequest request, ActionResponse response)
			throws ReadOnlyException, PortletModeException, ValidatorException,
			IOException, PreferenceException {
		String templateId = request.getParameter("templateId");
		String minHeight = request.getParameter("minHeight");
		String queryParamKey = request.getParameter("queryParamKey");
		String queryParamValue = request.getParameter("queryParamValue");
		
		String infoLinkURL = request.getParameter("infoLinkURL");
		
		String recCountPerPage = request.getParameter("recCountPerPage");
		String mainNumCount = request.getParameter("mainNumCount");
		String sideNumCount = request.getParameter("sideNumCount");
		String maxCharNumber = request.getParameter("maxCharNumber");
		
		String isCache = request.getParameter("isCache");
		String cacheMinutes = request.getParameter("cacheMinutes");
		
		PreferencesWrapper preferWapper = new PreferencesWrapper();		
		preferWapper.setValue("templateId", templateId);
		preferWapper.setValue("minHeight", minHeight);
		preferWapper.setValue("queryParamKey", queryParamKey);
		preferWapper.setValue("queryParamValue", queryParamValue);
		
		preferWapper.setValue("infoLinkURL", infoLinkURL);
		
		preferWapper.setValue("recCountPerPage", recCountPerPage);
		preferWapper.setValue("mainNumCount", mainNumCount);
		preferWapper.setValue("sideNumCount", sideNumCount);
		preferWapper.setValue("maxCharNumber", maxCharNumber);
		
		preferWapper.setValue("isCache", isCache);
		preferWapper.setValue("cacheMinutes", cacheMinutes);
		
		PreferencesHelper.savePublicPreferences(request, preferWapper.getPreferences());	
		response.setPortletMode(PortletMode.VIEW);
	}
	
	private String substring(String value ,String lenghtLimit){
		String result = value;
		if (value != null && value.length() > Integer.parseInt(lenghtLimit)){
			result = value.substring(0,Integer.parseInt(lenghtLimit))+"…";
		}
		return result;
	}
}
