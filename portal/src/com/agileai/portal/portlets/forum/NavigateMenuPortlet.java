package com.agileai.portal.portlets.forum;

import java.io.IOException;
import java.util.HashMap;

import javax.portlet.PortletException;
import javax.portlet.RenderMode;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.codehaus.jettison.json.JSONObject;

import com.agileai.domain.DataRow;
import com.agileai.portal.bizmoduler.forum.PostManage;
import com.agileai.portal.driver.GenericPotboyPortlet;
import com.agileai.portal.driver.common.PortletRequestHelper;
import com.agileai.portal.driver.model.MenuItem;
import com.agileai.util.StringUtil;

public class NavigateMenuPortlet extends GenericPotboyPortlet {
	private static final String NEW_LINE = "\r\n";
	
	@RenderMode(name = "view")
	public void view(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		try {
			HttpServletRequest httpServletRequest = PortletRequestHelper.getRequestContext(request).getServletRequest();
			String contentId = httpServletRequest.getParameter("contentId");
			HashMap<String,String> menu = new HashMap<String, String>();
			if(!StringUtil.isNullOrEmpty(contentId)){
				PostManage postManage = this.lookupService(PostManage.class);
				DataRow row = postManage.retrieveModuleInfo(contentId);
				String moduleInfo = row.get("FCI_MOTTO").toString();
				String title = row.getString("FPM_TITLE");
				JSONObject jsonObject = new JSONObject(moduleInfo); 
				String menuCode = jsonObject.getString("menuCode");
				String menuName = jsonObject.getString("menuName");
				menu.put("menuCode", menuCode);
				menu.put("menuName", menuName);
				menu.put("title", title);
			}
			MenuItem currentItem = this.getCurrentMenuItem(request);
			String html = this.buildHtml(currentItem,menu);
			request.setAttribute("content", html);
			super.doView(request, response);
		} catch (Exception e) {
			this.logger.error(e.getMessage());
		}
	}
	
	private String buildHtml(MenuItem currentItem,HashMap<String,String> menu){
		String result = "";
		StringBuffer temp = new StringBuffer();
		String currentItemCode = currentItem.getCode();
		String currentItemName = currentItem.getName();
		temp.append("<div style=\"").append("margin-top: 12px;font-weight: bold;\">").append(NEW_LINE);
		temp.append("	<span class=").append("span_nav").append(">").append(NEW_LINE);
		temp.append("当前位置：").append(NEW_LINE);
		temp.append("		<span>").append(NEW_LINE);
		if(StringUtil.isNullOrEmpty(menu.get("menuCode"))&&!"index".equals(currentItemCode)){
			temp.append("<a href='").append("index.ptml' style=").append("\"text-decoration: none;").append("\">首页").append("</a>").append(NEW_LINE);
			temp.append("&nbsp;  &gt;&gt;  &nbsp;").append(NEW_LINE);
			temp.append("			<a").append(" href='").append(currentItemCode).append(".ptml' style=").append("\"text-decoration: none;").append("\">").append(currentItemName).append("</a>").append(NEW_LINE);
		}else if(StringUtil.isNotNullNotEmpty(menu.get("menuCode"))&&!"index".equals(currentItemCode)){
			temp.append("<a href='").append("index.ptml' style=").append("\"text-decoration: none;font-weight: bold;").append("\">首页").append("</a>").append(NEW_LINE);
			temp.append("&nbsp;  &gt;&gt;  &nbsp;").append(NEW_LINE);
			temp.append("			<a").append(" href='").append(menu.get("menuCode")).append(".ptml' style=").append("\"text-decoration: none;").append("\">").append(menu.get("menuName")).append("</a>").append(NEW_LINE);
			temp.append("&nbsp;  &gt;&gt;  &nbsp;").append("<span>").append(menu.get("title")).append("</span>").append(NEW_LINE);
		}else{
			temp.append("<a href='").append("index.ptml' style=").append("\"text-decoration: none;font-weight: bold;").append("\">首页").append("</a>").append(NEW_LINE); 
		}
		temp.append("		</span>").append(NEW_LINE);
		temp.append("	</span>").append(NEW_LINE);
		temp.append("</div>").append(NEW_LINE);
		result = temp.toString();
		return result;
	}
}
