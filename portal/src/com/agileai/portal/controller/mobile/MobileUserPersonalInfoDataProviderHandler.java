package com.agileai.portal.controller.mobile;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.SimpleHandler;
import com.agileai.hotweb.domain.core.Group;
import com.agileai.hotweb.domain.core.Profile;
import com.agileai.hotweb.domain.core.Resource;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.auth.SecurityGroupManage;
import com.agileai.portal.bizmoduler.forum.UserLevel8BBSUserInfoManage;
import com.agileai.portal.bizmoduler.wcm.InfomationFavEditManage;
import com.agileai.portal.bizmoduler.wcm.InfomationManage;
import com.agileai.util.DateUtil;
import com.agileai.util.StringUtil;

public class MobileUserPersonalInfoDataProviderHandler extends SimpleHandler {

	public MobileUserPersonalInfoDataProviderHandler(){
		super();
	}
	@PageAction
	public ViewRenderer retrieveUserInfo(DataParam param){
		String responseText = null;
		try {
			User user = (User) this.getUser();
			String userCode = user.getUserCode();
			SecurityGroupManage securityGroupManage = this.lookupService(SecurityGroupManage.class);
			UserLevel8BBSUserInfoManage userLevel8BBSUserInfoManage = this.lookupService(UserLevel8BBSUserInfoManage.class);
			DataParam queryParam = new DataParam("USER_CODE",userCode);
			DataRow record =  securityGroupManage.getContentRecord("SecurityUser", queryParam);
			String id = user.getUserId();
			String name = user.getUserName();
			String sex = record.getString("USER_SEX");
			String email = record.getString("USER_MAIL");
			Date date = (Date) record.get("USER_CREATOR_DATE");
			String createTime = null;
			if(date!=null){
				createTime = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_HORIZONTAL, date);
			}
			String desc = record.getString("USER_DESC");
			String wxOpenId = record.getString("USER_WX_OPENID");
			DataRow forumUserInfo = userLevel8BBSUserInfoManage.getFuUserInfoRecord(userCode);
			String userLevel = forumUserInfo.getString("FU_LEVEL");
			int userIntegral = forumUserInfo.getInt("FU_INTEGRAL");
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("id", id);
			jsonObject.put("name", name);
			jsonObject.put("code",userCode);
			jsonObject.put("email", email);
			jsonObject.put("sex", sex);
			jsonObject.put("createTime", createTime);
			jsonObject.put("userLevel", userLevel);
			jsonObject.put("userIntegral", String.valueOf(userIntegral));
			jsonObject.put("desc", desc);
			if (StringUtil.isNotNullNotEmpty(wxOpenId)){
				jsonObject.put("wxOpenId", wxOpenId);				
			}
			String openId = (String)this.getSessionAttribute("openId");
			if (StringUtil.isNotNullNotEmpty(openId)){
				jsonObject.put("isInWeixin",true);
			}else{
				jsonObject.put("isInWeixin",false);
			}
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer findUserFavInfos(DataParam param){
		String responseText = null;
		try {
			List<DataRow> records = null;
			User user = (User) this.getUser();
			String userCode = user.getUserCode();
			InfomationManage infomationManage = this.lookupService(InfomationManage.class);
			String sortPolicy = "Top8PublishTime";
			String columnId = "";
			List<DataRow> tempRecords = infomationManage.findContentRecords2Fav(columnId, sortPolicy, InfomationManage.InfoState.Published, userCode);
			records = this.getAuthedRecords(tempRecords);
			int count = records.size();
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray = new JSONArray();
			jsonObject.put("datas", jsonArray);
			param.put("userCode", userCode);
			if(count>0){
				DataRow countResult = this.lookupService(InfomationFavEditManage.class).getInfoCountRecord(param);
				String counts = String.valueOf(countResult.get("count"));
				String topTitle = "共有收藏记录 "+counts+" 条";
				jsonObject.put("topTitle", topTitle);
				for(int i = 0;i<count;i++){
					DataRow row = records.get(i);
					JSONObject jsonObject1 = new JSONObject();
					jsonObject1.put("id", row.stringValue("INFO_ID"));
					jsonObject1.put("title", row.stringValue("INFO_TITLE"));
					jsonObject1.put("outLine", row.stringValue("INFO_OUTLINE"));
					jsonObject1.put("readCounts", String.valueOf(row.getInt("INFO_READ_COUNT")));
					String publishTime = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_HORIZONTAL, (Date)row.get("INFO_PUBLISH_TIME"));
					jsonObject1.put("publishTime", publishTime.substring(5));
					jsonObject1.put("reviewNums", String.valueOf(row.getInt("reviewNums")));
					jsonObject1.put("praiseNums", String.valueOf(row.getInt("praiseNums")));
					jsonObject1.put("favNums", String.valueOf(row.getInt("favNums")));
					jsonArray.put(jsonObject1);
				}
			}else{
				String topTitle = "共有收藏记录  0  条";
				jsonObject.put("topTitle", topTitle);
			}
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer findUserPrivateInfos(DataParam param){
		String responseText = null;
		try {
			List<DataRow> records =null;
			User user = (User) this.getUser();
			String userCode = user.getUserCode();
			InfomationManage infomationManage = this.lookupService(InfomationManage.class);
			List<DataRow> tempRecords = infomationManage.findUserPrivateInfos(userCode);
			records = this.getAuthedRecords(tempRecords);
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray = new JSONArray();
			if(records.size()>0){
				for(int i=0;i<records.size();i++){
					DataRow row = records.get(i);
					JSONObject jsonObject1 = new JSONObject();
					jsonObject1.put("id", row.getString("INFO_ID"));
					jsonObject1.put("title", row.getString("INFO_TITLE"));
					jsonObject1.put("outLine", row.getString("INFO_OUTLINE"));
					jsonObject1.put("readCounts", String.valueOf(row.getInt("INFO_READ_COUNT")));
					String publishTime = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_HORIZONTAL, (Date)row.get("INFO_PUBLISH_TIME"));
					jsonObject1.put("publishTime", publishTime.substring(5));
					jsonObject1.put("reviewNums", String.valueOf(row.getInt("reviewNums")));
					jsonObject1.put("praiseNums", String.valueOf(row.getInt("praiseNums")));
					jsonObject1.put("favNums", String.valueOf(row.getInt("favNums")));
					jsonArray.put(jsonObject1);
				}
				String counts = String.valueOf(records.size());
				String topTitle = "共有点赞记录 "+counts+" 条";
				jsonObject.put("datas",jsonArray);
				jsonObject.put("topTitle", topTitle);
			}else{
				String topTitle = "共有点赞记录  0  条";
				jsonObject.put("datas",jsonArray);
				jsonObject.put("topTitle", topTitle);
			}
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer findReceiverNames(DataParam param){
		String responseText = null;
		try {
			User user = (User) this.getUser();
			List<Group> groups = user.getGroupList();
			String isNewUser = "isNotNweUsers";
			for(int i=0;i<groups.size();i++){
				Group group = groups.get(i);
				String groupCode = group.getGroupCode();
				if("NewUsers".equals(groupCode)){
					isNewUser="isNweUsers";
					break;
				}
			}
			UserLevel8BBSUserInfoManage userLevel8BBSUserInfoManage = lookupService(UserLevel8BBSUserInfoManage.class);
			List<DataRow> records = userLevel8BBSUserInfoManage.findReceiverNames(isNewUser);
			JSONArray jsonArray = new JSONArray();
			for(int i = 0;i<records.size();i++){
				DataRow row = records.get(i);
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("userCode",row.getString("USER_CODE"));
				jsonObject.put("userName",row.getString("USER_NAME"));
				jsonArray.put(jsonObject);
			}
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("datas",jsonArray);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	
	
	private List<DataRow> getAuthedRecords(List<DataRow> tempRecords){
		List<DataRow> result = new ArrayList<DataRow>();
		Profile profile = this.getProfile();
		if (profile != null){
			User user = (User)profile.getUser();
			for (int i=0;i < tempRecords.size();i++){
				DataRow row = tempRecords.get(i);
				String authType = row.stringValue("INFO_AUTH_TYPE");
				if (InfomationManage.AuthType.Private.equalsIgnoreCase(authType)){
					String infomatonId = row.stringValue("INFO_ID");
					if (user.containResouce(Resource.Type.Infomation, infomatonId) || "admin".equals(user.getUserCode())){
						result.add(row);
					}
				}else{
					result.add(row);
				}
			}
		}else{
			for (int i=0;i < tempRecords.size();i++){
				DataRow row = tempRecords.get(i);
				String authType = row.stringValue("INFO_AUTH_TYPE");
				if (InfomationManage.AuthType.Public.equalsIgnoreCase(authType)){
					result.add(row);
				}
			}
		}
		return result;
	}
	
	
}
