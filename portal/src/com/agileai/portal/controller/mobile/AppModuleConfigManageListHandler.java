package com.agileai.portal.controller.mobile;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.mobile.AppModuleConfigManage;
import com.agileai.portal.extend.ExtendConsts;

public class AppModuleConfigManageListHandler
        extends StandardListHandler {
    public AppModuleConfigManageListHandler() {
        super();
        this.editHandlerClazz = AppModuleConfigManageEditHandler.class;
        this.serviceId = buildServiceId(AppModuleConfigManage.class);
    }

    protected void processPageAttributes(DataParam param) {
        setAttribute("acGroup",
                FormSelectFactory.create("AW_GROUP")
                                 .addSelectedValue(param.get("acGroup")));
        setAttribute("AC_GROUP",
                FormSelectFactory.create("AW_GROUP")
                                 .addSelectedValue(getOperaAttributeValue("AC_GROUP",
                                                                          "default")));
        setAttribute("AC_VAILD",
	           FormSelectFactory.create("SYS_VALID_TYPE")
	                            .addSelectedValue(getOperaAttributeValue("AC_VAILD",
	                                                                     "1")));
    }
    
    @PageAction
    public ViewRenderer reloadAll(DataParam param){
    	String responseText = FAIL;
    	try {
    		MobileDataProviderHandler.removeCaches();
    		User user = (User) this.getUser();
    		user.getTempProperties().remove(ExtendConsts.TempPropertyKeys.AppModules);
    		responseText = SUCCESS;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
    	
    	return new AjaxRenderer(responseText);
    }

    protected void initParameters(DataParam param) {
    	initParamItem(param, "acId", "");
        initParamItem(param, "acName", "");
        initParamItem(param, "acGroup", "");
        initMappingItem("AC_GROUP",
                FormSelectFactory.create("AW_GROUP").getContent());
        initMappingItem("AC_VAILD",
                FormSelectFactory.create("SYS_VALID_TYPE").getContent());
    }

    protected AppModuleConfigManage getService() {
        return (AppModuleConfigManage) this.lookupService(this.getServiceId());
    }
}
