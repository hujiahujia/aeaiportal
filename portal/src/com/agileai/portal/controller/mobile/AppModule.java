package com.agileai.portal.controller.mobile;

import java.io.Serializable;

public class AppModule implements Serializable{
	private static final long serialVersionUID = -7572653141359372888L;
	
	private String id = null;
	private String name = null;
	private String css = null;
	private String method = null;
	private String group = null;
	private int sort = 0;
	private String vaild = null;
	private String menthodType = null;
	public String getMenthodType() {
		return menthodType;
	}
	public void setMenthodType(String menthodType) {
		this.menthodType = menthodType;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCss() {
		return css;
	}
	public void setCss(String css) {
		this.css = css;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public Integer getSort() {
		return sort;
	}
	public void setSort(Integer sort) {
		this.sort = sort;
	}
	public String getVaild() {
		return vaild;
	}
	public void setVaild(String vaild) {
		this.vaild = vaild;
	}
}
