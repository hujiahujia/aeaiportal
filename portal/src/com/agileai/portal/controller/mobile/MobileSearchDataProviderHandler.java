package com.agileai.portal.controller.mobile;

import java.util.Date;
import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.SimpleHandler;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.forum.ForumSectionInfoTreeManage;
import com.agileai.portal.bizmoduler.wcm.InfomationManage;
import com.agileai.util.DateUtil;
import com.agileai.util.StringUtil;

public class MobileSearchDataProviderHandler extends SimpleHandler {
	public static String TODO = "todo";
	public static String ONDO = "ondo";
	public static String DID = "did";
	public MobileSearchDataProviderHandler(){
		super();
	}
	
	@PageAction
	public ViewRenderer searchDataList(DataParam param){
		String responseText = null;
		try {
			String category = param.getString("category");
			String searchWord = param.getString("searchWord");
			String result = null;
			if(StringUtil.isNullOrEmpty(category)){
				result = this.searchAll(searchWord);
			}else{
				result = this.searchAllByCategory(category,searchWord);
			}
			responseText = result;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}

	private String searchAllByCategory(String category, String searchWord) {
		String result = null;
		JSONObject jsonObject = new JSONObject();
		InfomationManage infomationManage = this.lookupService(InfomationManage.class);
		ForumSectionInfoTreeManage forumSectionInfoTreeManage = this.lookupService(ForumSectionInfoTreeManage.class);
		try {
			if("corpNews".equals(category)||"industryNews".equals(category)||"techTrends".equals(category)||"traincenter".equals(category)){
				if("corpNews".equals(category)){
					category = "corp-News";
				}else if("industryNews".equals(category)){
					category = "industry-News";
				}else if("techTrends".equals(category)){
					category = "tech-Trends";
				}else if("traincenter".equals(category)){
					category = "traincenter";
				}
				List<DataRow> results1 = infomationManage.findNewsBySearchWord(category,searchWord);
				JSONArray jsonArray1 = new JSONArray();
				for(int i=0;i<results1.size();i++){
					DataRow row = results1.get(i);
					JSONObject jsonObject1 = new JSONObject();
					jsonObject1.put("id", row.stringValue("INFO_ID"));
					jsonObject1.put("title", row.stringValue("INFO_TITLE"));
					jsonObject1.put("outLine", row.stringValue("INFO_OUTLINE"));
					jsonObject1.put("readCounts", String.valueOf(row.getInt("INFO_READ_COUNT")));
					String publishTime = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_HORIZONTAL, (Date)row.get("INFO_PUBLISH_TIME"));
					jsonObject1.put("publishTime", publishTime.substring(5));
					jsonObject1.put("reviewNums", String.valueOf(row.getInt("reviewNums")));
					jsonObject1.put("praiseNums", String.valueOf(row.getInt("praiseNums")));
					jsonObject1.put("favNums", String.valueOf(row.getInt("favNums")));
					jsonArray1.put(jsonObject1);
				}
				jsonObject.put("datas", jsonArray1);
			}else if("applicationplatform".equals(category)||"supportplatform".equals(category)||"platform".equals(category)){
				if("applicationplatform".equals(category)){
					category = "application-platform";
				}else if("supportplatform".equals(category)){
					category = "support-platform";
				}else{
					category = "platform";
				}
				List<DataRow> results2 = forumSectionInfoTreeManage.getSectionFormPostInfos(category,searchWord);
				JSONArray jsonArray2 = new JSONArray();
				for(int i=0;i<results2.size();i++){
					DataRow row = results2.get(i);
					JSONObject jsonObject2 = new JSONObject();
					jsonObject2.put("title", row.stringValue("FPM_TITLE"));
					jsonObject2.put("id", row.stringValue("FPM_ID"));
					jsonObject2.put("clickNum", String.valueOf(row.getInt("FPM_CLICK_NUMBER")));
					
					String createTime = row.stringValue("FPM_CREATE_TIME");
					createTime = DateUtil.getDateByType(DateUtil.YYMMDDHHMISS_HORIZONTAL, row.getTimestamp("FPM_CREATE_TIME"));
					createTime = createTime.substring(5, 16).replaceAll("/", "-");
					jsonObject2.put("createTime", createTime);
					jsonObject2.put("count", row.stringValue("FRI_ID_COUNT"));
					String type = "";
					if("ESSENCE_POSTS".equals(row.stringValue("FPM_TYPE"))){
						type = "essence";
					}else if("KEY_POSTS".equals(row.stringValue("FPM_TYPE"))){
						type = "key";
					}else if("ORDINARY_POSTS".equals(row.stringValue("FPM_TYPE"))){
						type = "ordinary";
					}
					jsonObject2.put("category", row.stringValue("FCI_CODE"));
					jsonObject2.put("type", type);
					jsonObject2.put("attentionCount", row.stringValue("FF_ID_COUNT"));//关注数
					jsonArray2.put(jsonObject2);
				}
				jsonObject.put("datas", jsonArray2);
			}
			result = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return result;
	}

	private String searchAll(String searchWord) {
		String result = null;
		try {
			JSONObject jsonObject = new JSONObject();
			InfomationManage infomationManage = this.lookupService(InfomationManage.class);
			ForumSectionInfoTreeManage forumSectionInfoTreeManage = this.lookupService(ForumSectionInfoTreeManage.class);
			//公司动态
			String category1 = "corp-news";
			List<DataRow> results1 = infomationManage.findNewsBySearchWord(category1,searchWord);
			JSONArray jsonArray1 = new JSONArray();
			for(int i=0;i<results1.size();i++){
				DataRow row = results1.get(i);
				JSONObject jsonObject1 = new JSONObject();
				jsonObject1.put("title", row.stringValue("INFO_TITLE"));
				jsonObject1.put("id", row.stringValue("INFO_ID"));
				jsonObject1.put("outLine", row.stringValue("INFO_OUTLINE"));
				jsonObject1.put("readCounts", String.valueOf(row.getInt("INFO_READ_COUNT")));
				String publishTime = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_HORIZONTAL, (Date)row.get("INFO_PUBLISH_TIME"));
				jsonObject1.put("publishTime", publishTime.substring(5));
				jsonObject1.put("reviewNums", String.valueOf(row.getInt("reviewNums")));
				jsonObject1.put("praiseNums", String.valueOf(row.getInt("praiseNums")));
				jsonObject1.put("favNums", String.valueOf(row.getInt("favNums")));
				jsonArray1.put(jsonObject1);
			}
			jsonObject.put("corpNews", jsonArray1);
			//业界资讯
			String category2 = "industry-news";
			List<DataRow> results2 = infomationManage.findNewsBySearchWord(category2,searchWord);
			JSONArray jsonArray2 = new JSONArray();
			for(int i=0;i<results2.size();i++){
				DataRow row = results2.get(i);
				JSONObject jsonObject2 = new JSONObject();
				jsonObject2.put("title", row.stringValue("INFO_TITLE"));
				jsonObject2.put("id", row.stringValue("INFO_ID"));
				jsonObject2.put("outLine", row.stringValue("INFO_OUTLINE"));
				jsonObject2.put("readCounts", String.valueOf(row.getInt("INFO_READ_COUNT")));
				String publishTime = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_HORIZONTAL, (Date)row.get("INFO_PUBLISH_TIME"));
				jsonObject2.put("publishTime", publishTime.substring(5));
				jsonObject2.put("reviewNums", String.valueOf(row.getInt("reviewNums")));
				jsonObject2.put("praiseNums", String.valueOf(row.getInt("praiseNums")));
				jsonObject2.put("favNums", String.valueOf(row.getInt("favNums")));
				jsonArray2.put(jsonObject2);
			}
			jsonObject.put("industryNews", jsonArray2);
			//技术集锦
			String category3 = "tech-trends";
			List<DataRow> results3 = infomationManage.findNewsBySearchWord(category3,searchWord);
			JSONArray jsonArray3 = new JSONArray();
			for(int i=0;i<results3.size();i++){
				DataRow row = results3.get(i);
				JSONObject jsonObject3 = new JSONObject();
				jsonObject3.put("title", row.stringValue("INFO_TITLE"));
				jsonObject3.put("id", row.stringValue("INFO_ID"));
				jsonObject3.put("outLine", row.stringValue("INFO_OUTLINE"));
				jsonObject3.put("readCounts", String.valueOf(row.getInt("INFO_READ_COUNT")));
				String publishTime = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_HORIZONTAL, (Date)row.get("INFO_PUBLISH_TIME"));
				jsonObject3.put("publishTime", publishTime.substring(5));
				jsonObject3.put("reviewNums", String.valueOf(row.getInt("reviewNums")));
				jsonObject3.put("praiseNums", String.valueOf(row.getInt("praiseNums")));
				jsonObject3.put("favNums", String.valueOf(row.getInt("favNums")));
				jsonArray3.put(jsonObject3);
			}
			jsonObject.put("techTrends", jsonArray3);
			//培训中心
			String category7 = "traincenter";
			List<DataRow> results7 = infomationManage.findNewsBySearchWord(category7,searchWord);
			JSONArray jsonArray7 = new JSONArray();
			for(int i=0;i<results7.size();i++){
				DataRow row = results7.get(i);
				JSONObject jsonObject7 = new JSONObject();
				jsonObject7.put("title", row.stringValue("INFO_TITLE"));
				jsonObject7.put("id", row.stringValue("INFO_ID"));
				jsonObject7.put("outLine", row.stringValue("INFO_OUTLINE"));
				jsonObject7.put("readCounts", String.valueOf(row.getInt("INFO_READ_COUNT")));
				String publishTime = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_HORIZONTAL, (Date)row.get("INFO_PUBLISH_TIME"));
				jsonObject7.put("publishTime", publishTime.substring(5));
				jsonObject7.put("reviewNums", String.valueOf(row.getInt("reviewNums")));
				jsonObject7.put("praiseNums", String.valueOf(row.getInt("praiseNums")));
				jsonObject7.put("favNums", String.valueOf(row.getInt("favNums")));
				jsonArray7.put(jsonObject7);
			}
			jsonObject.put("traincenter", jsonArray3);
			
			//集成平台
			String category4 = "platform";
			List<DataRow> results4 = forumSectionInfoTreeManage.getSectionFormPostInfos(category4,searchWord);
			JSONArray jsonArray4 = new JSONArray();
			for(int i=0;i<results4.size();i++){
				DataRow row = results4.get(i);
				JSONObject jsonObject4 = new JSONObject();
				jsonObject4.put("title", row.stringValue("FPM_TITLE"));
				jsonObject4.put("id", row.stringValue("FPM_ID"));
				jsonObject4.put("clickNum", String.valueOf(row.getInt("FPM_CLICK_NUMBER")));
				
				String createTime = row.stringValue("FPM_CREATE_TIME");
				createTime = DateUtil.getDateByType(DateUtil.YYMMDDHHMISS_HORIZONTAL, row.getTimestamp("FPM_CREATE_TIME"));
				createTime = createTime.substring(5, 16).replaceAll("/", "-");
				jsonObject4.put("createTime", createTime);
				jsonObject4.put("count", row.stringValue("FRI_ID_COUNT"));
				String type = "";
				if("ESSENCE_POSTS".equals(row.stringValue("FPM_TYPE"))){
					type = "essence";
				}else if("KEY_POSTS".equals(row.stringValue("FPM_TYPE"))){
					type = "key";
				}else if("ORDINARY_POSTS".equals(row.stringValue("FPM_TYPE"))){
					type = "ordinary";
				}
				jsonObject4.put("category", row.stringValue("FCI_CODE"));
				jsonObject4.put("type", type);
				jsonObject4.put("attentionCount", row.stringValue("FF_ID_COUNT"));//关注数
				jsonArray4.put(jsonObject4);
			}
			jsonObject.put("platform", jsonArray4);
			//应用平台
			String category5 = "application-platform";
			List<DataRow> results5 = forumSectionInfoTreeManage.getSectionFormPostInfos(category5,searchWord);
			JSONArray jsonArray5 = new JSONArray();
			for(int i=0;i<results5.size();i++){
				DataRow row = results5.get(i);
				JSONObject jsonObject5 = new JSONObject();
				jsonObject5.put("title", row.stringValue("FPM_TITLE"));
				jsonObject5.put("id", row.stringValue("FPM_ID"));
				jsonObject5.put("clickNum", String.valueOf(row.getInt("FPM_CLICK_NUMBER")));
				
				String createTime = row.stringValue("FPM_CREATE_TIME");
				createTime = DateUtil.getDateByType(DateUtil.YYMMDDHHMISS_HORIZONTAL, row.getTimestamp("FPM_CREATE_TIME"));
				createTime = createTime.substring(5, 16).replaceAll("/", "-");
				jsonObject5.put("createTime", createTime);
				jsonObject5.put("count", row.stringValue("FRI_ID_COUNT"));
				String type = "";
				if("ESSENCE_POSTS".equals(row.stringValue("FPM_TYPE"))){
					type = "essence";
				}else if("KEY_POSTS".equals(row.stringValue("FPM_TYPE"))){
					type = "key";
				}else if("ORDINARY_POSTS".equals(row.stringValue("FPM_TYPE"))){
					type = "ordinary";
				}
				jsonObject5.put("category", row.stringValue("FCI_CODE"));
				jsonObject5.put("type", type);
				jsonObject5.put("attentionCount", row.stringValue("FF_ID_COUNT"));//关注数
				jsonArray5.put(jsonObject5);
			}
			jsonObject.put("applicationplatform", jsonArray5);
			//应用平台
			String category6 = "support-platform";
			List<DataRow> results6 = forumSectionInfoTreeManage.getSectionFormPostInfos(category6,searchWord);
			JSONArray jsonArray6 = new JSONArray();
			for(int i=0;i<results6.size();i++){
				DataRow row = results6.get(i);
				JSONObject jsonObject6 = new JSONObject();
				jsonObject6.put("title", row.stringValue("FPM_TITLE"));
				jsonObject6.put("id", row.stringValue("FPM_ID"));
				jsonObject6.put("clickNum", String.valueOf(row.getInt("FPM_CLICK_NUMBER")));
				
				String createTime = row.stringValue("FPM_CREATE_TIME");
				createTime = DateUtil.getDateByType(DateUtil.YYMMDDHHMISS_HORIZONTAL, row.getTimestamp("FPM_CREATE_TIME"));
				createTime = createTime.substring(5, 16).replaceAll("/", "-");
				jsonObject6.put("createTime", createTime);
				jsonObject6.put("count", row.stringValue("FRI_ID_COUNT"));
				String type = "";
				if("ESSENCE_POSTS".equals(row.stringValue("FPM_TYPE"))){
					type = "essence";
				}else if("KEY_POSTS".equals(row.stringValue("FPM_TYPE"))){
					type = "key";
				}else if("ORDINARY_POSTS".equals(row.stringValue("FPM_TYPE"))){
					type = "ordinary";
				}
				jsonObject6.put("category", row.stringValue("FCI_CODE"));
				jsonObject6.put("type", type);
				jsonObject6.put("attentionCount", row.stringValue("FF_ID_COUNT"));//关注数
				jsonArray6.put(jsonObject6);
			}
			jsonObject.put("supportplatform", jsonArray6);
			result = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return result;
	}
	
}
