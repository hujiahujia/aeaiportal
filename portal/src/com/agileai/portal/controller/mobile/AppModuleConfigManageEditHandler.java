package com.agileai.portal.controller.mobile;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.mobile.AppModuleConfigManage;
import com.agileai.util.StringUtil;

public class AppModuleConfigManageEditHandler
        extends StandardEditHandler {
    public AppModuleConfigManageEditHandler() {
        super();
        this.listHandlerClass = AppModuleConfigManageListHandler.class;
        this.serviceId = buildServiceId(AppModuleConfigManage.class);
    }

    protected void processPageAttributes(DataParam param) {
        setAttribute("AC_GROUP",
                     FormSelectFactory.create("AW_GROUP")
                                      .addSelectedValue(getOperaAttributeValue("AC_GROUP",
                                                                               "default")));
        setAttribute("AC_VAILD",
                FormSelectFactory.create("SYS_VALID_TYPE")
                                 .addSelectedValue(getOperaAttributeValue("AC_VAILD",
                                                                          "1")));
        String operaType = param.get(OperaType.KEY);
        if(!OperaType.CREATE.equals(operaType)){
        	String currentTabId = (String)this.getSessionAttributes().get(AppModuleConfigManageEditHandler.class.getName()+"currentTabId");
    		if (StringUtil.isNullOrEmpty(currentTabId)){
    			currentTabId = "base";
    		}
    		this.setAttribute("currentTabId",currentTabId);
        }else{
        	this.setAttribute("currentTabId","base");
        }
		this.isInsertOpera(param);
    }
    
    private void isInsertOpera(DataParam param){
    	boolean result = false;
    	String operaType = param.get(OperaType.KEY);
    	if(StringUtil.isNotNullNotEmpty(operaType)){
    		if(OperaType.CREATE.equals(operaType)){
    			result = true;
    		}
    	}
    	this.setAttribute("isInsertOpera",result);
    }
    
    @PageAction
	public ViewRenderer focusTab(DataParam param){
		String responseText = FAIL;
		String currentTabId = param.get("currentTabId");
		this.getSessionAttributes().put(AppModuleConfigManageEditHandler.class.getName()+"currentTabId",currentTabId);
		responseText = SUCCESS;
		return new AjaxRenderer(responseText);
	}
    
    public ViewRenderer doSaveAction(DataParam param){
    	String responseText = FAIL;
    	try {
    		String operateType = param.get(OperaType.KEY);
    		if (OperaType.CREATE.equals(operateType)){
    			getService().createRecord(param);	
    		}
    		else if (OperaType.UPDATE.equals(operateType)){
    			getService().updateRecord(param);	
    		}
    		responseText = SUCCESS;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
    
    protected AppModuleConfigManage getService() {
        return (AppModuleConfigManage) this.lookupService(this.getServiceId());
    }
}
