package com.agileai.portal.controller.mobile;

import org.codehaus.jettison.json.JSONObject;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.SimpleHandler;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.mobile.ExtTaskManage;

public class MobileTaskListEditHandler extends SimpleHandler {
	public static String TODO = "TODO";
	public static String ONDO = "ONDO";
	public static String DID = "DID";
	private static String Y = "Y";
	public MobileTaskListEditHandler(){
		super();
	}
	
	@PageAction
	public ViewRenderer mobileSubmitTask(DataParam param){
		String responseText = FAIL;
		String inputString = this.getInputString();
		ExtTaskManage extTaskManage = this.lookupService(ExtTaskManage.class);
		try {
			JSONObject jsonObject = new JSONObject(inputString);
			String id = jsonObject.get("id").toString();
			String opinionContent = jsonObject.get("opinionContent").toString();
			String opinion = jsonObject.get("opinion").toString();
			if(Y.equals(opinion)){
				extTaskManage.updateTaskRecord(id,DID,opinionContent);
			}else{
				extTaskManage.updateTaskRecord(id,ONDO,opinionContent);
			}
			responseText = SUCCESS;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
}
