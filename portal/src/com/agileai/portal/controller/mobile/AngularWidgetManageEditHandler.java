package com.agileai.portal.controller.mobile;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.mobile.AngularWidgetManage;
import com.agileai.util.StringUtil;

public class AngularWidgetManageEditHandler
        extends StandardEditHandler {
    public AngularWidgetManageEditHandler() {
        super();
        this.listHandlerClass = AngularWidgetManageListHandler.class;
        this.serviceId = buildServiceId(AngularWidgetManage.class);
    }

    protected void processPageAttributes(DataParam param) {
        setAttribute("AW_GROUP",
                     FormSelectFactory.create("AW_GROUP")
                                      .addSelectedValue(getOperaAttributeValue("AW_GROUP",
                                                                               "default")));
    }
    
    @PageAction
    public ViewRenderer reload(DataParam param){
    	String responseText = FAIL;
    	try {
    		String widgetCode = param.get("AW_CODE");
    		String widgetId = param.get("AW_ID");
    		
    		MobileDataProviderHandler.removeCache(widgetCode);
    		if (StringUtil.isNotNullNotEmpty(widgetId)){
    			MobileDataProviderHandler.removeWidgetCache(widgetId);    			
    		}
    		responseText = SUCCESS;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
    	
    	return new AjaxRenderer(responseText);
    }

    protected AngularWidgetManage getService() {
        return (AngularWidgetManage) this.lookupService(this.getServiceId());
    }
}
