package com.agileai.portal.controller.mobile;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.SimpleHandler;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.forum.ForumConst;
import com.agileai.portal.bizmoduler.forum.UserLevel8BBSUserInfoManage;
import com.agileai.portal.bizmoduler.mobile.ExtTaskManage;
import com.agileai.util.DateUtil;

public class MobileTaskListDataProviderHandler extends SimpleHandler {
	public static String TODO = "TODO";
	public static String ONDO = "ONDO";
	public static String DID = "DID";
	public MobileTaskListDataProviderHandler(){
		super();
	}
	
	@PageAction
	public ViewRenderer findTaskLists(DataParam param){
		String responseText = null;
		try {
			ExtTaskManage extTaskManage = this.lookupService(ExtTaskManage.class);
			JSONObject jsonObject = new JSONObject();
			User user = (User) this.getUser();
			String userCode = user.getUserCode();
			//得到所有的记录
			List<DataRow> records = extTaskManage.findTaskRecords(userCode);
			List<DataRow> todoList = new ArrayList<DataRow>();
			List<DataRow> ondoList = new ArrayList<DataRow>();
			List<DataRow> didList = new ArrayList<DataRow>();
			//处理
			for(int i=0;i<records.size();i++ ){
				DataRow row = records.get(i);
				if(TODO.equals(row.getString("TASK_STATE"))){
					todoList.add(row);
				}else if(ONDO.equals(row.getString("TASK_STATE"))){
					ondoList.add(row);
				}else{
					didList.add(row);
				}
			}
			//待办
			String todoTaskcounts = String.valueOf(todoList.size());
			String state = ForumConst.TODO_STRING;
			JSONArray jsonArray1 = new JSONArray();
			for (int i = 0; i < todoList.size(); i++) {
				DataRow row = todoList.get(i);
				JSONObject jsonObject1 = new JSONObject();
				jsonObject1.put("id", row.getString("TASK_ID"));
				jsonObject1.put("title", row.getString("TASK_TITLE"));
				jsonObject1.put("person", user.getUserName());
				String opinionContent = row.getString("TASK_OPINION");
				if("".equals(opinionContent)){
					opinionContent = "无";
				}
				jsonObject1.put("opinionContent",opinionContent);
				String time = null;
				if(row.get("TASK_TIME")!=null){
					time = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_SLANTING, (Date)row.get("TASK_TIME"));
					time = time.substring(5);
				}
				jsonObject1.put("time",time);
				jsonObject1.put("state", state);
				jsonArray1.put(jsonObject1);
			}
			jsonObject.put("todoTasks",jsonArray1);
			jsonObject.put("todoTaskCount",todoTaskcounts);
			//在办
			String ondoTaskcounts = String.valueOf(ondoList.size());
			state = ForumConst.ONDO_STRING;
			JSONArray jsonArray2 = new JSONArray();
			for (int i = 0; i < ondoList.size(); i++) {
				DataRow row = ondoList.get(i);
				JSONObject jsonObject2 = new JSONObject();
				jsonObject2.put("id", row.getString("TASK_ID"));
				jsonObject2.put("title", row.getString("TASK_TITLE"));
				jsonObject2.put("person", user.getUserName());
				String opinionContent = row.getString("TASK_OPINION");
				if("".equals(opinionContent)){
					opinionContent = "无";
				}
				jsonObject2.put("opinionContent",opinionContent);
				String time = null;
				if(row.get("TASK_TIME")!=null){
					time = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_SLANTING, (Date)row.get("TASK_TIME"));
					time = time.substring(5);
				}
				jsonObject2.put("time",time);
				jsonObject2.put("state", state);
				jsonArray2.put(jsonObject2);
			}
			jsonObject.put("ondoTasks",jsonArray2);
			jsonObject.put("ondoTaskCount",ondoTaskcounts);
			//已办
			String didTaskcounts = String.valueOf(didList.size());
			state = ForumConst.DID_STRING;
			JSONArray jsonArray3 = new JSONArray();
			for (int i = 0; i < didList.size(); i++) {
				DataRow row = didList.get(i);
				JSONObject jsonObject3 = new JSONObject();
				jsonObject3.put("id", row.getString("TASK_ID"));
				jsonObject3.put("title", row.getString("TASK_TITLE"));
				jsonObject3.put("person", user.getUserName());
				String opinionContent = row.getString("TASK_OPINION");
				if("".equals(opinionContent)){
					opinionContent = "无";
				}
				jsonObject3.put("opinionContent",opinionContent);
				String time = null;
				if(row.get("TASK_TIME")!=null){
					time = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_SLANTING, (Date)row.get("TASK_TIME"));
					time = time.substring(5);
				}
				jsonObject3.put("time",time);
				jsonObject3.put("state", state);
				jsonArray3.put(jsonObject3);
			}
			jsonObject.put("didTasks",jsonArray3);
			jsonObject.put("didTaskCount",didTaskcounts);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer getTaskRecord(DataParam param){
		String responseText = null;
		try {
			User user = (User) this.getUser();
			String taskId = param.getString("ID");
			ExtTaskManage extTaskManage = this.lookupService(ExtTaskManage.class);
			UserLevel8BBSUserInfoManage userLevel8BBSUserInfoManage = lookupService(UserLevel8BBSUserInfoManage.class);
			DataRow record = extTaskManage.getTaskRecord(taskId);
			DataRow record1 = userLevel8BBSUserInfoManage.getCodeName("TASK_STATE",record.getString("TASK_STATE"));
			String state = record1.getString("CODE_NAME");
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("id", record.getString("TASK_ID"));
			jsonObject.put("title", record.getString("TASK_TITLE"));
			jsonObject.put("person", user.getUserName());
			jsonObject.put("money", record.get("TASK_MONEY"));
			jsonObject.put("name", user.getUserName());
			jsonObject.put("content", record.getString("TASK_CONTENT"));
			String opinionContent = record.getString("TASK_OPINION");
			if("".equals(opinionContent)){
				opinionContent = "无";
			}
			jsonObject.put("opinionContent",opinionContent);
			String time = null;
			if(record.get("TASK_TIME")!=null){
				time = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_SLANTING, (Date)record.get("TASK_TIME"));
				time = time.substring(5);
			}
			jsonObject.put("time",time);
			jsonObject.put("state", state);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
}
