package com.agileai.portal.controller.base;

import java.util.ArrayList;
import java.util.List;

import org.apache.pluto.container.PortletContainerException;
import org.apache.pluto.container.om.portlet.PortletApplicationDefinition;
import org.apache.pluto.container.om.portlet.PortletDefinition;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.common.ModuleManager;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.FormSelect;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.RedirectRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.base.PortletAppManage;
import com.agileai.portal.driver.container.PortletApplicationFactory;
import com.agileai.portal.driver.container.PortletContextManager;
import com.agileai.util.StringUtil;

public class PortletAppManageEditHandler extends StandardEditHandler{
	public PortletAppManageEditHandler(){
		super();
		this.listHandlerClass = PortletAppManageListHandler.class;
		this.serviceId = buildServiceId(PortletAppManage.class);
	}
	protected void processPageAttributes(DataParam param) {
		setAttribute("PA_GROUP",FormSelectFactory.create("PA_GROUP").addSelectedValue(getAttributeValue("PA_GROUP")));
		String applicationName = getAttributeValue("PA_CONTEXT");
		setAttribute("PA_CONTEXT",FormSelectFactory.create("PA_CONTEXT").addSelectedValue(applicationName));
		FormSelect paNameSelect = this.buildNameFormSelect(applicationName);
		String paName = getAttributeValue("PA_NAME");
		setAttribute("PA_NAME",paNameSelect.addSelectedValue(paName));
	}
	public ViewRenderer doRefreshContextAction(DataParam param){
		String applicationName = param.get("PA_CONTEXT");
		FormSelect paNameSelect = this.buildNameFormSelect(applicationName);
		String rspText = paNameSelect.getScriptSyntax("PA_NAME");
		return new AjaxRenderer(rspText);
	}
	private FormSelect buildNameFormSelect(String applicationName){
		FormSelect paNameSelect = new FormSelect();
		if (!StringUtil.isNullOrEmpty(applicationName)){
			try {
				PortletContextManager portletContextManager = (PortletContextManager)this.lookupService("PortletContextService");
				PortletApplicationDefinition pDefinition;
				pDefinition = portletContextManager.getPortletApplication(applicationName);
				
				List<String> portletNames = new ArrayList<String>();
				
				List<? extends PortletDefinition> portletDefinitions = pDefinition.getPortlets();
				for (int i=0;i < portletDefinitions.size();i++){
					PortletDefinition portletDefinition = portletDefinitions.get(i);
					String portletName = portletDefinition.getPortletName();
					portletNames.add(portletName);
					paNameSelect.addValue(portletName, portletName);
				}
				
				ClassLoader appClassLoader = PortletApplicationFactory.getAppClassLoader(applicationName);
				ModuleManager moduleManager = ModuleManager.getOnly(appClassLoader);
				List<String> portletIdList = moduleManager.getPortletIdList();
				for (int i=0;i < portletIdList.size();i++){
					String portletName = portletIdList.get(i);
					if (portletNames.contains(portletName)){
						continue;
					}
					paNameSelect.addValue(portletName, portletName);
				}
			} catch (PortletContainerException e) {
				e.printStackTrace();
			}
		}
		return paNameSelect;
	}
	public ViewRenderer doSaveAction(DataParam param){
		String operateType = param.get(OperaType.KEY);
		this.processParam(param);
		if (OperaType.CREATE.equals(operateType)){
			getService().createRecord(param);	
		}
		else if (OperaType.UPDATE.equals(operateType)){
			getService().updateRecord(param);
		}
		return new RedirectRenderer(getHandlerURL(listHandlerClass));
	}
	private void processParam(DataParam param){
		String paContext = param.get("PA_CONTEXT");
		String paName = param.get("PA_NAME");
		String paURL = paContext+"."+paName;
		param.put("PA_URL",paURL);
	}
	protected PortletAppManage getService() {
		return (PortletAppManage)this.lookupService(this.getServiceId());
	}
}
