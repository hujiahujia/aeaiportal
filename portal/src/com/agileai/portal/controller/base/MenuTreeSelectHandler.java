package com.agileai.portal.controller.base;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.TreeSelectHandler;
import com.agileai.hotweb.domain.TreeBuilder;
import com.agileai.hotweb.domain.TreeModel;
import com.agileai.portal.bizmoduler.base.MenuTreeSelect;

public class MenuTreeSelectHandler extends TreeSelectHandler{
	
	public MenuTreeSelectHandler(){
		super();
		this.serviceId = buildServiceId(MenuTreeSelect.class);
		this.isMuilSelect = false;
	}
	protected TreeBuilder provideTreeBuilder(DataParam param){
		List<DataRow> records = getService().queryPickTreeRecords(param);
		TreeBuilder treeBuilder = new TreeBuilder(records,"MENU_ID","MENU_NAME","MENU_PID");
		
		treeBuilder.setTypeKey("MENU_TYPE");
		
		String rootId = param.get("navId");
		treeBuilder.setRootId(rootId);
		
		String menuId = param.get("menuId");
		treeBuilder.getExcludeIds().add(menuId);
		
		return treeBuilder;
	}
	
	protected void buildTreeSyntax(StringBuffer treeSyntax,TreeModel treeModel){
		List<TreeModel> children = treeModel.getChildren();
		String parentId = treeModel.getId();
        for (int i=0;i < children.size();i++){
        	TreeModel child = children.get(i);
            String curNodeId = child.getId();
            String curNodeName = child.getName();
            if ("F".equals(child.getType())){
            	treeSyntax.append("d.add('"+curNodeId+"','"+parentId+"','"+curNodeName+"',\"javascript:setSelectTempValue('"+curNodeId+"','"+curNodeName+"')\",null,null,d.icon.folder,d.icon.folder.folderOpen,null);").append(newRow);
            }else{
            	continue;
            	//treeSyntax.append("d.add('"+curNodeId+"','"+parentId+"','"+curNodeName+"',\"javascript:setSelectTempValue('"+curNodeId+"','"+curNodeName+"')\");").append(newRow);	
            }
            this.buildTreeSyntax(treeSyntax,child);
        }
    }  
	
	protected MenuTreeSelect getService() {
		return (MenuTreeSelect)this.lookupService(this.getServiceId());
	}
}	
