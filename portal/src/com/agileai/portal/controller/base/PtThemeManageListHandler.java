package com.agileai.portal.controller.base;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.portal.bizmoduler.base.PtThemeManage;

public class PtThemeManageListHandler extends StandardListHandler{
	public PtThemeManageListHandler(){
		super();
		this.editHandlerClazz = PtThemeManageEditHandler.class;
		this.serviceId = buildServiceId(PtThemeManage.class);
	}
	protected void processPageAttributes(DataParam param) {
		setAttribute("themeType",FormSelectFactory.create("THEME_TYPE").addSelectedValue(param.get("themeType")));
		setAttribute("personalType",FormSelectFactory.create("BOOL_DEFINE").addSelectedValue(param.get("personalType")));
		
		initMappingItem("THEME_TYPE",FormSelectFactory.create("THEME_TYPE").getContent());		
		initMappingItem("PERSONALABLE",FormSelectFactory.create("BOOL_DEFINE").getContent());
	}
	protected void initParameters(DataParam param) {
		initParamItem(param,"themeName",""); 
		initParamItem(param,"themeType","");
		initParamItem(param,"personalType","");
	}
	protected PtThemeManage getService() {
		return (PtThemeManage)this.lookupService(this.getServiceId());
	}
}
