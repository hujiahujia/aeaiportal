package com.agileai.portal.controller.base;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.domain.FormSelect;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.base.MenuBarManage;
import com.agileai.portal.driver.model.MenuItem;
import com.agileai.util.StringUtil;

public class MenuLinkCreateFormHandler extends BaseHandler {
	public MenuLinkCreateFormHandler(){
		this.serviceId = buildServiceId(MenuBarManage.class);
	}
	public ViewRenderer prepareDisplay(DataParam param){
		this.setAttributes(param);
		String visiable = this.getAttributeValue("MENU_VISIABLE");
		if (StringUtil.isNullOrEmpty(visiable)){
			visiable = MenuItem.Visiable.visable;
		}
		FormSelect visiableSelect = FormSelectFactory.create("MENU_VISIABLE");
		this.setAttribute("MENU_VISIABLE", visiableSelect.addSelectedValue(visiable));
		return new LocalRenderer(this.getPage());
	}
	public ViewRenderer doSaveAction(DataParam param){
		String navId = param.get("NAV_ID");
		String menuCode = param.get("CHILD_MENU_CODE");
		boolean isCodeDuplicate = getService().isCodeDuplicate(navId,"", menuCode);
		if (isCodeDuplicate){
			return new AjaxRenderer("duplicate");
		}else{
			getService().insertChildRecord(param);
			String newMenuId = param.get("MENU_ID");
			return new AjaxRenderer(newMenuId);			
		}
	}
	protected MenuBarManage getService(){
		return (MenuBarManage)this.lookupService(serviceId);
	}	
}
