package com.agileai.portal.controller.base;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.bizmoduler.core.TreeManage;
import com.agileai.hotweb.domain.FormSelect;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.TreeBuilder;
import com.agileai.hotweb.domain.TreeModel;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.base.MenuBarManage;
import com.agileai.portal.bizmoduler.base.NavigaterManage;
import com.agileai.portal.bizmoduler.base.PtThemeManage;
import com.agileai.portal.controller.mobile.AppModuleConfigManageEditHandler;
import com.agileai.portal.driver.AttributeKeys;
import com.agileai.portal.driver.model.MenuBar;
import com.agileai.portal.driver.model.MenuItem;
import com.agileai.portal.driver.model.Theme;
import com.agileai.portal.driver.service.DriverConfiguration;
import com.agileai.portal.driver.service.PortalConfigService;
import com.agileai.util.ListUtil;
import com.agileai.util.StringUtil;

public class MobileMenuBarManageHandler extends MenuBarManageHandler {
	private static String ISMODAL = "isModal";
	private static String ISPAGE = "isPage";
	private static String ISFRAME = "isFrame";
	private static String ISTAB = "isTab";
	private static String ISPOPOVER = "isPopover";
	private static String ISPOPUP = "isPopup";
	public static String ADD_MODAL_IDS = "addModalIds";
	public static String ADD_POPOVER_IDS = "addPopoverIds";
	public static String ADD_POPUP_IDS = "addPopupIds";
	public MobileMenuBarManageHandler(){
		super();
		this.serviceId = buildServiceId(MenuBarManage.class);
		this.nodeIdField = "MENU_ID";
	}
	
	public ViewRenderer prepareDisplay(DataParam param) {
		this.getSessionAttributes().remove(AppModuleConfigManageEditHandler.class.getName()+"ADD_MODAL_IDS");
		this.getSessionAttributes().remove(AppModuleConfigManageEditHandler.class.getName()+"ADD_POPOVER_IDS");
		this.getSessionAttributes().remove(AppModuleConfigManageEditHandler.class.getName()+"ADD_POPUP_IDS");
		this.getSessionAttributes().clear();
		String nodeId = param.get(this.nodeIdField);
		if (StringUtil.isNullOrEmpty(nodeId)){
			nodeId = provideDefaultNodeId(param);
		}
		TreeBuilder treeBuilder = provideTreeBuilder(param);
		TreeModel treeModel = treeBuilder.buildTreeModel();
		
		TreeManage service = this.getService();
		DataParam queryParam = new DataParam(this.nodeIdField,nodeId);
		DataRow record = service.queryCurrentRecord(queryParam);
		if(null != record){
			String menuDesc = record.getString("MENU_DESC");
			if(null != menuDesc){
				try {
					JSONObject jsonObject = new JSONObject(menuDesc);
					if(menuDesc.contains(ISMODAL)){
						this.setAttribute("MOBILE_PAGE_PRO",ISMODAL);	
					}else if(menuDesc.contains(ISPAGE)){
						this.setAttribute("MOBILE_PAGE_PRO",ISPAGE);	
					}else if(menuDesc.contains(ISFRAME)){
						this.setAttribute("MOBILE_PAGE_PRO",ISFRAME);	
					}else if(menuDesc.contains(ISTAB)){
						this.setAttribute("MOBILE_PAGE_PRO",ISTAB);	
					}else if(menuDesc.contains(ISPOPOVER)){
						this.setAttribute("MOBILE_PAGE_PRO",ISPOPOVER);	
					}else if(menuDesc.contains(ISPOPUP)){
						this.setAttribute("MOBILE_PAGE_PRO",ISPOPUP);	
					}
					
					if(menuDesc.contains("title")){
						String title = jsonObject.get("title").toString();
						this.setAttribute("title",title);	
					}
					if(menuDesc.contains("dependsModules")){
						String dependsModules = jsonObject.get("dependsModules").toString();
						dependsModules = dependsModules.replaceAll("'","");
						this.setAttribute("dependsModules",dependsModules);	
					}
					if(menuDesc.contains("icon")){
						String icon = jsonObject.get("icon").toString();
						icon = icon.replaceAll("'", "");
						this.setAttribute("icon",icon);	
					}
					if(menuDesc.contains("enableIconOnOff")){
						String enableIconOnOff = jsonObject.get("enableIconOnOff").toString();
						enableIconOnOff = enableIconOnOff.replaceAll("'", "");
						this.setAttribute("enableIconOnOff",enableIconOnOff);	
					}
					if(menuDesc.contains("personal")){
						String personal = jsonObject.get("personal").toString();
						personal = personal.replaceAll("'", "");
						this.setAttribute("personal",personal);	
					}
					if(menuDesc.contains("refresher")){
						String refresher = jsonObject.get("refresher").toString();
						refresher = refresher.replaceAll("'", "");
						this.setAttribute("refresher",refresher);	
					}
					if(menuDesc.contains("reloader")){
						String reloader = jsonObject.get("reloader").toString();
						reloader = reloader.replaceAll("'", "");
						this.setAttribute("reloader",reloader);	
					}
					if(menuDesc.contains("pageCtrlFile")){
						String pageCtrlFile = jsonObject.get("pageCtrlFile").toString();
						pageCtrlFile = pageCtrlFile.replaceAll("'", "");
						this.setAttribute("pageCtrlFile",pageCtrlFile);	
					}
					if(menuDesc.contains("pageTmplFile")){
						String pageTmplFile = jsonObject.get("pageTmplFile").toString();
						pageTmplFile = pageTmplFile.replaceAll("'", "");
						this.setAttribute("pageTmplFile",pageTmplFile);	
					}
					if(menuDesc.contains("stateURL")){
						String stateURL = jsonObject.get("stateURL").toString();
						stateURL = stateURL.replaceAll("'", "");
						this.setAttribute("stateURL",stateURL);	
					}
					if(menuDesc.contains("stateView")){
						String stateView = jsonObject.get("stateView").toString();
						stateView = stateView.replaceAll("'", "");
						this.setAttribute("stateView",stateView);	
					}
					if(menuDesc.contains("customeHeader")){
						String customeHeader = jsonObject.get("customeHeader").toString();
						customeHeader = customeHeader.replaceAll("'", "");
						this.setAttribute("customeHeader",customeHeader);	
					}
					if(menuDesc.contains("hasFooter")){
						String hasFooter = jsonObject.get("hasFooter").toString();
						hasFooter = hasFooter.replaceAll("'", "");
						this.setAttribute("hasFooter",hasFooter);	
					}
					String navId = param.get("NAV_ID");
					if(menuDesc.contains("modals")){
						String modals = jsonObject.get("modals").toString();
						this.setAttribute("modals",modals);	
						List<String> modalsList = new ArrayList<String>();
						MobileMenuBarManageHandler.addArrayToList(modalsList, modals.split(","));
						List<String> modalCodesList = new ArrayList<String>();
						List<DataRow> modalNamesList = new ArrayList<DataRow>();
						
						for(int i=0;i<modalsList.size();i++){
							String modalName = modalsList.get(i);
							modalName = modalName.substring(1, modalName.length()-1);
							modalName = modalName.replaceAll("'", "");
							modalCodesList.add(modalName);
						}
						
						List<DataRow> menuCodeList = getService().findMenuInfos(modalCodesList,navId);
						for(int i=0;i<menuCodeList.size();i++){
							DataRow row = menuCodeList.get(i);
							String modalName = row.getString("MENU_NAME");
							String modalCode = row.getString("MENU_CODE");
							row.put("modalCode", modalCode);
							row.put("modalName", modalName);
							modalNamesList.add(row);
						}
						
						FormSelect modalSelect = new FormSelect();
						modalSelect.setKeyColumnName("modalCode");
						modalSelect.setValueColumnName("modalName");
						modalSelect.addHasBlankValue(false);
						modalSelect.putValues(modalNamesList);
						this.setAttribute("modalList",modalSelect);	
						
					}
					if(menuDesc.contains("popovers")){
						String popovers = jsonObject.get("popovers").toString();
						this.setAttribute("popovers",popovers);	
						List<String> popoversList = new ArrayList<String>();
						MobileMenuBarManageHandler.addArrayToList(popoversList, popovers.split(","));
						List<String> popoversCodesList = new ArrayList<String>();
						List<DataRow> popoversNamesList = new ArrayList<DataRow>();
						
						for(int i=0;i<popoversList.size();i++){
							String popoverName = popoversList.get(i);
							popoverName = popoverName.substring(1, popoverName.length()-1);
							popoverName = popoverName.replaceAll("'", "");
							popoversCodesList.add(popoverName);
						}
						
						List<DataRow> popoverCodeList = getService().findMenuInfos(popoversCodesList,navId);
						for(int i=0;i<popoverCodeList.size();i++){
							DataRow row = popoverCodeList.get(i);
							String popoverName = row.getString("MENU_NAME");
							String popoverCode = row.getString("MENU_CODE");
							row.put("popoverCode", popoverCode);
							row.put("popoverName", popoverName);
							popoversNamesList.add(row);
						}
						
						FormSelect popoversSelect = new FormSelect();
						popoversSelect.setKeyColumnName("popoverCode");
						popoversSelect.setValueColumnName("popoverName");
						popoversSelect.addHasBlankValue(false);
						popoversSelect.putValues(popoversNamesList);
						this.setAttribute("popoverList",popoversSelect);	
					}
					if(menuDesc.contains("popups")){
						String popups = jsonObject.get("popups").toString();
						this.setAttribute("popups",popups);	
						List<String> popupsList = new ArrayList<String>();
						MobileMenuBarManageHandler.addArrayToList(popupsList, popups.split(","));
						List<String> popupsCodesList = new ArrayList<String>();
						List<DataRow> popupsNamesList = new ArrayList<DataRow>();
						
						for(int i=0;i<popupsList.size();i++){
							String popupName = popupsList.get(i);
							popupName = popupName.substring(1, popupName.length()-1);
							popupName = popupName.replaceAll("'", "");
							popupsCodesList.add(popupName);
						}
						
						List<DataRow> menuCodeList = getService().findMenuInfos(popupsCodesList,navId);
						for(int i=0;i<menuCodeList.size();i++){
							DataRow row = menuCodeList.get(i);
							String popupName = row.getString("MENU_NAME");
							String popupCode = row.getString("MENU_CODE");
							row.put("popupName", popupName);
							row.put("popupCode", popupCode);
							popupsNamesList.add(row);
						}
						
						FormSelect popupsSelect = new FormSelect();
						popupsSelect.setKeyColumnName("popupCode");
						popupsSelect.setValueColumnName("popupName");
						popupsSelect.addHasBlankValue(false);
						popupsSelect.putValues(popupsNamesList);
						this.setAttribute("popupList",popupsSelect);
					}
					
				} catch (JSONException e) {
					log.error(e.getLocalizedMessage(), e);
				}
				
			}
		}
		this.setAttributes(record);	
		this.processPageAttributes(param);
		this.setAttribute(this.nodeIdField, nodeId);
		this.setAttribute("isRootNode",String.valueOf(isRootNode(param)));
		
		String menuTreeSyntax = this.getTreeSyntax(param,treeModel,new StringBuffer());
		this.setAttribute("menuTreeSyntax", menuTreeSyntax);
		return new LocalRenderer(getPage());
	}
	
	private PortalConfigService getPortalConfigService(ServletContext servletContext){
		DriverConfiguration driverConfiguration = (DriverConfiguration)servletContext.getAttribute(AttributeKeys.DRIVER_CONFIG);
		return driverConfiguration.getPortalConfigService();
	}
	
	protected void processPageAttributes(DataParam param) {
		
		TreeBuilder treeBuilder = (TreeBuilder)this.getAttribute("treeBuilder");
		TreeModel treeModel = treeBuilder.getRootTreeModel();
		
		if (StringUtil.isNullOrEmpty(this.getAttributeValue("MENU_NAME"))){
			String menuBarId = treeModel.getId();
			PortalConfigService portalConfigService = getPortalConfigService(this.getDispatchServlet().getServletContext());
			MenuBar menuBar = portalConfigService.getMenuBar(menuBarId);
			this.setAttribute("MENU_NAME", treeModel.getName());
			this.setAttribute("MENU_CODE", treeModel.getId());
			this.setAttribute("MENU_THEME", menuBar.getThemeId());
			this.setAttribute("MENU_TYPE", treeModel.getType());
			this.setAttribute("MENU_VISIABLE","Y");
		}
		
		this.setAttribute(param, new String[]{"NAV_ID"});
		FormSelect formSelect = FormSelectFactory.create("MENU_TYPE");
		this.setAttribute("MENU_TYPE", formSelect.addSelectedValue(this.getAttributeValue("MENU_TYPE")));
		
		String navId = param.get("NAV_ID");
		NavigaterManage navigaterManage = this.lookupService(NavigaterManage.class);
		DataParam queryParam = new DataParam("NAV_ID",navId);
		DataRow row = navigaterManage.getRecord(queryParam);
		String naviterType = row.stringValue("NAV_TYPE");
		FormSelect themeSelect = new FormSelect();
		themeSelect.setKeyColumnName("THEME_ID");
		themeSelect.setValueColumnName("THEME_NAME");
		PtThemeManage themeManage = this.lookupService(PtThemeManage.class);
		if (MenuBar.Type.LOGIN_BEFORE.equals(naviterType)){
			queryParam = new DataParam("themeType",MenuBar.Type.LOGIN_BEFORE);
		}else if (MenuBar.Type.LOGIN_AFTER.equals(naviterType)){
			queryParam = new DataParam("themeType",MenuBar.Type.LOGIN_AFTER);
		}
		this.setAttribute("isLoginBeforeMenuBar", String.valueOf(MenuBar.Type.LOGIN_BEFORE.equals(naviterType)));
		List<DataRow> records = themeManage.findRecords(queryParam);
		themeSelect.putValue(Theme.EXTEND_THEME_ID, Theme.EXTEND_THEME_NAME);
		themeSelect.putValues(records);
		
		String selectedTheme = this.getAttributeValue("MENU_THEME");
		this.setAttribute("MENU_THEME", themeSelect.addSelectedValue(selectedTheme));
		
		String visiable = this.getAttributeValue("MENU_VISIABLE");
		if (StringUtil.isNullOrEmpty(visiable)){
			visiable = MenuItem.Visiable.visable;
		}
		FormSelect visiableSelect = FormSelectFactory.create("MENU_VISIABLE");
		this.setAttribute("MENU_VISIABLE", visiableSelect.addSelectedValue(visiable));
		
		String target = this.getAttributeValue("MENU_TARGET");
		if (StringUtil.isNullOrEmpty(target)){
			target = MenuItem.Target.SELF;
		}
		FormSelect targetSelect = FormSelectFactory.create("MENU_TARGET");
		this.setAttribute("MENU_TARGET", targetSelect.addSelectedValue(target));
		
		String personalable = this.getAttributeValue("IS_PERSONAL","N");
		FormSelect personalableSelect = FormSelectFactory.create("BOOL_DEFINE");
		this.setAttribute("IS_PERSONAL", personalableSelect.addSelectedValue(personalable));
		
		String pagePropre = this.getAttributeValue("MOBILE_PAGE_PRO",ISPAGE);
		FormSelect pagePropreSelect = FormSelectFactory.create("MOBILE_PAGE_PRO");
		this.setAttribute("MOBILE_PAGE_PRO", pagePropreSelect.addSelectedValue(pagePropre));
		
		String customeHeader = this.getAttributeValue("customeHeader","false");
		FormSelect customeHeaderSelect = FormSelectFactory.create("LOGICAL_DEFINE");
		this.setAttribute("customeHeader", customeHeaderSelect.addSelectedValue(customeHeader));
		
		String hasFooter = this.getAttributeValue("hasFooter","false");
		FormSelect hasFooterSelect = FormSelectFactory.create("LOGICAL_DEFINE");
		this.setAttribute("hasFooter", hasFooterSelect.addSelectedValue(hasFooter));
		
		String enableIconOnOff = this.getAttributeValue("enableIconOnOff","false");
		FormSelect enableIconOnOffSelect = FormSelectFactory.create("LOGICAL_DEFINE");
		this.setAttribute("enableIconOnOff", enableIconOnOffSelect.addSelectedValue(enableIconOnOff));
		
		String personal = this.getAttributeValue("personal","false");
		FormSelect personalSelect = FormSelectFactory.create("LOGICAL_DEFINE");
		this.setAttribute("personal", personalSelect.addSelectedValue(personal));
		
		String refresher = this.getAttributeValue("refresher","false");
		FormSelect refresherSelect = FormSelectFactory.create("LOGICAL_DEFINE");
		this.setAttribute("refresher", refresherSelect.addSelectedValue(refresher));
		
		String reloader = this.getAttributeValue("reloader","false");
		FormSelect reloaderSelect = FormSelectFactory.create("LOGICAL_DEFINE");
		this.setAttribute("reloader", reloaderSelect.addSelectedValue(reloader));
		
		String mainLayout = getAttributeValue("MAIN_LAYOUT");
		if (StringUtil.isNullOrEmpty(mainLayout)){
			mainLayout = "R";
		}
		setAttribute("MAIN_LAYOUT",FormSelectFactory.create("LAYOUT_TYPE").addSelectedValue(mainLayout));
		
		String currentTabId = (String)this.getSessionAttributes().get(MenuBarManageHandler.class.getName()+"currentTabId");
		if (StringUtil.isNullOrEmpty(currentTabId)){
			currentTabId = "base";
		}
		this.setAttribute("currentTabId",currentTabId);
	}
	
	public ViewRenderer doSaveAction(DataParam param){
		String pagePro = param.get("MOBILE_PAGE_PRO");
		String menuDesc = "";
		String dependsModules ="";
		String modals ="";
		String popups ="";
		String popovers ="";
		String customeHeader = param.get("customeHeader");
		String hasFooter = param.get("hasFooter");
		String icon = param.get("icon");
		String enableIconOnOff = param.get("enableIconOnOff");
		String title = param.get("title");
		String stateURL = param.get("stateURL");
		String stateView = param.get("stateView");
		String pageCtrlFile = param.get("pageCtrlFile");
		String pageTmplFile = param.get("pageTmplFile");
		String personal = param.get("personal");
		String reloader = param.get("reloader");
		String refresher = param.get("refresher");
		
		if(param.get("dependsModules")!= null && !param.get("dependsModules").isEmpty()){
			List<String> dependsModulesList = new ArrayList<String>();
			MobileMenuBarManageHandler.addArrayToList(dependsModulesList, param.get("dependsModules").split(","));
			for(int i=0;i<dependsModulesList.size();i++){
				if(i == dependsModulesList.size()-1){
					dependsModules = dependsModules + dependsModulesList.get(i);
				}else{
					dependsModules = dependsModules + dependsModulesList.get(i)+",";
				}
			}
		}
		List<String> modalsList = new ArrayList<String>();
		if(param.get("modals") != null && !param.get("modals").isEmpty()){
			modals = this.modulesCodesSplit(modalsList, param.get("modals"));
		}
		String addModalIds = (String)this.getSessionAttributes().get(AppModuleConfigManageEditHandler.class.getName()+"addModalIds");
		if(addModalIds!= null && !addModalIds.isEmpty()){
			String moduleCodes = this.modulesListSplit(modals,modalsList.size(),addModalIds);
			modals = moduleCodes;
		}else if(param.get("addModalIds")!= null && !param.get("addModalIds").isEmpty()){
			String moduleCodes = this.modulesListSplit(modals,modalsList.size(),param.get("addModalIds"));
			modals = moduleCodes;
		}
		
		List<String> popupsList = new ArrayList<String>();
		if(param.get("popups")!= null && !param.get("popups").isEmpty()){
			popups = this.modulesCodesSplit(popupsList, param.get("popups"));
		}
		
		String addPopupIds = (String)this.getSessionAttributes().get(AppModuleConfigManageEditHandler.class.getName()+"addPopupIds");
		if(addPopupIds!= null && !addPopupIds.isEmpty()){
			String moduleCodes = this.modulesListSplit(popups,popupsList.size(),addPopupIds);
			popups = moduleCodes;
		}else if(param.get("addPopupIds")!= null && !param.get("addPopupIds").isEmpty()){
			String moduleCodes = this.modulesListSplit(popups,popupsList.size(),param.get("addPopupIds"));
			popups = moduleCodes;
		}
		
		List<String> popoversList = new ArrayList<String>();
		if(param.get("popovers")!= null && !param.get("popovers").isEmpty()){
			popovers = this.modulesCodesSplit(popoversList, param.get("popovers"));
		}
		
		String addPopoverIds = (String)this.getSessionAttributes().get(AppModuleConfigManageEditHandler.class.getName()+"addPopoverIds");
		if(addPopoverIds!= null && !addPopoverIds.isEmpty()){
			String moduleCodes = this.modulesListSplit(popovers,popoversList.size(),addPopoverIds);
			popovers = moduleCodes;
		}else if(param.get("addPopoverIds")!= null && !param.get("addPopoverIds").isEmpty()){
			String moduleCodes = this.modulesListSplit(popovers,popoversList.size(),param.get("addPopoverIds"));
			popovers = moduleCodes;
		}
		
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put(pagePro, true);
			if(ISFRAME.equals(pagePro)){
				if(StringUtil.isNotNullNotEmpty(dependsModules)){
					jsonObject.put("dependsModules", dependsModules);
				}
			}else if(ISTAB.equals(pagePro)){
				if(StringUtil.isNotNullNotEmpty(dependsModules)){
					jsonObject.put("dependsModules", dependsModules);
				}
				if(StringUtil.isNotNullNotEmpty(reloader)){
					if("true".equals(reloader)){
						jsonObject.put("reloader", true);
					}else{
						jsonObject.put("reloader", false);
					}
					
				}
				if(StringUtil.isNotNullNotEmpty(refresher)){
					if("true".equals(refresher)){
						jsonObject.put("refresher", true);
					}else{
						jsonObject.put("refresher", false);
					}
				}
				if(StringUtil.isNotNullNotEmpty(icon)){
					jsonObject.put("icon", icon);
				}
				if(StringUtil.isNotNullNotEmpty(enableIconOnOff)){
					jsonObject.put("enableIconOnOff", enableIconOnOff);
				}
				if(StringUtil.isNotNullNotEmpty(modals)){
					jsonObject.put("modals", modals);
				}
				if(StringUtil.isNotNullNotEmpty(popups)){
					jsonObject.put("popups", popups);
				}
				if(StringUtil.isNotNullNotEmpty(popovers)){
					jsonObject.put("popovers", popovers);
				}
				if(StringUtil.isNotNullNotEmpty(personal)){
					jsonObject.put("personal", personal);
				}
				if(StringUtil.isNotNullNotEmpty(pageCtrlFile)){
					jsonObject.put("pageCtrlFile", pageCtrlFile);
				}
				if(StringUtil.isNotNullNotEmpty(pageTmplFile)){
					jsonObject.put("pageTmplFile", pageTmplFile);
				}
				
			}else if(ISMODAL.equals(pagePro)){
				if(StringUtil.isNotNullNotEmpty(customeHeader)){
					jsonObject.put("customeHeader", customeHeader);
				}
				if(StringUtil.isNotNullNotEmpty(hasFooter)){
					jsonObject.put("hasFooter", hasFooter);
				}
				if(StringUtil.isNotNullNotEmpty(title)){
					jsonObject.put("title", title);
				}
				if(StringUtil.isNotNullNotEmpty(dependsModules)){
					jsonObject.put("dependsModules", dependsModules);
				}
			}else if(ISPAGE.equals(pagePro)){
				if(StringUtil.isNotNullNotEmpty(reloader)){
					if("true".equals(reloader)){
						jsonObject.put("reloader", true);
					}else{
						jsonObject.put("reloader", false);
					}
					
				}
				if(StringUtil.isNotNullNotEmpty(refresher)){
					if("true".equals(refresher)){
						jsonObject.put("refresher", true);
					}else{
						jsonObject.put("refresher", false);
					}
				}
				if(StringUtil.isNotNullNotEmpty(stateURL)){
					jsonObject.put("stateURL", stateURL);
				}
				if(StringUtil.isNotNullNotEmpty(stateView)){
					jsonObject.put("stateView", stateView);
				}
				if(StringUtil.isNotNullNotEmpty(icon)){
					jsonObject.put("icon", icon);
				}
				if(StringUtil.isNotNullNotEmpty(enableIconOnOff)){
					jsonObject.put("enableIconOnOff", enableIconOnOff);
				}
				if(StringUtil.isNotNullNotEmpty(dependsModules)){
					jsonObject.put("dependsModules", dependsModules);
				}
				if(StringUtil.isNotNullNotEmpty(title)){
					jsonObject.put("title", title);
				}
				if(StringUtil.isNotNullNotEmpty(modals)){
					jsonObject.put("modals", modals);
				}
				if(StringUtil.isNotNullNotEmpty(popups)){
					jsonObject.put("popups", popups);
				}
				if(StringUtil.isNotNullNotEmpty(popovers)){
					jsonObject.put("popovers", popovers);
				}
				if(StringUtil.isNotNullNotEmpty(pageCtrlFile)){
					jsonObject.put("pageCtrlFile", pageCtrlFile);
				}
				if(StringUtil.isNotNullNotEmpty(pageTmplFile)){
					jsonObject.put("pageTmplFile", pageTmplFile);
				}
				
			}else if(ISPOPOVER.equals(pagePro)){
				if(StringUtil.isNotNullNotEmpty(dependsModules)){
					jsonObject.put("dependsModules", dependsModules);
				}
				
			}else if(ISPOPUP.equals(pagePro)){
				if(StringUtil.isNotNullNotEmpty(dependsModules)){
					jsonObject.put("dependsModules", dependsModules);
				}
			}
			
			menuDesc = jsonObject.toString();
			param.put("MENU_DESC",menuDesc);
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		
		return super.doSaveAction(param);	
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static List addArrayToList(List list,Object[] objects){
		List temp = arrayToList(objects);
		list.addAll(temp);
		return list;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static List arrayToList(Object[] objects){
		List list = new ArrayList();
		for (int i=0;i < objects.length;i++){
			String object = (String)"'"+ objects[i]+"'";
			list.add(object);
		}
		return list;
	}
	
	public String modulesCodesSplit(List<String> modulesList,String modules){
		String moduleCodes = "";
		MobileMenuBarManageHandler.addArrayToList(modulesList, modules.split(","));
		for(int i=0;i<modulesList.size();i++){
			if(i == modulesList.size()-1){
				String modal = modulesList.get(i).replaceAll("'", "");
				if(!modal.isEmpty()){
					moduleCodes = moduleCodes + modal;
				}else{
					continue;
				}
			}else{
				String modal = modulesList.get(i).replaceAll("'", "");
				if(!modal.isEmpty()){
					moduleCodes = moduleCodes + modal +",";
				}else{
					continue;
				}
			}
		}
		
		return moduleCodes;
    }
	
	 public String modulesListSplit(String modals,int size,String modules){
		 String moduleCodes = "";
		 List<String> addCodesList = new ArrayList<String>();
			MobileMenuBarManageHandler.addArrayToList(addCodesList, modules.split(","));
			if(size > 0){
				moduleCodes = modals +",";
			}
			for(int i=0;i<addCodesList.size();i++){
				if(i == addCodesList.size()-1){
					String modal = addCodesList.get(i).replaceAll("'", "");
					if(!modal.isEmpty()){
						moduleCodes = moduleCodes + modal;
					}else{
						continue;
					}
				}else{
					String modal = addCodesList.get(i).replaceAll("'", "");
					if(!modal.isEmpty()){
						moduleCodes = moduleCodes + modal+",";
					}else{
						continue;
					}
				}
			}
			
			return moduleCodes;
	    }    
	
	@PageAction
	public ViewRenderer addModalTreeInfos(DataParam param){
		String responseText = "";
		String addModalIds = param.get("addModalIds");
		String navId = param.get("NAV_ID");
		
		List<String> addModalIdsList = new ArrayList<String>();
		ListUtil.addArrayToList(addModalIdsList, addModalIds.split(","));
		
		String hasAddModalIds = (String)this.getSessionAttributes().get(AppModuleConfigManageEditHandler.class.getName()+"addModalIds");
		if(hasAddModalIds != null){
			addModalIds = hasAddModalIds + "," + addModalIds;
		}
		
		this.getSessionAttributes().put(AppModuleConfigManageEditHandler.class.getName()+"addModalIds",addModalIds);//选中添加的模态Codes放入缓存中
		
		List<DataRow> modalRowList = new ArrayList<DataRow>();
		
		List<DataRow> menuCodeList = getService().findMenuInfos(addModalIdsList,navId);
		for(int i=0;i<menuCodeList.size();i++){
			DataRow row = menuCodeList.get(i);
			String modalName = row.getString("MENU_NAME");
			String modalCode = row.getString("MENU_CODE");
			row.put("modalName", modalName);
			row.put("modalCode", modalCode);
			modalRowList.add(row);
		}
		
		FormSelect modalSelect = new FormSelect();
		modalSelect.setKeyColumnName("modalCode");
		modalSelect.setValueColumnName("modalName");
		modalSelect.addHasBlankValue(false);
		modalSelect.putValues(modalRowList);
		
		responseText = String.valueOf(modalSelect);
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer addPopoverTreeInfos(DataParam param){
		String responseText = "";
		String addPopoverIds = param.get("addPopoverIds");
		String navId = param.get("NAV_ID");
		
		List<String> addPopoverIdsList = new ArrayList<String>();
		ListUtil.addArrayToList(addPopoverIdsList, addPopoverIds.split(","));
		
		String hasAddPopoverIds = (String)this.getSessionAttributes().get(AppModuleConfigManageEditHandler.class.getName()+"addPopoverIds");
		if(hasAddPopoverIds != null){
			addPopoverIds = hasAddPopoverIds + "," + addPopoverIds;
		}
		
		this.getSessionAttributes().put(AppModuleConfigManageEditHandler.class.getName()+"addPopoverIds",addPopoverIds);
		
		List<DataRow> popoverRowList = new ArrayList<DataRow>();
		
		List<DataRow> menuCodeList = getService().findMenuInfos(addPopoverIdsList,navId);
		for(int i=0;i<menuCodeList.size();i++){
			DataRow row = menuCodeList.get(i);
			String name = row.getString("MENU_NAME");
			String code = row.getString("MENU_CODE");
			row.put("name", name);
			row.put("code", code);
			popoverRowList.add(row);
		}
		
		FormSelect modalSelect = new FormSelect();
		modalSelect.setKeyColumnName("code");
		modalSelect.setValueColumnName("name");
		modalSelect.addHasBlankValue(false);
		modalSelect.putValues(popoverRowList);
		
		responseText = String.valueOf(modalSelect);
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer addPopupTreeInfos(DataParam param){
		String responseText = "";
		String addPopupIds = param.get("addPopupIds");
		String navId = param.get("NAV_ID");
		
		List<String> addPopupIdsList = new ArrayList<String>();
		ListUtil.addArrayToList(addPopupIdsList, addPopupIds.split(","));
		
		String hasAddPopupIds = (String)this.getSessionAttributes().get(AppModuleConfigManageEditHandler.class.getName()+"addPopupIds");
		if(hasAddPopupIds != null){
			addPopupIds = hasAddPopupIds + "," + addPopupIds;
		}
		
		this.getSessionAttributes().put(AppModuleConfigManageEditHandler.class.getName()+"addPopupIds",addPopupIds);
		
		List<DataRow> popupRowList = new ArrayList<DataRow>();
		
		List<DataRow> menuCodeList = getService().findMenuInfos(addPopupIdsList,navId);
		for(int i=0;i<menuCodeList.size();i++){
			DataRow row = menuCodeList.get(i);
			String name = row.getString("MENU_NAME");
			String code = row.getString("MENU_CODE");
			row.put("name", name);
			row.put("code", code);
			popupRowList.add(row);
		}
		
		FormSelect modalSelect = new FormSelect();
		modalSelect.setKeyColumnName("code");
		modalSelect.setValueColumnName("name");
		modalSelect.addHasBlankValue(false);
		modalSelect.putValues(popupRowList);
		
		responseText = String.valueOf(modalSelect);
		return new AjaxRenderer(responseText);
	}
	
	protected MenuBarManage getService(){
		return (MenuBarManage)this.lookupService(serviceId);
	}
}