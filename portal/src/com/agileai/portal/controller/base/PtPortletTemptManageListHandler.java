package com.agileai.portal.controller.base;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.portal.bizmoduler.base.PtPortletTemptManage;

public class PtPortletTemptManageListHandler
        extends StandardListHandler {
    public PtPortletTemptManageListHandler() {
        super();
        this.editHandlerClazz = PtPortletTemptManageEditHandler.class;
        this.serviceId = buildServiceId(PtPortletTemptManage.class);
    }

    protected void processPageAttributes(DataParam param) {
        setAttribute("temptGrp",
                     FormSelectFactory.create("PTLET_TEMPT_GRP")
                                      .addSelectedValue(param.get("temptGrp")));
        initMappingItem("TEMPT_GRP",
                        FormSelectFactory.create("PTLET_TEMPT_GRP").getContent());
    }

    protected void initParameters(DataParam param) {
        initParamItem(param, "temptCode", "");
        initParamItem(param, "temptGrp", "");
    }

    protected PtPortletTemptManage getService() {
        return (PtPortletTemptManage) this.lookupService(this.getServiceId());
    }
}
