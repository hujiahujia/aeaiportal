package com.agileai.portal.controller.base;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.portal.bizmoduler.base.PtPageTemptManage;

public class PtPageTemptManageEditHandler
        extends StandardEditHandler {
    public PtPageTemptManageEditHandler() {
        super();
        this.listHandlerClass = PtPageTemptManageListHandler.class;
        this.serviceId = buildServiceId(PtPageTemptManage.class);
    }

    protected void processPageAttributes(DataParam param) {
        setAttribute("TEMPT_GRP",
                     FormSelectFactory.create("PAGE_TEMPT_GRP")
                                      .addSelectedValue(getOperaAttributeValue("TEMPT_GRP",
                                                                               "default")));
    }

    protected PtPageTemptManage getService() {
        return (PtPageTemptManage) this.lookupService(this.getServiceId());
    }
}
