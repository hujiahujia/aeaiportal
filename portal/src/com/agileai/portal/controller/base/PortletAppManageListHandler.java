package com.agileai.portal.controller.base;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.portal.bizmoduler.base.PortletAppManage;

public class PortletAppManageListHandler extends StandardListHandler{
	public PortletAppManageListHandler(){
		super();
		this.editHandlerClazz = PortletAppManageEditHandler.class;
		this.serviceId = buildServiceId(PortletAppManage.class);
	}
	protected void processPageAttributes(DataParam param) {
		setAttribute("paGroup",FormSelectFactory.create("PA_GROUP").addSelectedValue(param.get("paGroup")));
		setAttribute("paContext",FormSelectFactory.create("PA_CONTEXT").addSelectedValue(param.get("paContext")));
		
		initMappingItem("PA_GROUP",FormSelectFactory.create("PA_GROUP").getContent());
	}
	protected void initParameters(DataParam param) {
		initParamItem(param, "paGroup", "");
		initParamItem(param, "paContext", "");		
	}
	protected PortletAppManage getService() {
		return (PortletAppManage)this.lookupService(this.getServiceId());
	}
}
