package com.agileai.portal.controller.sso;

import java.util.ArrayList;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.sso.UserSSOService;

public class SSOAppConfigHandler extends BaseHandler{
	public SSOAppConfigHandler(){
		super();
	}
	public ViewRenderer prepareDisplay(DataParam param){
		String ticketId = param.get("ticketId");
		UserSSOService userSSOService = this.lookupService(UserSSOService.class);
		DataRow ticketRow = userSSOService.findUserApplication(ticketId);
		List<DataRow> ticketEntrys = userSSOService.findUserMappingItem(ticketId);
		this.setRsList(ticketEntrys);
		this.setAttributes(ticketRow);
		this.setAttribute("IS_VISIABLE",FormSelectFactory.create("BOOL_DEFINE").addSelectedValue(ticketRow.stringValue("IS_VISIABLE")));
		return new LocalRenderer(getPage());
	}
	public ViewRenderer doSaveParamAction(DataParam param){
		String responseText = null;
		String ticketId = param.get("TICKET_ID");
		UserSSOService userSSOService = this.lookupService(UserSSOService.class);
		DataRow ticketRow = userSSOService.findUserApplication(ticketId);
		ticketRow.put("IS_VISIABLE",param.getString("IS_VISIABLE"));
		DataParam appParam = ticketRow.toDataParam();
		
		int entrySize = param.getInt("entrySize");
		List<DataParam> paramEntrys = new ArrayList<DataParam>();
		for (int i=0;i < entrySize;i++){
			DataParam entryParam = new DataParam();
			entryParam.put("TICKET_ID",ticketId);
			String paramId = param.get("PARAM_ID"+i);
			entryParam.put("PARAM_ID",paramId);
			String paramValue = param.get("PARAM_VALUE"+i);
			entryParam.put("PARAM_VALUE",paramValue);
			paramEntrys.add(entryParam);
		}
		userSSOService.editUserApplication(appParam, paramEntrys);
		responseText = "sucess";
		return new AjaxRenderer(responseText);
	}
}	
