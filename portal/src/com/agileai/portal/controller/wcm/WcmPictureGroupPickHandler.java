package com.agileai.portal.controller.wcm;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.TreeSelectHandler;
import com.agileai.hotweb.domain.TreeBuilder;
import com.agileai.portal.bizmoduler.wcm.WcmPictureGroup8ContentManage;

public class WcmPictureGroupPickHandler
        extends TreeSelectHandler {
    public WcmPictureGroupPickHandler() {
        super();
        this.serviceId = buildServiceId(WcmPictureGroup8ContentManage.class);
        this.isMuilSelect = false;
    }

    protected TreeBuilder provideTreeBuilder(DataParam param) {
        List<DataRow> records = getService().queryPickTreeRecords(param);
        TreeBuilder treeBuilder = new TreeBuilder(records, "GRP_ID",
                                                  "GRP_NAME", "GRP_PID");

        String excludeId = param.get("GRP_ID");
        treeBuilder.getExcludeIds().add(excludeId);

        return treeBuilder;
    }

    protected WcmPictureGroup8ContentManage getService() {
        return (WcmPictureGroup8ContentManage) this.lookupService(this.getServiceId());
    }
}
