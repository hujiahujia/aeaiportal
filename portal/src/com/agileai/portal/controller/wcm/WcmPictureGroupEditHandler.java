package com.agileai.portal.controller.wcm;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.TreeAndContentColumnEditHandler;
import com.agileai.portal.bizmoduler.wcm.WcmPictureGroup8ContentManage;

public class WcmPictureGroupEditHandler
        extends TreeAndContentColumnEditHandler {
    public WcmPictureGroupEditHandler() {
        super();
        this.serviceId = buildServiceId(WcmPictureGroup8ContentManage.class);
        this.columnIdField = "GRP_ID";
        this.columnParentIdField = "GRP_PID";
    }

    protected void processPageAttributes(DataParam param) {
    }

    protected WcmPictureGroup8ContentManage getService() {
        return (WcmPictureGroup8ContentManage) this.lookupService(this.getServiceId());
    }
}
