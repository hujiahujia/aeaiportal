package com.agileai.portal.controller.forum;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.TreeSelectHandler;
import com.agileai.hotweb.domain.TreeBuilder;
import com.agileai.portal.bizmoduler.forum.UserLevel8BBSUserInfoManage;

public class UserLevel8BBSUserInfoManageTreePickHandler
        extends TreeSelectHandler {
    public UserLevel8BBSUserInfoManageTreePickHandler() {
        super();
        this.serviceId = buildServiceId(UserLevel8BBSUserInfoManage.class);
        this.isMuilSelect = false;
    }

    protected TreeBuilder provideTreeBuilder(DataParam param) {
        List<DataRow> records = getService().queryPickTreeRecords(param);
        TreeBuilder treeBuilder = new TreeBuilder(records, "CODE_ID",
                                                  "CODE_NAME", "TYPE_ID");

        String excludeId = param.get("CODE_ID");
        treeBuilder.getExcludeIds().add(excludeId);

        return treeBuilder;
    }

    protected UserLevel8BBSUserInfoManage getService() {
        return (UserLevel8BBSUserInfoManage) this.lookupService(this.getServiceId());
    }
}
