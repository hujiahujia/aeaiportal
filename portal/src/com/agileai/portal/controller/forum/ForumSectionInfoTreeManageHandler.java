package com.agileai.portal.controller.forum;

import java.util.*;

import com.agileai.domain.*;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.TreeManageHandler;
import com.agileai.hotweb.domain.*;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.forum.ForumSectionInfoTreeManage;
import com.agileai.util.*;

public class ForumSectionInfoTreeManageHandler
        extends TreeManageHandler {
    public ForumSectionInfoTreeManageHandler() {
        super();
        this.serviceId = buildServiceId(ForumSectionInfoTreeManage.class);
        this.nodeIdField = "FCI_ID";
        this.nodePIdField = "FCI_PID";
        this.moveUpErrorMsg = "该节点是第一个节点，不能上移！";
        this.moveDownErrorMsg = "该节点是最后一个节点，不能下移！";
        this.deleteErrorMsg = "该节点还有子节点，不能删除！";
    }

    protected TreeBuilder provideTreeBuilder(DataParam param) {
        ForumSectionInfoTreeManage service = this.getService();
        List<DataRow> menuRecords = service.findTreeRecords(param);
        TreeBuilder treeBuilder = new TreeBuilder(menuRecords, "FCI_ID",
                                                  "FCI_NAME", "FCI_PID");

        return treeBuilder;
    }

    protected void processPageAttributes(DataParam param) {
        setAttribute("FCI_STATE",
                     FormSelectFactory.create("FCI_STATE")
                                      .addSelectedValue(getAttributeValue("FCI_STATE")));
        setAttribute("CHILD_FCI_STATE",
                     FormSelectFactory.create("FCI_STATE")
                                      .addSelectedValue(getAttributeValue("CHILD_FCI_STATE",
                                                                          "1")));
    }

    protected String provideDefaultNodeId(DataParam param) {
        return "00000000-0000-0000-00000000000000000";
    }

    protected boolean isRootNode(DataParam param) {
        boolean result = true;
        String nodeId = param.get(this.nodeIdField,
                                  this.provideDefaultNodeId(param));
        DataParam queryParam = new DataParam(this.nodeIdField, nodeId);
        DataRow row = this.getService().queryCurrentRecord(queryParam);

        if (row == null) {
            result = false;
        } else {
            String parentId = row.stringValue("FCI_PID");
            result = StringUtil.isNullOrEmpty(parentId);
        }

        return result;
    }
    
    @PageAction
    public ViewRenderer setupRefColumns(DataParam param){
    	String responseText = "";
    	try {
    		String fciId = param.get("FCI_ID");
    		String newRelfuIds = param.get("newRelfuIds");
    		String[] fuIdsArray = newRelfuIds.split(",");
    		List<String> newRelFuIdList = new ArrayList<String>();
    		ListUtil.addArrayToList(newRelFuIdList, fuIdsArray);
    		this.getService().addUserSecRelColumns(fciId, newRelFuIdList);
    		FormSelect formSelect = this.buildUserSecRelColumnSelect(fciId);
        	responseText = formSelect.getScriptSyntax("fuIds");
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
    	return new AjaxRenderer(responseText);
    }
    
    private FormSelect buildUserSecRelColumnSelect(String fciId){
    	FormSelect formSelect = new FormSelect();
        List<DataRow> userList = this.getService().findUserRecords(fciId);
    	try {
            formSelect.setKeyColumnName("FU_ID");
            formSelect.setValueColumnName("FU_NAME");
            formSelect.putValues(userList);
            formSelect.addHasBlankValue(false); 				
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
        return formSelect;
    }
    
    @PageAction
    public ViewRenderer delUserSecRel(DataParam param){
    	String responseText = FAIL;
    	try {
    		String fciId = param.get("FCI_ID");
    		String fuIds = param.get("fuIds");
    		String[] fuIdsArray = fuIds.split(",");
    		List<String> newRelFuIdList = new ArrayList<String>();
    		ListUtil.addArrayToList(newRelFuIdList, fuIdsArray);
    		String fuId = "";
    		for(int i=0;i<newRelFuIdList.size();i++){
    			fuId = newRelFuIdList.get(i);
    			if(!"".equals(fuId)){
    				this.getService().delUserSecRel(fciId, fuId);
    			}
    		}
    		
    		FormSelect formSelect = this.buildUserSecRelColumnSelect(fciId);
        	responseText = formSelect.getScriptSyntax("fuIds");
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
    	return new AjaxRenderer(responseText);
    }

    @PageAction
    public ViewRenderer retrieveRefColumns(DataParam param){
    	String responseText = "";
    	try {
    		String columnId = param.get("FCI_ID"); 
    		FormSelect formSelect = this.buildUserSecRelColumnSelect(columnId);
        	responseText = formSelect.getScriptSyntax("fuIds");			
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
    	return new AjaxRenderer(responseText);
    }
    protected ForumSectionInfoTreeManage getService() {
        return (ForumSectionInfoTreeManage) this.lookupService(this.getServiceId());
    }
}
