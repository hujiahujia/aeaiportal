package com.agileai.portal.controller.forum;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.TreeAndContentManageEditHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.forum.ForumSectionInfoTreeManage;
import com.agileai.portal.notice.PortalMessageSender;
import com.agileai.util.DateUtil;

public class ForumReportPostInfoHandler
        extends TreeAndContentManageEditHandler {
    public ForumReportPostInfoHandler() {
        super();
    }

    public ViewRenderer prepareDisplay(DataParam param) {
    	String fpfrId = param.get("FPFR_ID");
    	User user = (User)this.getUser();
    	ForumSectionInfoTreeManage forumSectionInfoTreeManage = this.lookupService(ForumSectionInfoTreeManage.class);
		String userId = forumSectionInfoTreeManage.getFuIdRecord(user.getUserCode());
		this.setAttribute("FRR_TIME", DateUtil.getDateByType(DateUtil.YYMMDDHHMISS_HORIZONTAL,new Date()));
		this.setAttribute("FRR_USER_NAME", user.getUserName());
		this.setAttribute("FRR_USERID", userId);
		this.setAttribute("fpfrId", fpfrId);
		this.setAttribute("fpmTitle", param.get("fpmTitle"));
		this.setAttribute("forumCurrentPostId", param.get("forumCurrentPostId"));
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
    
    protected void processPageAttributes(DataParam param) {
        setAttribute("FRR_RES_TYPE",
                     FormSelectFactory.create("FRR_RES_TYPE")
                                      .addSelectedValue(getAttributeValue("FRR_RES_TYPE",param.get("frrResType"))));
    }
    
    @PageAction
	public ViewRenderer createForumReportRel(final DataParam param) {
		String responseText = FAIL;
		try {
			User user = (User)this.getUser();
			String userCode = user.getUserCode();
			String fpfrId = param.get("fpfrId");
			String frrResType = param.get("FRR_RES_TYPE");
			String frrType = param.get("frrType");
			String frrUserid = param.get("FRR_USERID");
			String frrTime = param.get("FRR_TIME");
			ForumSectionInfoTreeManage forumSectionInfoTreeManage = this.lookupService(ForumSectionInfoTreeManage.class);
			forumSectionInfoTreeManage.createForumReportRel(fpfrId,frrResType,frrType,frrUserid,frrTime);
			
			List<String> userCodes = new ArrayList<String>();
			if("POSTMESSAGE".equals(frrResType)){
				List<DataRow> records = forumSectionInfoTreeManage.findModeratorRecords(fpfrId);
				for (int i = 0; i < records.size(); i++) {
					DataRow row = records.get(i);
					String fuCode = row.getString("FU_CODE");
					userCodes.add(fuCode);
				}
				forumSectionInfoTreeManage.updateFpmReport(fpfrId,"HAS_REPORT");
			}else{
				List<DataRow> records = forumSectionInfoTreeManage.findModeratoReplyRecords(fpfrId);
				for (int i = 0; i < records.size(); i++) {
					DataRow row = records.get(i);
					String fuCode = row.getString("FU_CODE");
					userCodes.add(fuCode);
				}
				forumSectionInfoTreeManage.updateFriState(fpfrId, "HAS_REPORT");
			}
			param.put("userCode", userCode);
			param.put("userCodes", userCodes);
			//发送消息
			Thread sendMessageToManger = new Thread() {
				@Override
				public void run() {
					sendMessageToManger(param);
				}
			};
			sendMessageToManger.start();
			responseText = SUCCESS;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
    
	private void sendMessageToManger(DataParam param){
		String userCode = param.get("userCode");
		String title = "帖子被举报,标题为:"+param.get("fpmTitle");
		String postUrl = "/portal/index?ForumPostLocator&contentId="+param.get("forumCurrentPostId");
		String blank = "_blank";
		String msgContent =	"标题为:"+"<a href='"+postUrl+"' target='"+blank+"'>"+title+"</a>"+",请查看!";
		@SuppressWarnings("unchecked")
		List<String> userCodes = param.getList("userCodes");
		PortalMessageSender port = new PortalMessageSender();
		port.sendMessage(userCodes,title,msgContent,userCode);
	}
    
}
