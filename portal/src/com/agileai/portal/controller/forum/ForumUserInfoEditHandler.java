package com.agileai.portal.controller.forum;

import org.codehaus.jettison.json.JSONObject;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.TreeAndContentManageEditHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.forum.UserLevel8BBSUserInfoManage;
import com.agileai.portal.extend.forum.ForumPrivilegeHelper;

public class ForumUserInfoEditHandler
        extends TreeAndContentManageEditHandler {
    public ForumUserInfoEditHandler() {
        super();
        this.serviceId = buildServiceId(UserLevel8BBSUserInfoManage.class);
        this.tabId = "ForumUser";
        this.columnIdField = "CODE_ID";
        this.contentIdField = "FU_ID";
    }
    public ViewRenderer prepareDisplay(DataParam param) {
		String operaType = param.get(OperaType.KEY);
		User user = (User) this.getUser();
		ForumPrivilegeHelper forumPrivilegeHelper = new ForumPrivilegeHelper(user);
		String fuId = forumPrivilegeHelper.getFuId();
		DataRow record = getService().retrieveForumUserRecord(null,fuId);
		this.setAttributes(record);			
		this.setOperaType(operaType);
		this.setAttribute(param, this.columnIdField);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
    protected void processPageAttributes(DataParam param) {
        setAttribute("FU_IDENTITY",
                     FormSelectFactory.create("USER_IDENTITY")
                                      .addSelectedValue(getOperaAttributeValue("FU_IDENTITY",
                                                                               "ORDINARY_USERS")));
        setAttribute("FU_LEVEL",
                     FormSelectFactory.create("USER_LEVEL")
                                      .addSelectedValue(getOperaAttributeValue("FU_LEVEL",
                                                                               "")));
        setAttribute("FU_STATE",
                     FormSelectFactory.create("SYS_VALID_TYPE")
                                      .addSelectedValue(getOperaAttributeValue("FU_STATE",
                                                                               "1")));
    }

    public ViewRenderer doSaveAction(DataParam param){
		String rspText = SUCCESS;
		getService().updatetContentRecord(tabId,param);	
		return new AjaxRenderer(rspText);
	}
    
    @PageAction
    public ViewRenderer mobileSaveProfile(DataParam param){
    	String responseText = FAIL;
    	try {
        	String inputString = this.getInputString();
        	JSONObject jsonObject = new JSONObject(inputString);
        	String name = jsonObject.get("name").toString();
        	String code = jsonObject.get("code").toString();
        	String sex = jsonObject.get("sex").toString();
        	String email = jsonObject.get("email").toString();
        	String desc = jsonObject.get("desc").toString();
        	DataParam updateParam = new DataParam("FU_NAME",name,"FU_CODE",code,"USER_MAIL",email,"USER_CODE",code,"USER_SEX",sex,"USER_DESC",desc);
        	getService().updatetForumUserRecord(updateParam);
    		getService().updatetSecurityUserRecord(updateParam);
        	responseText = SUCCESS;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
    	return new AjaxRenderer(responseText);
    }    
    
    protected UserLevel8BBSUserInfoManage getService() {
        return (UserLevel8BBSUserInfoManage) this.lookupService(this.getServiceId());
    }
}
