package com.agileai.portal.controller.forum;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.bizmoduler.core.TreeAndContentManage;
import com.agileai.hotweb.controller.core.TreeAndContentManageListHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.TreeBuilder;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.forum.ForumConst;
import com.agileai.portal.bizmoduler.forum.ForumSectionInfoTreeManage;
import com.agileai.portal.bizmoduler.forum.UserLevel8BBSUserInfoManage;
import com.agileai.portal.filter.UserCacheManager;

public class UserLevel8BBSUserInfoManageListHandler
        extends TreeAndContentManageListHandler {
    public UserLevel8BBSUserInfoManageListHandler() {
        super();
        this.serviceId = buildServiceId(UserLevel8BBSUserInfoManage.class);
        this.rootColumnId = "USER_LEVEL";
        this.defaultTabId = "ForumUser";
        this.columnIdField = "CODE_ID";
        this.columnNameField = "CODE_NAME";
        this.columnParentIdField = "TYPE_ID";
        this.columnSortField = "CODE_SORT";
    }

    protected void processPageAttributes(DataParam param) {
        String tabId = param.get(TreeAndContentManage.TAB_ID, this.defaultTabId);

        if (this.defaultTabId.equals(tabId)) {
            initMappingItem("FU_IDENTITY",
                            FormSelectFactory.create("USER_IDENTITY")
                                             .getContent());
            initMappingItem("FU_STATE",
                    FormSelectFactory.create("FCI_STATE")
                                     .getContent());
            setAttribute("fuState",
                    FormSelectFactory.create("FCI_STATE")
                                     .addSelectedValue(param.get("fuState")));
        }
    }

    protected void initParameters(DataParam param) {
        String tabId = param.get(TreeAndContentManage.TAB_ID, this.defaultTabId);

        if (this.defaultTabId.equals(tabId)) {
            initParamItem(param, "fuName", "");
            initParamItem(param, "fuCode", "");
            initParamItem(param, "fuEmail", "");
            initParamItem(param, "fuTel", "");
        }
    }

    protected TreeBuilder provideTreeBuilder(DataParam param) {
        UserLevel8BBSUserInfoManage service = this.getService();
        List<DataRow> menuRecords = service.findTreeRecords(new DataParam());
        TreeBuilder treeBuilder = new TreeBuilder(menuRecords,
                                                  this.columnIdField,
                                                  this.columnNameField,
                                                  this.columnParentIdField);

        return treeBuilder;
    }

    protected List<String> getTabList() {
        List<String> result = new ArrayList<String>();
        result.add("ForumUser");

        return result;
    }
    @PageAction
    public ViewRenderer syncUser(DataParam param){
    	String responseText = FAIL;
    	getService().syncUser();
    	UserCacheManager userCacheManager = UserCacheManager.getOnly();
    	userCacheManager.truncateUsers();
    	responseText = SUCCESS;
    	return new AjaxRenderer(responseText);
    }
    
	public ViewRenderer doDeleteAction(DataParam param){
		String responseText = FAIL;
		TreeAndContentManage service = this.getService();
		String tabId = param.get(TreeAndContentManage.TAB_ID);
		String columnId = param.get("curColumnId");
		Map<String,String> tabIdAndColFieldMapping = service.getTabIdAndColFieldMapping();
		String colField = tabIdAndColFieldMapping.get(tabId);
		param.put(colField,columnId);
		DataParam queryParam = new DataParam("FU_ID",param.getString("FU_ID"));
		DataRow record = getService().getContentRecord(tabId, queryParam);
		String userCode = record.getString("FU_CODE");
		ForumSectionInfoTreeManage forumSectionInfoTreeManage = this.lookupService(ForumSectionInfoTreeManage.class);
		List<DataRow> postInfoList = forumSectionInfoTreeManage.findForumPostInfosByUserCode(userCode);
		System.out.println(postInfoList);
		if(postInfoList.size()>0){
			responseText = "该用户存在发帖记录，不能删除！";
		}else {
			service.deletContentRecord(tabId,param);
			responseText = SUCCESS;
		}
		
		List<DataRow> fuIdentitys = getService().queryUserIdentitys(param);
		for(int i=0;i<fuIdentitys.size();i++){
			DataRow row = fuIdentitys.get(i);
			if(ForumConst.MODERATOR.equals(row.getString("ROLE_ID"))){
				responseText = "该用户身份为版主，请移除版主身份后再进行删除！";
				break;
			}
		}
		return new AjaxRenderer(responseText);
	}
    
    protected UserLevel8BBSUserInfoManage getService() {
        return (UserLevel8BBSUserInfoManage) this.lookupService(this.getServiceId());
    }
}
