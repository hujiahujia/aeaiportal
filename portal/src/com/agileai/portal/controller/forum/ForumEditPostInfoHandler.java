package com.agileai.portal.controller.forum;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.TreeAndContentManageEditHandler;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.forum.ForumSectionInfoTreeManage;

public class ForumEditPostInfoHandler
        extends TreeAndContentManageEditHandler {
    public ForumEditPostInfoHandler() {
        super();
    }

    public ViewRenderer prepareDisplay(DataParam param) {
    	String fpmId = param.getString("FPM_ID");
    	ForumSectionInfoTreeManage forumSectionInfoTreeManage = this.lookupService(ForumSectionInfoTreeManage.class);
    	DataRow row = forumSectionInfoTreeManage.getForumPostMessageRecord(fpmId);
    	String fpmContent  = row.getString("FPM_CONTENT");
    	String mobileFpmContent  = row.getString("FPM_CONTENT_MOBILE");
		this.setAttribute(param, this.columnIdField);
		this.setAttribute("fpmId", fpmId);
		this.setAttribute("fpmContent", fpmContent);
		this.setAttribute("mobileFpmContent", mobileFpmContent);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
    
    @PageAction
	public ViewRenderer updatePostInfo(DataParam param){
		String responseText = FAIL;
		try {
			String fpmId = param.getString("fpmId");
			String fpmContent = param.getString("FPM_CONTENT");
			ForumSectionInfoTreeManage forumSectionInfoTreeManage = this.lookupService(ForumSectionInfoTreeManage.class);
			forumSectionInfoTreeManage.updatePostInfo(fpmId, fpmContent);
			responseText = SUCCESS;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
    
    @PageAction
	public ViewRenderer updateMobilePostInfo(DataParam param){
		String responseText = FAIL;
		try {
			String fpmId = param.getString("fpmId");
			String mobileFpmContent = param.getString("FPM_CONTENT_MOBILE");
			ForumSectionInfoTreeManage forumSectionInfoTreeManage = this.lookupService(ForumSectionInfoTreeManage.class);
			forumSectionInfoTreeManage.updateMobilePostInfo(fpmId,mobileFpmContent);
			responseText = SUCCESS;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
    
    protected void processPageAttributes(DataParam param) {
    }

}
