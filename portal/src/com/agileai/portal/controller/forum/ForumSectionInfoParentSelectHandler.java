package com.agileai.portal.controller.forum;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.TreeSelectHandler;
import com.agileai.hotweb.domain.TreeBuilder;
import com.agileai.portal.bizmoduler.forum.ForumSectionInfoTreeManage;

public class ForumSectionInfoParentSelectHandler
        extends TreeSelectHandler {
    public ForumSectionInfoParentSelectHandler() {
        super();
        this.serviceId = buildServiceId(ForumSectionInfoTreeManage.class);
        this.isMuilSelect = false;
    }

    protected TreeBuilder provideTreeBuilder(DataParam param) {
        List<DataRow> records = getService().queryPickTreeRecords(param);
        TreeBuilder treeBuilder = new TreeBuilder(records, "FCI_ID",
                                                  "FCI_NAME", "FCI_PID");

        String excludeId = param.get("FCI_ID");
        treeBuilder.getExcludeIds().add(excludeId);

        return treeBuilder;
    }
    
    protected ForumSectionInfoTreeManage getService() {
        return (ForumSectionInfoTreeManage) this.lookupService(this.getServiceId());
    }
}
