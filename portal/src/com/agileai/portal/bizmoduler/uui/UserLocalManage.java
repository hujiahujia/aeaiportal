package com.agileai.portal.bizmoduler.uui;

import java.util.Date;

import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.BaseInterface;

public interface UserLocalManage extends BaseInterface{
	public DataRow retrieveUserRecord(String userCode);
	public void updateUserTokends(String userCode,String tokeId,Date tokenTime);
	public void updateUserStatus(String userCode,String status);
}
