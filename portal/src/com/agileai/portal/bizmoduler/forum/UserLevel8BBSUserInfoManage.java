package com.agileai.portal.bizmoduler.forum;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.TreeAndContentManage;

public interface UserLevel8BBSUserInfoManage
        extends TreeAndContentManage {

	void syncUser();
	List<DataRow> findUserIdentityRelations(String fuId);
	List<DataRow> queryUserIdentitys(DataParam param);
	void addUserIdentitysRelation(String fuId, List<DataRow> pureMappingRefs);
	void deluserIdentitysRelation(String fuId, String userIdentityRefs);
	DataRow retrieveForumUserRecord(String userCode,String fuId);
	DataRow retrieveForumUserSecRelRecord(String fuId,String moduleId);
	void updateUserLoginNums(String fuCode, Integer loginNums,Integer fuIntegrals);
	DataRow retrieveForumUserPostNums(DataParam param);
	void updateUserLevel(DataParam param);
	
	void updateUserFuIntegral(String fuId,String fuIntegral);
	DataRow getFuInfoRecord(String fuId);
	DataRow getFuUserInfoRecord(String userCode);
	void updateUserFuIntegralByFuCode(String fuCode,String fuIntegral);
	DataRow getResLocation(String resId);
	void updateUserProfile(DataParam updateParam);
	DataRow getCodeName(String typeId, String codeId);
	void updatetSecurityUserNameRecord(DataParam param);
	void updatetForumUserRecord(DataParam updateParam);
	void updatetSecurityUserRecord(DataParam updateParam);
	List<DataRow> findReceiverNames(String isNewUser);
}
