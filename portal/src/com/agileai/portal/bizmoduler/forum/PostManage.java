package com.agileai.portal.bizmoduler.forum;

import java.util.List;

import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.TreeAndContentManage;

public interface PostManage
        extends TreeAndContentManage {
	public static final class EditType{
		public static final String Profile = "profile";
		public static final String Content = "content";
	}
	
	public static final class AuthType{
		public static final String Public = "PUBLIC";
		public static final String Protected = "PROTECTED";
		public static final String Private = "PRIVATE";

	}
	
    public static final class EditMode {
    	public static final String Write = "Write";
    	public static final String Publish = "Publish";
    }
    public static final class BizActionType{
    	public static final String Submit = "submit";
    	public static final String UnSubmit = "unsubmit";
    	public static final String Approve = "approve";
    	public static final String UnApprove = "unapprove";
    }
    public static final class InfoState{
    	public static final String Draft = "draft";
    	public static final String WaitePublish = "waitepublish";
    	public static final String Published = "published";
    }
	public static class SortPolicy{
		public static final String Top8PublishTime = "Top8PublishTime";
		public static final String PublishTimeDesc = "PublishTimeDesc";
		public static final String Top8InfomationSort = "Top8InfomationSort";
		public static final String InfomationSort = "InfomationSort";
		public static final String ReadCountDesc = "ReadCountDesc";
	}
	
	List<DataRow> findPostRecords(String moduleId,String postTypeCode,String postTypeId);
	List<DataRow> findPostRecordsBysearchWord(String searchWord);
	List<DataRow> findFavPostGrids(String userId);
	Integer getFavCount(String userId);
	void deleteFavoriteRecord(String ffId);
	DataRow retrieveModuleInfo(String contentId);
	List<DataRow> findPostTypes(String isNotAdmin,String isNotAdmin8Moderator);
	void updatePostMessageTop(String fpmId,String isTop);
	void updateSectionPostMessageTop(String fpmId,String isSectionTop);
	DataRow retrieveRepliesCount(String postId);
	boolean sendNoticeMail(String userCode,String mailAddress,String category,String url,String title);
	
	public List<DataRow> findPostRecordsNotTop(String moduleId,String postTypeCode,String postTypeId);
}
