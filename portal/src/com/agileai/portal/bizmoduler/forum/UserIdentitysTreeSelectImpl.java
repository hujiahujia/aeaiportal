package com.agileai.portal.bizmoduler.forum;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.TreeSelectServiceImpl;

public class UserIdentitysTreeSelectImpl
        extends TreeSelectServiceImpl
        implements UserIdentitysTreeSelect {
    public UserIdentitysTreeSelectImpl() {
        super();
    }

	@Override
	public List<DataRow> queryUserIdentitys(DataParam param) {
		String statementId = sqlNameSpace+".queryUserIdentitys";
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}
}
