package com.agileai.portal.bizmoduler.forum;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.TreeManage;

public interface ForumSectionInfoTreeManage
        extends TreeManage {
	public List<DataRow> findUserSecRelRecords(String fciId);
	public List<DataRow> findModeratorInfoRecords(String fciId);
	public List<DataRow> findUserRecords(String fciId);
	public List<DataRow> findForumSectionInfoRecords(String fciPid);
	public void addUserSecRelColumns(String fciId,List<String> newRelFuIdList);
	public void delUserSecRel(String fciId,String fuIds);
	public List<DataRow> findHotPostRecords(int tabPage,int pageSize);
	public List<DataRow> findNewPostRecords();
	
	public DataRow getManagePostInfoRecord(String fpmId);
	public List<DataRow> findRepliesInfoRecords(String fpmId);
	
	public DataRow getPostTodayRecord(DataParam param);
	public DataRow getPostYesterdayRecord(DataParam param);
	public DataRow getPostStatisRecord(DataParam param);
	public DataRow getUsersTotalRecord(DataParam param);
	public int getReplyCount(String fpmId);
	
	public void createRepliesInfoRecord(DataParam param);
	public String getFuIdRecord(String fuCode);
	public String getFciIdRecord(String fpmId);
	public void createPostMessageRecord(DataParam param);
	
	public List<DataRow> findModuleFormSelectRecords(String fciPid);
	public List<DataRow> findFormSelectRecords(String fciIdDefValue);
	public String getFuLevelValRecord(String fuLevel);
	public  List<DataRow> findFpmTypeFormSelectRecords();
	
	public String getPostMessageRecord(String fpmId);
	public void updateFpmTypeRecord(String fpmId,String fpmType);
	public int getFpmClickNumber(String fpmId);
	public void updateFpmClickNumberRecord(String fpmId,int fpmClickNumber);
	
	public int getForumFavoriteRecord(String fpmId,String fuId);
	public void createForumFavoriteRecord(String fpmId,String fuId,String fpmTitle);
	String getFpmTitleRecord(String fpmId);
	DataRow getForumPostMessageRecord(String fpmId);
	String getFciMottoRecord(String fciId);
	
	String getFciPid(String fciIdDefValue);
	public  List<DataRow> findforumRepliesInfoRecords(String fpmId);
	public void deleForumPostMessage(String fpmId);
	public void deleForumRepliesInfos(String fpmId);
	public DataRow findCodeListRecord(String fuleve);
	public DataRow findFpmTypeRecord(String fpmType);
	
	public void updateFpmState(String fpmId,String fpmState);
	public void updateFriState(String friId,String friState);
	
	public DataRow getCurrentForumRepliesInfoRecord(String friId);
	public void updateFpmSharedPoints(String fpmId,String fpmSharedPoints);
	
	public List<DataRow> findFciIdFormSelectRecords(String fciIdDefValue);
	public String getPostScores(String typeId,String codeId);
	public List<DataRow> findShieldWordsList();
	public DataRow getSectionInfo(String sectionId,String typeCode,String typeId);
	public Integer getReplyPostCount(String sectionId);
	public String getNewCreateTime(String sectionId);
	public List<DataRow> getModeratorInfoRecords(String sectionId);
	public void updateFriIssharepoint(String friId,String friIssharepoint);
	public List<DataRow> findPostMessageRecords(String fciCode,int tabPage,int pageSize);
	public List<DataRow> findForumPostInfosByUserCode(String userCode);
	public List<DataRow> findUserAttentionForumPostInfos(String userCode);
	public DataRow getmaxFpmCreateTime(String userCode);
	public void updateFpmReport(String fpmId,String fpmReport);
	public List<DataRow> getSectionFormPostInfos(String category,String searchWord);
	public void updateFriTime(String friId,Date friTIme);
	public void updatePostInfo(String fpmId,String fpmContent);
	public void updateMobilePostInfo(String fpmId,String mobileFpmContent);
	public void updateReplyPostInfo(String friId,String friContent);
	public void updateMobileReplyPostInfo(String friId,String mobileFriContent);
	public DataRow getSectionInfo(String sectionCode);
	public void updatePostSctionRel(String fpmId, String sectionId);
	public void createForumReportRel(String fpfrId,String frrResType,String frrType,String frrUserid,String frrTime);
	public List<DataRow> findModeratorRecords(String postId);
	public List<DataRow> findModeratoReplyRecords(String replyId);
	public List<DataRow> findforumReplrtRecords(String fciId);
	public void deleForumReplrtRecords(String frrIds);
	
	public void delForumFavoriteRecord(String postId,String userId);
	public List<DataRow> mobilefindForumReportListInfos(String userId);
	public List<DataRow> mobilefindForumGarbageListInfos(String userId);
	public List<DataRow> mobilefindForumInfosBySectionCode(String sectionCode);
	
	HashMap<String, Integer> findPostListCount();
	void increaseClickNumber(String postId);
}
