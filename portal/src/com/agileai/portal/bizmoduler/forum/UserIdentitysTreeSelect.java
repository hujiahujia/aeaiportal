package com.agileai.portal.bizmoduler.forum;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.TreeSelectService;

public interface UserIdentitysTreeSelect
        extends TreeSelectService {

	List<DataRow> queryUserIdentitys(DataParam param);
}
