package com.agileai.portal.bizmoduler.forum;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.agileai.common.KeyGenerator;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.TreeAndContentManageImpl;
import com.agileai.hotweb.domain.TreeModel;
import com.agileai.util.DateUtil;
import com.agileai.util.ListUtil;

public class UserLevel8BBSUserInfoManageImpl
        extends TreeAndContentManageImpl
        implements UserLevel8BBSUserInfoManage {
	private static HashMap<String,DataRow> CodeName = new HashMap<String,DataRow>();
	private static HashMap<String,DataRow> UserSecRel = new HashMap<String,DataRow>();
    public UserLevel8BBSUserInfoManageImpl() {
        super();
        this.columnIdField = "CODE_ID";
        this.columnParentIdField = "TYPE_ID";
        this.columnSortField = "CODE_SORT";
    }

    
    public List<DataRow> findContentRecords(TreeModel treeModel,String tabId,DataParam param) {
		List<DataRow> result = null;
		String curColumnId = param.getString("columnId");
		String statementId = null;
		if("MODERATOR".equals(curColumnId)){
			statementId = sqlNameSpace+"."+"findForumUser4ModeratorRecords";
		}else{
			statementId = sqlNameSpace+"."+"findForumUserRecords";
		}
		
		DataParam newParam = new DataParam();
		newParam.append(param);
		boolean showChildNodeRecords = "Y".equals(param.get("showChildNodeRecords")); 
		if (showChildNodeRecords){
			newParam.remove("columnId");
		}
		result = this.daoHelper.queryRecords(statementId, newParam);
		if (result != null && showChildNodeRecords){
			Map<String,TreeModel> childrenMap = treeModel.getChildrenMap();
			String columnIdField = this.tabIdAndColFieldMapping.get(tabId);
			List<DataRow> temp = new ArrayList<DataRow>();
			for (int i=0;i < result.size();i++){
				DataRow row = result.get(i);
				String columnId = row.stringValue(columnIdField);
				if (columnId.equals(treeModel.getId()) || childrenMap.containsKey(columnId)){
					temp.add(row);
				}
			}
			result = temp;
		}
		return result;
	}
    
	@Override
	public void syncUser() {
		DataParam param = new DataParam();
		String statementId = sqlNameSpace+"."+"findPortalUserRecords";
		List<DataRow> portalUsers = this.daoHelper.queryRecords(statementId, param);
		statementId = this.sqlNameSpace+".insertForumUserRecord";
		List<DataParam> paramList = new ArrayList<DataParam>();
		List<DataParam> paramUserRoleList = new ArrayList<DataParam>();
		for (int i=0;i < portalUsers.size();i++){
			String userId = KeyGenerator.instance().genKey();
			DataRow portalUser = portalUsers.get(i);
			String name = portalUser.getString("USER_NAME");
			String code = portalUser.getString("USER_CODE");
			String mail = portalUser.getString("USER_MAIL");
			String tel = portalUser.getString("USER_PHONE");
			String state = portalUser.getString("USER_STATE");
			Date creationDate = (Date) portalUser.get("USER_CREATOR_DATE");
			String date = null;
			if(creationDate==null){
				date = DateUtil.getDateByType(DateUtil.YYMMDDHHMISS_HORIZONTAL, new Date());
			}else{
				date = DateUtil.getDateByType(DateUtil.YYMMDDHHMISS_HORIZONTAL, creationDate);
			}
			String level = ForumConst.SOLDIER;
			String userProfile = ForumConst.USERPROFILE;
			UserLevel8BBSUserInfoManage userLevel8BBSUserInfoManage = this.lookupService(UserLevel8BBSUserInfoManage.class);
			DataRow row = userLevel8BBSUserInfoManage.getCodeName("INTEGRAL_CHANGE", "NEW_USER_REGISTER");
			String integral = row.getString("CODE_NAME");
			Integer loginnums = 0;
			DataParam dataParam = 
					new DataParam("userId",userId,"name",name,"code",code,"mail",mail,
							"tel",tel,"state",state,"creationDate",date,"level",level,"integral",integral,"loginnums",loginnums,"userProfile",userProfile);
			paramList.add(dataParam);
			//添加用户角色
			String userRoleId = KeyGenerator.instance().genKey();
			String roleCode = ForumConst.ORDINARY_USERS;
			DataParam dataRoleRelParam = new DataParam("userRoleId",userRoleId,"userId",userId,"roleCode",roleCode);
			paramUserRoleList.add(dataRoleRelParam);
		}
		this.daoHelper.batchInsert(statementId, paramList);
		statementId = this.sqlNameSpace+".insertForumUserRoleRelRecord";
		this.daoHelper.batchInsert(statementId, paramUserRoleList);
	}

	@Override
	public List<DataRow> findUserIdentityRelations(String fuId) {
		String statementId = sqlNameSpace+".queryUserIdentityRoleRelation";
		DataParam param = new DataParam("FU_ID",fuId);
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}

	@Override
	public List<DataRow> queryUserIdentitys(DataParam param) {
		String statementId = sqlNameSpace+".queryUserIdentityRoleRelation";
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}

	@Override
	public void addUserIdentitysRelation(String fuId,List<DataRow> pureMappingRefs) {
		String statementId = sqlNameSpace+"."+"insertUserIdentitysRelation";
		for(int i=0;i<pureMappingRefs.size();i++){
			String key = KeyGenerator.instance().genKey();
			DataRow pureMappingRef = pureMappingRefs.get(i);
			pureMappingRef.put("FUCR_ID", key);
		}
		List<DataParam> dataParams = ListUtil.toParamList(pureMappingRefs);
		this.daoHelper.batchInsert(statementId, dataParams);
	}
	
	@Override
	public void deluserIdentitysRelation(String fuId,String roleId) {
		DataParam deleteParam = new DataParam("FU_ID",fuId,"ROLE_ID",roleId);
		String statementId = sqlNameSpace+"."+"deleteUserIdentitysRelation";
		this.daoHelper.deleteRecords(statementId, deleteParam);
	}

	@Override
	public DataRow retrieveForumUserRecord(String userCode,String fuId) {
		String statementId = sqlNameSpace+"."+"getForumUserRecord";
		DataParam param = new DataParam("fuCode",userCode,"FU_ID",fuId);
		DataRow result =  this.daoHelper.getRecord(statementId, param);
		return result;
	}

	@Override
	public DataRow retrieveForumUserSecRelRecord(String fuId, String moduleId) {
		DataRow result = null;
		if (!UserSecRel.containsKey(fuId+moduleId)){
			String statementId = sqlNameSpace+"."+"getForumUserSecRelRecord";
			DataParam param = new DataParam("fuId",fuId,"moduleId",moduleId);
			DataRow row =  this.daoHelper.getRecord(statementId, param);
			if (row != null && !row.isEmpty()){
				UserSecRel.put(fuId+moduleId, row);
			}
		}
		result = UserSecRel.get(fuId+moduleId);
		return result;
	}

	@Override
	public void updateUserLoginNums(String fuCode, Integer loginNums ,Integer fuIntegrals) {
		String statementId = sqlNameSpace+"."+"updateUserLoginNums";
		String strFuIntegrals = String.valueOf(fuIntegrals);
		DataParam param = new DataParam("FU_CODE",fuCode,"FU_LOGIN_NUMS",loginNums,"FU_INTEGRAL",strFuIntegrals);
		this.daoHelper.updateRecord(statementId, param);
	}

	@Override
	public DataRow retrieveForumUserPostNums(DataParam param) {
		String statementId = sqlNameSpace+"."+"getForumUserPostNums";
		DataRow result =  this.daoHelper.getRecord(statementId, param);
		return result;
	}

	@Override
	public void updateUserLevel(DataParam param) {
		String statementId = sqlNameSpace+"."+"updateUserLevel";
		this.daoHelper.updateRecord(statementId, param);
		
	}
	@Override
	public void updateUserFuIntegral(String fuId, String fuIntegral) {
		String statementId = sqlNameSpace+"."+"updateUserFuIntegral";
		DataParam param = new DataParam("FU_ID",fuId,"FU_INTEGRAL",fuIntegral);
		this.daoHelper.updateRecord(statementId, param);
	}

	@Override
	public DataRow getFuInfoRecord(String fuId) {
		String statementId = sqlNameSpace+"."+"getFuInfoRecord";
		DataParam param = new DataParam("fuId",fuId);
		DataRow results =  this.daoHelper.getRecord(statementId, param);
		return results;
	}

	@Override
	public DataRow getFuUserInfoRecord(String userCode) {
		String statementId = sqlNameSpace+"."+"getFuUserInfoRecord";
		DataParam param = new DataParam("fuCode",userCode);
		DataRow results =  this.daoHelper.getRecord(statementId, param);
		return results;
	}

	@Override
	public void updateUserFuIntegralByFuCode(String fuCode, String fuIntegral) {
		String statementId = sqlNameSpace+"."+"updateUserFuIntegralByFuCode";
		DataParam param = new DataParam("FU_CODE",fuCode,"FU_INTEGRAL",fuIntegral);
		this.daoHelper.updateRecord(statementId, param);
		
	}

	@Override
	public DataRow getResLocation(String resId) {
		String statementId = sqlNameSpace+"."+"getResLocationRecord";
		DataParam param = new DataParam("RES_ID",resId);
		DataRow result =  this.daoHelper.getRecord(statementId, param);
		return result;
	}

	@Override
	public void updateUserProfile(DataParam updateParam) {
		String statementId = sqlNameSpace+"."+"updateUserProfile";
		this.daoHelper.updateRecord(statementId, updateParam);
	}

	@Override
	public DataRow getCodeName(String typeId, String codeId) {
		DataRow result = null;
		if (!CodeName.containsKey(typeId+codeId)){
			String statementId = sqlNameSpace+"."+"getCodeNameRecord";
			DataParam param = new DataParam("TYPE_ID",typeId,"CODE_ID",codeId);
			DataRow row =  this.daoHelper.getRecord(statementId, param);
			if (row != null && !row.isEmpty()){
				CodeName.put(typeId+codeId, row);
			}
		}
		result = CodeName.get(typeId+codeId);
		return result;
	}

	@Override
	public void updatetSecurityUserNameRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"updatetSecurityUserNameRecord";
		this.daoHelper.updateRecord(statementId, param);
	}


	@Override
	public void updatetForumUserRecord(DataParam updateParam) {
		String statementId = sqlNameSpace+"."+"updatetForumUserRecord";
		this.daoHelper.updateRecord(statementId, updateParam);
	}


	@Override
	public void updatetSecurityUserRecord(DataParam updateParam) {
		String statementId = sqlNameSpace+"."+"updatetSecurityUserRecord";
		this.daoHelper.updateRecord(statementId, updateParam);
	}


	@Override
	public List<DataRow> findReceiverNames(String isNewUser) {
		DataParam param = new DataParam(isNewUser,isNewUser);
		String statementId = sqlNameSpace+"."+"findReceiverNames";
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}

}
