package com.agileai.portal.bizmoduler.wcm;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;

public class InfomationFavEditManageImpl
        extends StandardServiceImpl
        implements InfomationFavEditManage {
    public InfomationFavEditManageImpl() {
        super();
    }

	@Override
	public List<DataRow> findListRecords(DataParam param) {
		String statementId = sqlNameSpace+"."+"findListRecords";
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}

	@Override
	public List<DataRow> findInfoRecords(DataParam param) {
		String statementId = sqlNameSpace+"."+"findInfoRecords";
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		return result;
	}

	@Override
	public DataRow getInfoCountRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"getInfoCountRecord";
		DataRow result = this.daoHelper.getRecord(statementId, param);
		return result;
	}

	@Override
	public int findInfoFavCount(String infoId) {
		int result = 0;
		String statementId = sqlNameSpace+".findInfoFavCount";
		DataParam param = new DataParam("infoId",infoId);
		DataRow row = this.daoHelper.getRecord(statementId, param);
		result = row.getInt("infoCount");
		return result;
	}
    
}