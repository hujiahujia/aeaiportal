package com.agileai.portal.bizmoduler.base;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.bizmoduler.core.StandardService;

public interface PtPageTemptManage
        extends StandardService {
	public void updateContent(DataParam param);
}
