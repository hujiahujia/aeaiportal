package com.agileai.portal.bizmoduler.base;

import java.util.HashMap;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;

public class PtHandlerManageImpl
        extends StandardServiceImpl
        implements PtHandlerManage {
	private HashMap<String,PtHandler> handlerCache = new HashMap<String,PtHandler>();
	
    public PtHandlerManageImpl() {
        super();
    }

    public synchronized PtHandler getHandler(String handlerId){
    	if (handlerCache.isEmpty()){
    		List<DataRow> records = this.findRecords(new DataParam());
    		for (int i=0;i < records.size();i++){
    			DataRow row = records.get(i);
    			String handlerCode = row.stringValue("HANER_CODE");
    			String handlerAccType = row.stringValue("ACCESS_TYPE");
    			
    			PtHandler ptHandler = new PtHandler();
    			ptHandler.setHandlerId(handlerCode);
    			ptHandler.setAcccessType(handlerAccType);
    			handlerCache.put(handlerCode,ptHandler);
    		}
    	}
    	return handlerCache.get(handlerId);
    }
    
	public void createRecord(DataParam param) {
		handlerCache.clear();
		super.createRecord(param);
	}
	
	public void updateRecord(DataParam param) {
		handlerCache.clear();
		super.updateRecord(param);
	}
	
	public void deletRecord(DataParam param) {
		handlerCache.clear();
		super.deletRecord(param);
	}
}
