package com.agileai.portal.bizmoduler.base;

import java.util.ArrayList;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.TreeSelectServiceImpl;


public class MenuTreeSelectImpl extends TreeSelectServiceImpl implements MenuTreeSelect {
	public MenuTreeSelectImpl(){
		super();
		this.queryPickTreeRecordsSQL = "queryTreeRecords";
	}

	@Override
	public List<DataRow> queryMobileMenuTreeRecords(String navId,String pagepro,String moduleCodes) {
		DataParam param = new DataParam();
		if(moduleCodes.isEmpty()){
			param = new DataParam("navId",navId,"pagepro",pagepro);
		}else{
			param = new DataParam("navId",navId,"pagepro",pagepro,"modals",moduleCodes);
		}
		String statementId = sqlNameSpace+".queryMobileMenuTreeRecords";
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		List<DataRow> records = new ArrayList<DataRow>();
		if (result != null && !result.isEmpty()){
			for (int i=0 ;i < result.size();i++){
				DataRow row = result.get(i);
				String menuType = row.stringValue("MENU_TYPE");
				if("F".equals(menuType)){
					String menuPid = row.stringValue("MENU_CODE");
					List<DataRow> recordList = queryPageMenuTreeRecords(menuPid,navId,pagepro,moduleCodes);
					if(recordList.size() > 0){
						records.add(row);
					}
				}else if("L".equals(menuType)){
					String menuDesc = row.getString("MENU_DESC");
					if(null != menuDesc){
						if(menuDesc.contains(pagepro)){
							row.put("MOBILE_PAGE_PRO",pagepro);	
							records.add(row);
						}
					}
				}
			}	    		
    	}
		return records;
	}

	@Override
	public List<DataRow> queryPageMenuTreeRecords(String menuPid,String navId,String pagepro,String moduleCodes) {
		DataParam param = new DataParam();
		if(moduleCodes.isEmpty()){
			param = new DataParam("menuPid",menuPid,"navId",navId,"pagepro",pagepro);
		}else{
			param = new DataParam("menuPid",menuPid,"navId",navId,"pagepro",pagepro,"modals",moduleCodes);
		}
		String statementId = sqlNameSpace+".queryPageMenuTreeRecords";
		List<DataRow> result = this.daoHelper.queryRecords(statementId, param);
		List<DataRow> records = new ArrayList<DataRow>();
		if (result != null && !result.isEmpty()){
			for (int i=0 ;i < result.size();i++){
				DataRow row = result.get(i);
				String menuType = row.stringValue("MENU_TYPE");
				if("F".equals(menuType)){
					String curmenuPid = row.stringValue("MENU_CODE");
					List<DataRow> recordList = queryPageMenuTreeRecords(curmenuPid,navId,pagepro,moduleCodes);
					if(recordList.size() > 0){
						records.add(row);
					}else{
						continue;
					}
				}else if("L".equals(menuType)){
					String menuDesc = row.getString("MENU_DESC");
					if(null != menuDesc){
						if(menuDesc.contains(pagepro)){
							row.put("MOBILE_PAGE_PRO",pagepro);	
							records.add(row);
						}
					}
				}
			}	    		
    	}
		return records;
	}
}
