package com.agileai.portal.bizmoduler.auth;

import com.agileai.hotweb.bizmoduler.core.TreeSelectService;

public interface SecurityRoleTreeSelect
        extends TreeSelectService {
}
