package com.agileai.portal.bizmoduler.mobile;

import java.util.HashMap;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardService;
import com.agileai.portal.controller.mobile.AppModule;

public interface AppModuleConfigManage extends StandardService{
	public static final HashMap<String,AppModule> AppModuleCache = new HashMap<String,AppModule>();
	DataRow getUserAppConfig(DataParam queryParam);
	void saveAppModuleConfig(String appModuleConfig,String userId);
	AppModule retrieveppModuleRecord(String appModuleId);
	List<String> retrieveAppModuleResource(String userId);
}
