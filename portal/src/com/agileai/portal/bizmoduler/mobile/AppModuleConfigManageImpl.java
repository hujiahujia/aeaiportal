package com.agileai.portal.bizmoduler.mobile;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jettison.json.JSONObject;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;
import com.agileai.portal.controller.mobile.AppModule;
import com.agileai.util.StringUtil;

public class AppModuleConfigManageImpl extends StandardServiceImpl
	implements AppModuleConfigManage{

	@Override
	public DataRow getUserAppConfig(DataParam queryParam) {
		String statementId = sqlNameSpace+"."+"getUserAppConfig";
		DataRow result  = this.daoHelper.getRecord(statementId, queryParam);
		return result;
	}

	@Override
	public void saveAppModuleConfig(String appModuleConfig,String userId) {
		String statementId = sqlNameSpace+"."+"updateAppConfigRecord";
		DataParam param = new DataParam("APP_CONFIG",appModuleConfig,"USER_ID",userId);
		this.daoHelper.updateRecord(statementId, param);
	}
	
	@Override
	public AppModule retrieveppModuleRecord(String appModuleId) {
		AppModule result = null;
		try {
			if (AppModuleCache.containsKey(appModuleId)){
				result = AppModuleCache.get(appModuleId);
			}else{
				if (AppModuleCache.isEmpty()){
					List<DataRow> records = this.findRecords(new DataParam());
					if (records != null){
						for (int i=0;i < records.size();i++){
							DataRow row = records.get(i);
							if (row != null && row.size() > 0){
								AppModule appModule = new AppModule();
								appModule.setId(row.getString("AC_ID"));
								appModule.setName(row.getString("AC_NAME"));
								appModule.setCss(row.getString("AC_CSS"));
								String appMethod = row.getString("AC_METHOD");
								String key = null;
								String val = null;
								if(StringUtil.isNotNullNotEmpty(appMethod)){
									JSONObject methodJson = new JSONObject(appMethod);
									key = methodJson.getString("key");
									val = methodJson.getString("val");
								}
								appModule.setMenthodType(key);
								appModule.setMethod(val);
								appModule.setVaild(row.getString("AC_VAILD"));
								appModule.setGroup(row.getString("AC_GROUP"));
								appModule.setSort(row.getInt("AC_SORT"));
								AppModuleCache.put(appModule.getId(), appModule);
							}
						}
						result = AppModuleCache.get(appModuleId);
					}
				}else{
					DataParam param = new DataParam("AC_ID",appModuleId);
					DataRow row = this.getRecord(param);
					if (row != null && row.size() > 0){
						result = new AppModule();
						result.setId(row.getString("AC_ID"));
						result.setName(row.getString("AC_NAME"));
						result.setCss(row.getString("AC_CSS"));
						String appMethod = row.getString("AC_METHOD");
						JSONObject methodJson = new JSONObject(appMethod);
						result.setMenthodType(methodJson.getString("key"));
						result.setMethod(methodJson.getString("val"));
						result.setVaild(row.getString("AC_VAILD"));
						result.setGroup(row.getString("AC_GROUP"));
						result.setSort(row.getInt("AC_SORT"));
					}
					AppModuleCache.put(appModuleId, result);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public void updateRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"updateRecord";
		processDataType(param, tableName);
		this.daoHelper.updateRecord(statementId, param);
		AppModuleCache.remove(param.getString("AC_ID"));
	}
	
	public void deletRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"deleteRecord";
		this.daoHelper.deleteRecords(statementId, param);
		AppModuleCache.remove(param.getString("AC_ID"));
	}

	@Override
	public List<String> retrieveAppModuleResource(String userId) {
		List<String> result = new ArrayList<String>();
		String statementId = this.sqlNameSpace + ".receiveAppModuleIdList";
		DataParam param = new DataParam("userId",userId);
		List<DataRow> records = this.daoHelper.queryRecords(statementId, param);
		for (int i=0;i < records.size();i++){
			DataRow row = records.get(i);
			String appModuleId = row.stringValue("AC_ID");
			result.add(appModuleId);
		}
		return result;
	}
	
}
