package com.agileai.portal.bizmoduler.sso;

import java.io.Serializable;

public class SSOPrincipal implements Serializable{
	private static final long serialVersionUID = -2521171651810825013L;
	
	private String userId = null;
	private String userName = null;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
}
