package com.agileai.portal.extend;

public interface ExtendConsts {
	
	public static class ExtendPropertyKeys{
		public static final String ForumPrivlege = "_ForumPrivlege_";
		public static final String AppModuleIdList = "_AppModuleIdList_";
	}
	
	public static class TempPropertyKeys{
		public static final String AppModules = "_AppModules_";
	}
}
