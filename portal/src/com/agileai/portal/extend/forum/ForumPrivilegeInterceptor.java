package com.agileai.portal.extend.forum;

import java.util.HashMap;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.common.BeanFactory;
import com.agileai.hotweb.domain.core.User;
import com.agileai.portal.bizmoduler.forum.UserLevel8BBSUserInfoManage;
import com.agileai.portal.extend.ExtendConsts.ExtendPropertyKeys;
import com.agileai.portal.privilege.PrivilegeInterceptor;

public class ForumPrivilegeInterceptor implements PrivilegeInterceptor{
	
	@Override
	public void initExtendPrivilege(User user) {
		HashMap<String,Object> userForumProperties = new HashMap<String,Object>();
		BeanFactory beanFactory = BeanFactory.instance();
		UserLevel8BBSUserInfoManage userLevel8BBSUserInfoManage = (UserLevel8BBSUserInfoManage) beanFactory.getBean("userLevel8BBSUserInfoManageService");
		String userCode = user.getUserCode();
		DataRow fuInfo = userLevel8BBSUserInfoManage.retrieveForumUserRecord(userCode,null);
		if (fuInfo != null){
			DataParam param = new DataParam("FU_ID",fuInfo.getString("FU_ID"));
			List<DataRow> fuIdentitys = userLevel8BBSUserInfoManage.queryUserIdentitys(param);
			HashMap<String,Object> userIdentitys = new HashMap<String,Object>();
			for(int i=0;i<fuIdentitys.size();i++){
				DataRow userIdentity = fuIdentitys.get(i);
				HashMap<String,String> userIdentitys1 = new HashMap<String,String>();
				userIdentitys1.put("ROLE_ID", userIdentity.getString("ROLE_ID"));
				userIdentitys1.put("ROLE_NAME", userIdentity.getString("ROLE_NAME"));
				userIdentitys.put(String.valueOf(i), userIdentitys1);
			}
			userForumProperties.put("userIdentitys",userIdentitys);
			userForumProperties.put("FU_ID",fuInfo.getString("FU_ID"));
			userForumProperties.put("FU_CODE",fuInfo.getString("FU_CODE"));
			userForumProperties.put("FU_NAME",fuInfo.getString("FU_NAME"));
			userForumProperties.put("FU_LEVEL",fuInfo.getString("FU_LEVEL"));
			userForumProperties.put("FU_STATE",fuInfo.getString("FU_STATE"));
			userForumProperties.put("FU_TEL",fuInfo.getString("FU_TEL"));
			userForumProperties.put("FU_EMAIL",fuInfo.getString("FU_EMAIL"));
			userForumProperties.put("FU_INTEGRAL",fuInfo.getString("FU_INTEGRAL"));
			userForumProperties.put("FU_LOGIN_NUMS",fuInfo.get("FU_LOGIN_NUMS"));
			user.getExtendProperties().put(ExtendPropertyKeys.ForumPrivlege, userForumProperties);
		}
	}

	
}