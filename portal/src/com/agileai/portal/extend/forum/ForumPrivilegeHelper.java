package com.agileai.portal.extend.forum;

import java.math.BigDecimal;
import java.util.HashMap;

import com.agileai.domain.DataRow;
import com.agileai.hotweb.common.BeanFactory;
import com.agileai.hotweb.domain.core.User;
import com.agileai.portal.bizmoduler.forum.ForumConst;
import com.agileai.portal.bizmoduler.forum.UserLevel8BBSUserInfoManage;
import com.agileai.util.DateUtil;
import com.agileai.util.StringUtil;

public class ForumPrivilegeHelper {
	public static HashMap<String,Integer> UserLoginCountCache = new HashMap<String,Integer>();
	
	private User user = null;
	public ForumPrivilegeHelper(User user){
		this.user = user;
	}
	
	@SuppressWarnings("rawtypes")
	public String getFuId(){
		String result = null;
		if(user!=null){
			HashMap userForumProperties = (HashMap) user.getExtendProperties().get("_ForumPrivlege_");
			String fuId = userForumProperties.get("FU_ID").toString();
			result = fuId;
		}
		return result;
	}
	
	@SuppressWarnings("rawtypes")
	public String getFuCode(){
		String result = null;
		if(user!=null){
			HashMap userForumProperties = (HashMap) user.getExtendProperties().get("_ForumPrivlege_");
			String fuCode = userForumProperties.get("FU_CODE").toString();
			result = fuCode;
		}
		return result;
	}
	
	@SuppressWarnings("rawtypes")
	public String getFuName(){
		String result = null;
		if(user!=null){
			HashMap userForumProperties = (HashMap) user.getExtendProperties().get("_ForumPrivlege_");
			String fuName = userForumProperties.get("FU_NAME").toString();
			result = fuName;
		}
		return result;
	}
	
	@SuppressWarnings("rawtypes")
	public String getFuLevel(){
		String result = null;
		if(user!=null){
			HashMap userForumProperties = (HashMap) user.getExtendProperties().get("_ForumPrivlege_");
			String fuLevel = userForumProperties.get("FU_LEVEL").toString();
			result = fuLevel;
		}
		return result;
	}
	
	@SuppressWarnings("rawtypes")
	public String getFuState(){
		String result = null;
		if(user!=null){
			HashMap userForumProperties = (HashMap) user.getExtendProperties().get("_ForumPrivlege_");
			String fuState = userForumProperties.get("FU_STATE").toString();
			result = fuState;
		}
		return result;
	}
	
	@SuppressWarnings("rawtypes")
	public String getFuTel(){
		String result = null;
		if(user!=null){
			HashMap userForumProperties = (HashMap) user.getExtendProperties().get("_ForumPrivlege_");
			String fuTel = userForumProperties.get("FU_TEL").toString();
			result = fuTel;
		}
		return result;
	}
	
	@SuppressWarnings("rawtypes")
	public String getFuEmail(){
		String result = null;
		if(user!=null){
			HashMap userForumProperties = (HashMap) user.getExtendProperties().get("_ForumPrivlege_");
			String fuEmail = userForumProperties.get("FU_EMAIL").toString();
			result = fuEmail;
		}
		return result;
	}
	
	@SuppressWarnings("rawtypes")
	private String buildServiceId(Class serviceClass){
		String result = null;
		String className = serviceClass.getSimpleName();
		if (className.endsWith("Service")){
			result = StringUtil.lowerFirst(className);			
		}else{
			result = StringUtil.lowerFirst(className) + "Service";
		}
		return result;
	}
	
	private <ServiceType> ServiceType lookupService(Class<ServiceType> serviceClass){
		String serviceId = buildServiceId(serviceClass);
		Object service = BeanFactory.instance().getBean(serviceId);
		return serviceClass.cast(service);
	}
	
	@SuppressWarnings({ "rawtypes" })
	public String getFuIntegral(){
		String result = null;
		if(user!=null){
			HashMap userForumProperties = (HashMap) user.getExtendProperties().get("_ForumPrivlege_");
			String fuIntegral = userForumProperties.get("FU_INTEGRAL").toString();
			result = fuIntegral;
		}
		return result;
	}
	
	public  void setFuLoginNums(User user,Integer loginNums,Integer fuIntegrals){
		UserLevel8BBSUserInfoManage userLevel8BBSUserInfoManage = lookupService(UserLevel8BBSUserInfoManage.class);
		String fuCode = this.getFuCode();
		userLevel8BBSUserInfoManage.updateUserLoginNums(fuCode,loginNums,fuIntegrals);
	}
	
	@SuppressWarnings("rawtypes")
	public String getFuLoginNums(){
		String result = null;
		if(user!=null){
			HashMap userForumProperties = (HashMap) user.getExtendProperties().get("_ForumPrivlege_");
			if (userForumProperties.get("FU_LOGIN_NUMS") != null){
				result = userForumProperties.get("FU_LOGIN_NUMS").toString();	
			}else{
				result = "0";
			}
		}
		return result;
	}
	
	@SuppressWarnings("rawtypes")
	public boolean isModerator(){
		boolean result = false;
		HashMap userForumProperties = (HashMap) user.getExtendProperties().get("_ForumPrivlege_");
		HashMap userIdentitys = (HashMap) userForumProperties.get("userIdentitys");
		for(int i=0;i<userIdentitys.size();i++){
			HashMap role = (HashMap) userIdentitys.get(String.valueOf(i));
			String roleId = role.get("ROLE_ID").toString();
			if(ForumConst.MODERATOR.equals(roleId)){
				result = true;
				break;
			}
		}
		return result;
	}	
	
	@SuppressWarnings("rawtypes")
	public boolean isModerator(String moduleId){
		boolean result = false;
		HashMap userForumProperties = (HashMap) user.getExtendProperties().get("_ForumPrivlege_");
		HashMap userIdentitys = (HashMap) userForumProperties.get("userIdentitys");
		String fuId = this.getFuId();
		for(int i=0;i<userIdentitys.size();i++){
			HashMap role = (HashMap) userIdentitys.get(String.valueOf(i));
			String roleId = role.get("ROLE_ID").toString();
			if(ForumConst.MODERATOR.equals(roleId)){
				result = true;
				break;
			}
		}
		if(result){
			UserLevel8BBSUserInfoManage userLevel8BBSUserInfoManage = lookupService(UserLevel8BBSUserInfoManage.class);
			DataRow row = userLevel8BBSUserInfoManage.retrieveForumUserSecRelRecord(fuId, moduleId);
			if(row != null && row.size()>0){
				result = true;
			}else{
				result = false;
			}
		}
		
		return result;
	}
	
	@SuppressWarnings("rawtypes")
	public boolean isForumAdmin(){
		boolean result = false;
		HashMap userForumProperties = (HashMap) user.getExtendProperties().get("_ForumPrivlege_");
		HashMap userIdentitys = (HashMap) userForumProperties.get("userIdentitys");
		for(int i=0;i<userIdentitys.size();i++){
			HashMap role = (HashMap) userIdentitys.get(String.valueOf(i));
			String roleId = role.get("ROLE_ID").toString();
			if(ForumConst.ADMIN.equals(roleId)){
				result = true;
				break;
			}
		}
		return result;
	}
	
	public boolean isGuest(){
		return user != null && "guest".equalsIgnoreCase(user.getUserCode());
	}
	
	public Integer getUserLoginCount(String userCode){
		Integer result = 0;
		String key = userCode + DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL);
		synchronized (key) {
			if (!UserLoginCountCache.containsKey(key)){
				UserLoginCountCache.put(key, 0);
			}
			result = UserLoginCountCache.get(key);
		}
		return result;
	}
	
	private BigDecimal getBigDecimalCodeName(String typeId,String codeId) {
		BigDecimal codeName = new BigDecimal("0.00");
		UserLevel8BBSUserInfoManage userLevel8BBSUserInfoManage = lookupService(UserLevel8BBSUserInfoManage.class);
		DataRow row = userLevel8BBSUserInfoManage.getCodeName(typeId,codeId);
		codeName=new BigDecimal(row.getString("CODE_NAME"));
		return codeName;
	}
	
	
	public synchronized void increaseUserLoginCount(){
		String userCode = this.getFuCode();
		Integer currentCount = getUserLoginCount(userCode);
		Integer loginNum = 0;
		BigDecimal loginIntegral=this.getBigDecimalCodeName("INTEGRAL_CHANGE","DAILY_LOGIN");
		String fuLevel = this.getFuLevel();
		if(ForumConst.REGION_COMMANDER.equals(fuLevel)){
			loginIntegral = loginIntegral.multiply(this.getBigDecimalCodeName("GRADE_INTEGRAL_CHANGE","REGION_COMMANDER"));
		}else if(ForumConst.SQUAD_LEADER.equals(fuLevel)){
			loginIntegral = loginIntegral.multiply(this.getBigDecimalCodeName("GRADE_INTEGRAL_CHANGE","SQUAD_LEADER"));
		}else if(ForumConst.PLATOON_LEADER.equals(fuLevel)){
			loginIntegral = loginIntegral.multiply(this.getBigDecimalCodeName("GRADE_INTEGRAL_CHANGE","PLATOON_LEADER"));
		}else if(ForumConst.COMPANY_COMMANDER.equals(fuLevel)){
			loginIntegral = loginIntegral.multiply(this.getBigDecimalCodeName("GRADE_INTEGRAL_CHANGE","COMPANY_COMMANDER"));
		}else if(ForumConst.BATTALION_COMMANDER.equals(fuLevel)){
			loginIntegral = loginIntegral.multiply(this.getBigDecimalCodeName("GRADE_INTEGRAL_CHANGE","BATTALION_COMMANDER"));
		}else if(ForumConst.REGIMENT_COMMANDER.equals(fuLevel)){
			loginIntegral = loginIntegral.multiply(this.getBigDecimalCodeName("GRADE_INTEGRAL_CHANGE","REGIMENT_COMMANDER"));
		}else if(ForumConst.BRIGADE_COMMANDER.equals(fuLevel)){
			loginIntegral = loginIntegral.multiply(this.getBigDecimalCodeName("GRADE_INTEGRAL_CHANGE","BRIGADE_COMMANDER"));
		}else if(ForumConst.DIVISION_COMMANDER.equals(fuLevel)){
			loginIntegral = loginIntegral.multiply(this.getBigDecimalCodeName("GRADE_INTEGRAL_CHANGE","DIVISION_COMMANDER"));
		}else if(ForumConst.CORPS_COMMANDER.equals(fuLevel)){
			loginIntegral = loginIntegral.multiply(this.getBigDecimalCodeName("GRADE_INTEGRAL_CHANGE","CORPS_COMMANDER"));
		}else{
			loginIntegral = loginIntegral.multiply(this.getBigDecimalCodeName("GRADE_INTEGRAL_CHANGE","SOLDIER"));
		}
		Integer loginIntegrals = Integer.valueOf(loginIntegral.setScale(0, BigDecimal.ROUND_HALF_UP).toString());
		if(currentCount<=2){
			loginNum = 1;
			String strHisLoginNums = getFuLoginNums();
			Integer hisLoginNums = Integer.valueOf(strHisLoginNums);
			Integer loginNums = loginNum+hisLoginNums;
			String strHisIntegral = getFuIntegral();
			Integer hisIntegral = Integer.valueOf(strHisIntegral);
			Integer fuIntegrals = hisIntegral+loginIntegrals;
			setFuLoginNums(user, loginNums,fuIntegrals);
		}
		String key = userCode + DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL);
		UserLoginCountCache.put(key, ++currentCount);
	}
}
