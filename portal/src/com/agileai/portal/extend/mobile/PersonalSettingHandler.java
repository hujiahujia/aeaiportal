package com.agileai.portal.extend.mobile;

import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.MenuItemPersonalManage;
import com.agileai.portal.bizmoduler.mobile.AngularWidgetManage;
import com.agileai.portal.driver.mobile.AngularWidget;
import com.agileai.portal.driver.model.MenuBar;
import com.agileai.portal.driver.model.MenuItem;
import com.agileai.portal.driver.model.Page;
import com.agileai.portal.driver.service.PortalConfigService;

public class PersonalSettingHandler extends BaseHandler{
	
	@PageAction
	public ViewRenderer getCardInfos(DataParam param){
		String responseText = "";
		JSONObject jsonObject = null;
		try {
			String menuCode = param.get("menuCode");
			String menuBarId = param.get("menuBarId");
			PortalConfigService portalConfigService = getPortalConfigService();
			MenuBar menuBar = portalConfigService.getMenuBar(menuBarId);
			MenuItem menuItem = menuBar.getMenuItemCodeMap().get(menuCode);

			String pageId = menuItem.getPageId();
			Page page = portalConfigService.getPage(pageId);
			MobileHelper mobileHelper = new MobileHelper(request);
			List<AngularWidget> widgetList = mobileHelper.getAngularWidgetList(page);
			jsonObject = buildJsonObject(widgetList);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}

	private JSONObject buildJsonObject(List<AngularWidget> widgetList) throws JSONException{
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("cardSize", widgetList.size());
		JSONObject cardInfos = new JSONObject();
		jsonObject.put("cardInfos", cardInfos);
		for (int i=0;i < widgetList.size();i++){
			AngularWidget angularWidget = widgetList.get(i);
			String widgetCode = angularWidget.getCode();
			cardInfos.put(widgetCode, i);
		}
		return jsonObject;
	}
	
	@PageAction
	public ViewRenderer moveTopCard(DataParam param){
		String responseText = FAIL;
		String menuCode = param.get("menuCode");
		String menuBarId = param.get("menuBarId");
		String widgetCode = param.get("widgetCode");
		
		PortalConfigService portalConfigService = getPortalConfigService();
		MenuBar menuBar = portalConfigService.getMenuBar(menuBarId);
		MenuItem menuItem = menuBar.getMenuItemCodeMap().get(menuCode);
		String menuItemId = menuItem.getMenuId();
		
		User user = (User)this.getUser();
		String userCode = user.getUserCode();
		try {
			MobileHelper mobileHelper = new MobileHelper(request);
			String pageId = menuItem.getPageId();
			Page page = portalConfigService.getPage(pageId);
			List<AngularWidget> angularWidgetList = mobileHelper.getAngularWidgetList(page);
			for (int i = 0;i < angularWidgetList.size();i++){
				AngularWidget angularWidget = angularWidgetList.get(i);
				String tempCode = angularWidget.getCode();
				if (tempCode.equals(widgetCode)){
					angularWidgetList.remove(i);
					angularWidgetList.add(0,angularWidget);
					break;
				}
			}
			MenuItemPersonalManage menuItemPersonalManage = this.getMenuItemPersonalManage();
			JSONObject jsonObject = mobileHelper.buildPersonalJsonObject(angularWidgetList);
			String personalSetting = jsonObject.toString();
			menuItemPersonalManage.savePersonalSetting(userCode, menuItemId, personalSetting);
			responseText = SUCCESS;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}	
	
	@PageAction
	public ViewRenderer moveUpCard(DataParam param){
		String responseText = FAIL;
		String menuCode = param.get("menuCode");
		String menuBarId = param.get("menuBarId");
		String widgetCode = param.get("widgetCode");
		
		PortalConfigService portalConfigService = getPortalConfigService();
		MenuBar menuBar = portalConfigService.getMenuBar(menuBarId);
		MenuItem menuItem = menuBar.getMenuItemCodeMap().get(menuCode);
		String menuItemId = menuItem.getMenuId();
		
		User user = (User)this.getUser();
		String userCode = user.getUserCode();
		try {
			MobileHelper mobileHelper = new MobileHelper(request);
			String pageId = menuItem.getPageId();
			Page page = portalConfigService.getPage(pageId);
			List<AngularWidget> angularWidgetList = mobileHelper.getAngularWidgetList(page);
			for (int i = 0;i < angularWidgetList.size();i++){
				AngularWidget angularWidget = angularWidgetList.get(i);
				String tempCode = angularWidget.getCode();
				if (tempCode.equals(widgetCode)){
					angularWidgetList.remove(i);
					angularWidgetList.add(i-1,angularWidget);
					break;
				}
			}
			MenuItemPersonalManage menuItemPersonalManage = this.getMenuItemPersonalManage();
			JSONObject jsonObject = mobileHelper.buildPersonalJsonObject(angularWidgetList);
			String personalSetting = jsonObject.toString();
			menuItemPersonalManage.savePersonalSetting(userCode, menuItemId, personalSetting);
			responseText = SUCCESS;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer moveDownCard(DataParam param){
		String responseText = FAIL;
		String menuCode = param.get("menuCode");
		String menuBarId = param.get("menuBarId");
		String widgetCode = param.get("widgetCode");
		
		PortalConfigService portalConfigService = getPortalConfigService();
		MenuBar menuBar = portalConfigService.getMenuBar(menuBarId);
		MenuItem menuItem = menuBar.getMenuItemCodeMap().get(menuCode);
		String menuItemId = menuItem.getMenuId();
		
		User user = (User)this.getUser();
		String userCode = user.getUserCode();
		try {
			MobileHelper mobileHelper = new MobileHelper(request);
			String pageId = menuItem.getPageId();
			Page page = portalConfigService.getPage(pageId);
			List<AngularWidget> angularWidgetList = mobileHelper.getAngularWidgetList(page);
			for (int i = 0;i < angularWidgetList.size();i++){
				AngularWidget angularWidget = angularWidgetList.get(i);
				String tempCode = angularWidget.getCode();
				if (tempCode.equals(widgetCode)){
					angularWidgetList.remove(i);
					angularWidgetList.add(i+1,angularWidget);
					break;
				}
			}
			MenuItemPersonalManage menuItemPersonalManage = this.getMenuItemPersonalManage();
			JSONObject jsonObject = mobileHelper.buildPersonalJsonObject(angularWidgetList);
			String personalSetting = jsonObject.toString();
			menuItemPersonalManage.savePersonalSetting(userCode, menuItemId, personalSetting);
			responseText = SUCCESS;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer removeCard(DataParam param){
		String responseText = FAIL;
		String menuCode = param.get("menuCode");
		String menuBarId = param.get("menuBarId");
		String widgetCode = param.get("widgetCode");
		
		PortalConfigService portalConfigService = getPortalConfigService();
		MenuBar menuBar = portalConfigService.getMenuBar(menuBarId);
		MenuItem menuItem = menuBar.getMenuItemCodeMap().get(menuCode);
		String menuItemId = menuItem.getMenuId();
		
		User user = (User)this.getUser();
		String userCode = user.getUserCode();
		try {
			MobileHelper mobileHelper = new MobileHelper(request);
			String pageId = menuItem.getPageId();
			Page page = portalConfigService.getPage(pageId);
			List<AngularWidget> angularWidgetList = mobileHelper.getAngularWidgetList(page);
			for (int i = 0;i < angularWidgetList.size();i++){
				AngularWidget angularWidget = angularWidgetList.get(i);
				String tempCode = angularWidget.getCode();
				if (tempCode.equals(widgetCode)){
					angularWidgetList.remove(i);
					break;
				}
			}
			MenuItemPersonalManage menuItemPersonalManage = this.getMenuItemPersonalManage();
			JSONObject jsonObject = mobileHelper.buildPersonalJsonObject(angularWidgetList);
			String personalSetting = jsonObject.toString();
			menuItemPersonalManage.savePersonalSetting(userCode, menuItemId, personalSetting);
			responseText = SUCCESS;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer addCard(DataParam param){
		String responseText = FAIL;
		String menuCode = param.get("menuCode");
		String menuBarId = param.get("menuBarId");
		String widgetId = param.get("widgetId");
		
		PortalConfigService portalConfigService = getPortalConfigService();
		MenuBar menuBar = portalConfigService.getMenuBar(menuBarId);
		MenuItem menuItem = menuBar.getMenuItemCodeMap().get(menuCode);
		String menuItemId = menuItem.getMenuId();
		
		User user = (User)this.getUser();
		String userCode = user.getUserCode();
		try {
			MobileHelper mobileHelper = new MobileHelper(request);
			String pageId = menuItem.getPageId();
			Page page = portalConfigService.getPage(pageId);
			List<AngularWidget> angularWidgetList = mobileHelper.getAngularWidgetList(page);
			
			AngularWidgetManage angularWidgetManage = (AngularWidgetManage)this.lookupService("angularWidgetManageService");
			AngularWidget angularWidget = angularWidgetManage.retrieveAngularWidget(widgetId);
			angularWidgetList.add(angularWidget);
			
			MenuItemPersonalManage menuItemPersonalManage = this.getMenuItemPersonalManage();
			JSONObject jsonObject = mobileHelper.buildPersonalJsonObject(angularWidgetList);
			String personalSetting = jsonObject.toString();
			menuItemPersonalManage.savePersonalSetting(userCode, menuItemId, personalSetting);
			responseText = SUCCESS;
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer getSelectCardList(DataParam param){
		String responseText = "";
		String menuBarId = param.get("menuBarId");
		String menuCode = param.get("menuCode");
		
		PortalConfigService portalConfigService = getPortalConfigService();
		MenuBar menuBar = portalConfigService.getMenuBar(menuBarId);
		MenuItem menuItem = menuBar.getMenuItemCodeMap().get(menuCode);
		
		try {
			MobileHelper mobileHelper = new MobileHelper(request);
			String pageId = menuItem.getPageId();
			Page page = portalConfigService.getPage(pageId);
			List<AngularWidget> angularWidgetList = mobileHelper.getRestAngularWidgetList(page);
			
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray = new JSONArray();
			jsonObject.put("datas", jsonArray);
			for (int i=0;i < angularWidgetList.size();i++){
				AngularWidget angularWidget = angularWidgetList.get(i);
				JSONObject dataObject = new JSONObject();
				dataObject.put("id", angularWidget.getId());
				dataObject.put("name",angularWidget.getName());
				jsonArray.put(dataObject);
			}
			
			responseText = jsonObject.toString();
			
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return new AjaxRenderer(responseText);
	}
	
	private MenuItemPersonalManage getMenuItemPersonalManage(){
		return this.lookupService(MenuItemPersonalManage.class);
	}
	
	private PortalConfigService getPortalConfigService(){
		return (PortalConfigService)this.lookupService("PortalConfigService");
	}
}