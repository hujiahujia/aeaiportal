package com.agileai.portal.extend.mobile;

import java.util.List;

import com.agileai.hotweb.common.BeanFactory;
import com.agileai.hotweb.domain.core.User;
import com.agileai.portal.bizmoduler.mobile.AppModuleConfigManage;
import com.agileai.portal.extend.ExtendConsts.ExtendPropertyKeys;
import com.agileai.portal.privilege.PrivilegeInterceptor;

public class MobilePrivilegeInterceptor implements PrivilegeInterceptor{
	
	@Override
	public void initExtendPrivilege(User user) {
		BeanFactory beanFactory = BeanFactory.instance();
		AppModuleConfigManage appModuleConfigManage = (AppModuleConfigManage)beanFactory.getBean("appModuleConfigManageService");
		List<String> appModuleResource = appModuleConfigManage.retrieveAppModuleResource(user.getUserId());
		user.getExtendProperties().put(ExtendPropertyKeys.AppModuleIdList, appModuleResource);
	}
}
