package com.agileai.portal.bizmoduler.address;

import com.agileai.hotweb.bizmoduler.core.TreeManageImpl;

public class ContactGroupTreeQueryImpl
        extends TreeManageImpl
        implements ContactGroupTreeQuery {
    public ContactGroupTreeQueryImpl() {
    	 super();
         this.idField = "GRP_ID";
         this.nameField = "GRP_NAME";
         this.parentIdField = "GRP_PID";
         this.sortField = "GRP_SORT";
    }
}
