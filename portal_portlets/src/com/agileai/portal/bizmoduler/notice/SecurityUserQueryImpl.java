package com.agileai.portal.bizmoduler.notice;

import java.util.ArrayList;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.QueryModelServiceImpl;

public class SecurityUserQueryImpl
        extends QueryModelServiceImpl
        implements SecurityUserQuery {
    public SecurityUserQueryImpl() {
        super();
    }
	@Override
	public void addUserTreeRelation(String roleId, String[] groupIds) {
		String statementId = this.sqlNameSpace+".addUserTreeRelation";
		List<DataParam> paramList = new ArrayList<DataParam>();
		for (int i=0;i < groupIds.length;i++){
			DataParam dataParam = new DataParam("roleId",roleId,"userId",groupIds[i]);
			paramList.add(dataParam);
		}
		this.daoHelper.batchInsert(statementId, paramList);
	}

	@Override
	public void delUserTreeRelation(String roleId, String groupId) {
		String statementId = this.sqlNameSpace+".delUserTreeRelation";
		DataParam param = new DataParam("roleId",roleId,"userId",groupId);
		this.daoHelper.deleteRecords(statementId, param);
	}
	@Override
	public String getUserIdByCode(DataParam param) {
		// TODO Auto-generated method stub
		DataRow dataRow = daoHelper.getRecord("SecurityUserQuery.getUserIdByCode", param);
		if (dataRow != null && !dataRow.isEmpty()) {
			return dataRow.stringValue("USER_ID");
		}
		return null;
	}
	
	@Override
	public String[] getUserByIds(List<String> userIds) {
		DataParam param = new DataParam();
		param.put("userIds", getSplitableChars(userIds));
		List<DataRow> dataRowList = daoHelper.queryRecords("SecurityUserQuery.getUserCodesByIds", param);
		if (dataRowList == null || dataRowList.isEmpty()) {
			return null;
		}
		String[] userCodes = new String[dataRowList.size()];
		for (int i = 0; i < dataRowList.size(); i++) {
			DataRow dataRow = dataRowList.get(i);
			String userCode = dataRow.stringValue("USER_CODE");
			
			userCodes[i] = userCode;
		}
		return userCodes;
	}
	
	@Override
	public DataRow getUserByRoleId(List<String> roleIds) {
		DataRow returnValue = new DataRow();
		DataParam param = new DataParam();
		param.put("roles", getSplitableChars(roleIds));
		List<DataRow> dataRowList = daoHelper.queryRecords("SecurityUserQuery.getUserCodesByRole", param);
		if (dataRowList == null || dataRowList.isEmpty()) {
			return null;
		}
		String[] userCodes = new String[dataRowList.size()];
		String[] userIds = new String[dataRowList.size()];
		for (int i = 0; i < dataRowList.size(); i++) {
			DataRow dataRow = dataRowList.get(i);
			String userCode = dataRow.stringValue("USER_CODE");
			String userId = dataRow.stringValue("USER_ID");
			userCodes[i] = userCode;
			userIds[i] = userId;
		}
		
		returnValue.put("userCodes", userCodes);
		returnValue.put("userIds", userIds);
		return returnValue;
	}
	
	@Override
	public DataRow getUserByGroupId(List<String> groupIds) {
		DataRow returnValue = new DataRow();
		DataParam param = new DataParam();
		param.put("groups", getSplitableChars(groupIds));
		List<DataRow> dataRowList = daoHelper.queryRecords("SecurityUserQuery.getUserCodesByGroup", param);
		if (dataRowList == null || dataRowList.isEmpty()) {
			return null;
		}
		String[] userCodes = new String[dataRowList.size()];
		String[] userIds = new String[dataRowList.size()];
		for (int i = 0; i < dataRowList.size(); i++) {
			DataRow dataRow = dataRowList.get(i);
			String userCode = dataRow.stringValue("USER_CODE");
			String userId = dataRow.stringValue("USER_ID");
			userCodes[i] = userCode;
			userIds[i] = userId;
		}
		
		returnValue.put("userCodes", userCodes);
		returnValue.put("userIds", userIds);
		return returnValue;
	}
	
	public String getSplitableChars(List<String> array) {
		if (array == null || array.size() == 0) {
			return "";
		}
		String splitableChars = "";
		for (int i = 0; i < array.size(); i++) {
			splitableChars += array.get(i) + "', '";
		}
		splitableChars = splitableChars.substring(0, splitableChars.length() - 4);
		return splitableChars;
	}

}
