package com.agileai.portal.bizmoduler.dataauth;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardService;

public interface SecurityDataRestypeManage
        extends StandardService {

	DataRow getRecordByResType(DataParam param);
}
