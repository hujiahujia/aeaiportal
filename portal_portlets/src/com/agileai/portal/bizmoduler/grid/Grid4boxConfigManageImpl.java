package com.agileai.portal.bizmoduler.grid;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;

public class Grid4boxConfigManageImpl
        extends StandardServiceImpl
        implements Grid4boxConfigManage {
    public Grid4boxConfigManageImpl() {
        super();
    }
    
	public void deletRecord(DataParam param) {
		String statementId = sqlNameSpace+"."+"deleteRecord";
		this.daoHelper.deleteRecords(statementId, param);
		
		statementId = sqlNameSpace+"."+"deleteRecords";
		this.daoHelper.deleteRecords(statementId, param);
	}
}
