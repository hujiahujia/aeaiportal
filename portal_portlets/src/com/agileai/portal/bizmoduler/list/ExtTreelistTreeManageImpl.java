package com.agileai.portal.bizmoduler.list;

import com.agileai.hotweb.bizmoduler.core.TreeManageImpl;

public class ExtTreelistTreeManageImpl
        extends TreeManageImpl
        implements ExtTreelistTreeManage {
    public ExtTreelistTreeManageImpl() {
        super();
        this.idField = "LIST_ID";
        this.nameField = "LIST_NAME";
        this.parentIdField = "LIST_PID";
        this.sortField = "LIST_SORT";
    }
}
