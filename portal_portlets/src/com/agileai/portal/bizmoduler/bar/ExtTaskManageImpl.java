package com.agileai.portal.bizmoduler.bar;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;

public class ExtTaskManageImpl
        extends StandardServiceImpl
        implements ExtTaskManage {
    public ExtTaskManageImpl() {
        super();
    }

	@Override
	public List<DataRow> findTaskRecords(String userCode, String state) {
		String statementId = sqlNameSpace+".findTaskRecords";
		DataParam param = new DataParam("USER_CODE", userCode,"TASK_STATE",state);
		return this.daoHelper.queryRecords(statementId, param);
	}
	
	@Override
	public String getTaskCount(String userCode, String state) {
		String statementId = sqlNameSpace+".getTaskCount";
		DataParam param = new DataParam("USER_CODE", userCode,"TASK_STATE",state);
		DataRow result = this.daoHelper.getRecord(statementId, param);
		String counts = String.valueOf(result.getInt("counts"));
		return counts;
	}

	@Override
	public DataRow getTaskRecord(String taskId) {
		String statementId = sqlNameSpace+".getTaskRecord";
		DataParam param = new DataParam("TASK_ID", taskId);
		DataRow result = this.daoHelper.getRecord(statementId, param);
		return result;
	}
	
	@Override
	public DataRow getUserInfoRecord(String userCode) {
		String statementId = sqlNameSpace+".getUserInfoRecord";
		DataParam param = new DataParam("USER_CODE", userCode);
		DataRow result = this.daoHelper.getRecord(statementId, param);
		return result;
	}  
}
