package com.agileai.portal.bizmoduler.bar;

import java.util.HashMap;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardServiceImpl;
import com.agileai.util.MapUtil;

public class ExtGeneralQueryConfigManageImpl
        extends StandardServiceImpl
        implements ExtGeneralQueryConfigManage {
    private HashMap<String,List<DataRow>> barConfigCache = new HashMap<String,List<DataRow>>();
	
	public ExtGeneralQueryConfigManageImpl() {
        super();
    }
	
	public void updateRecord(DataParam param) {
		super.updateRecord(param);
		String pageId = param.get("PAGE_CODE");
		String portletId = param.get("PORTLET_CODE");
		String cacheKey = pageId + "!"+portletId;
		if (barConfigCache.containsKey(cacheKey)){
			barConfigCache.remove(cacheKey);
		}
	}
	
	public void deletRecord(DataParam param) {
		DataRow row = this.getRecord(param);
		String pageId = row.stringValue("PAGE_CODE");
		String portletId = row.stringValue("PORTLET_CODE");
		String cacheKey = pageId + "!"+portletId;
		if (barConfigCache.containsKey(cacheKey)){
			barConfigCache.remove(cacheKey);
		}
		super.deletRecord(param);
	}
	public List<DataRow> findRecords(DataParam param) {
		String pageId = param.get("pageId");
		String portletId = param.get("portletId");
		String cacheKey = pageId + "!"+portletId;
		if (barConfigCache.containsKey(cacheKey)){
			List<DataRow> records = barConfigCache.get(cacheKey);
			return records;
		}else{
			return super.findRecords(param);
		}
	}
	public int getMaxSortNO(DataParam param){
    	int result = 0;
    	String statementId = getSqlNameSpace()+".getMaxSortNO";
    	DataRow row = this.daoHelper.getRecord(statementId, param);
    	if (!MapUtil.isNullOrEmpty(row)){
    		result = row.getInt("SORT_NO");
    	}
    	result = result +1;
    	return result;
    }
	public void updateSortNO(List<DataParam> paramList) {
		String statementId = getSqlNameSpace()+".updateSortNO";
		this.daoHelper.batchUpdate(statementId, paramList);
	}
	@Override
	public void saveDefaultValues(String id, String defaultValues) {
		DataParam param = new DataParam();
		String statementId = this.sqlNameSpace+ ".saveDefaultValues";
		param.put("ID",id,"DEFAULT_VALUES",defaultValues);
		this.daoHelper.updateRecord(statementId, param);
	}
}