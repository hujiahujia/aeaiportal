package com.agileai.portal.controller.list;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.portal.bizmoduler.list.ExtDatalistManage;

public class ExtDatalistManageEditHandler
        extends StandardEditHandler {
    public ExtDatalistManageEditHandler() {
        super();
        this.listHandlerClass = ExtDatalistManageListHandler.class;
        this.serviceId = buildServiceId(ExtDatalistManage.class);
    }

    protected void processPageAttributes(DataParam param) {
        setAttribute("LIST_TYPE",this.getAttributeValue("LIST_TYPE", param.get("listType")));
    	setAttribute("LIST_ISVALID",
                     FormSelectFactory.create("SYS_VALID_TYPE")
                                      .addSelectedValue(getOperaAttributeValue("LIST_ISVALID",
                                                                               "1")));
    }

    protected ExtDatalistManage getService() {
        return (ExtDatalistManage) this.lookupService(this.getServiceId());
    }
}
