package com.agileai.portal.controller.list;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.dom4j.Document;
import org.dom4j.Element;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.SimpleHandler;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.list.ExtDatalistManage;
import com.agileai.portal.bizmoduler.list.ExtTreelistTreeManage;
import com.agileai.portal.portlets.list.MultiDataListPortlet;
import com.agileai.util.ListUtil;
import com.agileai.util.StringUtil;
import com.agileai.util.XmlUtil;

public class ListDataProviderHandler extends SimpleHandler{
	public static final class ResouceType{
		public static final String DataList = "DataList";
		public static final String TreeList = "TreeList";
	}
	
	private List<String> selectIdList = new ArrayList<String>();
	
	public ListDataProviderHandler(){
		super();
	}
	
	public ViewRenderer prepareDisplay(DataParam param) {
		String listType = param.get("listType");
		String resourceType = param.get("resourceType");//DataList、TreeList
		String isMulti = param.get("isMulti","N");
		String listItemKey = param.get("listItemKey");
		
		String datas = null;
		if (ResouceType.DataList.equals(resourceType)){
			ExtDatalistManage datalistManage = this.lookupService(ExtDatalistManage.class);
			DataParam queryParam = new DataParam("listType",listType);
			List<DataRow> records = datalistManage.findRecords(queryParam);
			String listItemValue = param.get(listItemKey);
			if (isTrue(isMulti)){
				String idSpliter = param.get("idSpliter",MultiDataListPortlet.DefaultSpliter);
				datas = this.buildDataListJson(records, idSpliter, listItemValue);
			}else{
				datas = this.buildDataListXML(records, listItemKey, listItemValue);				
			}
		}
		else if (ResouceType.TreeList.equals(resourceType)){
			ExtTreelistTreeManage treelistTreeManage = this.lookupService(ExtTreelistTreeManage.class);
			DataParam queryParam = new DataParam("listType",listType);
			List<DataRow> records = treelistTreeManage.findTreeRecords(queryParam);
			datas = this.buildTreeListJson(records);
		}
		this.setAttribute("datas",datas);
		return new LocalRenderer(getPage());
	}
	
	private String buildDataListXML(List<DataRow> records,String listItemKey,String listItemValue){
		String result = null;
		Document document = XmlUtil.createDocument();
		Element charElement = document.addElement("table");
		charElement.addAttribute("width","100%");
		charElement.addAttribute("cellspacing","1");
		charElement.addAttribute("cellpadding","1");
		charElement.addAttribute("width","100%");
		
		for (int i=0;i < records.size();i++){
			Element trElement = charElement.addElement("tr");
			DataRow row = records.get(i);
			Element tdElement = trElement.addElement("td");
			String itemCode = String.valueOf(row.get("LIST_CODE"));
			String itemName = String.valueOf(row.get("LIST_NAME"));
			if (itemCode.equals(listItemValue)){
				tdElement.addAttribute("class", "selected");
			}
			Element aElement = tdElement.addElement("a").addAttribute("href","javascript:doAjaxRefreshPortalPage('"+listItemKey+":"+itemCode+"')");
			aElement.setText(itemName);
		}
		result = document.asXML();
		return result;
	}
	
	private String buildDataListJson(List<DataRow> records,String idSpliter,String listItemValue){
		String result = null;
		try {
			JSONArray jsonArray = new JSONArray();
			if (StringUtil.isNotNullNotEmpty(listItemValue)){
				String[] selectIds = listItemValue.split(idSpliter);
				ListUtil.addArrayToList(selectIdList, selectIds);
			}
			for (int i=0;i < records.size();i++){
				DataRow row = records.get(i);
				String itemCode = String.valueOf(row.get("LIST_CODE"));
				String itemName = String.valueOf(row.get("LIST_NAME"));
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("id", itemCode);
				jsonObject.put("name", itemName);
				if (selectIdList.contains(itemCode)){
					jsonObject.put("selected","true");
				}
				jsonArray.put(jsonObject);
			}
			result = jsonArray.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	private String buildTreeListJson(List<DataRow> records){
		String result = null;
		try {
			JSONArray jsonArray = new JSONArray();
			for (int i=0;i < records.size();i++){
				DataRow row = records.get(i);
				String itemCode = String.valueOf(row.get("LIST_CODE"));
				String itemName = String.valueOf(row.get("LIST_NAME"));
				String itemPCode = String.valueOf(row.get("LIST_PCODE"));
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("id", itemCode);
				jsonObject.put("name", itemName);
				jsonObject.put("parentId", itemPCode);
				jsonArray.put(jsonObject);
			}
			result = jsonArray.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
}
