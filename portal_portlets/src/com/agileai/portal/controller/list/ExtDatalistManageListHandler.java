package com.agileai.portal.controller.list;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.portal.bizmoduler.list.ExtDatalistManage;

public class ExtDatalistManageListHandler
        extends StandardListHandler {
    public ExtDatalistManageListHandler() {
        super();
        this.editHandlerClazz = ExtDatalistManageEditHandler.class;
        this.serviceId = buildServiceId(ExtDatalistManage.class);
    }

    protected void processPageAttributes(DataParam param) {
        setAttribute("listType",
                     FormSelectFactory.create("DATA_LIST_TYPE")
                                      .addSelectedValue(param.get("listType")));
        initMappingItem("LIST_TYPE",
                        FormSelectFactory.create("DATA_LIST_TYPE").getContent());
        initMappingItem("LIST_ISVALID",
                        FormSelectFactory.create("SYS_VALID_TYPE").getContent());
    }

    protected void initParameters(DataParam param) {
        initParamItem(param, "listType", "Sample");
    }

    protected ExtDatalistManage getService() {
        return (ExtDatalistManage) this.lookupService(this.getServiceId());
    }
}
