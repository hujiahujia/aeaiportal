package com.agileai.portal.controller.dyndata;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.domain.DataSet;
import com.agileai.hotweb.controller.core.SimpleHandler;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;

public class BacklogDataProviderHandler extends SimpleHandler{	
	public BacklogDataProviderHandler(){       
		super();
	} 
	
	public ViewRenderer prepareDisplay(DataParam param) {
		String resourceType = param.get("resourceType");
		BacklogDataBuilder builder = new BacklogDataBuilder();
		String datas = null;
		if (resourceType.equals("tabData")){			
			JSONObject tabData = builder.getTabJsonData();
			datas = this.buildTabJson(tabData);
		}			
		else if (resourceType.equals("GridData")){
			String typeCode = param.get("typeCode");
			String type = null;
			if("workinghours".equals(typeCode)){
				type = "工时统计";
			}else if("projectarrangement".equals(typeCode)){
				type = "项目安排";
			}else if("reimbursement".equals(typeCode)){
				type = "报销手续";
			}
			DataSet gridSet = builder.getGridJsonData(type);
			datas = this.buildDataGridJson(gridSet);
		}
		else if (resourceType.equals("BoxData")){
			String id = param.get("id");
			DataRow row = builder.getBoxJsonData(id);
			datas = this.buildBoxJson(row);
		}
		this.setAttribute("datas",datas);
		return new LocalRenderer(getPage());
	}
	
	private String buildDataGridJson(DataSet set) {		
		String result = null;
		String fields[] = {"ID","TYPE","TITLE","SPONSOR","DATE"};
		try {
			JSONArray jsonArray = new JSONArray();
			for(int i=0;i<set.size();i++){
				DataRow row = set.getDataRow(i);
				JSONObject object = new JSONObject();
				for(int j=0;j<fields.length;j++){								
					object.put(fields[j], row.stringValue(fields[j]));
				}	
				jsonArray.put(object);
			}
			result = jsonArray.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	private String buildBoxJson(DataRow row) {
		String result = null;
		String fields[] = {"ID","TYPE","TITLE","SPONSOR","DATE","CONTENT"};
		try {
			JSONObject object = new JSONObject();
			for(int j=0;j<fields.length;j++){								
				object.put(fields[j], row.stringValue(fields[j]));
			}									
			result = object.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	private String buildTabJson(JSONObject tabData){
		JSONArray array = new JSONArray(), proArray = new JSONArray();
		JSONObject object1 = new JSONObject(),object2 = new JSONObject(),object3 = new JSONObject();
		JSONObject proObject1 = new JSONObject(),proObject2 = new JSONObject(),proObject3 = new JSONObject(),proObject4 = new JSONObject();
		try {
			proObject1.put("code", "taskTitle");
			proObject1.put("name", "任务名称");
			proObject1.put("isLink", true);
			proObject1.put("isLinkURL", false);
			
			proObject2.put("code", "taskSponsor");
			proObject2.put("name", "创建者");
			proObject2.put("isLink", false);
			proObject2.put("isLinkURL", false);
			
			proObject3.put("code", "taskTime");
			proObject3.put("name", "创建时间");
			proObject3.put("isLink", false);
			proObject3.put("isLinkURL", false);
			
			proObject4.put("code", "taskTitleLink");
			proObject4.put("name", "任务链接");
			proObject4.put("isLink", false);
			proObject4.put("isLinkURL", true);
			
			proArray.put(proObject1);
			proArray.put(proObject2);
			proArray.put(proObject3);
			proArray.put(proObject4);
			
			object1.put("tabTitle", "工时统计");
			object1.put("tabCode", "workinghours");
			object1.put("moreURL", "javascript:doRedirectPortalPage('targetPage:03/backlog-select.ptml,typeCode:workinghours')");
			object1.put("showHeader", true);
			object1.put("propertyConfigs", proArray);
			object1.put("datas", tabData.getJSONArray("datas1"));
			
			object2.put("tabTitle", "项目安排");
			object2.put("tabCode", "projectarrangement");
			object2.put("moreURL", "javascript:doRedirectPortalPage('targetPage:03/backlog-select.ptml,typeCode:projectarrangement')");
			object2.put("showHeader", true);
			object2.put("propertyConfigs", proArray);
			object2.put("datas", tabData.getJSONArray("datas2"));
			
			object3.put("tabTitle", "报销手续");
			object3.put("tabCode", "reimbursement");
			object3.put("moreURL", "javascript:doRedirectPortalPage('targetPage:03/backlog-select.ptml,typeCode:reimbursement')");
			object3.put("showHeader", true);
			object3.put("propertyConfigs", proArray);
			object3.put("datas", tabData.getJSONArray("datas3"));
			array.put(object1);
			array.put(object2);
			array.put(object3);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return array.toString();
	}
}
