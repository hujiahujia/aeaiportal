package com.agileai.portal.controller.dyndata;

import com.agileai.domain.DataRow;
import com.agileai.domain.DataSet;
import com.agileai.util.DateUtil;

public class YearBudgetDataBuilder {
	private DataSet data = new DataSet();

	public YearBudgetDataBuilder(){
		String year = DateUtil.getYear();
		DataRow row1 = new DataRow("ID","1","YEAR",year,"PROJECT","ERP系统开发","BUDGET_IN","220","BUDGET_OUT","192","DESCRIPTION","这是ERP项目的财年预算");
		DataRow row2 = new DataRow("ID","2","YEAR",year,"PROJECT","系统对接","BUDGET_IN","180","BUDGET_OUT","153","DESCRIPTION","这是系统对接项目的财年预算");
		DataRow row3 = new DataRow("ID","3","YEAR",year,"PROJECT","网站设计","BUDGET_IN","123","BUDGET_OUT","100","DESCRIPTION","这是网站设计项目的财年预算");
		
		data.addDataRow(row1);
		data.addDataRow(row2);
		data.addDataRow(row3);
	}
	
	public DataRow getXmlData(String year,String chartType){
		DataRow xmlRow = new DataRow();
		DataSet set = new DataSet();
		for(int i=0;i<data.size();i++){
			DataRow row = data.getDataRow(i);
			if(year.equals(row.get("YEAR"))){
				set.addDataRow(row);
			}
		}
		if(chartType.equals("line")){
			xmlRow.put("ERP_FLOW",set.getDataRow(0).get("BUDGET_IN"),"DOCK_FLOW",set.getDataRow(1).get("BUDGET_IN"),"WEB_FLOW",set.getDataRow(2).get("BUDGET_IN"));
		}else{
			xmlRow.put("ERP_FLOW",set.getDataRow(0).get("BUDGET_OUT"),"DOCK_FLOW",set.getDataRow(1).get("BUDGET_OUT"),"WEB_FLOW",set.getDataRow(2).get("BUDGET_OUT"));
		}
		return xmlRow;
	}
	
	public DataSet getGridJsonData(String year){ 
		DataSet gridSet = new DataSet();
		for(int i=0;i<data.size();i++){
			DataRow row  = data.getDataRow(i);
			if (year.equals(row.get("YEAR"))) {
				gridSet.addDataRow(row);
			}		
		}
		return gridSet;
	}
	
	public DataSet getBoxJsonData(String year,String chartType){
		DataSet boxSet = new DataSet();
		DataSet set = new DataSet();
		for(int i=0;i<data.size();i++){
			DataRow row = data.getDataRow(i);
			if(year.equals(row.get("YEAR"))){
				set.addDataRow(row);
			}
		}
		for(int j=0;j<set.size();j++){
			DataRow row = new DataRow();
			if(chartType.equals("line")){
				row.put("ID",set.getDataRow(j).get("ID"),"YEAR",set.getDataRow(j).get("YEAR"),"PROJECT",set.getDataRow(j).get("PROJECT"),"BUDGET",set.getDataRow(j).get("BUDGET_IN"));
			}else{
				row.put("ID",set.getDataRow(j).get("ID"),"YEAR",set.getDataRow(j).get("YEAR"),"PROJECT",set.getDataRow(j).get("PROJECT"),"BUDGET",set.getDataRow(j).get("BUDGET_OUT"));
			}
			boxSet.addDataRow(row);
		}
		return boxSet;
	}
	
	public DataRow getHtmlJsonData(String id){
		DataRow htmlRow = data.getDataRow(Integer.parseInt(id)-1);
		return htmlRow;
	}
}
