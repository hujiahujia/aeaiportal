package com.agileai.portal.controller.dyndata;

import com.agileai.domain.DataRow;
import com.agileai.domain.DataSet;
import com.agileai.util.DateUtil;

public class ManagerPerformanceDataBuilder {
	private DataSet data = new DataSet();

	public ManagerPerformanceDataBuilder(){
		String currentDate = DateUtil.getYear();
		DataRow row1 = new DataRow("MANAGER","李经理","DEPARTMENT","开发部","DATE",currentDate,"ID","1","PLACE","辽宁","CH_TASK","50",
			    "CH_REALITY","44");
		DataRow row2 = new DataRow("MANAGER","王经理","DEPARTMENT","市场部","DATE",currentDate,"ID","2","PLACE","北京","CH_TASK","45",
			    "CH_REALITY","40");
		DataRow row3 = new DataRow("MANAGER","赵经理","DEPARTMENT","财务部","DATE",currentDate,"ID","3","PLACE","上海","CH_TASK","39",
			    "CH_REALITY","35");
		
		data.addDataRow(row1);
		data.addDataRow(row2);
		data.addDataRow(row3);
	}
	
	public DataSet getXmlData(String date){
		DataSet xmlSet = new DataSet();
		for(int i=0;i<data.size();i++){
			DataRow row = data.getDataRow(i);			
			if (date.equals(row.get("DATE"))) {
				DataRow xmlRow = new DataRow();
				String chtask = row.getString("CH_TASK");
				String chreality = row.getString("CH_REALITY");
				String manager = row.getString("MANAGER");				
				xmlRow.put("CH_TASK", chtask);
				xmlRow.put("CH_REALITY", chreality);
				xmlRow.put("MANAGER", manager);
				
				xmlSet.addDataRow(xmlRow);
			}
		}
		return xmlSet;
	}
	
	public DataSet getGridJsonData(String date){
		DataSet gridSet = new DataSet();
		for(int i=0;i<data.size();i++){
			DataRow row  = data.getDataRow(i);
			if (date.equals(row.get("DATE"))) {
				gridSet.addDataRow(row);
			}
		}
		return gridSet;
	}
	public DataRow getHtmlJsonData(String id){
		DataRow htmlRow = new DataRow();
		for(int i=0;i<data.size();i++){
			DataRow row  = data.getDataRow(i);
			if (id.equals(row.get("ID"))) {
				htmlRow = row;
			}		
		}
		return htmlRow;
	}
	public DataSet getBoxJsonData(String classify){
		DataSet boxSet = new DataSet();
		for(int i=0;i<data.size();i++){
			DataRow row = data.getDataRow(i);
				DataRow boxRow = new DataRow();
				if(classify.equals("task")){
					boxRow.put("ID",row.get("ID"),"DEPARTMENT",row.get("DEPARTMENT"),"MANAGER",row.get("MANAGER"),"PLACE",row.get("PLACE"),"DATE",row.get("DATE"),"PERFORMANCEDATA",row.get("CH_TASK"));
				}
				else if(classify.equals("reality")){
					boxRow.put("ID",row.get("ID"),"DEPARTMENT",row.get("DEPARTMENT"),"MANAGER",row.get("MANAGER"),"PLACE",row.get("PLACE"),"DATE",row.get("DATE"),"PERFORMANCEDATA",row.get("CH_REALITY"));
				}
				boxSet.addDataRow(boxRow);
			}
		return boxSet;
	}
	
}
