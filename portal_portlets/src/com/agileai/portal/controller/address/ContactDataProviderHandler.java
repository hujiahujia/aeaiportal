package com.agileai.portal.controller.address;

import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.SimpleHandler;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.address.ContactQuery;

public class ContactDataProviderHandler extends SimpleHandler
{
	  public ContactDataProviderHandler()
	  {
	    this.serviceId = buildServiceId(ContactQuery.class);
	  }

	  public ViewRenderer prepareDisplay(DataParam param) {
		  String resourceType = param.get("resourceType");
		  ContactQuery contactQuery = getService();
		  String datas = null;
		  if(resourceType.equals("ContactList")){
			  List<DataRow> records = contactQuery.findContactRecords(param);
			  datas = this.buildContactListJson(records);
		  }else if(resourceType.equals("ContactDetail")){
			  DataRow row = contactQuery.queryCurrentRecord(param);
			  datas = this.buildContactDetailJson(row);
		  }
		  this.setAttribute("datas",datas);
		  return new LocalRenderer(getPage());
	  }
	  
	  private String buildContactListJson(List<DataRow> records){
			String result = null;
			String fields[] = {"USER_CODE","USER_NAME","USER_SEX","USER_MAIL","USER_PHONE","GRP_NAME"};
			try {
				JSONArray jsonArray = new JSONArray();
				for (int i=0;i < records.size();i++){
					DataRow row = records.get(i);
					JSONObject object = new JSONObject();
					for(int j=0;j<fields.length;j++){
						object.put(fields[j], row.stringValue(fields[j]));
						if(fields[j].equals("USER_SEX")){
							if(row.stringValue(fields[j]).equals("M")){
								object.put(fields[j], "男");
							}else if(row.stringValue(fields[j]).equals("F")){
								object.put(fields[j], "女");
							}else{
								object.put(fields[j], "未知");
							}
						}
					}
					jsonArray.put(object);
				}
				result = jsonArray.toString();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return result;
	  }
	  
	  private String buildContactDetailJson(DataRow row){
			String result = null;
			String fields[] = {"USER_CODE","USER_NAME","USER_SEX","USER_STATE","USER_SORT","USER_MAIL","USER_PHONE","USER_DESC"};
			try {
				JSONObject object = new JSONObject();
				for(int i=0;i<fields.length;i++){
					object.put(fields[i], row.stringValue(fields[i]));
					if(fields[i].equals("USER_SEX")){
						if(row.stringValue(fields[i]).equals("M")){
							object.put(fields[i], "男");
						}
						else if(row.stringValue(fields[i]).equals("F")){
							object.put(fields[i], "女");
						}
						else{
							object.put(fields[i], "未知");
						}
					}
					if(fields[i].equals("USER_STATE")){
						if(row.stringValue(fields[i]).equals("1")){
							object.put(fields[i], "有效");
						}else{
							object.put(fields[i], "无效");
						}
					}
				}
				result = object.toString();
			}catch (Exception e) {
				e.printStackTrace();
			}
			return result;
	  }

	  protected ContactQuery getService() {
	    return (ContactQuery)lookupService(getServiceId());
	  }
	}