package com.agileai.portal.controller.bar;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.SimpleHandler;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.bar.ExtTaskManage;
import com.agileai.util.DateUtil;

public class ExtTaskDataProviderHandler extends SimpleHandler{	
	public static String TODO = "TODO";
	public static String ONDO = "ONDO";
	public static String DID = "DID";
	public ExtTaskDataProviderHandler(){       
		super();
	} 
	
	@PageAction
	public ViewRenderer findTaskList(DataParam param){
		String responseText = null;
		ExtTaskManage extTaskManage = lookupService(ExtTaskManage.class);
		JSONArray jsonArray = new JSONArray();
		try {
			//得到所有的记录
			List<DataRow> records = extTaskManage.findTaskRecords("","");
			List<DataRow> todoList = new ArrayList<DataRow>();
			List<DataRow> ondoList = new ArrayList<DataRow>();
			List<DataRow> didList = new ArrayList<DataRow>();
			//处理
			for(int i=0;i<records.size();i++ ){
				DataRow row = records.get(i);
				if(TODO.equals(row.getString("TASK_STATE"))){
					todoList.add(row);
				}else if(ONDO.equals(row.getString("TASK_STATE"))){
					ondoList.add(row);
				}else{
					didList.add(row);
				}
			}
			//待办
			String todoCount = String.valueOf(todoList.size());
			JSONObject jsonObjectTodo = new JSONObject();
			jsonObjectTodo.put("tabTitle", "待办("+todoCount+")");
			jsonObjectTodo.put("tabCode", "TODO");
			jsonObjectTodo.put("moreURL", "javascript:doRedirectPortalPage('targetPage:03/backlog-select.ptml,typeCode:TODO')");
			jsonObjectTodo.put("showHeader", true);
			JSONArray jsonArray1 = new JSONArray();
			JSONObject jsonObject11 = new JSONObject();
			jsonObject11.put("code", "taskTitle");
			jsonObject11.put("name", "任务名称");
			jsonObject11.put("isLink", true);
			jsonObject11.put("isLinkURL", false);
			JSONObject jsonObject12 = new JSONObject();
			jsonObject12.put("code", "taskSponsor");
			jsonObject12.put("name", "创建人");
			jsonObject12.put("isLink", false);
			jsonObject12.put("isLinkURL", false);
			JSONObject jsonObject13 = new JSONObject();
			jsonObject13.put("code", "taskTime");
			jsonObject13.put("name", "时间");
			jsonObject13.put("isLink", false);
			jsonObject13.put("isLinkURL", false);
			JSONObject jsonObject14 = new JSONObject();
			jsonObject14.put("code", "taskTitleLink");
			jsonObject14.put("name", "任务链接");
			jsonObject14.put("isLink", false);
			jsonObject14.put("isLinkURL", true);
			jsonArray1.put(jsonObject11);
			jsonArray1.put(jsonObject12);
			jsonArray1.put(jsonObject13);
			jsonArray1.put(jsonObject14);
			jsonObjectTodo.put("propertyConfigs", jsonArray1);
			JSONArray jsonArray2 = new JSONArray();
			for(int i=0;i<todoList.size();i++){
				DataRow row = todoList.get(i);
				JSONObject jsonObject21 = new JSONObject();
				jsonObject21.put("taskTitle", row.getString("TASK_TITLE"));
				jsonObject21.put("taskSponsor", row.getString("TASK_PERSON_NAME"));
				String date = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_HORIZONTAL, (Date)row.get("TASK_TIME"));
				jsonObject21.put("taskTime",date);
				jsonObject21.put("taskTitleLink", "javascript:doPopupWebPage('targetPage:03/backlog-detail.ptml,name:"+row.getString("TASK_PERSON_NAME")+",id:"+row.getString("TASK_ID")+"',{width:900,height:350})");
				jsonArray2.put(jsonObject21);
			}
			jsonObjectTodo.put("datas", jsonArray2);
			jsonArray.put(jsonObjectTodo);
			//在办
			String ondoCount = String.valueOf(ondoList.size());
			JSONObject jsonObjectOndo = new JSONObject();
			jsonObjectOndo.put("tabTitle", "在办("+ondoCount+")");
			jsonObjectOndo.put("tabCode", "ONDO");
			jsonObjectOndo.put("moreURL", "javascript:doRedirectPortalPage('targetPage:03/backlog-select.ptml,typeCode:ONDO')");
			jsonObjectOndo.put("showHeader", true);
			jsonArray1 = new JSONArray();
			jsonObject11 = new JSONObject();
			jsonObject11.put("code", "taskTitle");
			jsonObject11.put("name", "任务名称");
			jsonObject11.put("isLink", true);
			jsonObject11.put("isLinkURL", false);
			jsonObject12 = new JSONObject();
			jsonObject12.put("code", "taskSponsor");
			jsonObject12.put("name", "创建人");
			jsonObject12.put("isLink", false);
			jsonObject12.put("isLinkURL", false);
			jsonObject13 = new JSONObject();
			jsonObject13.put("code", "taskTime");
			jsonObject13.put("name", "时间");
			jsonObject13.put("isLink", false);
			jsonObject13.put("isLinkURL", false);
			jsonObject14 = new JSONObject();
			jsonObject14.put("code", "taskTitleLink");
			jsonObject14.put("name", "任务链接");
			jsonObject14.put("isLink", false);
			jsonObject14.put("isLinkURL", true);
			jsonArray1.put(jsonObject11);
			jsonArray1.put(jsonObject12);
			jsonArray1.put(jsonObject13);
			jsonArray1.put(jsonObject14);
			jsonObjectOndo.put("propertyConfigs", jsonArray1);
			jsonArray2 = new JSONArray();
			for(int i=0;i<ondoList.size();i++){
				DataRow row = ondoList.get(i);
				JSONObject jsonObject21 = new JSONObject();
				jsonObject21.put("taskTitle", row.getString("TASK_TITLE"));
				jsonObject21.put("taskSponsor", row.getString("TASK_PERSON_NAME"));
				String date = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_HORIZONTAL, (Date)row.get("TASK_TIME"));
				jsonObject21.put("taskTime",date);
				jsonObject21.put("taskTitleLink", "javascript:doPopupWebPage('targetPage:03/backlog-detail.ptml,name:"+row.getString("TASK_PERSON_NAME")+",id:"+row.getString("TASK_ID")+"',{width:900,height:350})");
				jsonArray2.put(jsonObject21);
			}
			jsonObjectOndo.put("datas", jsonArray2);
			jsonArray.put(jsonObjectOndo);
			//已办
			String didCount = String.valueOf(didList.size());
			JSONObject jsonObjectDid = new JSONObject();
			jsonObjectDid.put("tabTitle", "已办("+didCount+")");
			jsonObjectDid.put("tabCode", "ONDO");
			jsonObjectDid.put("moreURL", "javascript:doRedirectPortalPage('targetPage:03/backlog-select.ptml,typeCode:DID')");
			jsonObjectDid.put("showHeader", true);
			jsonArray1 = new JSONArray();
			jsonObject11 = new JSONObject();
			jsonObject11.put("code", "taskTitle");
			jsonObject11.put("name", "任务名称");
			jsonObject11.put("isLink", true);
			jsonObject11.put("isLinkURL", false);
			jsonObject12 = new JSONObject();
			jsonObject12.put("code", "taskSponsor");
			jsonObject12.put("name", "创建人");
			jsonObject12.put("isLink", false);
			jsonObject12.put("isLinkURL", false);
			jsonObject13 = new JSONObject();
			jsonObject13.put("code", "taskTime");
			jsonObject13.put("name", "时间");
			jsonObject13.put("isLink", false);
			jsonObject13.put("isLinkURL", false);
			jsonObject14 = new JSONObject();
			jsonObject14.put("code", "taskTitleLink");
			jsonObject14.put("name", "任务链接");
			jsonObject14.put("isLink", false);
			jsonObject14.put("isLinkURL", true);
			jsonArray1.put(jsonObject11);
			jsonArray1.put(jsonObject12);
			jsonArray1.put(jsonObject13);
			jsonArray1.put(jsonObject14);
			jsonObjectDid.put("propertyConfigs", jsonArray1);
			jsonArray2 = new JSONArray();
			for(int i=0;i<didList.size();i++){
				DataRow row = didList.get(i);
				JSONObject jsonObject21 = new JSONObject();
				jsonObject21.put("taskTitle", row.getString("TASK_TITLE"));
				jsonObject21.put("taskSponsor", row.getString("TASK_PERSON_NAME"));
				String date = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_HORIZONTAL, (Date)row.get("TASK_TIME"));
				jsonObject21.put("taskTime",date);
				jsonObject21.put("taskTitleLink", "javascript:doPopupWebPage('targetPage:03/backlog-detail.ptml,name:"+row.getString("TASK_PERSON_NAME")+",id:"+row.getString("TASK_ID")+"',{width:900,height:350})");
				jsonArray2.put(jsonObject21);
			}
			jsonObjectDid.put("datas", jsonArray2);
			jsonArray.put(jsonObjectDid);
			responseText = jsonArray.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer getTaskRecord(DataParam param){
		String responseText = null;
		try {
			String taskId = param.getString("id");
			String name = param.getString("name");
			ExtTaskManage extTaskManage = this.lookupService(ExtTaskManage.class);
			DataRow record = extTaskManage.getTaskRecord(taskId);
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("id", record.getString("TASK_ID"));
			jsonObject.put("title", record.getString("TASK_TITLE"));
			jsonObject.put("person", record.getString("TASK_PERSON"));
			jsonObject.put("money", record.get("TASK_MONEY"));
			jsonObject.put("name", name);
			jsonObject.put("content", record.getString("TASK_CONTENT"));
			String opinionContent = record.getString("TASK_OPINION");
			if("".equals(opinionContent)){
				opinionContent = "无";
			}
			jsonObject.put("opinionContent",opinionContent);
			String time = DateUtil.getDateByType(DateUtil.YYMMDDHHMISS_HORIZONTAL, (Date)record.get("TASK_TIME"));
			jsonObject.put("time",time);
			jsonObject.put("state", record.getString("TASK_STATE"));
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	
	@PageAction
	public ViewRenderer getGridData(DataParam param){
		String responseText = null;
		try {
			String typeCode = param.getString("typeCode");
			ExtTaskManage extTaskManage = this.lookupService(ExtTaskManage.class);
			DataRow row = extTaskManage.getUserInfoRecord("admin");
			List<DataRow> records  = null;
			if(TODO.equals(typeCode)){
				records = extTaskManage.findTaskRecords("",TODO);
			}else if(ONDO.equals(typeCode)){
				records = extTaskManage.findTaskRecords("",ONDO);
			}else{
				records = extTaskManage.findTaskRecords("",DID);
			}
			JSONArray jsonArray = new JSONArray();
			for(int i = 0;i<records.size();i++){
				JSONObject jsonObject = new JSONObject();
				DataRow record = records.get(i);
				jsonObject.put("id", record.getString("TASK_ID"));
				jsonObject.put("title", record.getString("TASK_TITLE"));
				jsonObject.put("person", record.getString("TASK_PERSON"));
				String money = String.valueOf(record.getInt("TASK_MONEY"));
				jsonObject.put("money",money);
				jsonObject.put("name", row.getString("USER_NAME"));
				jsonObject.put("content", record.getString("TASK_CONTENT"));
				String opinionContent = record.getString("TASK_OPINION");
				if("".equals(opinionContent)){
					opinionContent = "无";
				}
				jsonObject.put("opinionContent",opinionContent);
				String time = DateUtil.getDateByType(DateUtil.YYMMDDHHMISS_HORIZONTAL, (Date)record.get("TASK_TIME"));
				jsonObject.put("time",time);
				jsonObject.put("state", record.getString("TASK_STATE"));
				jsonArray.put(jsonObject);
			}
			responseText = jsonArray.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
	}
	
	
}
