package com.agileai.portal.controller.bar;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.portal.bizmoduler.bar.ExtBarLinkManage;

public class ExtBarLinkManageListHandler
        extends StandardListHandler {
    public ExtBarLinkManageListHandler() {
        super();
        this.editHandlerClazz = ExtBarLinkManageEditHandler.class;
        this.serviceId = buildServiceId(ExtBarLinkManage.class);
    }

    protected void processPageAttributes(DataParam param) {
        initMappingItem("NEW_PAGE",
                        FormSelectFactory.create("BOOL_DEFINE").getContent());
    }

    protected void initParameters(DataParam param) {
        initParamItem(param, "pageId", "");
        initParamItem(param, "portletId", "");
    }

    protected ExtBarLinkManage getService() {
        return (ExtBarLinkManage) this.lookupService(this.getServiceId());
    }
}
