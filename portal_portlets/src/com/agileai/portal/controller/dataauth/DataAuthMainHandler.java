package com.agileai.portal.controller.dataauth;

import java.util.Enumeration;
import java.util.List;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.SimpleHandler;
import com.agileai.hotweb.domain.FormSelect;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.dataauth.SecurityDataRestypeManage;
import com.agileai.portal.bizmoduler.dataauth.SecurityGroupAuthManage;

public class DataAuthMainHandler extends SimpleHandler{
	
	public DataAuthMainHandler(){
		super();
	}
	
	public ViewRenderer prepareDisplay(DataParam param) {
		this.setAttributes(param);
		Enumeration<String> enumer = request.getAttributeNames();
		while(enumer.hasMoreElements()){
			String key = enumer.nextElement();
			Object value = request.getAttribute(key);
			this.setAttribute(key, value);
		}
		
		//显示资源类别
		this.setAttribute("sel_resType", getResTypeSelect(param));
		this.processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
	
	/**
	 * 显示实体
	 * @param param
	 * @return
	 */
	@PageAction
	public ViewRenderer displayAuthValue(DataParam param) {
		DataRow resTypeRow = getSecurityDataRestypeService().getRecordByResType(param);
		FormSelect authValueSelect = FormSelectFactory
			.create(resTypeRow.getString("SEC_ENTITY_CODE"))
				.addHasBlankValue(false)
					.addSelectedValue(param.get("sel_resId"));
					
        return new AjaxRenderer(authValueSelect.toString());
	}
	
	/**
	 * 显示群组
	 * @param param
	 * @return
	 */
	@PageAction
	public ViewRenderer displayGroupRef(DataParam param) {
		FormSelect groupRefSelect = getGroupRefSelect(param);
        return new AjaxRenderer(groupRefSelect.toString());
	}
	
	/**
	 * 保存群组权限控制表
	 * @param param
	 * @return
	 */
	@PageAction
	public ViewRenderer saveGroupAuth(DataParam param) {
		getSecurityGroupAuthService().saveGroupAuth(param);
        return new AjaxRenderer("SUCCESS");
	}
	
	/**
	 * 删除群组权限控制表
	 * @param param
	 * @return
	 */
	@PageAction
	public ViewRenderer delGroupAuth(DataParam param) {
		getSecurityGroupAuthService().delGroupAuth(param);
        return new AjaxRenderer("SUCCESS");
	}
	
	private FormSelect getResTypeSelect(DataParam param){
		List<DataRow> authTypeList = this.getSecurityDataRestypeService().findRecords(param);
		FormSelect authTypeSelect = new FormSelect();
		authTypeSelect.setKeyColumnName("SEC_RES_CODE");
		authTypeSelect.setValueColumnName("SEC_RES_NAME");
		authTypeSelect.addHasBlankValue(false);
		authTypeSelect.putValues(authTypeList);
		authTypeSelect.addSelectedValue(param.get("sel_resType"));
		return authTypeSelect;
	}
	
	private FormSelect getGroupRefSelect(DataParam param){
		List<DataRow> groupRefList = this.getSecurityGroupAuthService().findRecords(param);
		FormSelect groupRefSelect = new FormSelect();
		groupRefSelect.setKeyColumnName("GRP_ID");
		groupRefSelect.setValueColumnName("GRP_NAME");
		groupRefSelect.addHasBlankValue(false);
		groupRefSelect.putValues(groupRefList);
		groupRefSelect.addSelectedValue(param.get("sel_groupId"));
		return groupRefSelect;
	}
	
    protected SecurityDataRestypeManage getSecurityDataRestypeService() {
        return (SecurityDataRestypeManage) this.lookupService("securityDataRestypeManageService");
    }
    
    protected SecurityGroupAuthManage getSecurityGroupAuthService() {
        return (SecurityGroupAuthManage) this.lookupService("securityGroupAuthManageService");
    }
    
}
