package com.agileai.portal.controller.notice;

import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.portal.bizmoduler.notice.ExtMsgReceiveboxManage;

public class ExtMsgReceiveboxManageListHandler
        extends StandardListHandler {
	
    public ExtMsgReceiveboxManageListHandler() {
        super();
        this.editHandlerClazz = ExtMsgReceiveboxManageEditHandler.class;
        this.serviceId = buildServiceId(ExtMsgReceiveboxManage.class);
    }
    
    @PageAction
 	public ViewRenderer displayWin(DataParam param){
     	String userCode = request.getParameter("userCode");
     	param.put("userCode", userCode);
 		List<DataRow> dataList = getService().findWinRecords(param);
 		DataRow dataTotal = getService().findWinTotal(param);
 		
 		JSONArray jsonArray = new JSONArray();
 		if (dataList != null && dataList.size() > 0) {
 			for (int i = 0; i < dataList.size(); i++) {
 				
 				DataRow dataLine = dataList.get(i);
 				JSONObject jsonObject = new JSONObject();
 				jsonObject.put("id", dataLine.stringValue("RECEIVE_ID"));
 				jsonObject.put("theme", dataLine.stringValue("MSG_THEME"));
 				jsonObject.put("time", dataLine.stringValue("MSG_CREATE_TIME").substring(5));
 				jsonArray.add(jsonObject);
 			}
 		}
 		
 		JSONObject json = new JSONObject();
 		json.put("total", dataTotal.stringValue("TOTAL"));
 		json.put("list", jsonArray);
 		
 		return new AjaxRenderer(json.toString());
 	}
    
    public  ViewRenderer doBatchDeleteAction(DataParam param) {
    	String ids = param.get("ids");
    	String[] idArray = ids.split(",");
    	for(int i=0;i < idArray.length;i++ ){
        	String id = idArray[i];
        	DataParam idParam = new DataParam();
        	idParam.put("GUID", id);
        	getService().deletRecord(idParam);
    	}
		return prepareDisplay(param);
    	
   	}

    protected void processPageAttributes(DataParam param) {
        setAttribute("msgViewFlag",
                FormSelectFactory.create("MSG_VIEW_FLAG")
                                 .addSelectedValue(param.get("msgViewFlag")));
    }

    protected void initParameters(DataParam param) {
        initParamItem(param, "msgTheme", "");
    }

    protected ExtMsgReceiveboxManage getService() {
        return (ExtMsgReceiveboxManage) this.lookupService(this.getServiceId());
    }
}
