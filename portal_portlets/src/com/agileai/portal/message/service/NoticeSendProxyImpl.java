package com.agileai.portal.message.service;

import java.util.List;

import javax.jws.WebService;

import com.agileai.common.AppConfig;
import com.agileai.domain.DataParam;
import com.agileai.hotweb.common.BeanFactory;
import com.agileai.portal.bizmoduler.notice.ExtMsgContentManage;
import com.agileai.portal.bizmoduler.notice.SecurityUserQuery;
import com.agileai.portal.message.MessageHandler;

@WebService(serviceName = "NoticeSend", endpointInterface = "com.agileai.portal.message.service.NoticeSendProxy")
public class NoticeSendProxyImpl implements NoticeSendProxy {
	private MessageHandler messageHandler = null;
	private AppConfig appConfig = null;
	
	public NoticeSendProxyImpl(){
		
	}
	
	private MessageHandler getMessageHandler(){
		if (this.messageHandler == null){
			this.messageHandler = new MessageHandler(this.appConfig);
		}
		return this.messageHandler;
	}
	
	@Override
	public String sendMessage(List<String> userCodes, String msgTitle,
			String msgContent, String senderCode) {
		String messageId = null; 
		messageId = saveMsg(listToString(userCodes), msgTitle, msgContent, senderCode);
		getMessageHandler().publishNotice(userCodes);
		return messageId;
	}
	
	private String saveMsg(String userCodes, String msgTheme, String msgContent, String senderCode) {
		DataParam param = new DataParam();
		param.put("MSG_THEME", msgTheme);
		param.put("MSG_RECEIVEOR_CODE", userCodes);
		param.put("MSG_NOTICE_CONTENT", msgContent);
		param.put("MSG_SENDER_CODE", senderCode);
		
		getNoticeService().createRecord(param);
		
		String messageId = param.get("MSG_ID");
		return messageId;
	}

	private String listToString(List<String> list) {  
	    StringBuilder sb = new StringBuilder();  
	    if (list != null && list.size() > 0) {  
	        for (int i = 0; i < list.size(); i++) {  
	            if (i < list.size() - 1) {  
	                sb.append(list.get(i) + ",");  
	            } else {  
	                sb.append(list.get(i));  
	            }  
	        }  
	    }  
	    return sb.toString();  
	} 
	
    protected SecurityUserQuery getUserService() {
        return (SecurityUserQuery) BeanFactory.instance().getBean("securityUserQueryService");
    }
    
    protected ExtMsgContentManage getNoticeService() {
    	return (ExtMsgContentManage) BeanFactory.instance().getBean("extMsgContentManageService");
    }

	@Override
	public void kickoutOnline(String userId, String loginTime) {
		getMessageHandler().publishKickout(userId, loginTime);
	}

	public AppConfig getAppConfig() {
		return appConfig;
	}

	public void setAppConfig(AppConfig appConfig) {
		this.appConfig = appConfig;
	}
}
