
(function(jQuery){
		  
	this._notice = function(params) {
		var _total		= params.data.total;//总数
		var _auto		= params.auto;		//是否自动关闭
		var _time 		= params.time;		//显示时间
		var _data 		= params.data;		//显示内容
		var _path		= params.path;		//当前工作路径
		var _usercode	= params.usercode;	//发送人
		
		if (_total == 0) {
			return;
		}
		
		this._init(_total, _data.list, _path, _usercode);
		
		if(_auto == 'true') {
			setTimeout('this._close()', _time);
		}
		
	};
	
	this._init = function(total, data, path, usercode) {
		if ($("#div_msg").is("div")) {
			$("#div_msg_title span").html('消息提醒('+ total +')');
			var lis = '';
		    for(var i in data){
		    	lis += '<li><a href="javascript:doPopupPageBox(\'/portal_portlets/index?ExtMsgFrame&userCode='+ usercode +'\', {title:\'消息提醒\',width:\'980\',height:\'460\'});"><span id="span_msg_type">'+ data[i].theme +'</span><span id="span_msg_time">'+ data[i].time +'</span></a></li>';
		    }
			$("#div_msg_content").html('<ul>'+ lis +'</ul>');
		}else{
			var lis = '';
		    for(var i in data){
		    	lis += '<li><a href="javascript:doPopupPageBox(\'/portal_portlets/index?ExtMsgFrame&userCode='+ usercode +'\', {title:\'消息提醒\',width:\'980\',height:\'460\'});"><span id="span_msg_type">'+ data[i].theme +'</span><span id="span_msg_time">'+ data[i].time +'</span></a></li>';
		    }
			$(document.body).prepend(
						'<div id="div_msg">' +			
							'<div id="div_msg_title">' +	
								'<span>消息提醒('+ total +')</span>' +	
								'<a href="javascript:this._close();" id="a_msg_close"><img title="点击关闭" src="'+ path +'/images/notice/guanbi.jpg"/></a>' +
							'</div>' +		
							'<div id="div_msg_content">' +	
								'<ul>'+ lis +'</ul>' +
							'</div>' +
							'<div id="div_msg_more"><a id="a_msg_more" href="#"><img src="'+ path +'/images/notice/msgsearch.png" />&nbsp;查看</a></div>' +			
						'</div>');			
		}
		$("#a_msg_more").attr("href", "javascript:doPopupPageBox('/portal_portlets/index?ExtMsgFrame&userCode="+ usercode +"', {title:'消息提醒',width:'980',height:'460'});");		
		$("#div_msg").slideDown(2000);
	};
	
	this._close = function() {
		$("#div_msg").slideUp(1000);
		setTimeout('this._remove()', 2000);
	};
	
	this._remove = function() {
		$("#div_msg").remove();
	};

	jQuery.messager = this;
	return jQuery;
})(jQuery);


