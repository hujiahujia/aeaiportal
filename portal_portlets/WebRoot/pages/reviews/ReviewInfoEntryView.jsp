<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<portlet:resourceURL id="saveReview" var="saveReviewURL"></portlet:resourceURL>
<style>
.postrequired{
	border: solid 1px #CCC;
	background-color: #FFFFFF;
	margin: 0px;
	width:auto;
	float: left;
	width:100%;
	height:200px;
}
#button_publish{
	height: 30px;
	width: 80px;
	font-size: 15px;
	font-weight: bolder;
}
#rewtable{
	padding:2px;
	margin: auto;
}
</style>
<%
	String portletId = (String)request.getAttribute("portletId");
%>
<script type="text/javascript">
function checkChanged() {
	var checked = $('#rew_anonymity').attr('checked');
	if (checked) {
		$('#REW_ANONYMITY').val('1');
	} else {
		$('#REW_ANONYMITY').val('0');
	}
}
function saveReivew(){
	postAction('${saveReviewURL}',{formId:'<portlet:namespace/>Form',onComplete:function(responeText){
		if (responeText == 'success'){
			$ ("textarea").val ('');//清空textarea
			recordtotal = recordtotal+1;
			doAjaxRefreshPortalPage('recordtotal:'+recordtotal); 
		}
	}});
}

</script>
<form id="<portlet:namespace/>Form">
<div>
<table width="100%" id="rewtable">
  <tr>
	<td height:100px align="center">
  	  <textarea name="REW_CONTENT" id="REW_CONTENT" class="postrequired" rows="10" cols="70"></textarea>
	</td>
  </tr>
  <tr>
	<td align="right">
	  <input type="checkbox" id="rew_anonymity" onclick="checkChanged()"/><a style="font-size:15px;">匿名方式</a>
  	  <input type="button" id="button_publish" value="发  布 " onclick="saveReivew()"/>
	</td>
  </tr>
</table>
</div>
<input type="hidden" name="REW_ANONYMITY" id="REW_ANONYMITY" value="0"/>
<input type="hidden" name="USER_CODE" id="USER_CODE" value="${currentUserId }"/>
<input type="hidden" name="INFO_ID" id="INFO_ID" value="${infomationId }"/>
</form>
