<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<link rel="stylesheet" href="<%=request.getContextPath()%>/css/noticewin.css" type="text/css">
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.notice.js"></script>
<portlet:defineObjects/>
<portlet:resourceURL id="getAjaxData" var="getAjaxDataURL"></portlet:resourceURL>
<%
Boolean enableKickoutPolicy = (Boolean)request.getAttribute("enableKickoutPolicy");
Boolean immediateLoad = (Boolean)request.getAttribute("immediateLoad");
String httpSessionId = request.getSession(false).getId();
%>
<script type="text/javascript">
<!-- 消息提醒 -->
$(function(){
	 function showNoticeMessage() {
		var handler = "<%=request.getContextPath() %>/resource?ExtMsgReceiveboxManageList&actionType=displayWin&userCode=${userCode}";
 		$.ajax({   
		    url:handler,
		    type:'post',
		    dataType:'json',
		    error:function(XMLHttpRequest, textStatus, errorThrown) {
			       alert('消息提醒列表读取失败，请重新刷新页面或联系管理员,状态码:' + XMLHttpRequest.readyState + ', ErrorThrow:' + errorThrown + ', Status: ' + XMLHttpRequest.status);
			},
		    success:function(responseText){
 				params = {
		 				auto:'${isHide}',
						time:'${time}',
						data:responseText,
						path:'<%=request.getContextPath()%>',
						usercode:'${userCode}',
					};
				$.messager._notice(params);
		    }
		});
	}
	 
	function kickoutOnline(loginTime){
		var handler = "/portal/index?Portal&actionType=kickout&loginTime="+loginTime;
 		$.ajax({   
		    url:handler,
		    type:'post',
		    dataType:'text',
		    error:function(XMLHttpRequest, textStatus, errorThrown) {
			       alert('消息提醒列表读取失败，请重新刷新页面或联系管理员,状态码:' + XMLHttpRequest.readyState + ', ErrorThrow:' + errorThrown + ', Status: ' + XMLHttpRequest.status);
			},
		    success:function(responseText){
		    	//alert(responseText);
		    	if(responseText == "Y"){
		    		alert("用户${userCode}再次登录系统，当前登录用户将被踢出！");
		    		logout();
		    	}
		    }
		});		
	}	 
	<%if (immediateLoad){%>
	setTimeout(showNoticeMessage, 1000);
	<%}%>
	
	
	var messageHandler = {
		excute:function(message){
			//alert('message is ' + message);
			if (message != null) {
				var text = $(message).text();
				//alert('text is ' + text);
				var msgObj = $.parseJSON(text);
				if (msgObj.type == 'kickout'){
					var loginTime = msgObj.loginTime;
					kickoutOnline(loginTime);
				}else{
					setTimeout(showNoticeMessage, 10);
				}
			}
		}
	};

	var listenerId = "<portlet:namespace/><%=httpSessionId%>";
	amq.addListener(listenerId, 'topic://${userCode}', messageHandler.excute);
	
	
	$(window).unload(function() {
		var listenerId = "<portlet:namespace/><%=httpSessionId%>";
		amq.removeListener(listenerId, 'topic://${userCode}');
	});
});
</script>
