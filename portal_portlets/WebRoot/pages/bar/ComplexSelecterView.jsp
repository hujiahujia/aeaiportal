<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<portlet:resourceURL id="getAjaxData" var="getAjaxDataURL">
</portlet:resourceURL>
<%@page import="java.util.HashMap"%>
<script language="javascript" type="text/javascript" src="<%=request.getContextPath()%>/js/datepicker/WdatePicker.js"></script>
<%
String portletId = (String)request.getAttribute("portletId");

String namespace = (String)request.getAttribute("NAMESPACE");
String isSetting = (String)request.getAttribute("isSetting");
String defaultShow = (String)request.getAttribute("defaultShow");
String showList = (String)request.getAttribute("showList");
String showListName = (String)request.getAttribute("showListName");

String redirectURL = (String)request.getAttribute("redirectURL");
String redirectText = (String)request.getAttribute("redirectText");

String isShowTimeRange = (String)request.getAttribute("isShowTimeRange");
String relativePosition = (String)request.getAttribute("relativePosition");
String isOnlyOneLine = (String)request.getAttribute("isOnlyOneLine");
String isExtendBar = (String)request.getAttribute("isExtendBar");
String isConvertText = (String)request.getAttribute("isConvertText");
String extBarSyntax = (String)request.getAttribute("extBarSyntax");

String trigerCascade = (String)request.getAttribute("trigerCascade");

String isExtendLink = (String)request.getAttribute("isExtendLink");

String[] list = showList.split(",");
String[] listName = showListName.split(",");

HashMap filerMap = (HashMap)request.getAttribute("filerMap");
if ("true".equals(isSetting)){
%>
<form style='margin:3px 2px 3px 8px;;padding:2px;' id='<%=namespace%>_search'>
<%if ("Y".equals(isExtendBar) && "before".equals(relativePosition)){%>
<span id='extQueryBar' style='display:inline-block;margin-bottom:3px;'>
<%=extBarSyntax%>
</span>
<%if ("Y".equals(isOnlyOneLine) && "Y".equals(isShowTimeRange)){%>
<br />
<%}}%>

<span id='basQueryBar' style='display:inline-block;margin-left:5px;'>
<%
if ("Y".equals(isShowTimeRange)){
%>
查询方式 <select onchange='<%=namespace%>_doChangeFilterType();' name='filterType' id='filterType'>
<%
if (!"".equals(showList) && list.length > 0){
	for(int i = 0; i < list.length; i ++){
%>
<option value='<%=list[i]%>'><%=listName[i] %></option>
<%}}%>
</select>

<select name='year' id='year'>
<%
for (int i = 2000; i <= 2025; i ++){
%>
<option value='<%=i%>'><%=i%>年</option>
<%}%>
</select>

<span id='quarter_span'>
<input type='radio' name='quarter' onclick='doSearch<portlet:namespace/>();' id='quarter1' value='1'>第一季度</input>
<input type='radio' name='quarter' onclick='doSearch<portlet:namespace/>();' id='quarter2' value='2'>第二季度</input>
<input type='radio' name='quarter' onclick='doSearch<portlet:namespace/>();' id='quarter3' value='3'>第三季度</input>
<input type='radio' name='quarter' onclick='doSearch<portlet:namespace/>();' id='quarter4' value='4'>第四季度</input>
</span>
<select name='month' id='month'>
<%
for (int i = 1; i <= 12; i ++){
	String month = String.valueOf(i);
	if (i < 10){
		month = "0"+month;
	}
%>
<option value='<%=month%>'><%=month%>月</option>
<%} %>
</select>

<span id='date_span'>
<input type='text' class='Wdate' id='dateFrom' onFocus='WdatePicker({readOnly:true,doubleCalendar:true})' size="12"/>
至
<input type='text' class='Wdate' id='dateTo' onFocus='WdatePicker({readOnly:true,doubleCalendar:true})' size="12"/>
</span>

<span id='only_date_span'>
<input type='text' class='Wdate' id='selectedDate' onFocus='WdatePicker({readOnly:true,doubleCalendar:true})' size="12"/>
<%}%>
</span>
</span>

<%if ("Y".equals(isExtendBar) && "after".equals(relativePosition)){%>
<%if ("Y".equals(isOnlyOneLine) && "Y".equals(isShowTimeRange)){%>
<br />
<%}%>
<span id='extQueryBar' style='display:inline-block;margin-top:3px;'>
<%=extBarSyntax%>
</span>
<%}%>

<span id='querybtn_span'>
<input type='button' name='btnSearch' onclick='doSearch<portlet:namespace/>();' style="cursor:pointer" id='btnSearch' value='查 询' />
</span>

<%
if (redirectText != null && !redirectText.trim().equals("")){
%>
<span id='back_span'>
<input type='button' onclick='<%=namespace%>_doRedirect();' value='<%=redirectText%>' />
</span>
<%}%>

<%
if ("Y".equals(isExtendLink)){
%>
<span style="float:right;line-height:22px;height:25px;">
<%=request.getAttribute("extBarLinkSyntax")%>
</span>
<%}%>

<script language="javascript">
$('#<%=namespace%>_search #filterType').val('<%=defaultShow%>');
<%
if (filerMap.get("filterType") != null){
%>
$('#<%=namespace%>_search #filterType').val('<%=filerMap.get("filterType")%>');
<%}%>

<%
if (filerMap.get("quarter") != null){
%>
$('#<%=namespace%>_search input[name="quarter"][value="<%=filerMap.get("quarter")%>"]').attr('checked','checked');
<%}%>
<%=namespace%>_doChangeFilterType();

<%
if (filerMap.get("year") != null){
%>	
$('#<%=namespace%>_search #year').val('<%=filerMap.get("year")%>'); 
<%}%>

<%
if (filerMap.get("month") != null){
%>
$('#<%=namespace%>_search #month').val('<%=filerMap.get("month")%>');
<%}%>

<%
if (filerMap.get("dateFrom") != null){
%>
$('#<%=namespace%>_search #dateFrom').val('<%=filerMap.get("dateFrom")%>');
<%}%>

<%
if (filerMap.get("dateTo") != null){
%>
$('#<%=namespace%>_search #dateTo').val('<%=filerMap.get("dateTo")%>');
<%}%>

<%
if (filerMap.get("selectedDate") != null){
%>
$('#<%=namespace%>_search #selectedDate').val('<%=filerMap.get("selectedDate")%>');
<%}%>

function <%=namespace%>_setVisable(s){ 
    if(s == 'year'){ 
        $('#<%=namespace%>_search #year').show(); 
        $('#<%=namespace%>_search #month').hide(); 
        $('#<%=namespace%>_search #quarter_span').hide(); 
        $('#<%=namespace%>_search #date_span').hide(); 
		$('#<%=namespace%>_search #only_date_span').hide(); 
     } 
    if(s == 'yearMonth'){ 
        $('#<%=namespace%>_search #year').show(); 
        $('#<%=namespace%>_search #month').show(); 
        $('#<%=namespace%>_search #quarter_span').hide(); 
        $('#<%=namespace%>_search #date_span').hide(); 
		$('#<%=namespace%>_search #only_date_span').hide(); 
        $('#<%=namespace%>_search #month option').remove();
        $('#<%=namespace%>_search #month').append('<option value=01>1月</option>');
        $('#<%=namespace%>_search #month').append('<option value=02>2月</option>');
        $('#<%=namespace%>_search #month').append('<option value=03>3月</option>');
        $('#<%=namespace%>_search #month').append('<option value=04>4月</option>');
        $('#<%=namespace%>_search #month').append('<option value=05>5月</option>');
        $('#<%=namespace%>_search #month').append('<option value=06>6月</option>');
        $('#<%=namespace%>_search #month').append('<option value=07>7月</option>');
        $('#<%=namespace%>_search #month').append('<option value=08>8月</option>');
        $('#<%=namespace%>_search #month').append('<option value=09>9月</option>');
        $('#<%=namespace%>_search #month').append('<option value=10>10月</option>');
        $('#<%=namespace%>_search #month').append('<option value=11>11月</option>');
        $('#<%=namespace%>_search #month').append('<option value=12>12月</option>');
        $('#<%=namespace%>_search #month').val('" + filerMap.get("month") + "'); 
     } 
    if(s == 'yearQuarter'){ 
        $('#<%=namespace%>_search #year').show(); 
        $('#<%=namespace%>_search #month').hide(); 
        $('#<%=namespace%>_search #date_span').hide(); 
        $('#<%=namespace%>_search #only_date_span').hide(); 
		$('#<%=namespace%>_search #quarter_span').show(); 
     } 
    if(s == 'yearQuarterMonth'){ 
        $('#<%=namespace%>_search #year').show(); 
        $('#<%=namespace%>_search #month').show(); 
        $('#<%=namespace%>_search #quarter_span').show(); 
        $('#<%=namespace%>_search #date_span').hide(); 
		$('#<%=namespace%>_search #only_date_span').hide(); 
        if ($('#<%=namespace%>_search #quarter1').attr('checked') == true) { 
            <%=namespace%>_doChangeQuarter(1);
        } 
        if ($('#<%=namespace%>_search #quarter2').attr('checked') == true) { 
            <%=namespace%>_doChangeQuarter(2);
        } 
        if ($('#<%=namespace%>_search #quarter3').attr('checked') == true) { 
            <%=namespace%>_doChangeQuarter(3);
        } 
        if ($('#<%=namespace%>_search #quarter4').attr('checked') == true) { 
            <%=namespace%>_doChangeQuarter(4);
        } 
        $('#<%=namespace%>_search #month').val('" + filerMap.get("month") + "'); 
     } 
    if(s == 'date'){ 
        $('#<%=namespace%>_search #year').hide(); 
        $('#<%=namespace%>_search #month').hide(); 
        $('#<%=namespace%>_search #quarter_span').hide(); 
        $('#<%=namespace%>_search #date_span').show(); 
		$('#<%=namespace%>_search #only_date_span').hide(); 
     } 
    if(s == 'onlyDate'){ 
        $('#<%=namespace%>_search #year').hide(); 
        $('#<%=namespace%>_search #month').hide(); 
        $('#<%=namespace%>_search #quarter_span').hide(); 
        $('#<%=namespace%>_search #date_span').hide(); 
		$('#<%=namespace%>_search #only_date_span').show(); 
     }	 
} 

function <%=namespace%>_doChangeFilterType(){
    var s = $('#<%=namespace%>_search #filterType').val();
    <%=namespace%>_setVisable(s);
}

function <%=namespace%>_validate(flg) {
    if (flg == 0) {
		var dateFrom = $('#<%=namespace%>_search #dateFrom').val();
		var dateTo = $('#<%=namespace%>_search #dateTo').val();		
       if (dateFrom == ''){
            alert('开始日期不能为空');
            return false;
        }
        if (dateTo == ''){
            alert('结束日期不能为空');
            return false;
        }
        if (dateFrom > dateTo){
            alert('日期范围错误');
            return false; 
        }
    }
	else if (flg == 1) {
		var selectedDate = $('#<%=namespace%>_search #selectedDate').val();		
       if (selectedDate == ''){
            alert('指定日期不能为空');
            return false;
        }	
	}
    return true; 
}

function <%=namespace%>_getSelectedQuarter(){
    var quarter = '';
    if ($('#<%=namespace%>_search #quarter1').attr('checked') == 'checked'){
        quarter = '1';
    }
    if ($('#<%=namespace%>_search #quarter2').attr('checked') == 'checked'){
        quarter = '2';
    }
    if ($('#<%=namespace%>_search #quarter3').attr('checked') == 'checked'){
        quarter = '3';
    }
    if ($('#<%=namespace%>_search #quarter4').attr('checked') == 'checked'){
        quarter = '4';
    }
    return quarter;
}

function doSearch<portlet:namespace/>(){ 
	var filterType = $('#<%=namespace%>_search #filterType').val();
	var value = ''; 
	if (filterType == 'year') { 
	    value=',year:' + $('#<%=namespace%>_search #year').val(); 
	} 
	if (filterType == 'yearMonth') { 
	    value=',year:' + $('#<%=namespace%>_search #year').val(); 
	    value+=',month:' + $('#<%=namespace%>_search #month').val(); 
	} 
	if (filterType == 'yearQuarter') { 
	    value=',year:' + $('#<%=namespace%>_search #year').val(); 
	    var quarter = <%=namespace%>_getSelectedQuarter(); 
	    value+=',quarter:' + quarter; 
	} 
	if (filterType == 'yearQuarterMonth') { 
	    value=',year:' + $('#<%=namespace%>_search #year').val(); 
	    var quarter = <%=namespace%>_getSelectedQuarter(); 
	    value+=',quarter:' + quarter; 
	    value+=',month:' + $('#<%=namespace%>_search #month').val(); 
	} 
	if (filterType == 'date') { 
	    if (!<%=namespace%>_validate(0)) { 
	        return; 
	    } 
	    value=',dateFrom:' + $('#<%=namespace%>_search #dateFrom').val(); 
	    value+=',dateTo:' + $('#<%=namespace%>_search #dateTo').val(); 
	}
	//alert('filterType is ' + filterType);
	if (filterType == 'onlyDate') { 
	    if (!<%=namespace%>_validate(1)) { 
	        return; 
	    } 
	    value=',selectedDate:' + $('#<%=namespace%>_search #selectedDate').val(); 
	}	
	var params =  'filterType:' + filterType + value;
	//alert(params);
	params = appendParams(params);
	doAjaxRefreshPortalPage(params,{curPortletId:"<%=portletId%>"});
}

function appendParams(params){
	$("#extQueryBar input[type='text']").each(function(index, domEle){
		var paramName = $(domEle).attr("name"); 
		params=params+","+paramName+":"+ $(domEle).val();
	});
	$("#extQueryBar select").each(function(index, domEle){
		var paramName = $(domEle).attr("name"); 
		params=params+","+paramName+":"+ $(domEle).val();
	});
	var checkBoxMap = new Map();
	$("#extQueryBar input[type='checkbox']").each(function(index, domEle){
		var tempCheckBoxName = $(domEle).attr("name");
		if (!checkBoxMap.containsKey(tempCheckBoxName)){
			checkBoxMap.put(tempCheckBoxName,'');
		}
		if ($(domEle).attr('checked')){
			var tempCheckBoxValue = checkBoxMap.get(tempCheckBoxName);
			if (tempCheckBoxValue == ''){
				tempCheckBoxValue = $(domEle).val();				
			}else{
				tempCheckBoxValue = tempCheckBoxValue + ";" + $(domEle).val();
			}
			checkBoxMap.remove(tempCheckBoxName);
			checkBoxMap.put(tempCheckBoxName,tempCheckBoxValue);
		}
	});
	if (!checkBoxMap.isEmpty()){
		for (var i=0;i < checkBoxMap.size();i++){
			var element = checkBoxMap.element(i);
			var paramName = element.key;
			var paramValue = element.value;
			params=params+","+paramName+":" + paramValue;
		}
	}
	
	<%
	if ("Y".equals(isConvertText)){
	%>
	var convertKeys = "";
	$("#extQueryBar input[type='text']").each(function(index, domEle){
		convertKeys = convertKeys+";"+$(domEle).attr("name");
	});
	if (convertKeys.length > 1){
		params=params+",convertKeys:"+convertKeys.substring(1,convertKeys.length);	
	}
	<%}%>
	return params;
}

function <portlet:namespace/>_doRedirect(){
	window.location.href="${redirectURL}";
}

<%
if ("Y".equals(isExtendBar)){
%>
var __renderPortlet<portlet:namespace/> = function(){
	sendAction('${getAjaxDataURL}',{dataType:'text',onComplete:function(responseText){
		//alert(responseText);	
		var data = jQuery.parseJSON(responseText);
		buildQueryBar<portlet:namespace/>(data);
		
		<%if ("Y".equals(trigerCascade)){%>
		var params = appendParams('');
		if (params.length > 0 && params.substring(0,1)==','){
			params = params.substring(1,params.length);
		}
		var datas = parsePortalParams(params);
		var targetPage = parsePageId();
		datas = 'targetPage='+targetPage+'&'+datas;
		$.ajax({
		   type: "POST",
		   url: "/portal/ParamInteractive",
		   data:datas,
		   dataType:'text',
		   success:function(msg){
				sendAction('${getAjaxDataURL}',{dataType:'text',onComplete:function(responseText){
					//alert(responseText);
					var newData = jQuery.parseJSON(responseText);
					buildQueryBar<portlet:namespace/>(newData);		
					doSearch<portlet:namespace/>();				
				}});			   
		   }
		});
		
		<%}else{%>
		doSearch<portlet:namespace/>();
		<%}%>
	}});
};
__renderPortlets.put("<%=portletId%>",__renderPortlet<portlet:namespace/>);
__renderPortlet<portlet:namespace/>();

function buildQueryBar<portlet:namespace/>(data){
	for (var i=0;i < data.length;i++){
		var formAtom = data[i];
		var atomId = formAtom.id;
		var atomType = formAtom.type;
		if ("text" == atomType){
			var atomValue = formAtom.value;
			$("#"+atomId).val(atomValue);
		}
		else if ("select" == atomType){
			$("#"+atomId).empty();
			var values = formAtom.values;
			if (values){
				for (var j=0;j < values.length;j++){
					var temp = values[j];
					var optionText = temp.name;
					var optionValue = temp.value;
					$("#"+atomId).append("<option value='"+optionValue+"'>"+optionText+"</option>");
					if (temp.selected){
						$("#"+atomId).val(optionValue)
					}
				}			
			}
		}
		else if ("checkbox" == atomType){
			var values = formAtom.values;
			$("#"+atomId+"Span").html("");
			for (var j=0;j < values.length;j++){
				var temp = values[j];
				var text = temp.name;
				var value = temp.value;
				var checkBoxId = atomId+"_"+ j;
				var checkboxHtml = "<label><input type='checkbox' name='" + atomId +"' value='" + value +"' id='" + checkBoxId +"' />";
				checkboxHtml = checkboxHtml + text + "</label>";
				$("#"+atomId+"Span").append(checkboxHtml);	
				if (temp.selected){
					$("#"+checkBoxId).attr("checked",true);
				}
			}
		}
	}
}

function doReloadQueryBar<portlet:namespace/>(){
	var params = appendParams('');
	var datas = parsePortalParams(params);
	var targetPage = parsePageId();
	datas = 'targetPage='+targetPage+'&'+datas;
	$.ajax({
	   type: "POST",
	   url: "/portal/ParamInteractive",
	   data:datas,
	   dataType:'text',
	   success:function(msg){
		   __renderPortlet<portlet:namespace/>();			   
	   }
	});
}
<%}%>
</script>
</form>
<%
}else{
out.println("请正确设置属性");
}
%>