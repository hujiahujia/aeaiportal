<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<%
String isCache = (String)request.getAttribute("isCache");
%>
<portlet:actionURL name="saveConfig" var="saveConfigURL"></portlet:actionURL>
<portlet:renderURL portletMode="edit" var="editURL"></portlet:renderURL>

<form id="<portlet:namespace/>DataListConfig">
  <table width="90%" border="1">
    <tr>
      <td width="120">高度</td>
      <td>
        <input type="text" name="height" id="height" value="${height}" />
      </td>
    </tr>
    <tr>
      <td width="120">数据路径</td>
      <td>
        <input name="dataURL" type="text" id="dataURL" value="${dataURL}" size="60" /></td>
    </tr>
     <tr>
      <td width="120">默认值</td>
      <td>
        <input type="text" name="defaultVariableValues" id="defaultVariableValues" size="50" value="${defaultVariableValues}" />
      </td>
    </tr>
    <tr>
      <td width="120">列表样式</td>
      <td>
        <select name="tdClass" id="tdClass">
        	<option value="">请选择</option>
          	<option value="1">简洁样式</option>
          	<option value="2">图标样式1</option>
          	<option value="3">图标样式2</option>
          	<option value="4">图标样式3</option>
          	<option value="5">图标样式4</option>
          	<option value="6">图标样式5</option>
        </select>
      </td>
    </tr>
    <tr>
      <td width="120">是否缓存</td>
      <td>
        <input type="radio" name="isCache" id="isCacheY" value="Y" />
        是
      &nbsp;&nbsp;
        <input type="radio" name="isCache" id="isCacheN" value="N" />
      否</td>
    </tr> 
    <tr>
      <td width="120">缓存时间</td>
      <td>
        <input type="text" name="cacheMinutes" id="cacheMinutes" value="${cacheMinutes}" />
        分钟</td>
    </tr> 
    <tr>
      <td colspan="2" align="center">
        <input type="button" name="button" id="button" value="保存" onclick="submitAction('${saveConfigURL}',{formId:'<portlet:namespace/>DataListConfig'})" /> &nbsp;
        <input type="button" name="button2" id="button2" value="取消" onclick="fireAction('${editURL}')"/></td>
    </tr>
  </table>
</form>
<script language="javascript">
$('#<portlet:namespace/>DataListConfig #tdClass').val('${tdClass}');

<%if ("Y".equals(isCache)){%>
	$('#<portlet:namespace/>DataListConfig #isCacheY').attr("checked","checked");
<%}else{%>
	$('#<portlet:namespace/>DataListConfig #isCacheN').attr("checked","checked");
<%}%>

</script>