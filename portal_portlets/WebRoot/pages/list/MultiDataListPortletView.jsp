<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<portlet:resourceURL id="getAjaxData" var="getAjaxDataURL">
</portlet:resourceURL>
<%
String portletId = (String)request.getAttribute("portletId");
String isSetting = (String)request.getAttribute("isSetting");
String idSpliter = (String)request.getAttribute("idSpliter");
String columnName = (String)request.getAttribute("columnName");
String listItemKey = (String)request.getAttribute("listItemKey");
String selectedIds = (String)request.getAttribute("selectedIds");

if("true".equals(isSetting)){
 %>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/TableView.css" />
<script type="text/javascript" language="javascript" src="<%=request.getContextPath()%>/js/TableView.js"></script>
<div id="table_<portlet:namespace/>"></div>
<script type=text/javascript>
    var table<portlet:namespace/> = null;
	TableView.prototype.selectAllCallBack=function(isAllChecked){
		var selectedAllValue = 'N';
		if (isAllChecked){
			selectedAllValue = 'Y';
		}
		var selectDim = this.getSelectedKeys();
		var dim = '';
		if (isAllChecked){
			for (var i=0;i < selectDim.length;i++){
				dim += "<%=idSpliter%>"+selectDim[i];
			}
			if (dim.length > 0){
				dim = dim.substring('<%=idSpliter%>'.length,dim.length);
			}	
		}else{
			$('.datagrid tr th input').each(function(){
				this.checked = '';
			});
			$('.datagrid tr td input').each(function(){
				this.checked = '';
			});
		}
		doAjaxRefreshPortalPage('selectedAll:'+selectedAllValue+',<%=listItemKey%>:'+dim);
	}
	
	TableView.prototype.selectOneCallBack=function(){
		var selectDim = this.getSelectedKeys();
		//alert(selectDim);
		var dim = '';
		for (var i=0;i < selectDim.length;i++){
			dim += "<%=idSpliter%>"+selectDim[i];
		}
		if (dim.length > 0){
			dim = dim.substring('<%=idSpliter%>'.length,dim.length);
		}
		doAjaxRefreshPortalPage('<%=listItemKey%>:'+dim);
	}
	
	// 获取数据
	table<portlet:namespace/> = new TableView("table_<portlet:namespace/>");
	table<portlet:namespace/>.header = {
		id			: 'Id',
		name		: '<%= columnName%>'
	};
	
	table<portlet:namespace/>.dataKey = 'id';
    table<portlet:namespace/>.display.count = false;
	table<portlet:namespace/>.display.filter = false;
	table<portlet:namespace/>.display.title = true;
	table<portlet:namespace/>.display.marker = true;
	table<portlet:namespace/>.display.pager = false;

	var __renderPortlet<portlet:namespace/> = function(){
		sendAction('${getAjaxDataURL}',{dataType:'text',asynchronous:'true',onComplete:function(responseText){
			if(responseText != null){
				//alert(responseText);
			    var jsonData= eval(responseText);
				//jsonData = [{id: 0, name: 'None'},{id: 1, name: 'Tom'},{id: 2, name: 'Jerry'},{id: 3, name: 'ghc'}];
				table<portlet:namespace/>.addRange(jsonData);
				table<portlet:namespace/>.render();
				var selectCount = 0;
				for(var k=0;k<jsonData.length;k++){
					var curObj =  jsonData[k];
					if (curObj.selected){
						selectCount = selectCount+1;
						var curObjId = curObj.id;
						$('.datagrid tr td input').each(function(){
							//alert('this.value is ' + this.value);
							if (curObjId == this.value){
								this.checked = true;
								$(this).parent().parent().addClass('tv_row odd marked')
							}
						});						
					}
				}
				if (selectCount == jsonData.length){
					$('.datagrid tr th input').each(function(){
						this.checked = true;
					});		
				}
			}
		}});
	};
	__renderPortlets.put("<%=portletId%>",__renderPortlet<portlet:namespace/>);
	__renderPortlet<portlet:namespace/>();
	
</script>
<%
}else{
	out.println("请正确相关设置属性！");
} 
%>
