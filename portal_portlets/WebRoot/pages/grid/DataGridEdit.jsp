<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>
<%
String pageId = (String)request.getAttribute("pageId"); 
String portletId = (String)request.getAttribute("portletId"); 

String isCache = (String)request.getAttribute("isCache"); 
String isShowIndexColumn = (String)request.getAttribute("isShowIndexColumn"); 
String isStripeRows = (String)request.getAttribute("isStripeRows"); 
%>
<portlet:actionURL name="saveConfig" var="saveConfigURL"></portlet:actionURL>
<portlet:renderURL portletMode="edit" var="editURL"></portlet:renderURL>
<form id="<portlet:namespace/>DataGridConfig">

<table border="1" >
	<tr>
		<td width="120">高度&nbsp;<span style="color: red;">*</span></td>
		<td><input id="height" name="height" type="text" value="${height}" />
	    <input type="button" name="button3" id="button3" value="字段配置" onclick="popupGridFieldsConfig('<%=pageId%>','${portletId}')" /></td>
	</tr>
	<tr>
		<td width="120">宽度&nbsp;<span style="color: red;">*</span></td>
		<td><input id="width" name="width" type="text" value="${width}"></td>
	</tr>
	<tr>
		<td width="120">自定义表头(html)</td>
		<td><textarea id="customHead" name="customHead" rows="5" cols="80">${customHead}</textarea></td>
	</tr>
	<tr>
		<td width="120">数据URL&nbsp;<span style="color: red;">*</span></td>
		<td><input id="dataURL" name="dataURL" size="80" type="text" value="${dataURL}"></td>
	</tr>
	<tr>
		<td width="120">默认值</td>
		<td><input id="defaultVariableValues" name="defaultVariableValues" size="80" type="text" value="${defaultVariableValues}" /></td>
	</tr>
	<tr>
		<td width="120">默认每页行数&nbsp;<span style="color: red;">*</span></td>
		<td><input type="text" name="pageSize" id="pageSize" value="${pageSize}" /></td>
	</tr>
	<tr>
		<td width="120">默认分页列表&nbsp;<span style="color: red;">*</span></td>
		<td><input name="pageSizeList" type="text" id="pageSizeList" value="${pageSizeList}" size="80" /></td>
	</tr>    
	<!-- 
    <tr>
      <td width="120">是否显示行号列&nbsp;</td>
      <td>
        <input type="radio" name="isShowIndexColumn" id="isShowIndexColumnY" value="Y" />是&nbsp;&nbsp;
        <input type="radio" name="isShowIndexColumn" id="isShowIndexColumnN" value="N" />否</td>
    </tr>    
    <tr>
      <td width="120">是否交替行颜色</td>
      <td>
        <input type="radio" name="isStripeRows" id="isStripeRowsY" value="Y" />是&nbsp;&nbsp;
        <input type="radio" name="isStripeRows" id="isStripeRowsN" value="N" />否</td>
    </tr>
     -->
	<tr>
		<td width="120">双击回调</td>
		<td>
		<select id="hasClickCallBack" name="hasClickCallBack">
        	<option value="N">没有回调</option>
			<option value="Y">存在回调</option>
		</select>		</td>
	</tr> 
	<tr>
		<td width="120">回调函数</td>
		<td><textarea name="callbackFunction" cols="80" rows="5" id="callbackFunction">${callbackFunction}</textarea></td>
	</tr>              	
    <tr>
      <td width="120">是否缓存</td>
      <td>
        <input type="radio" name="isCache" id="isCacheY" value="Y" />是&nbsp;&nbsp;
        <input type="radio" name="isCache" id="isCacheN" value="N" />否</td>
    </tr> 
    <tr>
      <td width="120">缓存时间</td>
      <td>
        <input type="text" name="cacheMinutes" id="cacheMinutes" value="${cacheMinutes}" /> 分钟</td>
    </tr> 
    <tr>
      <td colspan="2" align="center">
        <input type="button" name="button" id="button" value="保存" onclick="submitAction('${saveConfigURL}',{formId:'<portlet:namespace/>DataGridConfig'})" /> &nbsp;
        <input type="button" name="button2" id="button2" value="取消" onclick="fireAction('${editURL}')"/></td>
    </tr>
</table>
</form>
<script language="javascript">
$('#hasClickCallBack').val('${hasClickCallBack}');

<%if ("Y".equals(isCache)){%>
	$('#<portlet:namespace/>DataGridConfig #isCacheY').attr("checked","checked");
<%}else{%>
	$('#<portlet:namespace/>DataGridConfig #isCacheN').attr("checked","checked");
<%}%>

<%if ("Y".equals(isShowIndexColumn)){%>
	$('#<portlet:namespace/>DataGridConfig #isShowIndexColumnY').attr("checked","checked");
<%}else{%>
	$('#<portlet:namespace/>DataGridConfig #isShowIndexColumnN').attr("checked","checked");
<%}%>

<%if ("Y".equals(isStripeRows)){%>
	$('#<portlet:namespace/>DataGridConfig #isStripeRowsY').attr("checked","checked");
<%}else{%>
	$('#<portlet:namespace/>DataGridConfig #isStripeRowsN').attr("checked","checked");
<%}%>
</script>