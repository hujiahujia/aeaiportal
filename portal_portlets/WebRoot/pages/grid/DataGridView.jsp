<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.*"%>
<portlet:defineObjects/>
<portlet:resourceURL id="getDataJSON" var="getDataJSONURL">
</portlet:resourceURL>
<portlet:resourceURL id="retrieveHeadHtml" var="retrieveHeadHtmlURL">
</portlet:resourceURL>
<%
String portletId = (String)request.getAttribute("portletId");
String height = (String)request.getAttribute("height");
String width = (String)request.getAttribute("width");
String isSetting = (String)request.getAttribute("isSetting");
String pageSize = (String)request.getAttribute("pageSize");
String pageSizeList = (String)request.getAttribute("pageSizeList");
String isStripeRows = (String)request.getAttribute("isStripeRows");
String isShowIndexColumn = (String)request.getAttribute("isShowIndexColumn");
String customHead = (String)request.getAttribute("customHead");
if (customHead == null){
	customHead = "";
}

String dynamicHead = (String)request.getAttribute("dynamicHead");
String fieldsConfig = (String)request.getAttribute("fieldsConfig");
String colsConfig = (String)request.getAttribute("colsConfig");

boolean hasClickCallBack = (Boolean)request.getAttribute("hasClickCallBack");
String callbackFunction = (String)request.getAttribute("callbackFunction");

if ("Y".equals(isSetting)){
%>
<script type="text/javascript" >
var dsConfig<portlet:namespace/>= {
	data : [],
	fields :<%=fieldsConfig%>
};

var colsConfig<portlet:namespace/> = <%=colsConfig%>;
var gridConfig<portlet:namespace/>={
	id : "<portlet:namespace/>grid_container",
	dataset : dsConfig<portlet:namespace/> ,
	columns : colsConfig<portlet:namespace/> ,
	container : '<portlet:namespace/>grid_container', 
	//width:"<%=width%>",
	toolbarPosition : 'bottom',
	toolbarContent : 'nav | goto | pagesize | reload | print | state' ,
	beforeSave : function(reqParam){},
	pageSize : <%=pageSize%> ,
	pageSizeList : [<%=pageSizeList%>],
	//isStripeRows : <%="Y".equals(isStripeRows)%>,
	//isShowIndexColumn : <%="Y".equals(isShowIndexColumn)%>,
	defaultRecord : {},
	<%if (hasClickCallBack){%>
	onDblClickCell:${callbackFunction},
	<%}%>	
	customHead : '<%=customHead%>'
};

var grid<portlet:namespace/>=new GT.Grid(gridConfig<portlet:namespace/>);

GT.Utils.onLoad( function(){
	grid<portlet:namespace/>.render();
});

$(function(){
	var __renderPortlet<portlet:namespace/> = function(){
		sendAction('${getDataJSONURL}',{dataType:'text',onComplete:function(responseText){
			var data = jQuery.parseJSON(responseText);
			if ("<%=dynamicHead%>"== "Y"){
				sendAction('${retrieveHeadHtmlURL}',{dataType:'text',onComplete:function(responseText){
					if (responseText == gridConfig<portlet:namespace/>.customHead){
						grid<portlet:namespace/>.refresh(data);		
					}else{
						resetWindow();
					}
				}});
			}else{
				grid<portlet:namespace/>.refresh(data);				
			}
		}});
	};
	
	
	__renderPortlets.put("<%=portletId%>",__renderPortlet<portlet:namespace/>);
	__renderPortlet<portlet:namespace/>();	
}); 

</script>
<div id="<portlet:namespace/>grid_container" style="border:0px solid #cccccc;background-color:#f3f3f3;padding:3px;height:${height}px;width:<%=width%>px" ></div>
<%
}else{
	out.println("请正确设置相关属性！");
}
%>