<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*"%>
<%@page import="com.agileai.util.StringUtil"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<%
String targetPage = pageBean.getStringValue("targetPage");
String gridCode = pageBean.getStringValue("gridCode");
String height = pageBean.getStringValue("height");
String width = pageBean.getStringValue("width");
String pageSize = pageBean.getStringValue("pageSize");
String pageSizeList = pageBean.getStringValue("pageSizeList");
String isStripeRows = pageBean.getStringValue("isStripeRows");
String isShowIndexColumn = pageBean.getStringValue("isShowIndexColumn");
String customHead = pageBean.getStringValue("customHead");
String fieldsConfig = pageBean.getStringValue("fieldsConfig");
String colsConfig = pageBean.getStringValue("colsConfig");

boolean hasClickCallBack = (Boolean)pageBean.getAttribute("hasClickCallBack");
String callbackFunction = pageBean.getStringValue("callbackFunction");

String onloadCallback = pageBean.getStringValue("onloadCallback");
boolean isExistOnloadCallback = !StringUtil.isNullOrEmpty(onloadCallback);
%>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="-1" />
<meta http-equiv="Cache-Control" content="no-cache" />
<%@include file="/jsp/inc/resource.inc.jsp"%>
<link rel="stylesheet" href="/portal/css/gt_grid.css" type="text/css" />	
<link rel="stylesheet" href="/portal/css/skin/china/skinstyle.css"  type="text/css" />
<link rel="stylesheet" href="/portal/css/skin/vista/skinstyle.css" type="text/css" />
<link rel="stylesheet" href="/portal/css/skin/mac/skinstyle.css" type="text/css" />
<script type="text/javascript" src="/portal/js/gt_msg_cn.js"></script>
<script type="text/javascript" src="/portal/js/gt_grid_all.js"></script>
	
<script type="text/javascript" >
var dsConfigBoxy= {
	data : [],
	fields :<%=fieldsConfig%>
};

var colsConfigBoxy = <%=colsConfig%>;
var gridConfigBoxy={
	id : "Boxygrid_container",
	dataset : dsConfigBoxy ,
	columns : colsConfigBoxy ,
	container :'Boxygrid_container', 
	width:<%=width%>,
	toolbarPosition : 'bottom',
	toolbarContent : 'nav | goto | pagesize | reload | print | state' ,
	beforeSave : function(reqParam){},
	pageSize : <%=pageSize%> ,
	pageSizeList : [<%=pageSizeList%>],
	//isStripeRows : <%="Y".equals(isStripeRows)%>,
	//isShowIndexColumn : <%="Y".equals(isShowIndexColumn)%>,
	defaultRecord : {},
	<%if (hasClickCallBack){%>
	onDblClickCell:<%=callbackFunction%>,
	<%}%>		
	customHead : '<%=customHead%>'
};

var gridBoxy=new GT.Grid(gridConfigBoxy);

GT.Utils.onLoad( function(){
	gridBoxy.render();
});

function loadGridData(){
	var url = '<%=pageBean.getHandlerURL()%>&actionType=retrieveJson&targetPage=<%=targetPage%>&gridCode=<%=gridCode%>';  
	sendRequest(url,{dataType:'text',onComplete:function(responseText){
		var data = jQuery.parseJSON(responseText);
		gridBoxy.refresh(data);
	}});
}

$(function(){
	loadGridData();
});

<%if (isExistOnloadCallback){%>
<%=onloadCallback%>
<%}%>
</script>
</head>
<body style="margin:1px;padding:0px;">
<div id="Boxygrid_container" style="background-color:#f3f3f3;padding:0px;height:<%=height%>px;width:100%;" ></div>
</body>
</html>