<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://www.ecside.org" prefix="ec"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>个人日程管理</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">
&nbsp;<input type="hidden" id="userCode" name="userCode" value="<%=pageBean.inputValue("userCode")%>" />
&nbsp;起止日期<input id="sDate" label="起止日期" name="sDate" type="text" value="<%=pageBean.inputDate("sDate")%>" size="10" class="text" /><img id="sDatePicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" />
&nbsp;-<input id="eDate" label="-" name="eDate" type="text" value="<%=pageBean.inputDate("eDate")%>" size="10" class="text" /><img id="eDatePicker" src="images/calendar.gif" width="16" height="16" alt="日期/时间选择框" />
&nbsp;<input type="button" name="button" id="button" value="查询" class="formbutton" onclick="doQuery()" />
</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="A" align="center" onclick="doRequest('insertRequest')"><input value="&nbsp;" title="新增" type="button" class="createImgBtn" />新增</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="E" align="center" onclick="doRequest('updateRequest')"><input value="&nbsp;" title="编辑" type="button" class="editImgBtn" />编辑</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="C" align="center" onclick="doRequest('copyRequest')"><input value="&nbsp;" title="复制" type="button" class="copyImgBtn" />复制</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="V" align="center" onclick="doRequest('viewDetail')"><input value="&nbsp;" title="查看" type="button" class="detailImgBtn" />查看</td>   
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" hotKey="D" align="center" onclick="doDelete($('#'+rsIdTagId).val());"><input value="&nbsp;" title="删除" type="button" class="delImgBtn" />删除</td>
</tr>
</table>
</div>
<ec:table 
form="form1"
var="row"
items="pageBean.rsList" csvFileName="个人日程管理.csv"
retrieveRowsCallback="process" xlsFileName="个人日程管理.xls"
useAjax="true" sortable="true"
doPreload="false" toolbarContent="navigation|pagejump |pagesize |export|extend|status"
width="100%" rowsDisplayed="15"
listWidth="100%" 
height="390px"
>
<ec:row styleClass="odd" ondblclick="doRequest('viewDetail')" oncontextmenu="selectRow(this,{SCHEDULE_ID:'${row.SCHEDULE_ID}'});refreshConextmenu()" onclick="selectRow(this,{SCHEDULE_ID:'${row.SCHEDULE_ID}'})">
	<ec:column width="50" style="text-align:center" property="_0" title="序号" value="${GLOBALROWCOUNT}" />
	<ec:column width="200" property="SCHEDULE_TITLE" title="标题"   />
	<ec:column width="100" property="SCHEDULE_TIME" title="时间" cell="date" format="yyyy-MM-dd HH:mm" />
	<ec:column width="150" property="SCHEDULE_PLACE" title="地点"   />
</ec:row>
</ec:table>
<input type="hidden" name="SCHEDULE_ID" id="SCHEDULE_ID" value="" />
<input type="hidden" name="actionType" id="actionType" />
<script language="JavaScript">
setRsIdTag('SCHEDULE_ID');
var ectableMenu = new EctableMenu('contextMenu','ec_table');
initCalendar('sDate','%Y-%m-%d','sDatePicker');
initCalendar('eDate','%Y-%m-%d','eDatePicker');
</script>
</form>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>